CREATE DATABASE  IF NOT EXISTS `aegon_storage` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `aegon_storage`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: aegon_storage
-- ------------------------------------------------------
-- Server version	5.6.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aegon_entry_log`
--

DROP TABLE IF EXISTS `aegon_entry_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aegon_entry_log` (
  `id_entry_log` int(11) NOT NULL AUTO_INCREMENT,
  `current_balance` decimal(10,2) DEFAULT NULL,
  `data_from_day_date` varchar(100) NOT NULL,
  `last_login_date` varchar(100) NOT NULL,
  `last_modified_date` varchar(100) NOT NULL,
  `reading_date` varchar(100) NOT NULL,
  PRIMARY KEY (`id_entry_log`),
  UNIQUE KEY `reading_date` (`reading_date`),
  KEY `data_from_day_date` (`data_from_day_date`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aegon_entry_log`
--

LOCK TABLES `aegon_entry_log` WRITE;
/*!40000 ALTER TABLE `aegon_entry_log` DISABLE KEYS */;
INSERT INTO `aegon_entry_log` VALUES (10,23813.24,'2015-11-06','2015-11-11 15:11:21','2015-11-11','2015-11-11 20:00:54'),(11,23795.49,'2015-11-09','2015-11-11 19:59:20','2015-11-12','2015-11-12 20:00:58'),(12,23710.89,'2015-11-10','2015-11-12 19:59:18','2015-11-13','2015-11-13 20:01:26'),(14,23710.89,'2015-11-10','2015-11-15 17:27:56','2015-11-13','2015-11-15 20:01:15'),(18,23688.61,'2015-11-12','2015-11-16 00:41:54','2015-11-16','2015-11-16 22:11:19'),(19,23501.65,'2015-11-13','2015-11-16 22:41:54','2015-11-17','2015-11-17 20:00:53'),(20,23459.36,'2015-11-16','2015-11-17 19:59:34','2015-11-18','2015-11-18 21:22:03'),(21,23581.72,'2015-11-17','2015-11-18 21:20:34','2015-11-19','2015-11-19 20:18:30'),(22,23583.98,'2015-11-18','2015-11-20 01:28:39','2015-11-20','2015-11-20 20:48:04'),(23,23583.98,'2015-11-18','2015-11-21 02:46:31','2015-11-20','2015-11-21 20:00:47'),(24,23583.98,'2015-11-18','2015-11-21 19:59:17','2015-11-20','2015-11-22 20:04:48'),(26,23666.32,'2015-11-19','2015-11-23 01:43:18','2015-11-23','2015-11-23 20:00:46'),(27,23748.66,'2015-11-20','2015-11-23 19:59:07','2015-11-24','2015-11-24 20:01:17'),(28,23644.05,'2015-11-23','2015-11-24 19:59:41','2015-11-25','2015-11-25 20:08:09'),(29,23561.71,'2015-11-24','2015-11-25 20:06:36','2015-11-26','2015-11-26 20:01:51'),(30,23664.05,'2015-11-25','2015-11-26 20:00:15','2015-11-27','2015-11-27 20:00:48'),(31,23664.05,'2015-11-25','2015-11-27 19:59:12','2015-11-27','2015-11-28 20:04:59'),(32,23664.05,'2015-11-25','2015-11-29 03:03:00','2015-11-27','2015-11-29 20:00:29'),(33,24095.40,'2015-11-26','2015-11-29 19:58:55','2015-11-30','2015-11-30 20:07:21'),(34,24003.25,'2015-11-27','2015-11-30 20:05:44','2015-12-01','2015-12-01 20:47:55'),(35,24026.04,'2015-11-30','2015-12-01 20:46:20','2015-12-02','2015-12-02 20:01:05'),(36,24026.04,'2015-12-01','2015-12-02 19:59:03','2015-12-03','2015-12-03 20:01:52'),(37,24005.80,'2015-12-02','2015-12-03 19:59:59','2015-12-04','2015-12-04 20:01:13'),(38,24005.80,'2015-12-02','2015-12-04 19:59:23','2015-12-04','2015-12-05 20:00:42'),(39,24005.80,'2015-12-02','2015-12-05 19:58:53','2015-12-04','2015-12-06 20:01:24'),(40,23823.58,'2015-12-03','2015-12-07 01:07:06','2015-12-07','2015-12-07 20:00:55'),(41,23737.50,'2015-12-04','2015-12-08 02:03:33','2015-12-08','2015-12-08 20:00:53'),(42,23798.24,'2015-12-07','2015-12-08 19:59:07','2015-12-09','2015-12-09 20:00:46'),(43,23633.73,'2015-12-08','2015-12-09 19:58:59','2015-12-10','2015-12-10 20:03:16'),(44,23428.71,'2015-12-09','2015-12-11 00:51:29','2015-12-11','2015-12-11 20:00:33'),(45,23428.71,'2015-12-09','2015-12-11 19:58:49','2015-12-11','2015-12-12 20:00:46'),(47,23428.71,'2015-12-09','2015-12-13 19:30:14','2015-12-11','2015-12-13 20:00:35'),(48,23426.17,'2015-12-10','2015-12-13 19:58:49','2015-12-14','2015-12-14 20:00:56'),(50,23175.57,'2015-12-11','2015-12-15 17:59:00','2015-12-15','2015-12-15 21:37:07'),(51,22993.35,'2015-12-14','2015-12-15 21:35:21','2015-12-16','2015-12-16 22:16:03'),(52,23178.12,'2015-12-15','2015-12-16 22:13:54','2015-12-17','2015-12-17 20:06:09'),(53,23264.20,'2015-12-16','2015-12-17 20:04:17','2015-12-18','2015-12-18 20:00:40'),(54,23264.20,'2015-12-16','2015-12-18 19:58:50','2015-12-18','2015-12-19 20:01:22'),(55,23264.20,'2015-12-16','2015-12-20 12:07:27','2015-12-18','2015-12-20 20:02:27'),(56,23446.41,'2015-12-17','2015-12-20 20:00:21','2015-12-21','2015-12-21 20:29:53'),(57,23297.06,'2015-12-18','2015-12-22 02:29:59','2015-12-22','2015-12-22 20:40:25'),(58,23340.09,'2015-12-21','2015-12-23 18:55:37','2015-12-23','2015-12-23 20:20:29'),(63,23340.09,'2015-12-21','2015-12-24 10:28:57','2015-12-24','2015-12-24 20:00:28'),(64,23340.09,'2015-12-21','2015-12-24 19:58:29','2015-12-25','2015-12-25 20:00:48'),(65,23340.09,'2015-12-21','2015-12-25 19:58:44','2015-12-25','2015-12-26 20:02:07'),(66,23340.09,'2015-12-21','2015-12-26 19:59:59','2015-12-27','2015-12-27 20:01:20'),(67,23279.36,'2015-12-22','2015-12-27 20:34:43','2015-12-28','2015-12-28 20:01:47'),(68,23423.62,'2015-12-23','2015-12-28 19:59:45','2015-12-29','2015-12-29 20:07:06'),(69,23388.66,'2015-12-28','2015-12-29 20:05:01','2015-12-30','2015-12-30 20:03:59'),(70,23514.92,'2015-12-29','2015-12-30 20:01:39','2015-12-31','2015-12-31 20:00:48'),(72,23514.92,'2015-12-29','2015-12-31 20:57:33','2016-01-01','2016-01-01 20:01:41'),(73,23514.92,'2015-12-29','2016-01-01 19:59:18','2016-01-01','2016-01-02 20:03:17'),(74,23514.92,'2015-12-29','2016-01-02 20:00:37','2016-01-01','2016-01-03 20:03:00'),(75,23514.92,'2015-12-29','2016-01-04 01:30:55','2016-01-04','2016-01-04 20:06:25'),(76,23818.65,'2015-12-30','2016-01-04 20:04:14','2016-01-05','2016-01-05 20:00:53'),(77,23818.65,'2015-12-30','2016-01-05 19:58:41','2016-01-06','2016-01-06 20:00:50'),(78,23502.74,'2016-01-04','2016-01-07 17:43:50','2016-01-07','2016-01-07 20:00:33');
/*!40000 ALTER TABLE `aegon_entry_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aegon_policy`
--

DROP TABLE IF EXISTS `aegon_policy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aegon_policy` (
  `id_policy` int(11) NOT NULL AUTO_INCREMENT,
  `current_balance` decimal(10,2) DEFAULT NULL,
  `policy_name` varchar(100) NOT NULL,
  `policy_number` varchar(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id_policy`),
  UNIQUE KEY `policy_number` (`policy_number`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aegon_policy`
--

LOCK TABLES `aegon_policy` WRITE;
/*!40000 ALTER TABLE `aegon_policy` DISABLE KEYS */;
INSERT INTO `aegon_policy` VALUES (1,23502.74,'Moja emerytura','EFE2000203','AEGON Indywidualny Plan Finansowy Alfa');
/*!40000 ALTER TABLE `aegon_policy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aegon_policy_history`
--

DROP TABLE IF EXISTS `aegon_policy_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aegon_policy_history` (
  `balance` decimal(10,2) DEFAULT NULL,
  `id_entry_log` int(11) NOT NULL,
  `id_policy` int(11) NOT NULL,
  PRIMARY KEY (`id_entry_log`,`id_policy`),
  KEY `FK_aegon_policy_history_id_policy` (`id_policy`),
  CONSTRAINT `FK_aegon_policy_history_id_entry_log` FOREIGN KEY (`id_entry_log`) REFERENCES `aegon_entry_log` (`id_entry_log`),
  CONSTRAINT `FK_aegon_policy_history_id_policy` FOREIGN KEY (`id_policy`) REFERENCES `aegon_policy` (`id_policy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aegon_policy_history`
--

LOCK TABLES `aegon_policy_history` WRITE;
/*!40000 ALTER TABLE `aegon_policy_history` DISABLE KEYS */;
INSERT INTO `aegon_policy_history` VALUES (23813.24,10,1),(23795.49,11,1),(23710.89,12,1),(23710.89,14,1),(23688.61,18,1),(23501.65,19,1),(23459.36,20,1),(23581.72,21,1),(23583.98,22,1),(23583.98,23,1),(23583.98,24,1),(23666.32,26,1),(23748.66,27,1),(23644.05,28,1),(23561.71,29,1),(23664.05,30,1),(23664.05,31,1),(23664.05,32,1),(24095.40,33,1),(24003.25,34,1),(24026.04,35,1),(24026.04,36,1),(24005.80,37,1),(24005.80,38,1),(24005.80,39,1),(23823.58,40,1),(23737.50,41,1),(23798.24,42,1),(23633.73,43,1),(23428.71,44,1),(23428.71,45,1),(23428.71,47,1),(23426.17,48,1),(23175.57,50,1),(22993.35,51,1),(23178.12,52,1),(23264.20,53,1),(23264.20,54,1),(23264.20,55,1),(23446.41,56,1),(23297.06,57,1),(23340.09,58,1),(23340.09,63,1),(23340.09,64,1),(23340.09,65,1),(23340.09,66,1),(23279.36,67,1),(23423.62,68,1),(23388.66,69,1),(23514.92,70,1),(23514.92,72,1),(23514.92,73,1),(23514.92,74,1),(23514.92,75,1),(23818.65,76,1),(23818.65,77,1),(23502.74,78,1);
/*!40000 ALTER TABLE `aegon_policy_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aegon_sub_account`
--

DROP TABLE IF EXISTS `aegon_sub_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aegon_sub_account` (
  `id_sub_account` int(11) NOT NULL AUTO_INCREMENT,
  `current_balance` decimal(10,2) DEFAULT NULL,
  `sub_account_name` varchar(100) NOT NULL,
  `id_policy` int(11) NOT NULL,
  PRIMARY KEY (`id_sub_account`),
  KEY `FK_aegon_sub_account_id_policy` (`id_policy`),
  CONSTRAINT `FK_aegon_sub_account_id_policy` FOREIGN KEY (`id_policy`) REFERENCES `aegon_policy` (`id_policy`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aegon_sub_account`
--

LOCK TABLES `aegon_sub_account` WRITE;
/*!40000 ALTER TABLE `aegon_sub_account` DISABLE KEYS */;
INSERT INTO `aegon_sub_account` VALUES (1,14037.00,'Subkonto Składek Regularnych',1),(2,9465.74,'Subkonto Składek Dodatkowych',1);
/*!40000 ALTER TABLE `aegon_sub_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aegon_sub_account_balance_history`
--

DROP TABLE IF EXISTS `aegon_sub_account_balance_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aegon_sub_account_balance_history` (
  `id_sub_account_balance_history` int(11) NOT NULL AUTO_INCREMENT,
  `ufk_invest_unit_collect_value` decimal(10,2) NOT NULL,
  `ufk_invest_unit_count` double NOT NULL,
  `ufk_invest_unit_value` double NOT NULL,
  `ufk_name` varchar(100) NOT NULL,
  `id_entry_log` int(11) DEFAULT NULL,
  `id_sub_account` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_sub_account_balance_history`),
  KEY `FK_aegon_sub_account_balance_history_id_entry_log` (`id_entry_log`),
  KEY `aegon_sub_account_balance_history_id_sub_account` (`id_sub_account`),
  CONSTRAINT `FK_aegon_sub_account_balance_history_id_entry_log` FOREIGN KEY (`id_entry_log`) REFERENCES `aegon_entry_log` (`id_entry_log`),
  CONSTRAINT `aegon_sub_account_balance_history_id_sub_account` FOREIGN KEY (`id_sub_account`) REFERENCES `aegon_sub_account` (`id_sub_account`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aegon_sub_account_balance_history`
--

LOCK TABLES `aegon_sub_account_balance_history` WRITE;
/*!40000 ALTER TABLE `aegon_sub_account_balance_history` DISABLE KEYS */;
INSERT INTO `aegon_sub_account_balance_history` VALUES (28,11373.86,951.787415,11.95,'UFK - Alfa Kapitałowy 1 (PLN)',10,1),(29,2603.55,226.7902,11.48,'UFK - Alfa Regularny 1 (PLN)',10,1),(30,9835.83,823.082326,11.95,'UFK - Alfa Kapitałowy 1 (PLN)',10,2),(31,11364.34,951.787415,11.94,'UFK - Alfa Kapitałowy 1 (PLN)',11,1),(32,2603.55,226.7902,11.48,'UFK - Alfa Regularny 1 (PLN)',11,1),(33,9827.60,823.082326,11.94,'UFK - Alfa Kapitałowy 1 (PLN)',11,2),(34,11326.27,951.787415,11.9,'UFK - Alfa Kapitałowy 1 (PLN)',12,1),(35,2589.94,226.7902,11.42,'UFK - Alfa Regularny 1 (PLN)',12,1),(36,9794.68,823.082326,11.9,'UFK - Alfa Kapitałowy 1 (PLN)',12,2),(40,11326.27,951.787415,11.9,'UFK - Alfa Kapitałowy 1 (PLN)',14,1),(41,2589.94,226.7902,11.42,'UFK - Alfa Regularny 1 (PLN)',14,1),(42,9794.68,823.082326,11.9,'UFK - Alfa Kapitałowy 1 (PLN)',14,2),(52,11316.75,951.787415,11.89,'UFK - Alfa Kapitałowy 1 (PLN)',18,1),(53,2585.41,226.7902,11.4,'UFK - Alfa Regularny 1 (PLN)',18,1),(54,9786.45,823.082326,11.89,'UFK - Alfa Kapitałowy 1 (PLN)',18,2),(55,11231.09,951.787415,11.8,'UFK - Alfa Kapitałowy 1 (PLN)',19,1),(56,2558.19,226.7902,11.28,'UFK - Alfa Regularny 1 (PLN)',19,1),(57,9712.37,823.082326,11.8,'UFK - Alfa Kapitałowy 1 (PLN)',19,2),(58,11212.06,951.787415,11.78,'UFK - Alfa Kapitałowy 1 (PLN)',20,1),(59,2551.39,226.7902,11.25,'UFK - Alfa Regularny 1 (PLN)',20,1),(60,9695.91,823.082326,11.78,'UFK - Alfa Kapitałowy 1 (PLN)',20,2),(61,11269.16,951.787415,11.84,'UFK - Alfa Kapitałowy 1 (PLN)',21,1),(62,2567.27,226.7902,11.32,'UFK - Alfa Regularny 1 (PLN)',21,1),(63,9745.29,823.082326,11.84,'UFK - Alfa Kapitałowy 1 (PLN)',21,2),(64,11269.16,951.787415,11.84,'UFK - Alfa Kapitałowy 1 (PLN)',22,1),(65,2569.53,226.7902,11.33,'UFK - Alfa Regularny 1 (PLN)',22,1),(66,9745.29,823.082326,11.84,'UFK - Alfa Kapitałowy 1 (PLN)',22,2),(67,11269.16,951.787415,11.84,'UFK - Alfa Kapitałowy 1 (PLN)',23,1),(68,2569.53,226.7902,11.33,'UFK - Alfa Regularny 1 (PLN)',23,1),(69,9745.29,823.082326,11.84,'UFK - Alfa Kapitałowy 1 (PLN)',23,2),(70,11269.16,951.787415,11.84,'UFK - Alfa Kapitałowy 1 (PLN)',24,1),(71,2569.53,226.7902,11.33,'UFK - Alfa Regularny 1 (PLN)',24,1),(72,9745.29,823.082326,11.84,'UFK - Alfa Kapitałowy 1 (PLN)',24,2),(73,11307.23,951.787415,11.88,'UFK - Alfa Kapitałowy 1 (PLN)',26,1),(74,2580.87,226.7902,11.38,'UFK - Alfa Regularny 1 (PLN)',26,1),(75,9778.22,823.082326,11.88,'UFK - Alfa Kapitałowy 1 (PLN)',26,2),(76,11345.31,951.787415,11.92,'UFK - Alfa Kapitałowy 1 (PLN)',27,1),(77,2592.21,226.7902,11.43,'UFK - Alfa Regularny 1 (PLN)',27,1),(78,9811.14,823.082326,11.92,'UFK - Alfa Kapitałowy 1 (PLN)',27,2),(79,11297.72,951.787415,11.87,'UFK - Alfa Kapitałowy 1 (PLN)',28,1),(80,2576.34,226.7902,11.36,'UFK - Alfa Regularny 1 (PLN)',28,1),(81,9769.99,823.082326,11.87,'UFK - Alfa Kapitałowy 1 (PLN)',28,2),(82,11259.65,951.787415,11.83,'UFK - Alfa Kapitałowy 1 (PLN)',29,1),(83,2565.00,226.7902,11.31,'UFK - Alfa Regularny 1 (PLN)',29,1),(84,9737.06,823.082326,11.83,'UFK - Alfa Kapitałowy 1 (PLN)',29,2),(85,11307.23,951.787415,11.88,'UFK - Alfa Kapitałowy 1 (PLN)',30,1),(86,2578.60,226.7902,11.37,'UFK - Alfa Regularny 1 (PLN)',30,1),(87,9778.22,823.082326,11.88,'UFK - Alfa Kapitałowy 1 (PLN)',30,2),(88,11307.23,951.787415,11.88,'UFK - Alfa Kapitałowy 1 (PLN)',31,1),(89,2578.60,226.7902,11.37,'UFK - Alfa Regularny 1 (PLN)',31,1),(90,9778.22,823.082326,11.88,'UFK - Alfa Kapitałowy 1 (PLN)',31,2),(91,11307.23,951.787415,11.88,'UFK - Alfa Kapitałowy 1 (PLN)',32,1),(92,2578.60,226.7902,11.37,'UFK - Alfa Regularny 1 (PLN)',32,1),(93,9778.22,823.082326,11.88,'UFK - Alfa Kapitałowy 1 (PLN)',32,2),(94,11354.82,951.787415,11.93,'UFK - Alfa Kapitałowy 1 (PLN)',33,1),(95,2921.21,255.574102,11.43,'UFK - Alfa Regularny 1 (PLN)',33,1),(96,9819.37,823.082326,11.93,'UFK - Alfa Kapitałowy 1 (PLN)',33,2),(97,11309.22,948.759731,11.92,'UFK - Alfa Kapitałowy 1 (PLN)',34,1),(98,2906.82,254.76078,11.41,'UFK - Alfa Regularny 1 (PLN)',34,1),(99,9787.21,821.074776,11.92,'UFK - Alfa Kapitałowy 1 (PLN)',34,2),(100,11318.70,948.759731,11.93,'UFK - Alfa Kapitałowy 1 (PLN)',35,1),(101,2911.92,254.76078,11.43,'UFK - Alfa Regularny 1 (PLN)',35,1),(102,9795.42,821.074776,11.93,'UFK - Alfa Kapitałowy 1 (PLN)',35,2),(103,11318.70,948.759731,11.93,'UFK - Alfa Kapitałowy 1 (PLN)',36,1),(104,2911.92,254.76078,11.43,'UFK - Alfa Regularny 1 (PLN)',36,1),(105,9795.42,821.074776,11.93,'UFK - Alfa Kapitałowy 1 (PLN)',36,2),(106,11309.22,948.759731,11.92,'UFK - Alfa Kapitałowy 1 (PLN)',37,1),(107,2909.37,254.76078,11.42,'UFK - Alfa Regularny 1 (PLN)',37,1),(108,9787.21,821.074776,11.92,'UFK - Alfa Kapitałowy 1 (PLN)',37,2),(109,11309.22,948.759731,11.92,'UFK - Alfa Kapitałowy 1 (PLN)',38,1),(110,2909.37,254.76078,11.42,'UFK - Alfa Regularny 1 (PLN)',38,1),(111,9787.21,821.074776,11.92,'UFK - Alfa Kapitałowy 1 (PLN)',38,2),(112,11309.22,948.759731,11.92,'UFK - Alfa Kapitałowy 1 (PLN)',39,1),(113,2909.37,254.76078,11.42,'UFK - Alfa Regularny 1 (PLN)',39,1),(114,9787.21,821.074776,11.92,'UFK - Alfa Kapitałowy 1 (PLN)',39,2),(115,11223.83,948.759731,11.83,'UFK - Alfa Kapitałowy 1 (PLN)',40,1),(116,2886.44,254.76078,11.33,'UFK - Alfa Regularny 1 (PLN)',40,1),(117,9713.31,821.074776,11.83,'UFK - Alfa Kapitałowy 1 (PLN)',40,2),(118,11185.88,948.759731,11.79,'UFK - Alfa Kapitałowy 1 (PLN)',41,1),(119,2871.15,254.76078,11.27,'UFK - Alfa Regularny 1 (PLN)',41,1),(120,9680.47,821.074776,11.79,'UFK - Alfa Kapitałowy 1 (PLN)',41,2),(121,11214.34,948.759731,11.82,'UFK - Alfa Kapitałowy 1 (PLN)',42,1),(122,2878.80,254.76078,11.3,'UFK - Alfa Regularny 1 (PLN)',42,1),(123,9705.10,821.074776,11.82,'UFK - Alfa Kapitałowy 1 (PLN)',42,2),(124,11138.44,948.759731,11.74,'UFK - Alfa Kapitałowy 1 (PLN)',43,1),(125,2855.87,254.76078,11.21,'UFK - Alfa Regularny 1 (PLN)',43,1),(126,9639.42,821.074776,11.74,'UFK - Alfa Kapitałowy 1 (PLN)',43,2),(127,11043.56,948.759731,11.64,'UFK - Alfa Kapitałowy 1 (PLN)',44,1),(128,2827.84,254.76078,11.1,'UFK - Alfa Regularny 1 (PLN)',44,1),(129,9557.31,821.074776,11.64,'UFK - Alfa Kapitałowy 1 (PLN)',44,2),(130,11043.56,948.759731,11.64,'UFK - Alfa Kapitałowy 1 (PLN)',45,1),(131,2827.84,254.76078,11.1,'UFK - Alfa Regularny 1 (PLN)',45,1),(132,9557.31,821.074776,11.64,'UFK - Alfa Kapitałowy 1 (PLN)',45,2),(136,11043.56,948.759731,11.64,'UFK - Alfa Kapitałowy 1 (PLN)',47,1),(137,2827.84,254.76078,11.1,'UFK - Alfa Regularny 1 (PLN)',47,1),(138,9557.31,821.074776,11.64,'UFK - Alfa Kapitałowy 1 (PLN)',47,2),(139,11043.56,948.759731,11.64,'UFK - Alfa Kapitałowy 1 (PLN)',48,1),(140,2825.30,254.76078,11.09,'UFK - Alfa Regularny 1 (PLN)',48,1),(141,9557.31,821.074776,11.64,'UFK - Alfa Kapitałowy 1 (PLN)',48,2),(145,10929.71,948.759731,11.52,'UFK - Alfa Kapitałowy 1 (PLN)',50,1),(146,2787.08,254.76078,10.94,'UFK - Alfa Regularny 1 (PLN)',50,1),(147,9458.78,821.074776,11.52,'UFK - Alfa Kapitałowy 1 (PLN)',50,2),(148,10844.32,948.759731,11.43,'UFK - Alfa Kapitałowy 1 (PLN)',51,1),(149,2764.15,254.76078,10.85,'UFK - Alfa Regularny 1 (PLN)',51,1),(150,9384.88,821.074776,11.43,'UFK - Alfa Kapitałowy 1 (PLN)',51,2),(151,10929.71,948.759731,11.52,'UFK - Alfa Kapitałowy 1 (PLN)',52,1),(152,2789.63,254.76078,10.95,'UFK - Alfa Regularny 1 (PLN)',52,1),(153,9458.78,821.074776,11.52,'UFK - Alfa Kapitałowy 1 (PLN)',52,2),(154,10967.66,948.759731,11.56,'UFK - Alfa Kapitałowy 1 (PLN)',53,1),(155,2804.92,254.76078,11.01,'UFK - Alfa Regularny 1 (PLN)',53,1),(156,9491.62,821.074776,11.56,'UFK - Alfa Kapitałowy 1 (PLN)',53,2),(157,10967.66,948.759731,11.56,'UFK - Alfa Kapitałowy 1 (PLN)',54,1),(158,2804.92,254.76078,11.01,'UFK - Alfa Regularny 1 (PLN)',54,1),(159,9491.62,821.074776,11.56,'UFK - Alfa Kapitałowy 1 (PLN)',54,2),(160,10967.66,948.759731,11.56,'UFK - Alfa Kapitałowy 1 (PLN)',55,1),(161,2804.92,254.76078,11.01,'UFK - Alfa Regularny 1 (PLN)',55,1),(162,9491.62,821.074776,11.56,'UFK - Alfa Kapitałowy 1 (PLN)',55,2),(163,11053.05,948.759731,11.65,'UFK - Alfa Kapitałowy 1 (PLN)',56,1),(164,2827.84,254.76078,11.1,'UFK - Alfa Regularny 1 (PLN)',56,1),(165,9565.52,821.074776,11.65,'UFK - Alfa Kapitałowy 1 (PLN)',56,2),(166,10986.64,948.759731,11.58,'UFK - Alfa Kapitałowy 1 (PLN)',57,1),(167,2802.37,254.76078,11,'UFK - Alfa Regularny 1 (PLN)',57,1),(168,9508.05,821.074776,11.58,'UFK - Alfa Kapitałowy 1 (PLN)',57,2),(169,11005.61,948.759731,11.6,'UFK - Alfa Kapitałowy 1 (PLN)',58,1),(170,2810.01,254.76078,11.03,'UFK - Alfa Regularny 1 (PLN)',58,1),(171,9524.47,821.074776,11.6,'UFK - Alfa Kapitałowy 1 (PLN)',58,2),(184,11005.61,948.759731,11.6,'UFK - Alfa Kapitałowy 1 (PLN)',63,1),(185,2810.01,254.76078,11.03,'UFK - Alfa Regularny 1 (PLN)',63,1),(186,9524.47,821.074776,11.6,'UFK - Alfa Kapitałowy 1 (PLN)',63,2),(187,11005.61,948.759731,11.6,'UFK - Alfa Kapitałowy 1 (PLN)',64,1),(188,2810.01,254.76078,11.03,'UFK - Alfa Regularny 1 (PLN)',64,1),(189,9524.47,821.074776,11.6,'UFK - Alfa Kapitałowy 1 (PLN)',64,2),(190,11005.61,948.759731,11.6,'UFK - Alfa Kapitałowy 1 (PLN)',65,1),(191,2810.01,254.76078,11.03,'UFK - Alfa Regularny 1 (PLN)',65,1),(192,9524.47,821.074776,11.6,'UFK - Alfa Kapitałowy 1 (PLN)',65,2),(193,11005.61,948.759731,11.6,'UFK - Alfa Kapitałowy 1 (PLN)',66,1),(194,2810.01,254.76078,11.03,'UFK - Alfa Regularny 1 (PLN)',66,1),(195,9524.47,821.074776,11.6,'UFK - Alfa Kapitałowy 1 (PLN)',66,2),(196,10977.15,948.759731,11.57,'UFK - Alfa Kapitałowy 1 (PLN)',67,1),(197,2802.37,254.76078,11,'UFK - Alfa Regularny 1 (PLN)',67,1),(198,9499.84,821.074776,11.57,'UFK - Alfa Kapitałowy 1 (PLN)',67,2),(199,11043.56,948.759731,11.64,'UFK - Alfa Kapitałowy 1 (PLN)',68,1),(200,2822.75,254.76078,11.08,'UFK - Alfa Regularny 1 (PLN)',68,1),(201,9557.31,821.074776,11.64,'UFK - Alfa Kapitałowy 1 (PLN)',68,2),(202,11023.28,946.204366,11.65,'UFK - Alfa Kapitałowy 1 (PLN)',69,1),(203,2817.69,254.074576,11.09,'UFK - Alfa Regularny 1 (PLN)',69,1),(204,9547.69,819.544304,11.65,'UFK - Alfa Kapitałowy 1 (PLN)',69,2),(205,11080.05,946.204366,11.71,'UFK - Alfa Kapitałowy 1 (PLN)',70,1),(206,2838.01,254.074576,11.17,'UFK - Alfa Regularny 1 (PLN)',70,1),(207,9596.86,819.544304,11.71,'UFK - Alfa Kapitałowy 1 (PLN)',70,2),(211,11080.05,946.204366,11.71,'UFK - Alfa Kapitałowy 1 (PLN)',72,1),(212,2838.01,254.074576,11.17,'UFK - Alfa Regularny 1 (PLN)',72,1),(213,9596.86,819.544304,11.71,'UFK - Alfa Kapitałowy 1 (PLN)',72,2),(214,11080.05,946.204366,11.71,'UFK - Alfa Kapitałowy 1 (PLN)',73,1),(215,2838.01,254.074576,11.17,'UFK - Alfa Regularny 1 (PLN)',73,1),(216,9596.86,819.544304,11.71,'UFK - Alfa Kapitałowy 1 (PLN)',73,2),(217,11080.05,946.204366,11.71,'UFK - Alfa Kapitałowy 1 (PLN)',74,1),(218,2838.01,254.074576,11.17,'UFK - Alfa Regularny 1 (PLN)',74,1),(219,9596.86,819.544304,11.71,'UFK - Alfa Kapitałowy 1 (PLN)',74,2),(220,11080.05,946.204366,11.71,'UFK - Alfa Kapitałowy 1 (PLN)',75,1),(221,2838.01,254.074576,11.17,'UFK - Alfa Regularny 1 (PLN)',75,1),(222,9596.86,819.544304,11.71,'UFK - Alfa Kapitałowy 1 (PLN)',75,2),(223,11070.59,946.204366,11.7,'UFK - Alfa Kapitałowy 1 (PLN)',76,1),(224,3159.39,283.60779,11.14,'UFK - Alfa Regularny 1 (PLN)',76,1),(225,9588.67,819.544304,11.7,'UFK - Alfa Kapitałowy 1 (PLN)',76,2),(226,11070.59,946.204366,11.7,'UFK - Alfa Kapitałowy 1 (PLN)',77,1),(227,3159.39,283.60779,11.14,'UFK - Alfa Regularny 1 (PLN)',77,1),(228,9588.67,819.544304,11.7,'UFK - Alfa Kapitałowy 1 (PLN)',77,2),(229,10928.66,946.204366,11.55,'UFK - Alfa Kapitałowy 1 (PLN)',78,1),(230,3108.34,283.60779,10.96,'UFK - Alfa Regularny 1 (PLN)',78,1),(231,9465.74,819.544304,11.55,'UFK - Alfa Kapitałowy 1 (PLN)',78,2);
/*!40000 ALTER TABLE `aegon_sub_account_balance_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aegon_sub_account_history`
--

DROP TABLE IF EXISTS `aegon_sub_account_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aegon_sub_account_history` (
  `balance` decimal(10,2) DEFAULT NULL,
  `id_entry_log` int(11) NOT NULL,
  `id_sub_account` int(11) NOT NULL,
  PRIMARY KEY (`id_entry_log`,`id_sub_account`),
  KEY `FK_aegon_sub_account_history_id_sub_account` (`id_sub_account`),
  CONSTRAINT `FK_aegon_sub_account_history_id_entry_log` FOREIGN KEY (`id_entry_log`) REFERENCES `aegon_entry_log` (`id_entry_log`),
  CONSTRAINT `FK_aegon_sub_account_history_id_sub_account` FOREIGN KEY (`id_sub_account`) REFERENCES `aegon_sub_account` (`id_sub_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aegon_sub_account_history`
--

LOCK TABLES `aegon_sub_account_history` WRITE;
/*!40000 ALTER TABLE `aegon_sub_account_history` DISABLE KEYS */;
INSERT INTO `aegon_sub_account_history` VALUES (13977.41,10,1),(9835.83,10,2),(13967.89,11,1),(9827.60,11,2),(13916.21,12,1),(9794.68,12,2),(13916.21,14,1),(9794.68,14,2),(13902.16,18,1),(9786.45,18,2),(13789.28,19,1),(9712.37,19,2),(13763.45,20,1),(9695.91,20,2),(13836.43,21,1),(9745.29,21,2),(13838.69,22,1),(9745.29,22,2),(13838.69,23,1),(9745.29,23,2),(13838.69,24,1),(9745.29,24,2),(13888.10,26,1),(9778.22,26,2),(13937.52,27,1),(9811.14,27,2),(13874.06,28,1),(9769.99,28,2),(13824.65,29,1),(9737.06,29,2),(13885.83,30,1),(9778.22,30,2),(13885.83,31,1),(9778.22,31,2),(13885.83,32,1),(9778.22,32,2),(14276.03,33,1),(9819.37,33,2),(14216.04,34,1),(9787.21,34,2),(14230.62,35,1),(9795.42,35,2),(14230.62,36,1),(9795.42,36,2),(14218.59,37,1),(9787.21,37,2),(14218.59,38,1),(9787.21,38,2),(14218.59,39,1),(9787.21,39,2),(14110.27,40,1),(9713.31,40,2),(14057.03,41,1),(9680.47,41,2),(14093.14,42,1),(9705.10,42,2),(13994.31,43,1),(9639.42,43,2),(13871.40,44,1),(9557.31,44,2),(13871.40,45,1),(9557.31,45,2),(13871.40,47,1),(9557.31,47,2),(13868.86,48,1),(9557.31,48,2),(13716.79,50,1),(9458.78,50,2),(13608.47,51,1),(9384.88,51,2),(13719.34,52,1),(9458.78,52,2),(13772.58,53,1),(9491.62,53,2),(13772.58,54,1),(9491.62,54,2),(13772.58,55,1),(9491.62,55,2),(13880.89,56,1),(9565.52,56,2),(13789.01,57,1),(9508.05,57,2),(13815.62,58,1),(9524.47,58,2),(13815.62,63,1),(9524.47,63,2),(13815.62,64,1),(9524.47,64,2),(13815.62,65,1),(9524.47,65,2),(13815.62,66,1),(9524.47,66,2),(13779.52,67,1),(9499.84,67,2),(13866.31,68,1),(9557.31,68,2),(13840.97,69,1),(9547.69,69,2),(13918.06,70,1),(9596.86,70,2),(13918.06,72,1),(9596.86,72,2),(13918.06,73,1),(9596.86,73,2),(13918.06,74,1),(9596.86,74,2),(13918.06,75,1),(9596.86,75,2),(14229.98,76,1),(9588.67,76,2),(14229.98,77,1),(9588.67,77,2),(14037.00,78,1),(9465.74,78,2);
/*!40000 ALTER TABLE `aegon_sub_account_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-07 20:02:32
