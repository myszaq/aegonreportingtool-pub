package pl.aegon.youraccount.main;

import java.util.List;
import javax.persistence.PersistenceException;
import org.apache.logging.log4j.*;
import pl.aegon.youraccount.config.ConfigurationProvider;
import pl.aegon.youraccount.dbservice.DatabaseServiceProvider;
import pl.aegon.youraccount.entity.*;
import pl.aegon.youraccount.utils.Utils;
import pl.aegon.youraccount.website.WebSiteHandler;

public class ApplicationRunner {
	private static org.apache.logging.log4j.Logger log = LogManager.getLogger(ApplicationRunner.class);
	private DatabaseServiceProvider dbProvider = null;
	private WebSiteHandler handler = null;
	private String userLanguage = null;
	private String trayTitle = null;
	
	private boolean collectAndSaveAegonData() throws Exception {
		dbProvider = DatabaseServiceProvider.getInstance();
		handler = new WebSiteHandler();
		
		try {
			handler.collectAllRelevantDataFromApplication();
		} catch (Exception e) {
			log.fatal("An exception occured while trying to collect all data! " + e.getMessage());
			log.fatal(Utils.getExceptionStackTrace(e));
			return false;
		}
		
		// get all collected entities from the handler
		EntryLog entryLog = handler.getEntryLogEntity();
		List<Policy> policyList = handler.getPolicyEntityList();
		List<PolicyHistory> policyHistoryList = handler.getPolicyHistoryEntityList();
		List<SubAccount> subAccountList = handler.getSubAccountEntityList();
		List<SubAccountHistory> subAccountHistoryList = handler.getSubAccountHistoryEntityList();
		List<SubAccountBalanceHistory> subAccountBalanceHistoryList = handler
				.getSubAccountBalanceHistoryEntityList();
		
		// save all entities in the database
		try {
			dbProvider.setEntryLog(entryLog);
			dbProvider.setPolicyList(policyList);
			dbProvider.setPolicyHistoryList(policyHistoryList);
			dbProvider.setSubAccountList(subAccountList);
			dbProvider.setSubAccountHistoryList(subAccountHistoryList);
			dbProvider.setSubAccountBalanceHistoryList(subAccountBalanceHistoryList);
			
			dbProvider.startTransaction();
			dbProvider.storeAllEntitiesInDatabase();
			dbProvider.commitTransaction(true);
		} catch (Exception e) {
			dbProvider.rollbackTransaction();
			log.fatal("An exception occured while trying to save data in the database! " + e.getMessage());
			log.fatal(Utils.getExceptionStackTrace(e));
			throw new PersistenceException("An exception occured while trying to save data in the database!");
		}
		
		return true;
	}
	
	public static void main(String[] args) throws InterruptedException {
		ApplicationRunner runner = new ApplicationRunner();
		runner.userLanguage = ConfigurationProvider.getPropertyValue("user_language");
		runner.trayTitle = ConfigurationProvider.getMessageByLanguage("aegon_app_name", runner.userLanguage);
		
		try {
			int retriesNumber = Integer.parseInt(ConfigurationProvider.getPropertyValue("download_retries_number"));
			long retryDelay = Integer.parseInt(ConfigurationProvider.getPropertyValue("download_retries_minutes_delay"));
			retryDelay = retryDelay * 60 * 1000l;	// get the delay in miliseconds
			
			for (int i = 1; i <= retriesNumber + 1; i++) {
				boolean isOperationSuccessful = false;
				
				try {
					isOperationSuccessful = runner.collectAndSaveAegonData();
				} catch (Exception e) {
					// if the exception came from the database (persistence layer), we do not retry
					if (e instanceof PersistenceException) {
						break;
					// otherwise it may be something less relevant, so we try to continue
					} else {
						log.fatal("An unexpected error occured while trying to collect and save Aegon data! " + e.getMessage());
						log.fatal(Utils.getExceptionStackTrace(e));
					}
				}

				// if operation finished successfully, we can stop
				if (isOperationSuccessful) {
					break;
				// otherwise we wait for another retry (but only if retrying is enabled and it's not the last attempt)
				} else if (retriesNumber > 0 && i < retriesNumber + 1) {
					// display tray messages informing user about waiting and retrying
					log.info("Retrying is enabled, waiting for " + retryDelay + " milliseconds.");
					String trayMsg = ConfigurationProvider.getMessageByLanguage("waiting_for_retry_msg", runner.userLanguage);
					Utils.showTrayMessage(runner.trayTitle, trayMsg, Utils.ERROR);
					Thread.sleep(retryDelay);
					
					log.info("Retrying data download - attempt " + i + " of " + retriesNumber + ".");
					trayMsg = ConfigurationProvider.getMessageByLanguage("retry_attempt_msg", runner.userLanguage);
					trayMsg = trayMsg.replace("x_nr", Integer.toString(i)).replace("y_nr", Integer.toString(retriesNumber));
					Utils.showTrayMessage(runner.trayTitle, trayMsg, Utils.ERROR);
				}
				
				ConfigurationProvider.reloadConfigurationFromFile();
			}
		} catch (Exception e) {
			log.fatal("An unexpected error occured in the main method! Exception: " + e.getMessage());
			log.fatal(Utils.getExceptionStackTrace(e));
		}
		
		log.info("ApplicationRunner finished successfully. Exiting now...");
		System.exit(0);
	}
	
}
