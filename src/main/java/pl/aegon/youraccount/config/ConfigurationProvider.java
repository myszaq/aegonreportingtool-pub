/**
 * This is ConfigurationProvider class providing various method for accessing and managing global configuration
 * options and language translations for the whole Aegon application (including GUI and "backend" application).
 * The class methods allows to:
 * - read properties and language files,
 * - update configuration options and save them in the file,
 * - reload configuration file during execution of the application,
 * - access properties and localized messages for supported language,
 * - handle security through encryption and decryption of configuration file,
 * - manage system Task Scheduler and add/modify scheduled task used by the application,
 * - put computer to sleep.
 * 
 * All the class methods are made static for easier access in other classes.
 *  
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.UnsupportedCharsetException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import pl.aegon.youraccount.utils.Utils;

public class ConfigurationProvider {
	private static final String TASK_SCHEDULER_NAME = "AegonReportingToolScheduler";
	// unfortunately this can't be made final variable because of more complex initialization
	private static String PROJECT_JAR_NAME = "AegonReportingTool-1.0.0.jar"; // default value
	
	private static org.apache.logging.log4j.Logger log = LogManager.getLogger(ConfigurationProvider.class);
	private static Properties prop;
	private static HashMap<String, HashMap<String, String>> languageMap;
	private static String filePath = System.getProperty("user.dir") + "\\res\\config.dat";
	private static String encDecKey;
	private static String schedulerXmlTemplate = null;
	
	// Variables only for development purposes! In production always set to true.
	private static boolean encryptionEnabled = false;
	private static boolean decryptionEnabled = false;
	
	// initialization block for the class - when the class is accessed, we ensure that config files are
	// already loaded
	static {
		try {
			Properties properties = new Properties();
			properties.load(ConfigurationProvider.class.getClassLoader().getResourceAsStream(
					"project.properties"));
			PROJECT_JAR_NAME = properties.getProperty("artifactId") + "-" + properties.getProperty("version")
					+ ".jar";
		} catch (IOException ioe) {
			log.error("An exception occured while reading project properties file! Message: "
					+ ioe.getMessage());
			try {
				log.error(Utils.getExceptionStackTrace(ioe));
				throw ioe;
			} catch (Exception e) {
				// can't use Utils.getExceptionStackTrace - unless we would create another nested try/catch...
				e.printStackTrace();
			}
		}
		
		// secret key for encryption and decryption (we make sure it is 16 bytes in length)
		encDecKey = new String(Base64.encodeBase64(ConfigurationProvider.class.getName().getBytes()))
				.substring(0, 16);
		try {
			readConfigurationFile();
			readLanguageLocalizationFile();
			readSchedulerTemplateFile();
		} catch (Exception e) {
			log.error("An exception occured while reading one or more configuration files! Message: "
					+ e.getMessage());
			try {
				log.error(Utils.getExceptionStackTrace(e));
				throw e;
			} catch (Exception e1) {
				// can't use Utils.getExceptionStackTrace - unless we would create another nested
				// try/catch...
				e1.printStackTrace();
			}
		}
	}
	
	/**
	 * Method reading configuration file and building the properties of configuration options, executed at the
	 * class static initialization time. The configuration file is encrypted for security reasons, so this
	 * method is also responsible for the proper decryption of the file.
	 * 
	 * @throws Exception when the file with configuration could not be accessed or read
	 */
	private static void readConfigurationFile() throws Exception {
		try {
			// read configuration file
			String fileContent = FileUtils.readFileToString(new File(filePath), "UTF-8");
			if (decryptionEnabled) {
				// combine all the lines into one line and decrypt it with the provided secret key
				fileContent = fileContent.replace(System.getProperty("line.separator"), "+");
				fileContent = EncryptionUtils.decryptConfiguration(encDecKey, fileContent);
			}
			
			// before reading configuration, escape backslash (needed for proper reading user directory path)
			BufferedReader br = new BufferedReader(new StringReader(fileContent.replace("\\", "\\\\")));
			prop = new Properties();
			prop.load(br);
			br.close();
			
			// user login and password are additionally encrypted, so decrypt them on-the-fly
			EncryptionUtils eu = new EncryptionUtils();
			try {
				if (decryptionEnabled) {
					prop.setProperty("user_login", eu.decryptPassword(prop.getProperty("user_login")));
					prop.setProperty("user_password", eu.decryptPassword(prop.getProperty("user_password")));
				}
			} catch (Exception e) {
				log.fatal("Could not decrypt user credentials! An exception occured: " + e.getMessage());
				// do not log exception stack trace for security reasons
				throw e;
			}
			
			log.info("File " + filePath + " was read successfully.");
			// handle exceptions
		} catch (FileNotFoundException e) {
			log.error("An exception occured while reading configuration file (" + filePath + ")! Message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		} catch (IOException ioe) {
			log.error("An exception occured while reading configuration file! (" + filePath + ")! Message: "
					+ ioe.getMessage());
			log.error(Utils.getExceptionStackTrace(ioe));
			throw ioe;
		}
	}
	
	/**
	 * Method reading language file and building the map of translations from this file, executed at the class
	 * static initialization time. The language file is Base64 encoded, with the language localization values
	 * for the whole Aegon application, which are later available through the appropriate method.
	 * 
	 * @throws Exception when the file with language localization could not be accessed or read
	 */
	private static void readLanguageLocalizationFile() throws Exception {
		String filePath = System.getProperty("user.dir") + "\\res\\lang_localization.dat";
		String fileContent = null;
		
		// first get the content of the file into String
		try {
			BufferedReader br = new BufferedReader(new FileReader(filePath));
			StringBuffer tmpBuffer = new StringBuffer();
			
			String line = br.readLine();
			while (line != null) {
				tmpBuffer.append(line);
				tmpBuffer.append(System.getProperty("line.separator"));
				line = br.readLine();
			}
			
			br.close();
			fileContent = tmpBuffer.toString();
			log.info("File " + filePath + " was read successfully.");
			// handle exceptions
		} catch (FileNotFoundException e) {
			log.error("An exception occured while reading language localization file (" + filePath
					+ ")! Message: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		} catch (IOException ioe) {
			log.error("An exception occured while reading language localization file! (" + filePath
					+ ")! Message: " + ioe.getMessage());
			log.error(Utils.getExceptionStackTrace(ioe));
		}
		
		// if the file was read correctly, decode it with base64
		if (fileContent != null && !fileContent.isEmpty()) {
			byte[] decodedFile = Base64.decodeBase64(fileContent);
			fileContent = new String(decodedFile, "UTF-8");
			
			// finally, deserialize the content of the file as the HashMap
			Gson gson = new Gson();
			languageMap = gson.fromJson(fileContent,
					new TypeToken<HashMap<String, HashMap<String, String>>>() {
					}.getType());
		} else {
			throw new IllegalArgumentException(
					"The language localization file could not be read! File is null or empty.");
		}
	}
	
	/**
	 * Method reading template file of Windows Task Scheduler in XML format used to create a new scheduled
	 * task. The file is encoded in Base64 in order to protect if from unauthrized reading (with UTF-16 Little
	 * Endian charset encoding), so the method performs the appropriate decoding first and then saves the
	 * template in the class property.
	 * 
	 * @throws Exception when the file with task scheduler template could not be accessed or read
	 */
	private static void readSchedulerTemplateFile() throws Exception {
		String filePath = System.getProperty("user.dir") + "\\res\\wt_scheduler.dat";
		String fileContent = null;
		
		try {
			// get the content of the file into String
			fileContent = FileUtils.readFileToString(new File(filePath), "UTF-8");
			log.info("File " + filePath + " was read successfully.");
			// handle exceptions
		} catch (IOException ioe) {
			log.error("An exception occured while reading scheduler template file! (" + filePath
					+ ")! Message: " + ioe.getMessage());
			log.error(Utils.getExceptionStackTrace(ioe));
		} catch (UnsupportedCharsetException uce) {
			log.error("An exception occured while reading scheduler template file! (" + filePath
					+ ")! Message: " + uce.getMessage());
			log.error(Utils.getExceptionStackTrace(uce));
		}
		
		// if the file was read correctly, decode it with base64 (double encoded with chars transposition)
		if (fileContent != null && !fileContent.isEmpty()) {
			// first reverse transposition (replace new lines with 'K' char and decode)
			fileContent = fileContent.replace("\n", "K");
			byte[] decodedFile = Base64.decodeBase64(fileContent);
			fileContent = new String(decodedFile, "UTF-8");
			
			// second reverse transposition (replace new lines with 'H' char and decode)
			fileContent = fileContent.replace("\n", "H");
			decodedFile = Base64.decodeBase64(fileContent);
			// store the file as class String property
			schedulerXmlTemplate = new String(decodedFile, "UTF-8");
		} else {
			throw new IllegalArgumentException(
					"The scheduler template file could not be read! File is null or empty.");
		}
	}
	
	/**
	 * Method used to execute external command or program in the running operating system. The execution of
	 * the command takes place with the use of Java ProcessBuilder class designed for such tasks. The method
	 * allows specifically to perform any calls to schtasks.exe program in order to manage Windows Task
	 * Scheduler utility, but can be used also to run almost any command.
	 * 
	 * @param command the list containing the program and its arguments (parameter passed to ProcessBuilder)
	 * @param noWait whether to execute the command in non-waiting mode (without waiting for the output and
	 * blocking main thread)
	 * @return the output resulting from given program/command execution (either normal or error output)
	 * @throws IllegalArgumentException when input command is invalid (null or empty)
	 * @throws Exception when command execution failed in the system (for example IOException) for some reason
	 */
	private static String executeSystemCommand(List<String> command, boolean noWait) throws Exception {
		// simple check to avoid at least NullPointerException
		if (command == null || command.isEmpty()) {
			throw new IllegalArgumentException("Wrong command list passed as input parameter for execution!");
		}
		
		String line, stdOut = "", stdErr = "";
		String execResult = null;
		
		log.debug("Running command: " + String.join(" ", command));
		try {
			// start command execution
			ProcessBuilder builder = new ProcessBuilder(command);
			
			// with noWait switch, just execute the process and do not wait for the result
			// (it will not block the main thread)
			if (noWait) {
				builder.inheritIO();
				builder.start();
			} else {
				Process p = builder.start();
				
				// get stdout from command execution
				InputStream is = p.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				while ((line = br.readLine()) != null) {
					stdOut += line + "\n";
				}
				stdOut = stdOut.trim();
				
				// get stderr from command execution
				is = p.getErrorStream();
				isr = new InputStreamReader(is);
				br = new BufferedReader(isr);
				while ((line = br.readLine()) != null) {
					stdErr += line + "\n";
				}
				stdErr = stdErr.trim();
				
				p.waitFor();
				// close resources
				is.close();
				isr.close();
				br.close();
			}
			
			if (!stdOut.isEmpty()) {
				log.debug("Command output is:\n" + stdOut);
				execResult = stdOut;
			}
			if (!stdErr.isEmpty()) {
				log.debug("Error command output is:\n" + stdErr);
				execResult = stdErr;
			}
		} catch (Exception e) {
			log.error("An exception has occured while running command <" + String.join(" ", command)
					+ ">! Message: " + e.getMessage());
			throw e;
		}
		
		return execResult;
	}
	
	/**
	 * Method sending query to Windows Task Scheduler (through schtasks.exe utility) which checks if the
	 * scheduled task for the application exists (has been already created).
	 * 
	 * @return the result of Task Scheduler query - true if application task exists, false otherwise
	 * @throws Exception when there was an error during command execution (through ProcessBuilder)
	 */
	private static boolean checkIfTaskSchedulerExists() throws Exception {
		String cmdOutput = null;
		List<String> commands = new ArrayList<String>();
		
		// this will execute command like <schtasks /query /tn "AegonReportingToolScheduler" /fo list>
		commands.add("schtasks.exe");
		commands.add("/query");
		commands.add("/tn");
		commands.add("\"" + TASK_SCHEDULER_NAME + "\"");
		commands.add("/fo");
		commands.add("list");
		
		try {
			cmdOutput = executeSystemCommand(commands, false);
		} catch (Exception e) {
			log.error("An exception has occured while trying to access Application Scheduler Task! Message: "
					+ e.getMessage());
			throw e;
		}
		
		// check if the command was executed properly (if there was no exception before of course)
		if (cmdOutput.contains(TASK_SCHEDULER_NAME) || cmdOutput.contains("Ready")) {
			return true;
		} else if (cmdOutput.contains("The system cannot find the file specified")) {
			return false;
		}
		
		return false;
	}
	
	/**
	 * Method used to create a new task in the Windows Task Scheduler (used also in recreation after deleting)
	 * which allows to execute periodically the task of downloading and storing user data from Aegon online
	 * service. The method first reads the task template and creates the xml file (with particular user
	 * related data) which is then imported into Task Scheduler through schtasks.exe utility. At the end the
	 * temporary xml file is deleted. If everything was completed successfully, no exception is thrown.
	 * 
	 * @throws Exception when creating temporary xml file from the template failed or when the creation of the
	 * new task in system Task Scheduler failed for some reason
	 */
	private static void createNewTaskScheduler() throws Exception {
		List<String> commands = new ArrayList<String>();
		String scStartTime = getPropertyValue("collecting_date_time");
		String scDaysInterval = getPropertyValue("collecting_days_frequency");
		String wakeToRun = getPropertyValue("collecting_allow_wake_computer_to_run");
		
		// build the command and its arguments (additional variables used for better visibility)
		String runCommand = "cmd";
		String commandWindowTitle = "\"" + TASK_SCHEDULER_NAME + " - " + PROJECT_JAR_NAME + "\"";
		String commandJavaPath = "\"" + System.getProperty("java.home") + "\\bin\\java.exe" + "\"";
		String commandJarPath = "\"" + System.getProperty("user.dir") + "\\" + PROJECT_JAR_NAME + "\"";
		String runCommandArgs = "/c start /min " + commandWindowTitle + " " + commandJavaPath + " -jar "
				+ commandJarPath;
		
		// build scheduled task start and creation datetimes
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String startDateTime = dateFormat.format(new Date()) + "T" + scStartTime + ":00";
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String creationDateTime = dateFormat.format(new Date()).replace(" ", "T");
		
		// create in-memory xml file from the the xml template (and modify it before saving)
		String schedulerXmlFile = new String(schedulerXmlTemplate);
		// update new xml file with the particular values (by replacing "dummy" fields in the file)
		schedulerXmlFile = schedulerXmlFile.replace("$date", creationDateTime);
		schedulerXmlFile = schedulerXmlFile.replace("$author", System.getenv("USERNAME"));
		schedulerXmlFile = schedulerXmlFile.replace("$startBoundary", startDateTime);
		schedulerXmlFile = schedulerXmlFile.replace("$daysInterval", scDaysInterval);
		schedulerXmlFile = schedulerXmlFile.replace("$userId",
				System.getenv("USERDOMAIN") + "\\" + System.getenv("USERNAME"));
		schedulerXmlFile = schedulerXmlFile.replace("$wakeToRun", wakeToRun);
		schedulerXmlFile = schedulerXmlFile.replace("$command", runCommand);
		schedulerXmlFile = schedulerXmlFile.replace("$arguments", runCommandArgs);
		schedulerXmlFile = schedulerXmlFile.replace("$workingDirectory", System.getProperty("user.dir"));
		
		log.debug("Command to execute by Task Scheduler is: " + runCommand + " " + runCommandArgs);
		
		// build the path to the temporary xml file ready to be used to create a new Task Scheduler task
		String tmpXmlFilePath = System.getProperty("user.dir"); // the "last resort" of temp directories
		// get the path to the most suitable temp directory on user's system
		try {
			String tempPath = Utils.getTempFolderPath();
			if (tempPath != null) {
				tmpXmlFilePath = tempPath;
			}
			
			tmpXmlFilePath += "\\schtasks_xml_template.xml";
			// finally save the file on disk
			schedulerXmlFile = "\uFEFF" + schedulerXmlFile;
			FileUtils.writeStringToFile(new File(tmpXmlFilePath), schedulerXmlFile, "UTF-16LE");
			log.info("File " + tmpXmlFilePath + " was saved successfully.");
		} catch (Exception e) {
			log.error("An exception occured while saving scheduler template file (" + tmpXmlFilePath + ")! "
					+ "Message: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
		
		// this will execute command like <schtasks /create /tn AegonReportingToolScheduler /xml (xmlFilePath)>
		commands.add("schtasks.exe");
		commands.add("/create");
		commands.add("/tn");
		commands.add("\"" + TASK_SCHEDULER_NAME + "\"");
		commands.add("/xml");
		commands.add(tmpXmlFilePath);
		
		String cmdOutput = null;
		try {
			cmdOutput = executeSystemCommand(commands, false);
		} catch (Exception e) {
			log.error("An exception has occured while trying to create Application Scheduler Task! Message: "
					+ e.getMessage());
			throw e;
		} finally {
			// delete the temporary xml file even if the command execution completely failed
			try {
				FileUtils.forceDelete(new File(tmpXmlFilePath));
				log.info("File " + tmpXmlFilePath + " was deleted successfully.");
			} catch (IOException e) {
				log.error("An exception has occured while deleting file (" + tmpXmlFilePath + ")! Message: "
						+ e.getMessage());
				log.error(Utils.getExceptionStackTrace(e));
			}
		}
		
		// check if the command was executed properly (if there was no exception before of course)
		if (!cmdOutput.matches("(.*)The scheduled task \".*\" has successfully been created(.*)")) {
			throw new NullPointerException(
					"The scheduled task creation failed! Unexpected result retrieved: " + cmdOutput);
		}
	}
	
	/**
	 * Method sending query to Windows Task Scheduler (through schtasks.exe utility) which deletes the
	 * scheduled task for the application.
	 * 
	 * @throws Exception when there was an error during command execution (through ProcessBuilder) or if the
	 * scheduled task deletion failed
	 */
	public static void deleteTaskScheduler() throws Exception {
		String cmdOutput = null;
		List<String> commands = new ArrayList<String>();
		
		// this will execute command like <schtasks /delete /tn "AegonReportingToolScheduler">
		commands.add("schtasks.exe");
		commands.add("/delete");
		commands.add("/tn");
		commands.add("\"" + TASK_SCHEDULER_NAME + "\"");
		commands.add("/f");
		
		try {
			cmdOutput = executeSystemCommand(commands, false);
		} catch (Exception e) {
			log.error("An exception has occured while trying to delete Application Scheduler Task! Message: "
					+ e.getMessage());
			throw e;
		}
		
		// check if the command was executed properly (if there was no exception before of course)
		if (!cmdOutput.matches("(.*)The scheduled task \".*\" was successfully deleted(.*)")) {
			throw new NullPointerException(
					"The scheduled task deletion failed! Unexpected result retrieved: " + cmdOutput);
		}
	}
	
	/**
	 * Gets the whole map of language localized messages for the Aegon application.
	 * 
	 * @return the map containing localized messages per language
	 */
	public static HashMap<String, HashMap<String, String>> getLanguageMap() {
		return languageMap;
	}
	
	/**
	 * Gets the translated message for the specified language and the message key.
	 * 
	 * @param messageKey the hashmap key for the message to be translated
	 * @param langKey the key of the language to be used (such as "pl" for polish)
	 * @return translated message
	 * @throws NoSuchElementException when the message or the language key does not exist
	 */
	public static String getMessageByLanguage(String messageKey, String langKey)
			throws NoSuchElementException {
		if (languageMap.containsKey(messageKey)) {
			if (languageMap.get(messageKey).containsKey(langKey)) {
				return languageMap.get(messageKey).get(langKey);
			} else {
				throw new NoSuchElementException("Localized message with the language key \"" + langKey
						+ "\" does not exist!");
			}
		} else {
			throw new NoSuchElementException("Localized message with the message key \"" + messageKey
					+ "\" does not exist!");
		}
	}
	
	/**
	 * Returns whole Properties object.
	 * 
	 * @return Properties object
	 */
	public static Properties getProperties() {
		return prop;
	}
	
	/**
	 * Returns the value of the property for the specified key.
	 * 
	 * @param key the key of the property
	 * @return value for the given key
	 * @throws NoSuchElementException when the given key does not exist
	 */
	public static String getPropertyValue(String key) throws NoSuchElementException {
		if (prop.containsKey(key)) {
			return prop.getProperty(key);
		} else {
			throw new NoSuchElementException("Property named \"" + key
					+ "\" was not found in the project configuration!");
		}
	}
	
	/**
	 * Sets specified key and value in the properties set.
	 * 
	 * @param key the key of the property
	 * @param value the value of the property
	 * @throws NoSuchElementException when the property with given key does not exist
	 */
	public static void setPropertyValue(String key, String value) throws NoSuchElementException {
		// escape backslash with 2 backslashes (needed in order to read user folder path correctly)
		value = value.replace("\\", "\\\\");
		
		if (prop.containsKey(key)) {
			prop.setProperty(key, value);
		} else {
			throw new NoSuchElementException("Property named \"" + key
					+ "\" was not found in the project configuration!");
		}
	}
	
	/**
	 * Method used to reload configuration (stored in the VM memory) from the config file, which is useful
	 * when the file was changed.
	 */
	public static void reloadConfigurationFromFile() {
		try {
			readConfigurationFile();
		} catch (Exception e) {
			log.error("An exception occured while reloading configuration file! Message: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
	}
	
	/**
	 * Method saving configuration file with updated properties and handling proper file decryption and
	 * encryption.
	 * 
	 * @throws Exception when the file with configuration could not be accessed or saved
	 */
	public static void saveConfigurationFile() throws Exception {
		String configContent = null;
		// read the content of the config file...
		try {
			configContent = FileUtils.readFileToString(new File(filePath), "UTF-8");
			if (decryptionEnabled) {
				// ... combine all the lines into one line and decrypt it with the provided secret key
				configContent = configContent.replace(System.getProperty("line.separator"), "+");
				configContent = EncryptionUtils.decryptConfiguration(encDecKey, configContent);
			}
		} catch (Exception e) {
			log.error("An exception occured while reading configuration file (" + filePath + ")! Message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
		
		// user login and password must be additionally encrypted, so encrypt them on-the-fly
		EncryptionUtils eu = new EncryptionUtils();
		try {
			if (encryptionEnabled) {
				prop.setProperty("user_login", eu.encryptPassword(prop.getProperty("user_login")));
				prop.setProperty("user_password", eu.encryptPassword(prop.getProperty("user_password")));
			}
		} catch (Exception e) {
			log.fatal("Could not encrypt user credentials! An exception occured: " + e.getMessage());
			// do not log exception stack trace for security reasons
			throw e;
		}
		
		// iterate through all the properties in order to change their values in the config file
		Iterator<Entry<Object, Object>> it = prop.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Object, Object> pair = (Map.Entry<Object, Object>) it.next();
			String paramKey = (String) pair.getKey();
			String paramValue = (String) pair.getValue();
			
			// escape "$" sign - without this, if it occurs in some property, it will not work with
			// replaceFirst method
			paramValue = paramValue.replace("$", "\\$");
			// we omit the default parameter values since they are read-only
			if (paramKey.startsWith("default_")) {
				continue;
			}
			// update each property from the file by replacing it with new pair key=value (even if the current
			// property might not have changed)
			configContent = configContent.replaceFirst("(?m)^" + paramKey + "=.*$", paramKey + "="
					+ paramValue);
		}
		
		try {
			String encryptedConfig = null;
			if (encryptionEnabled) {
				// encrypt the configuration in the opposite way to decryption
				encryptedConfig = EncryptionUtils.encryptConfiguration(encDecKey, configContent);
				encryptedConfig = encryptedConfig.replace("+", System.getProperty("line.separator"));
			} else {
				encryptedConfig = configContent;
			}
			
			// finally save the file
			FileUtils.write(new File(filePath), encryptedConfig, "UTF-8");
		} catch (Exception e) {
			log.error("An exception occured while saving configuration file (" + filePath + ")! "
					+ "Message: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
		
		log.info("File " + filePath + " was saved successfully.");
	}
	
	/**
	 * Method retrieving the datetime for scheduled next execution of the Application task in Task Scheduler.
	 * This is used to present the user value taken directly from Task Scheduler for informative purpose.
	 * 
	 * @return the next run time for the Application Task Scheduler or null/empty string when nothing was
	 * found
	 */
	public static String getApplicationTaskSchedulerNextRun() {
		String cmdOutput = "";
		List<String> commands = new ArrayList<String>();
		
		// this will execute command like <schtasks /query /tn "AegonReportingToolScheduler" /fo list>
		commands.add("schtasks.exe");
		commands.add("/query");
		commands.add("/tn");
		commands.add("\"" + TASK_SCHEDULER_NAME + "\"");
		commands.add("/fo");
		commands.add("list");
		
		try {
			cmdOutput = executeSystemCommand(commands, false);
		} catch (Exception e) {
			log.error("An exception has occured while trying to access Application Scheduler Task! Message: "
					+ e.getMessage());
			// we do not throw exception here (logging stack trace is enough)
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		// use regular expression to extract the next run time from the command output
		// the input should contain the string like "Next Run Time: 2015-07-27 20:00:00"
		String nextRunTime = null;
		Pattern pattern = Pattern.compile("Next Run Time:\\s+(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2})");
		Matcher matcher = pattern.matcher(cmdOutput);
		// check all occurences
		while (matcher.find()) {
			nextRunTime = matcher.group(1);
		}
		
		if (nextRunTime == null || nextRunTime.isEmpty()) {
			log.warn("No Next Run Time value found for Application Scheduler Task! "
					+ "This will be treated as an undefined value yet.");
		}
		
		return nextRunTime;
	}
	
	/**
	 * The main method used to manage Windows Task Scheduler - that is to create a new scheduled task for
	 * Aegon application if it does not exist yet or to change it (by deleting and recreating task) when its
	 * settings (time or frequency altered) have been modified. The created task is deployed in Windows Task
	 * Scheduler and is responsible for periodic download and storage of user data from Aegon service.
	 * 
	 * @throws Exception when reading system Task Scheduler failed or when creating/deleting task failed for
	 * some reason
	 */
	public static void manageApplicationTaskScheduler() throws Exception {
		boolean taskExists = false;
		
		// first check if we need to create a new scheduler task or if it already exists
		try {
			taskExists = checkIfTaskSchedulerExists();
		} catch (Exception e) {
			log.fatal("Fatal error occured while reading Task Scheduler data! Exception message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
		
		try {
			// if the task does not exist - create it
			if (!taskExists) {
				createNewTaskScheduler();
				log.info("Application Scheduler Task <" + TASK_SCHEDULER_NAME
						+ "> has been created successfully.");
				// otherwise change it by deleting the existing task and creating it again from scratch (with
				// new settings)
			} else {
				// do not log exception message right here, but at the end (to avoid duplicates)
				deleteTaskScheduler();
				log.info("Application Scheduler Task <" + TASK_SCHEDULER_NAME
						+ "> has been deleted successfully.");
				createNewTaskScheduler();
				log.info("Application Scheduler Task <" + TASK_SCHEDULER_NAME
						+ "> has been recreated successfully.");
			}
		} catch (Exception e) {
			log.fatal("Fatal error occured while managing Task Scheduler data! Exception message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Allows to switch computer to sleep mode if the application configuration allows to do it. Method checks
	 * the value of the config property <i>collecting_allow_sleep_computer_after_run</i> - if it is set to
	 * true, then wherever this method is called, it will try to put computer to sleep (with 15 seconds
	 * delay). Before suspending computer, method displays to user the warning message about putting computer
	 * to sleep soon.
	 */
	public static void putComputerToSleepIfApplicable() {
		String isSleepMode = getPropertyValue("collecting_allow_sleep_computer_after_run");
		
		// quit if sleep mode is disabled
		if (!Boolean.parseBoolean(isSleepMode)) {
			return;
		}
		
		// build the commands to put computer to sleep
		List<String> commands = new ArrayList<String>();
		commands.add("cmd.exe");
		commands.add("/C");
		// ping is used here as a workaround for "timeout" command, which does not work in Java
		// so "ping localhost -n 15" will cause 15 seconds delay before executing suspend command
		commands.add("ping localhost -n 15 > nul && %windir%\\System32\\rundll32.exe powrprof.dll,SetSuspendState Standby");
		
		try {
			log.info("Sleep mode is active, putting computer to sleep in 15 seconds.");
			
			// run sleep command
			executeSystemCommand(commands, true);
			
			// display in advance the tray message with the information for the user
			try {
				String userLanguage = ConfigurationProvider.getPropertyValue("user_language");
				String trayTitle = ConfigurationProvider.getMessageByLanguage("aegon_app_name", userLanguage);
				String trayMsg = ConfigurationProvider.getMessageByLanguage("switch_to_sleep_mode_msg",
						userLanguage);
				Utils.showTrayMessage(trayTitle, trayMsg, Utils.WARNING);
			} catch (Exception e) {
				log.error("An error occured while trying to display tray message! Exception message: "
						+ e.getMessage());
				log.error(Utils.getExceptionStackTrace(e));
			}
		} catch (Exception e) {
			log.error("An error occured while trying to put computer to sleep! Exception message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
	}
}