/**
 * EncryptionUtils is a utility class providing encryption and decryption interface for safe storage
 * of user credentials in the configuration file as well as an encryption of the configuration file itself.
 * The process of encryption and decryption is based on few ciphering algorithms, including AES and SHA-2
 * (see the code for more details).
 * This class is used only in ConfigurationProvider class and it's source code should be additionally protected
 * (therefore there are limited comments to the code not to reveal implementation details).
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.config;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.util.text.StrongTextEncryptor;

public class EncryptionUtils {
	private static String encryptionKey = null;

	/**
	 * Method encrypting content of configuration file (although it can encrypt any String) using provided
	 * encryption key. The method has the package-private access and is supposed to be used only inside
	 * ConfigurationProvider class.
	 * 
	 * @param key external key used for encryption
	 * @param value text to be encrypted
	 * @return encrypted value
	 * @throws Exception when encryption failed for some reason (few different Exceptions possible)
	 */
	static String encryptConfiguration(String key, String value) throws Exception {
		try {
			IvParameterSpec iv = new IvParameterSpec(key.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
			byte[] encrypted = cipher.doFinal(value.getBytes());

			return Base64.encodeBase64String(encrypted);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method decrypting content of previously encrypted configuration file (although it can decrypt any
	 * String) using provided encryption key. The method has the package-private access and is supposed to be
	 * used only inside ConfigurationProvider class.
	 * 
	 * @param key external key used for decryption
	 * @param encrypted text to be decrypted
	 * @return decrypted value (plain text)
	 * @throws Exception when decryption failed for some reason (few different Exceptions possible)
	 */
	static String decryptConfiguration(String key, String encrypted) throws Exception {
		try {
			IvParameterSpec iv = new IvParameterSpec(key.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

			return new String(original);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method encrypting securely given password or another text value.
	 * 
	 * @param plainPassword password or another text to be securely encrypted
	 * @return encrypted value of the given parameter
	 * @throws Exception when encryption process failed (few different Exceptions possible)
	 */
	String encryptPassword(String plainPassword) throws Exception {
		String encryptedPasswordStr = null;
		if (encryptionKey == null) {
			encryptionKey = new String(Base64.encodeBase64(EncryptionUtils.class.getName().getBytes()))
					.substring(0, 24);
		}

		try {
			StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
			textEncryptor.setPassword(encryptionKey);

			byte[] strongEncryptionBytes = textEncryptor.encrypt(plainPassword).getBytes();
			byte[] encryptBytes = new AESCrypt().encrypt("AES", 256, strongEncryptionBytes);
			encryptedPasswordStr = new String(Base64.encodeBase64(encryptBytes));
		} catch (Exception e) {
			throw e;
		}

		return encryptedPasswordStr;
	}

	/**
	 * Method decrypting previously encripted password or another text value.
	 * 
	 * @param encryptedPassword password or another text to be decrypted
	 * @return decrypted value (plain text) of the given parameter
	 * @throws Exception when decryption process failed (few different Exceptions possible)
	 */
	String decryptPassword(String encryptedPassword) throws Exception {
		String decryptedPasswordStr = null;
		if (encryptionKey == null) {
			encryptionKey = new String(Base64.encodeBase64(EncryptionUtils.class.getName().getBytes()))
					.substring(0, 24);
		}

		try {
			StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
			textEncryptor.setPassword(encryptionKey);

			byte[] decryptionBytes = new AESCrypt().decrypt("AES", 256,
					Base64.decodeBase64(encryptedPassword));
			String strongDecryptedStr = new String(decryptionBytes);
			decryptedPasswordStr = textEncryptor.decrypt(strongDecryptedStr);
		} catch (Exception e) {
			throw e;
		}

		return decryptedPasswordStr;
	}

	/**
	 * Internal helper class providing methods for encryption/decryption mechanism based on AES algorithm.
	 * Parts of this code were copied from the StandardPBEByteEncryptor class from the Jasypt (www.jasypt.org)
	 * project.
	 * Source: http://stackoverflow.com/questions/13684602/encrypt-decrypt-with-aes-and-salt-size-of-32
	 */
	private class AESCrypt {
		private final String KEY_ALGORITHM = "PBEWithSHA256And256BitAES-CBC-BC";
		private final String MODE_PADDING = "/CBC/PKCS5Padding";
		private final int DEFAULT_SALT_SIZE_BYTES = 32;
		private final SecureRandom rand;
		private final String passwd = "VkRCa2FrMVhUbGRWVkU1UFVsaFNjbFpzV25KUVVUMDk=";

		public AESCrypt() throws Exception {
			Security.addProvider(new BouncyCastleProvider());
			rand = SecureRandom.getInstance("SHA1PRNG");
		}

		private byte[] generateSalt(int size) {
			byte[] salt = new byte[size];
			rand.nextBytes(salt);
			return salt;
		}

		private SecretKey generateKey(String algorithm, int keySize, byte[] salt)
				throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException {
			SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_ALGORITHM);

			byte[] dwssap = passwd.getBytes();
			for (int i = 0; i < passwd.length() / 11; i++) {
				dwssap = Base64.decodeBase64(dwssap);
			}
			PBEKeySpec pbeKeySpec = new PBEKeySpec(new String(dwssap).toCharArray(), salt, 100000);
			SecretKey tmpKey = factory.generateSecret(pbeKeySpec);
			byte[] keyBytes = new byte[keySize / 8];
			System.arraycopy(tmpKey.getEncoded(), 0, keyBytes, 0, keyBytes.length);

			return new SecretKeySpec(keyBytes, algorithm);
		}

		private byte[] generateIV(Cipher cipher) {
			byte[] iv = new byte[cipher.getBlockSize()];
			rand.nextBytes(iv);
			return iv;
		}

		private byte[] appendArrays(byte[] firstArray, byte[] secondArray) {
			final byte[] result = new byte[firstArray.length + secondArray.length];

			System.arraycopy(firstArray, 0, result, 0, firstArray.length);
			System.arraycopy(secondArray, 0, result, firstArray.length, secondArray.length);
			return result;
		}

		private byte[] encrypt(String algorithm, int keySize, final byte[] message) throws Exception {
			Cipher cipher = Cipher.getInstance(algorithm + MODE_PADDING);

			// The salt size for the chosen algorithm is set to be equal
			// to the algorithm's block size (if it is a block algorithm).
			int saltSizeBytes = DEFAULT_SALT_SIZE_BYTES;
			int algorithmBlockSize = cipher.getBlockSize();
			if (algorithmBlockSize > 0) {
				saltSizeBytes = algorithmBlockSize;
			}

			// create salt
			final byte[] salt = generateSalt(saltSizeBytes);
			SecretKey key = generateKey(algorithm, keySize, salt);
			// create a new IV for each encryption
			final IvParameterSpec ivParamSpec = new IvParameterSpec(generateIV(cipher));

			// perform encryption using the Cipher
			cipher.init(Cipher.ENCRYPT_MODE, key, ivParamSpec);
			byte[] encryptedMessage = cipher.doFinal(message);

			// append the IV and salt
			encryptedMessage = appendArrays(ivParamSpec.getIV(), encryptedMessage);
			encryptedMessage = appendArrays(salt, encryptedMessage);

			return encryptedMessage;
		}

		private byte[] decrypt(String algorithm, int keySize, final byte[] encryptedMessage) throws Exception {
			Cipher cipher = Cipher.getInstance(algorithm + MODE_PADDING);

			// determine the salt size for the first layer of encryption
			int saltSizeBytes = DEFAULT_SALT_SIZE_BYTES;
			int algorithmBlockSize = cipher.getBlockSize();
			if (algorithmBlockSize > 0) {
				saltSizeBytes = algorithmBlockSize;
			}

			byte[] decryptedMessage = new byte[encryptedMessage.length];
			System.arraycopy(encryptedMessage, 0, decryptedMessage, 0, encryptedMessage.length);

			// extract the salt and IV from the incoming message
			byte[] salt = null;
			byte[] iv = null;
			byte[] encryptedMessageKernel = null;
			final int saltStart = 0;
			final int saltSize = (saltSizeBytes < decryptedMessage.length ? saltSizeBytes
					: decryptedMessage.length);

			final int ivStart = (saltSizeBytes < decryptedMessage.length ? saltSizeBytes
					: decryptedMessage.length);
			final int ivSize = cipher.getBlockSize();
			final int encMesKernelStart = (saltSizeBytes + ivSize < decryptedMessage.length ? saltSizeBytes
					+ ivSize : decryptedMessage.length);
			final int encMesKernelSize = (saltSizeBytes + ivSize < decryptedMessage.length ? (decryptedMessage.length
					- saltSizeBytes - ivSize)
					: 0);

			salt = new byte[saltSize];
			iv = new byte[ivSize];
			encryptedMessageKernel = new byte[encMesKernelSize];

			System.arraycopy(decryptedMessage, saltStart, salt, 0, saltSize);
			System.arraycopy(decryptedMessage, ivStart, iv, 0, ivSize);
			System.arraycopy(decryptedMessage, encMesKernelStart, encryptedMessageKernel, 0, encMesKernelSize);

			SecretKey key = generateKey(algorithm, keySize, salt);
			IvParameterSpec ivParamSpec = new IvParameterSpec(iv);

			// perform decryption using the Cipher
			cipher.init(Cipher.DECRYPT_MODE, key, ivParamSpec);
			decryptedMessage = cipher.doFinal(encryptedMessageKernel);

			// return the results
			return decryptedMessage;
		}
	}

}