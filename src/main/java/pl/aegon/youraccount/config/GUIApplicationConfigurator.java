package pl.aegon.youraccount.config;

import java.util.Arrays;

import org.apache.logging.log4j.*;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.commons.validator.routines.EmailValidator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.*;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.jface.dialogs.MessageDialog;

import pl.aegon.youraccount.utils.Utils;
import pl.aegon.youraccount.utils.Utils.*;


public class GUIApplicationConfigurator {
	private static Logger log = LogManager.getLogger(GUIApplicationConfigurator.class);
	private static final Color WIDGET_BG_GREY_COLOR = SWTResourceManager.getColor(240, 240, 240);
	private static final Color INVALID_FORM_FIELD_COLOR = SWTResourceManager.getColor(255, 85, 85);
	private static final Color VALID_FORM_FIELD_COLOR = SWTResourceManager.getColor(SWT.COLOR_WHITE);
	private static final String OPT_TRUE_VALUE = "true";
	private static final String OPT_FALSE_VALUE = "false";
	private static Image helpImage;
	private static String userLanguage;
	private static boolean isLanguageChanged = false;
	private static boolean isBrowserVisible;
	private static ChartRecordsChoice selectedReportRecordsType;
	private static ChartWholeAccountChoice selectedWholeAccountChartType;
	private static ChartPoliciesTypeChoice selectedPoliciesChartType;
	private static ChartScaleTypeChoice selectedChartScaleType;

	protected Shell shell;
	private Text urlAddress;
	private Text userLogin;
	private Text userPassword;
	private Combo languageSelected;
	private Button btnRadioHideBrowser;
	private Button btnRadioShowBrowser;
	private Combo browserSelected;
	private Combo retriesNumber;
	private Text retriesTimeInterval;
	private DateTime collectingDateTime;
	private Combo collectingDaysFrequency;
	private Label lblCollectingSchedulerInfo;
	private Button btnAllowWakeToRun;
	private Button btnAllowSleepAfterRun;
	private Button btnSendReportToUserEmail;
	private Text emailAddress;
	private Button btnSaveReportInFolder;
	private Label folderPathSelected;
	private Button btnNumberOfDays;
	private Button btnNumberOfRecords;
	private Text selectedRecordsNumber;
	private Button btnCreateWholeAccountChartSingle;
	private Button btnCreateWholeAccountChartPolicies;
	private Button btnCreateWholeAccountChartDisabled;
	private Button btnCreatePoliciesChart;
	private Button btnCreatePoliciesChartJoined;
	private Button btnCreatePoliciesChartSeparated;
	private Button btnCreatePoliciesChartSubaccounts;
	private Button btnCreateOnlySubAccountsChart;
	private Button btnCreateSubAccountsUFKChart;
	private Button btnCreateSubAccountsUFKChartWithSubAccount;
	private Button btnCreatePoliciesPieChart;
	private Button btnCreateSubAccountsPieChart;
	private Button btnCreateSubAccountsUFKPieChart;
	private Button btnSkipCreatingSingleItemPieChart;
	private Button btnCombinedChartsScaleCommon;
	private Button btnCombinedChartsScaleSeparated;
	private Button btnCombinedChartsScaleLogarithmic;

	/**
	 * Default class constructor.
	 */
	public GUIApplicationConfigurator() {
		helpImage = SWTResourceManager.getImage(System.getProperty("user.dir") + "\\res\\img\\help.gif");
		userLanguage = ConfigurationProvider.getPropertyValue("user_language");
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			GUIApplicationConfigurator window = new GUIApplicationConfigurator();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents(display);
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents(Display display) {
		// disable resizing the main window
		shell = new Shell(SWT.CLOSE | SWT.MIN | SWT.TITLE);
		shell.setMinimumSize(640, 480);
		shell.setBackground(WIDGET_BG_GREY_COLOR);
		shell.setSize(966, 608);
		shell.setText(getTextForWidget("shell_title"));
		shell.setLayout(null);

		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();

		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation(x, y);

		Button btnSaveChanges = new Button(shell, SWT.CENTER);
		btnSaveChanges.setBounds(230, 515, 115, 32);
		btnSaveChanges.setText(getTextForWidget("btn_save_changes"));
		btnSaveChanges.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				// if the form is not valid, display the warning about the error
				if (!validateApplicationForm()) {
					log.info("Application form errors detected - changes are not saved");
					String dialogTitle = getTextForWidget("save_changes_warn_dialog_title");
					String dialogMsg = getTextForWidget("save_changes_warn_dialog_msg");
					MessageDialog dialog = new MessageDialog(shell, dialogTitle, null, dialogMsg,
							MessageDialog.WARNING, new String[] { "OK" }, 0);
					dialog.open();
				} else {
					try {
						saveConfigurationChanges();
						String dialogTitle = getTextForWidget("save_changes_ok_dialog_title");
						String dialogMsg = getTextForWidget("save_changes_ok_dialog_msg");
						MessageDialog dialog = new MessageDialog(shell, dialogTitle, null, dialogMsg,
								MessageDialog.INFORMATION, new String[] { "OK" }, 0);
						dialog.open();
						
						// if the interface language was changed, reopen the window; that's why we call
						// shell.dispose() and call open() again (this is not the best method, because it
						// causes the window to blink once quickly, but no other working solution was found...
						if (isLanguageChanged) {
							// to avoid reopening window after each new save, clear the flag
							isLanguageChanged = false;
							shell.dispose();
							open();
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});

		Button btnRestoreDefaults = new Button(shell, SWT.CENTER);
		btnRestoreDefaults.setBounds(416, 515, 115, 32);
		btnRestoreDefaults.setText(getTextForWidget("btn_restore_defaults"));
		btnRestoreDefaults.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {

				// customized MessageDialog with configured buttons
				String dialogTitle = getTextForWidget("restore_defaults_dialog_title");
				String dialogMsg = getTextForWidget("restore_defaults_dialog_msg");
				MessageDialog dialog = new MessageDialog(shell, dialogTitle, null, dialogMsg,
						MessageDialog.QUESTION, new String[] { getTextForWidget("dialog_yes"),
								getTextForWidget("dialog_no") }, 0);
				if (dialog.open() == MessageDialog.OK) {
					try {
						restoreDefaultConfiguration();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});

		Button btnCancel = new Button(shell, SWT.CENTER);
		btnCancel.setBounds(602, 515, 115, 32);
		btnCancel.setText(getTextForWidget("btn_cancel"));
		// add event listener closing the main window after clicking on "Cancel" button
		btnCancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				shell.dispose();
			}
		});

		Composite compositeContainer = new Composite(shell, SWT.NO_BACKGROUND | SWT.NO_FOCUS
				| SWT.NO_MERGE_PAINTS | SWT.EMBEDDED);
		compositeContainer.setBounds(0, 0, 950, 570);

		TabFolder tabFolder = new TabFolder(compositeContainer, SWT.NONE);
		tabFolder.setLocation(0, 0);
		tabFolder.setSize(950, 570);
		tabFolder.setForeground(SWTResourceManager.getColor(0, 0, 0));
		tabFolder.setBackground(WIDGET_BG_GREY_COLOR);

		TabItem tabItemGeneral = new TabItem(tabFolder, SWT.NONE);
		tabItemGeneral.setText(getTextForWidget("tab_item_general"));

		Composite compositeTab1 = new Composite(tabFolder, SWT.NONE);
		compositeTab1.setBackground(WIDGET_BG_GREY_COLOR);
		compositeTab1.setForeground(WIDGET_BG_GREY_COLOR);
		tabItemGeneral.setControl(compositeTab1);
		compositeTab1.setLayout(null);

		Group grpConnection = new Group(compositeTab1, SWT.NONE);
		grpConnection.setBackground(WIDGET_BG_GREY_COLOR);
		grpConnection.setForeground(WIDGET_BG_GREY_COLOR);
		grpConnection.setText(getTextForWidget("grp_connection"));
		grpConnection.setBounds(10, 10, 922, 142);

		Label lblUrlAddress = new Label(grpConnection, SWT.NONE);
		lblUrlAddress.setBounds(22, 34, 75, 16);
		lblUrlAddress.setText(getTextForWidget("lbl_url_address"));

		urlAddress = new Text(grpConnection, SWT.BORDER);
		urlAddress.setTextLimit(60);
		urlAddress.setText(getConfigOption("website_url", false));
		urlAddress.setToolTipText(getTextForWidget("url_address_tooltip"));
		urlAddress.setBounds(110, 31, 240, 22);
		urlAddress.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent event) {
				// get the widget whose text was modified
				Text text = (Text) event.widget;
				if (text.getText().isEmpty() || !new UrlValidator().isValid(text.getText())) {
					text.setBackground(INVALID_FORM_FIELD_COLOR);
				} else {
					text.setBackground(VALID_FORM_FIELD_COLOR);
				}
			}
		});

		Label lblHelpUrlAddress = new Label(grpConnection, SWT.WRAP | SWT.CENTER);
		lblHelpUrlAddress.setToolTipText(getTextForWidget("lbl_help_url_address_tooltip"));
		lblHelpUrlAddress.setAlignment(SWT.CENTER);
		lblHelpUrlAddress.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpUrlAddress.setImage(helpImage);
		lblHelpUrlAddress.setBounds(360, 30, 24, 24);

		Label lblLogin = new Label(grpConnection, SWT.NONE);
		lblLogin.setBounds(22, 68, 55, 16);
		lblLogin.setText("Login"); // this one does not need translation

		userLogin = new Text(grpConnection, SWT.BORDER);
		userLogin.setTextLimit(32);
		userLogin.setText(getConfigOption("user_login", false));
		userLogin.setToolTipText(getTextForWidget("user_login_tooltip"));
		userLogin.setBounds(110, 65, 172, 22);
		// it is possible that at the launch the login will be empty (but only on the initial run)
		if (userLogin.getText().isEmpty()) {
			userLogin.setBackground(INVALID_FORM_FIELD_COLOR);
		}
		userLogin.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent event) {
				// get the widget whose text was modified
				Text text = (Text) event.widget;
				if (text.getText().isEmpty()) {
					text.setBackground(INVALID_FORM_FIELD_COLOR);
				} else {
					text.setBackground(VALID_FORM_FIELD_COLOR);
				}
			}
		});

		Label lblHelpLogin = new Label(grpConnection, SWT.WRAP | SWT.CENTER);
		lblHelpLogin.setToolTipText(getTextForWidget("lbl_help_login_tooltip"));
		lblHelpLogin.setImage(helpImage);
		lblHelpLogin.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpLogin.setAlignment(SWT.CENTER);
		lblHelpLogin.setBounds(360, 64, 24, 24);

		Label lblPassword = new Label(grpConnection, SWT.NONE);
		lblPassword.setBounds(22, 102, 55, 16);
		lblPassword.setText(getTextForWidget("lbl_password"));

		userPassword = new Text(grpConnection, SWT.PASSWORD | SWT.BORDER);
		userPassword.setTextLimit(32);
		userPassword.setText(getConfigOption("user_password", false));
		userPassword.setToolTipText(getTextForWidget("user_password_tooltip"));
		userPassword.setBounds(110, 99, 172, 22);
		// it is possible that at the launch password will be empty (but only on the initial run)
		if (userPassword.getText().isEmpty()) {
			userPassword.setBackground(INVALID_FORM_FIELD_COLOR);
		}
		userPassword.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent event) {
				// get the widget whose text was modified
				Text text = (Text) event.widget;
				if (text.getText().isEmpty()) {
					text.setBackground(INVALID_FORM_FIELD_COLOR);
				} else {
					text.setBackground(VALID_FORM_FIELD_COLOR);
				}
			}
		});

		Label lblHelpPassword = new Label(grpConnection, SWT.WRAP | SWT.CENTER);
		lblHelpPassword.setToolTipText(getTextForWidget("lbl_help_password_tooltip"));
		lblHelpPassword.setImage(helpImage);
		lblHelpPassword.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpPassword.setAlignment(SWT.CENTER);
		lblHelpPassword.setBounds(360, 98, 24, 24);

		Group grpLangAndBrowser = new Group(compositeTab1, SWT.NONE);
		grpLangAndBrowser.setBackground(WIDGET_BG_GREY_COLOR);
		grpLangAndBrowser.setText(getTextForWidget("grp_lang_and_browser"));
		grpLangAndBrowser.setBounds(10, 165, 922, 269);

		Label lblSelectLanguage = new Label(grpLangAndBrowser, SWT.NONE);
		lblSelectLanguage.setBounds(22, 35, 120, 18);
		lblSelectLanguage.setText(getTextForWidget("lbl_select_language"));

		languageSelected = new Combo(grpLangAndBrowser, SWT.READ_ONLY);
		languageSelected.setToolTipText(getTextForWidget("language_selected_tooltip"));
		languageSelected.setItems(new String[] { "English", "Polski" });
		languageSelected.setBounds(196, 32, 148, 22);
		int languageIndex = Arrays.asList(languageSelected.getItems()).indexOf(
				getConfigOption("user_language", false));
		languageSelected.select(languageIndex);

		Label lblSelectBrowser = new Label(grpLangAndBrowser, SWT.NONE);
		lblSelectBrowser.setText(getTextForWidget("lbl_select_browser"));
		lblSelectBrowser.setBounds(22, 71, 160, 18);

		btnRadioHideBrowser = new Button(grpLangAndBrowser, SWT.RADIO);
		btnRadioHideBrowser.setEnabled(true);
		btnRadioHideBrowser.setToolTipText(getTextForWidget("btn_radio_hide_browser_tooltip"));
		btnRadioHideBrowser.setBounds(196, 71, 160, 20);
		btnRadioHideBrowser.setText(getTextForWidget("btn_radio_hide_browser"));
		if (getConfigOption("browser_selected", false).equals("Phantom JS")) {
			btnRadioHideBrowser.setSelection(true);
			isBrowserVisible = false;
		}
		btnRadioHideBrowser.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				isBrowserVisible = false;
			}
		});

		Label lblHideBrowserDesc = new Label(grpLangAndBrowser, SWT.WRAP | SWT.SHADOW_IN);
		lblHideBrowserDesc.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblHideBrowserDesc.setBounds(196, 101, 400, 41);
		lblHideBrowserDesc.setText(getTextForWidget("lbl_hide_browser_desc"));

		btnRadioShowBrowser = new Button(grpLangAndBrowser, SWT.RADIO);
		btnRadioShowBrowser.setEnabled(true);
		btnRadioShowBrowser.setToolTipText(getTextForWidget("btn_radio_show_browser_tooltip"));
		btnRadioShowBrowser.setBounds(196, 151, 160, 20);
		btnRadioShowBrowser.setText(getTextForWidget("btn_radio_show_browser"));
		if (!getConfigOption("browser_selected", false).equals("Phantom JS")) {
			btnRadioShowBrowser.setSelection(true);
			isBrowserVisible = true;
		}
		btnRadioShowBrowser.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				isBrowserVisible = true;
			}
		});

		browserSelected = new Combo(grpLangAndBrowser, SWT.READ_ONLY);
		browserSelected.setToolTipText(getTextForWidget("browser_selected_tooltip"));
		browserSelected.setItems(new String[] { "Mozilla Firefox", "Google Chrome", "Internet Explorer" });
		browserSelected.setBounds(196, 181, 135, 22);
		int browserIndex = Arrays.asList(browserSelected.getItems()).indexOf(
				getConfigOption("browser_selected", false));
		if (browserIndex == -1) {
			browserIndex = 0;
		}
		browserSelected.select(browserIndex);

		Label lblShowBrowserDesc = new Label(grpLangAndBrowser, SWT.WRAP | SWT.SHADOW_IN);
		lblShowBrowserDesc.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblShowBrowserDesc.setText(getTextForWidget("lbl_show_browser_desc"));
		lblShowBrowserDesc.setBounds(195, 214, 400, 41);

		TabItem tabItemDataDownload = new TabItem(tabFolder, SWT.NONE);
		tabItemDataDownload.setText(getTextForWidget("tab_item_data_download"));

		Composite compositeTab2 = new Composite(tabFolder, SWT.NONE);
		compositeTab2.setBackground(WIDGET_BG_GREY_COLOR);
		tabItemDataDownload.setControl(compositeTab2);
		compositeTab2.setLayout(null);

		Label lblRetriesNumber = new Label(compositeTab2, SWT.NONE);
		lblRetriesNumber.setBounds(24, 30, 280, 16);
		lblRetriesNumber.setText(getTextForWidget("lbl_retries_number"));

		retriesNumber = new Combo(compositeTab2, SWT.READ_ONLY);
		retriesNumber.setToolTipText(getTextForWidget("retries_number_tooltip"));
		retriesNumber.setItems(new String[] { "0", "1", "2", "3", "4", "5" });
		retriesNumber.setBounds(310, 27, 38, 23);
		int retriesIndex = Arrays.asList(retriesNumber.getItems()).indexOf(
				getConfigOption("download_retries_number", false));
		retriesNumber.select(retriesIndex);

		Label lblHelpRetriesNumber = new Label(compositeTab2, SWT.WRAP | SWT.CENTER);
		lblHelpRetriesNumber.setBounds(372, 26, 24, 24);
		lblHelpRetriesNumber.setToolTipText(getTextForWidget("lbl_help_retries_number_tooltip"));
		lblHelpRetriesNumber.setImage(helpImage);
		lblHelpRetriesNumber.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpRetriesNumber.setAlignment(SWT.CENTER);

		Label lblWaitFor = new Label(compositeTab2, SWT.NONE);
		lblWaitFor.setBounds(24, 68, 70, 16);
		lblWaitFor.setText(getTextForWidget("lbl_wait_for"));

		retriesTimeInterval = new Text(compositeTab2, SWT.BORDER);
		retriesTimeInterval.setTextLimit(2);
		retriesTimeInterval.setText(getConfigOption("download_retries_minutes_delay", false));
		retriesTimeInterval.setBounds(100, 65, 32, 22);
		retriesTimeInterval.setToolTipText(getTextForWidget("retries_time_interval_tooltip"));
		retriesTimeInterval.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				e.doit = Character.isDigit(e.character) || e.keyCode == SWT.ARROW_LEFT
						|| e.keyCode == SWT.NONE || e.keyCode == SWT.ARROW_RIGHT || e.keyCode == SWT.BS
						|| e.keyCode == SWT.DEL;
			}
		});
		retriesTimeInterval.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent event) {
				// get the widget whose text was modified
				Text text = (Text) event.widget;
				
				try {
					if (Integer.parseInt(text.getText()) < Utils.DOWNLOAD_RETRIES_TIME_INTERVAL_MIN_RANGE
							|| Integer.parseInt(text.getText()) > Utils.DOWNLOAD_RETRIES_TIME_INTERVAL_MAX_RANGE) {
						text.setBackground(INVALID_FORM_FIELD_COLOR);
					} else {
						text.setBackground(VALID_FORM_FIELD_COLOR);
					}
				} catch (NumberFormatException e) {
					text.setBackground(INVALID_FORM_FIELD_COLOR);
				}
			}
		});
		Label lblRetriesTimeInterval = new Label(compositeTab2, SWT.NONE);
		lblRetriesTimeInterval.setBounds(140, 68, 185, 16);
		lblRetriesTimeInterval.setText(getTextForWidget("lbl_retries_time_interval"));

		Label lblHelpRetriesTimeInterval = new Label(compositeTab2, SWT.WRAP | SWT.CENTER);
		lblHelpRetriesTimeInterval.setToolTipText(getTextForWidget("lbl_help_retries_time_interval_tooltip"));
		lblHelpRetriesTimeInterval.setImage(helpImage);
		lblHelpRetriesTimeInterval.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpRetriesTimeInterval.setAlignment(SWT.CENTER);
		lblHelpRetriesTimeInterval.setBounds(372, 64, 24, 24);

		Label lblCollectingDateTime = new Label(compositeTab2, SWT.NONE);
		lblCollectingDateTime.setBounds(24, 106, 142, 16);
		lblCollectingDateTime.setText(getTextForWidget("lbl_collecting_date_time"));

		collectingDateTime = new DateTime(compositeTab2, SWT.BORDER | SWT.TIME | SWT.SHORT);
		collectingDateTime.setToolTipText(getTextForWidget("collecting_date_time_tooltip"));
		collectingDateTime.setBounds(176, 102, 60, 24);
		int userHours = Integer.parseInt(getConfigOption("collecting_date_time", false).substring(0, 2));
		int userMinutes = Integer.parseInt(getConfigOption("collecting_date_time", false).substring(3, 5));
		collectingDateTime.setTime(userHours, userMinutes, 0);

		Label lblHelpCollectingDateTime = new Label(compositeTab2, SWT.WRAP | SWT.CENTER);
		lblHelpCollectingDateTime.setToolTipText(getTextForWidget("lbl_help_collecting_date_time_tooltip"));
		lblHelpCollectingDateTime.setImage(helpImage);
		lblHelpCollectingDateTime.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpCollectingDateTime.setAlignment(SWT.CENTER);
		lblHelpCollectingDateTime.setBounds(372, 102, 24, 24);

		Label lblCollectingDaysFrequency = new Label(compositeTab2, SWT.NONE);
		lblCollectingDaysFrequency.setText(getTextForWidget("lbl_collecting_days_frequency"));
		lblCollectingDaysFrequency.setBounds(24, 144, 160, 17);

		collectingDaysFrequency = new Combo(compositeTab2, SWT.READ_ONLY);
		collectingDaysFrequency.setToolTipText(getTextForWidget("collecting_days_frequency_tooltip"));
		collectingDaysFrequency.setItems(new String[] { "1", "2", "3", "4", "5" });
		collectingDaysFrequency.setBounds(188, 141, 35, 23);
		int collectingIndex = Arrays.asList(collectingDaysFrequency.getItems()).indexOf(
				getConfigOption("collecting_days_frequency", false));
		collectingDaysFrequency.select(collectingIndex);

		Label lblCollectingDays = new Label(compositeTab2, SWT.NONE);
		lblCollectingDays.setBounds(232, 144, 55, 17);
		lblCollectingDays.setText(getTextForWidget("lbl_collecting_days"));

		Label lblHelpCollectingDaysFrequency = new Label(compositeTab2, SWT.WRAP | SWT.CENTER);
		lblHelpCollectingDaysFrequency
				.setToolTipText(getTextForWidget("lbl_help_collecting_days_frequency_tooltip"));
		lblHelpCollectingDaysFrequency.setImage(helpImage);
		lblHelpCollectingDaysFrequency.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpCollectingDaysFrequency.setAlignment(SWT.CENTER);
		lblHelpCollectingDaysFrequency.setBounds(372, 140, 24, 24);
		
		btnAllowWakeToRun = new Button(compositeTab2, SWT.CHECK);
		btnAllowWakeToRun.setBounds(24, 182, 335, 20);
		btnAllowWakeToRun.setText(getTextForWidget("btn_allow_wake_to_run"));
		btnAllowWakeToRun.setToolTipText(getTextForWidget("btn_allow_wake_to_run_tooltip"));
		btnAllowWakeToRun.setSelection(Boolean.valueOf(getConfigOption("collecting_allow_wake_computer_to_run", false)));
		
		Label lblHelpAllowWakeToRun = new Label(compositeTab2, SWT.WRAP | SWT.CENTER);
		lblHelpAllowWakeToRun.setToolTipText(getTextForWidget("lbl_help_allow_wake_to_run_tooltip"));
		lblHelpAllowWakeToRun.setImage(helpImage);
		lblHelpAllowWakeToRun.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpAllowWakeToRun.setAlignment(SWT.CENTER);
		lblHelpAllowWakeToRun.setBounds(372, 180, 24, 24);
		
		btnAllowSleepAfterRun = new Button(compositeTab2, SWT.CHECK);
		btnAllowSleepAfterRun.setBounds(24, 212, 335, 20);
		btnAllowSleepAfterRun.setText(getTextForWidget("btn_allow_sleep_after_run"));
		btnAllowSleepAfterRun.setToolTipText(getTextForWidget("btn_allow_sleep_after_run_tooltip"));
		btnAllowSleepAfterRun.setSelection(Boolean.valueOf(getConfigOption("collecting_allow_sleep_computer_after_run", false)));
		
		Label lblHelpAllowSleepAfterRun = new Label(compositeTab2, SWT.WRAP | SWT.CENTER);
		lblHelpAllowSleepAfterRun.setToolTipText(getTextForWidget("lbl_help_allow_sleep_after_run_tooltip"));
		lblHelpAllowSleepAfterRun.setImage(helpImage);
		lblHelpAllowSleepAfterRun.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpAllowSleepAfterRun.setAlignment(SWT.CENTER);
		lblHelpAllowSleepAfterRun.setBounds(372, 210, 24, 24);

		lblCollectingSchedulerInfo = new Label(compositeTab2, SWT.WRAP | SWT.SHADOW_IN);
		lblCollectingSchedulerInfo.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblCollectingSchedulerInfo.setBounds(24, 250, 364, 40);

		String schedulerNextRun = ConfigurationProvider.getApplicationTaskSchedulerNextRun();
		if (schedulerNextRun == null || schedulerNextRun.isEmpty()) {
			lblCollectingSchedulerInfo.setText(getTextForWidget("lbl_collecting_scheduler_info_no_run"));
		} else {
			lblCollectingSchedulerInfo.setText(getTextForWidget("lbl_collecting_scheduler_info_next_run")
					+ schedulerNextRun + ".");
		}

		TabItem tabItemReportsCreation = new TabItem(tabFolder, SWT.NONE);
		tabItemReportsCreation.setText(getTextForWidget("tab_item_reports_creation"));

		Composite compositeTab3 = new Composite(tabFolder, SWT.NONE);
		compositeTab3.setBackground(WIDGET_BG_GREY_COLOR);
		tabItemReportsCreation.setControl(compositeTab3);
		compositeTab3.setLayout(null);

		Composite grpSendAndSaveReportOptions = new Composite(compositeTab3, SWT.NONE);
		grpSendAndSaveReportOptions.setBounds(24, 30, 412, 155);
		
		btnSendReportToUserEmail = new Button(grpSendAndSaveReportOptions, SWT.CHECK);
		btnSendReportToUserEmail.setBounds(0, 0, 328, 20);
		btnSendReportToUserEmail.setText(getTextForWidget("btn_send_report_to_user_email"));
		btnSendReportToUserEmail.setToolTipText(getTextForWidget("btn_send_report_to_user_email_tooltip"));
		btnSendReportToUserEmail
				.setSelection(Boolean.valueOf(getConfigOption("send_report_and_notification_email", false)));
		btnSendReportToUserEmail.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (btnSendReportToUserEmail.getSelection()) {
					if (!EmailValidator.getInstance().isValid(emailAddress.getText())) {
						emailAddress.setBackground(INVALID_FORM_FIELD_COLOR);
					} else {
						emailAddress.setBackground(VALID_FORM_FIELD_COLOR);
					}
				} else {
					if (!emailAddress.getText().isEmpty()
							&& !EmailValidator.getInstance().isValid(emailAddress.getText())) {
						emailAddress.setBackground(INVALID_FORM_FIELD_COLOR);
					} else {
						emailAddress.setBackground(VALID_FORM_FIELD_COLOR);
					}
				}
			}
		});

		Label lblEmailAddress = new Label(grpSendAndSaveReportOptions, SWT.NONE);
		lblEmailAddress.setBounds(0, 32, 85, 16);
		lblEmailAddress.setText(getTextForWidget("lbl_email_address"));

		emailAddress = new Text(grpSendAndSaveReportOptions, SWT.BORDER);
		emailAddress.setBounds(91, 29, 240, 22);
		emailAddress.setTextLimit(50);
		emailAddress.setText(getConfigOption("user_email_address", false));
		emailAddress.setToolTipText(getTextForWidget("email_address_tooltip"));
		// it is possible that at the launch the email will be empty (but only on the initial run)
		if (btnSendReportToUserEmail.getSelection() && emailAddress.getText().isEmpty()) {
			emailAddress.setBackground(INVALID_FORM_FIELD_COLOR);
		}
		emailAddress.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent event) {
				// get the widget whose text was modified
				Text text = (Text) event.widget;
				// validate the email address field
				if (!EmailValidator.getInstance().isValid(text.getText())) {
					if (btnSendReportToUserEmail.getSelection()) {
						text.setBackground(INVALID_FORM_FIELD_COLOR);
					} else if (!text.getText().isEmpty()) {
						text.setBackground(INVALID_FORM_FIELD_COLOR);
						// if the email checkbox is not selected we allow empty field
					} else {
						text.setBackground(VALID_FORM_FIELD_COLOR);
					}
				} else {
					text.setBackground(VALID_FORM_FIELD_COLOR);
				}
			}
		});

		Label lblHelpEmailAddress = new Label(grpSendAndSaveReportOptions, SWT.WRAP | SWT.CENTER);
		lblHelpEmailAddress.setBounds(362, 28, 24, 24);
		lblHelpEmailAddress.setImage(helpImage);
		lblHelpEmailAddress.setToolTipText(getTextForWidget("lbl_help_email_address_tooltip"));
		lblHelpEmailAddress.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpEmailAddress.setAlignment(SWT.CENTER);
		
		btnSaveReportInFolder = new Button(grpSendAndSaveReportOptions, SWT.CHECK);
		btnSaveReportInFolder.setBounds(0, 60, 352, 20);
		btnSaveReportInFolder.setText(getTextForWidget("btn_save_report_in_folder"));
		btnSaveReportInFolder.setToolTipText(getTextForWidget("btn_save_report_in_folder_tooltip"));
		btnSaveReportInFolder.setSelection(Boolean.valueOf(getConfigOption("save_report_in_folder", false)));

		Label lblHelpSaveReportInFolder = new Label(grpSendAndSaveReportOptions, SWT.WRAP | SWT.CENTER);
		lblHelpSaveReportInFolder.setBounds(362, 58, 24, 24);
		lblHelpSaveReportInFolder.setImage(helpImage);
		lblHelpSaveReportInFolder.setToolTipText(getTextForWidget("lbl_help_save_report_in_folder_tooltip"));
		lblHelpSaveReportInFolder.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpSaveReportInFolder.setAlignment(SWT.CENTER);

		Label lblSelectedFolder = new Label(grpSendAndSaveReportOptions, SWT.NONE);
		lblSelectedFolder.setBounds(0, 92, 151, 16);
		lblSelectedFolder.setText(getTextForWidget("lbl_selected_folder"));

		folderPathSelected = new Label(grpSendAndSaveReportOptions, SWT.WRAP | SWT.BORDER);
		folderPathSelected.setBounds(0, 116, 300, 33);
		folderPathSelected.setText(getConfigOption("user_report_folder", false));
		folderPathSelected.setToolTipText(getConfigOption("user_report_folder", false));
		// adjust the length of the text displayed inside the label (if it's too long to fit)
		adjustFolderPathTextLength(folderPathSelected);

		Button btnChangeFolder = new Button(grpSendAndSaveReportOptions, SWT.NONE);
		btnChangeFolder.setBounds(324, 103, 86, 24);
		btnChangeFolder.setText(getTextForWidget("btn_change_folder"));
		btnChangeFolder.setToolTipText(getTextForWidget("btn_change_folder_tooltip"));
		btnChangeFolder.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dirDialog = new DirectoryDialog(shell);
				dirDialog.setFilterPath(folderPathSelected.getText());
				dirDialog.setText(getTextForWidget("btn_change_folder_dialog_title"));
				dirDialog.setMessage(getTextForWidget("btn_change_folder_dialog_msg"));

				String folderPath = dirDialog.open();
				if (folderPath != null && !folderPath.isEmpty()) {
					folderPathSelected.setText(folderPath);
				}
			}
		});
		
		// composite to hold the options related to report creation
		Composite grpCreateTheReport = new Composite(compositeTab3, SWT.NONE);
		grpCreateTheReport.setBounds(24, 200, 412, 72);
		
		Label lblCreateTheReport = new Label(grpCreateTheReport, SWT.NONE);
		lblCreateTheReport.setBounds(0, 2, 200, 18);
		lblCreateTheReport.setText("Create the report based on:");
		
		Label lblHelpCreateReport = new Label(grpCreateTheReport, SWT.WRAP | SWT.CENTER);
		lblHelpCreateReport.setBounds(362, 0, 24, 24);
		lblHelpCreateReport.setImage(helpImage);
		lblHelpCreateReport.setToolTipText(getTextForWidget("lbl_help_create_report_tooltip"));
		lblHelpCreateReport.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpCreateReport.setAlignment(SWT.CENTER);
		
		btnNumberOfDays = new Button(grpCreateTheReport, SWT.RADIO);
		btnNumberOfDays.setBounds(0, 24, 130, 20);
		btnNumberOfDays.setText("Number of days");
		btnNumberOfDays.setToolTipText("Select this option to create the report based on chosen number of days");
		btnNumberOfDays.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedReportRecordsType = ChartRecordsChoice.DAYS;
			}
		});
		if (getConfigOption("selected_records_type", false).equals("days")) {
			btnNumberOfDays.setSelection(true);
			selectedReportRecordsType = ChartRecordsChoice.DAYS;
		}
		
		btnNumberOfRecords = new Button(grpCreateTheReport, SWT.RADIO);
		btnNumberOfRecords.setBounds(150, 24, 130, 20);
		btnNumberOfRecords.setText("Number of records");
		btnNumberOfRecords.setToolTipText("Select this option to create the report based on chosen number of records");
		btnNumberOfRecords.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedReportRecordsType = ChartRecordsChoice.ENTRIES;
			}
		});
		if (getConfigOption("selected_records_type", false).equals("entries")) {
			btnNumberOfRecords.setSelection(true);
			selectedReportRecordsType = ChartRecordsChoice.ENTRIES;
		}
		
		Label lblSelectLastRecords = new Label(grpCreateTheReport, SWT.NONE);
		lblSelectLastRecords.setText("Select last");
		lblSelectLastRecords.setBounds(0, 52, 92, 16);

		selectedRecordsNumber = new Text(grpCreateTheReport, SWT.BORDER);
		selectedRecordsNumber.setBounds(100, 49, 30, 22);
		selectedRecordsNumber.setTextLimit(3);
		selectedRecordsNumber.setText(getConfigOption("selected_records_number", false));
		selectedRecordsNumber.setToolTipText("Enter the number of days or records depending on the above choice");
		selectedRecordsNumber.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent e) {
				e.doit = Character.isDigit(e.character) || e.keyCode == SWT.ARROW_LEFT
						|| e.keyCode == SWT.NONE || e.keyCode == SWT.ARROW_RIGHT || e.keyCode == SWT.BS
						|| e.keyCode == SWT.DEL;
			}
		});
		selectedRecordsNumber.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent event) {
				// get the widget whose text was modified
				Text text = (Text) event.widget;
				
				try {
					if (Integer.parseInt(text.getText()) < Utils.REPORT_RECORDS_COUNT_MIN_RANGE
							|| Integer.parseInt(text.getText()) > Utils.REPORT_RECORDS_COUNT_MAX_RANGE) {
						text.setBackground(INVALID_FORM_FIELD_COLOR);
					} else {
						text.setBackground(VALID_FORM_FIELD_COLOR);
					}
				} catch (NumberFormatException e) {
					text.setBackground(INVALID_FORM_FIELD_COLOR);
				}
			}
		});
		
		Label lblDaysOrRecords = new Label(grpCreateTheReport, SWT.NONE);
		lblDaysOrRecords.setText("days/records");
		lblDaysOrRecords.setBounds(136, 52, 75, 16);
		
		Composite grpWholeAccountChart = new Composite(compositeTab3, SWT.NONE);
		grpWholeAccountChart.setBounds(24, 287, 412, 96);

		Label lblCreateWholeAccountChart = new Label(grpWholeAccountChart, SWT.NONE);
		lblCreateWholeAccountChart.setBounds(0, 2, 345, 18);
		lblCreateWholeAccountChart.setText(getTextForWidget("lbl_create_whole_account_chart"));
		
		btnCreateWholeAccountChartSingle = new Button(grpWholeAccountChart, SWT.RADIO);
		btnCreateWholeAccountChartSingle.setBounds(0, 24, 320, 20);
		btnCreateWholeAccountChartSingle.setText(getTextForWidget("btn_create_whole_account_chart_single"));
		btnCreateWholeAccountChartSingle.setToolTipText(getTextForWidget("btn_create_whole_account_chart_single_tooltip"));
		btnCreateWholeAccountChartSingle.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedWholeAccountChartType = ChartWholeAccountChoice.YES;
			}
		});
		if (getConfigOption("chart_generate_whole_account", false).equals("yes")) {
			btnCreateWholeAccountChartSingle.setSelection(true);
			selectedWholeAccountChartType = ChartWholeAccountChoice.YES;
		}
		
		btnCreateWholeAccountChartPolicies = new Button(grpWholeAccountChart, SWT.RADIO);
		btnCreateWholeAccountChartPolicies.setBounds(0, 49, 320, 20);
		btnCreateWholeAccountChartPolicies.setText(getTextForWidget("btn_create_whole_account_chart_policies"));
		btnCreateWholeAccountChartPolicies.setToolTipText(getTextForWidget("btn_create_whole_account_chart_policies_tooltip"));
		btnCreateWholeAccountChartPolicies.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedWholeAccountChartType = ChartWholeAccountChoice.POLICIES;
			}
		});
		if (getConfigOption("chart_generate_whole_account", false).equals("policies")) {
			btnCreateWholeAccountChartPolicies.setSelection(true);
			selectedWholeAccountChartType = ChartWholeAccountChoice.POLICIES;
		}

		btnCreateWholeAccountChartDisabled = new Button(grpWholeAccountChart, SWT.RADIO);
		btnCreateWholeAccountChartDisabled.setBounds(0, 74, 320, 20);
		btnCreateWholeAccountChartDisabled.setText(getTextForWidget("btn_create_whole_account_chart_disabled"));
		btnCreateWholeAccountChartDisabled.setToolTipText(getTextForWidget("btn_create_whole_account_chart_disabled_tooltip"));
		btnCreateWholeAccountChartDisabled.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedWholeAccountChartType = ChartWholeAccountChoice.NO;
			}
		});
		if (getConfigOption("chart_generate_whole_account", false).equals("no")) {
			btnCreateWholeAccountChartDisabled.setSelection(true);
			selectedWholeAccountChartType = ChartWholeAccountChoice.NO;
		}

		Label lblHelpCreateWholeAccountChart = new Label(grpWholeAccountChart, SWT.WRAP | SWT.CENTER);
		lblHelpCreateWholeAccountChart.setBounds(362, 0, 24, 24);
		lblHelpCreateWholeAccountChart.setImage(helpImage);
		lblHelpCreateWholeAccountChart.setToolTipText(getTextForWidget("lbl_help_create_whole_account_chart_tooltip"));
		lblHelpCreateWholeAccountChart.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpCreateWholeAccountChart.setAlignment(SWT.CENTER);
		
		Composite grpPoliciesAndSubaccountsChart = new Composite(compositeTab3, SWT.NONE);
		grpPoliciesAndSubaccountsChart.setBounds(475, 30, 430, 205);
		
		Label lblCreatePoliciesAndSubaccountsChart = new Label(grpPoliciesAndSubaccountsChart, SWT.NONE);
		lblCreatePoliciesAndSubaccountsChart.setBounds(0, 2, 370, 18);
		lblCreatePoliciesAndSubaccountsChart.setText(getTextForWidget("lbl_create_policies_and_subaccounts_chart"));
		
		btnCreatePoliciesChart = new Button(grpPoliciesAndSubaccountsChart, SWT.CHECK);
		btnCreatePoliciesChart.setBounds(0, 24, 340, 20);
		btnCreatePoliciesChart.setText(getTextForWidget("btn_create_policies_chart"));
		btnCreatePoliciesChart.setToolTipText(getTextForWidget("btn_create_policies_chart_tooltip"));
		btnCreatePoliciesChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_policies", false)));
		btnCreatePoliciesChart.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (btnCreatePoliciesChart.getSelection()) {
					btnCreatePoliciesChartJoined.setEnabled(true);
					btnCreatePoliciesChartSeparated.setEnabled(true);
					btnCreatePoliciesChartSubaccounts.setEnabled(true);
				} else {
					btnCreatePoliciesChartJoined.setEnabled(false);
					btnCreatePoliciesChartSeparated.setEnabled(false);
					btnCreatePoliciesChartSubaccounts.setEnabled(false);
				}
			}
		});
		
		btnCreatePoliciesChartJoined = new Button(grpPoliciesAndSubaccountsChart, SWT.RADIO);
		btnCreatePoliciesChartJoined.setBounds(8, 48, 350, 20);
		btnCreatePoliciesChartJoined.setText(getTextForWidget("btn_create_policies_chart_joined"));
		btnCreatePoliciesChartJoined.setToolTipText(getTextForWidget("btn_create_policies_chart_joined_tooltip"));
		btnCreatePoliciesChartJoined.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedPoliciesChartType = ChartPoliciesTypeChoice.JOINED;
			}
		});
		if (getConfigOption("chart_generate_policies_type", false).equals("joined")) {
			btnCreatePoliciesChartJoined.setSelection(true);
			selectedPoliciesChartType = ChartPoliciesTypeChoice.JOINED;
		}
		
		btnCreatePoliciesChartSeparated = new Button(grpPoliciesAndSubaccountsChart, SWT.RADIO);
		btnCreatePoliciesChartSeparated.setBounds(8, 73, 350, 20);
		btnCreatePoliciesChartSeparated.setText(getTextForWidget("btn_create_policies_chart_separated"));
		btnCreatePoliciesChartSeparated.setToolTipText(getTextForWidget("btn_create_policies_chart_separated_tooltip"));
		btnCreatePoliciesChartSeparated.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedPoliciesChartType = ChartPoliciesTypeChoice.SEPARATED;
			}
		});
		if (getConfigOption("chart_generate_policies_type", false).equals("separated")) {
			btnCreatePoliciesChartSeparated.setSelection(true);
			selectedPoliciesChartType = ChartPoliciesTypeChoice.SEPARATED;
		}

		btnCreatePoliciesChartSubaccounts = new Button(grpPoliciesAndSubaccountsChart, SWT.RADIO);
		btnCreatePoliciesChartSubaccounts.setBounds(8, 98, 350, 20);
		btnCreatePoliciesChartSubaccounts.setText(getTextForWidget("btn_create_policies_chart_subaccounts"));
		btnCreatePoliciesChartSubaccounts.setToolTipText(getTextForWidget("btn_create_policies_chart_subaccounts_tooltip"));
		btnCreatePoliciesChartSubaccounts.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedPoliciesChartType = ChartPoliciesTypeChoice.SUBACCOUNTS;
				
				if (((Button) e.widget).getSelection()) {
					btnCreateOnlySubAccountsChart.setSelection(false);
				}
			}
		});
		if (getConfigOption("chart_generate_policies_type", false).equals("subaccounts")) {
			btnCreatePoliciesChartSubaccounts.setSelection(true);
			selectedPoliciesChartType = ChartPoliciesTypeChoice.SUBACCOUNTS;
		}
		
		// after creating the buttons for policies chart options, enable or disable them
		// (depending on configuration of chart_generate_policies option (btnCreatePoliciesChart button)
		if (btnCreatePoliciesChart.getSelection()) {
			btnCreatePoliciesChartJoined.setEnabled(true);
			btnCreatePoliciesChartSeparated.setEnabled(true);
			btnCreatePoliciesChartSubaccounts.setEnabled(true);
		} else {
			btnCreatePoliciesChartJoined.setEnabled(false);
			btnCreatePoliciesChartSeparated.setEnabled(false);
			btnCreatePoliciesChartSubaccounts.setEnabled(false);
		}
		
		Label lblHelpCreatePoliciesChart = new Label(grpPoliciesAndSubaccountsChart, SWT.WRAP | SWT.CENTER);
		lblHelpCreatePoliciesChart.setBounds(398, 0, 24, 24);
		lblHelpCreatePoliciesChart.setImage(helpImage);
		lblHelpCreatePoliciesChart.setToolTipText(getTextForWidget("lbl_help_create_policies_chart_tooltip"));
		lblHelpCreatePoliciesChart.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpCreatePoliciesChart.setAlignment(SWT.CENTER);
		
		btnCreateOnlySubAccountsChart = new Button(grpPoliciesAndSubaccountsChart, SWT.CHECK);
		btnCreateOnlySubAccountsChart.setBounds(0, 129, 365, 20);
		btnCreateOnlySubAccountsChart.setText(getTextForWidget("btn_create_only_subaccounts_chart"));
		btnCreateOnlySubAccountsChart.setToolTipText(getTextForWidget("btn_create_only_subaccounts_chart_tooltip"));
		btnCreateOnlySubAccountsChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_subaccounts_without_policies", false)));
		btnCreateOnlySubAccountsChart.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (btnCreatePoliciesChartSubaccounts.getSelection()) {
					btnCreatePoliciesChartJoined.setSelection(false);
					btnCreatePoliciesChartSeparated.setSelection(true);
					btnCreatePoliciesChartSubaccounts.setSelection(false);
					selectedPoliciesChartType = ChartPoliciesTypeChoice.SEPARATED;
				}
			}
		});
				
		btnCreateSubAccountsUFKChart = new Button(grpPoliciesAndSubaccountsChart, SWT.CHECK);
		btnCreateSubAccountsUFKChart.setBounds(0, 154, 365, 20);
		btnCreateSubAccountsUFKChart.setText(getTextForWidget("btn_create_subaccounts_ufk_chart"));
		btnCreateSubAccountsUFKChart.setToolTipText(getTextForWidget("btn_create_subaccounts_ufk_chart_tooltip"));
		btnCreateSubAccountsUFKChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_subaccounts_ufk", false)));
		btnCreateSubAccountsUFKChart.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (btnCreateSubAccountsUFKChart.getSelection()) {
					btnCreateSubAccountsUFKChartWithSubAccount.setEnabled(true);
				} else {
					btnCreateSubAccountsUFKChartWithSubAccount.setEnabled(false);
				}
			}
		});
		
		btnCreateSubAccountsUFKChartWithSubAccount = new Button(grpPoliciesAndSubaccountsChart, SWT.CHECK);
		btnCreateSubAccountsUFKChartWithSubAccount.setBounds(0, 179, 365, 20);
		btnCreateSubAccountsUFKChartWithSubAccount.setText(getTextForWidget("btn_create_subaccounts_ufk_chart_with_subaccount"));
		btnCreateSubAccountsUFKChartWithSubAccount.setToolTipText(getTextForWidget("btn_create_subaccounts_ufk_chart_with_subaccount_tooltip"));
		btnCreateSubAccountsUFKChartWithSubAccount.setSelection(Boolean.valueOf(getConfigOption("chart_generate_subaccounts_ufk_with_subaccounts", false)));
		// enable or disable this button depending on the settings of previous button
		// (btnCreateSubAccountsUFKChart), because these options are strictly related
		if (btnCreateSubAccountsUFKChart.getSelection()) {
			btnCreateSubAccountsUFKChartWithSubAccount.setEnabled(true);
		} else {
			btnCreateSubAccountsUFKChartWithSubAccount.setEnabled(false);
		}
		
		Label lblHelpCreateSubAccountsUFKChart = new Label(grpPoliciesAndSubaccountsChart, SWT.WRAP | SWT.CENTER);
		lblHelpCreateSubAccountsUFKChart.setBounds(398, 152, 24, 24);
		lblHelpCreateSubAccountsUFKChart.setImage(helpImage);
		lblHelpCreateSubAccountsUFKChart.setToolTipText(getTextForWidget("lbl_help_create_subaccounts_ufk_chart_tooltip"));
		lblHelpCreateSubAccountsUFKChart.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpCreateSubAccountsUFKChart.setAlignment(SWT.CENTER);
		
		Composite grpPieChartAndScaleOptions = new Composite(compositeTab3, SWT.NONE);
		grpPieChartAndScaleOptions.setBounds(475, 250, 430, 222);
		
		Label lblCreatePieCharts = new Label(grpPieChartAndScaleOptions, SWT.NONE);
		lblCreatePieCharts.setBounds(0, 2, 330, 18);
		lblCreatePieCharts.setText(getTextForWidget("lbl_create_pie_charts"));
		
		btnCreatePoliciesPieChart = new Button(grpPieChartAndScaleOptions, SWT.CHECK);
		btnCreatePoliciesPieChart.setBounds(0, 24, 340, 20);
		btnCreatePoliciesPieChart.setText(getTextForWidget("btn_create_policies_pie_chart"));
		btnCreatePoliciesPieChart.setToolTipText(getTextForWidget("btn_create_policies_pie_chart_tooltip"));
		btnCreatePoliciesPieChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_pie_chart_for_policies", false)));

		btnCreateSubAccountsPieChart = new Button(grpPieChartAndScaleOptions, SWT.CHECK);
		btnCreateSubAccountsPieChart.setBounds(0, 49, 340, 20);
		btnCreateSubAccountsPieChart.setText(getTextForWidget("btn_create_subaccounts_pie_chart"));
		btnCreateSubAccountsPieChart.setToolTipText(getTextForWidget("btn_create_subaccounts_pie_chart_tooltip"));
		btnCreateSubAccountsPieChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_pie_chart_for_subaccounts", false)));

		btnCreateSubAccountsUFKPieChart = new Button(grpPieChartAndScaleOptions, SWT.CHECK);
		btnCreateSubAccountsUFKPieChart.setBounds(0, 74, 340, 20);
		btnCreateSubAccountsUFKPieChart.setText(getTextForWidget("btn_create_subaccounts_ufk_pie_chart"));
		btnCreateSubAccountsUFKPieChart.setToolTipText(getTextForWidget("btn_create_subaccounts_ufk_pie_chart_tooltip"));
		btnCreateSubAccountsUFKPieChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_pie_chart_for_ufk", false)));
		
		btnSkipCreatingSingleItemPieChart = new Button(grpPieChartAndScaleOptions, SWT.CHECK);
		btnSkipCreatingSingleItemPieChart.setBounds(0, 99, 340, 20);
		btnSkipCreatingSingleItemPieChart.setText(getTextForWidget("btn_skip_creating_single_item_pie_chart"));
		btnSkipCreatingSingleItemPieChart.setToolTipText(getTextForWidget("btn_skip_creating_single_item_pie_chart_tooltip"));
		btnSkipCreatingSingleItemPieChart.setSelection(Boolean.valueOf(getConfigOption("chart_do_not_generate_single_item_pie_chart", false)));
		
		Label lblHelpCreatePieCharts = new Label(grpPieChartAndScaleOptions, SWT.WRAP | SWT.CENTER);
		lblHelpCreatePieCharts.setBounds(398, 0, 24, 24);
		lblHelpCreatePieCharts.setImage(helpImage);
		lblHelpCreatePieCharts.setToolTipText(getTextForWidget("lbl_help_create_pie_charts_tooltip"));
		lblHelpCreatePieCharts.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpCreatePieCharts.setAlignment(SWT.CENTER);
		
		Label lblCombinedChartsScale = new Label(grpPieChartAndScaleOptions, SWT.NONE);
		lblCombinedChartsScale.setBounds(0, 128, 290, 18);
		lblCombinedChartsScale.setText(getTextForWidget("lbl_combined_charts_scale"));
		
		btnCombinedChartsScaleCommon = new Button(grpPieChartAndScaleOptions, SWT.RADIO);
		btnCombinedChartsScaleCommon.setBounds(8, 150, 310, 20);
		btnCombinedChartsScaleCommon.setText(getTextForWidget("btn_combined_charts_scale_common"));
		btnCombinedChartsScaleCommon.setToolTipText(getTextForWidget("btn_combined_charts_scale_common_tooltip"));
		btnCombinedChartsScaleCommon.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedChartScaleType = ChartScaleTypeChoice.COMMON;
			}
		});
		if (getConfigOption("chart_combined_charts_scale_type", false).equals("common")) {
			btnCombinedChartsScaleCommon.setSelection(true);
			selectedChartScaleType = ChartScaleTypeChoice.COMMON;
		}
		
		btnCombinedChartsScaleSeparated = new Button(grpPieChartAndScaleOptions, SWT.RADIO);
		btnCombinedChartsScaleSeparated.setBounds(8, 175, 310, 20);
		btnCombinedChartsScaleSeparated.setText(getTextForWidget("btn_combined_charts_scale_separated"));
		btnCombinedChartsScaleSeparated.setToolTipText(getTextForWidget("btn_combined_charts_scale_separated_tooltip"));
		btnCombinedChartsScaleSeparated.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedChartScaleType = ChartScaleTypeChoice.SEPARATED;
			}
		});
		if (getConfigOption("chart_combined_charts_scale_type", false).equals("separated")) {
			btnCombinedChartsScaleSeparated.setSelection(true);
			selectedChartScaleType = ChartScaleTypeChoice.SEPARATED;
		}
		
		btnCombinedChartsScaleLogarithmic = new Button(grpPieChartAndScaleOptions, SWT.RADIO);
		btnCombinedChartsScaleLogarithmic.setBounds(8, 200, 310, 20);
		btnCombinedChartsScaleLogarithmic.setText(getTextForWidget("btn_combined_charts_scale_logarithmic"));
		btnCombinedChartsScaleLogarithmic.setToolTipText(getTextForWidget("btn_combined_charts_scale_logarithmic_tooltip"));
		btnCombinedChartsScaleLogarithmic.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedChartScaleType = ChartScaleTypeChoice.LOGARITHMIC;
			}
		});
		if (getConfigOption("chart_combined_charts_scale_type", false).equals("logarithmic")) {
			btnCombinedChartsScaleLogarithmic.setSelection(true);
			selectedChartScaleType = ChartScaleTypeChoice.LOGARITHMIC;
		}
		
		Label lblHelpCombinedChartsScale = new Label(grpPieChartAndScaleOptions, SWT.WRAP | SWT.CENTER);
		lblHelpCombinedChartsScale.setBounds(398, 125, 24, 24);
		lblHelpCombinedChartsScale.setImage(helpImage);
		lblHelpCombinedChartsScale.setToolTipText(getTextForWidget("lbl_help_combined_charts_scale_tooltip"));
		lblHelpCombinedChartsScale.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblHelpCombinedChartsScale.setAlignment(SWT.CENTER);	
	}

	private void adjustFolderPathTextLength(Label folderPath) {
		// get the original length of the folder path string (to be displayed in the label)
		int origTextLength = folderPath.getText().length();
		// get the max width (in pixels) of the label displaying folder path string
		int folderLabelMaxWidth = folderPath.getBounds().width * 2;
		// use SWT GC class to get the total width (in pixels) of displayed text
		GC gc = new GC(folderPath);
		int textPixelWidth = gc.stringExtent(folderPath.getText()).x;
		
		// count the ratio of label width/capacity to the text displayed to check
		// if it will fit in the label or not
		double labelTextRatio = (double) folderLabelMaxWidth / textPixelWidth;
		// if the counted ratio is less than 1, it means that the folder path text
		// will not fit inside the label and it must be shortened
		if (labelTextRatio < 1) {
			// new length of the text inside the label
			int newTextLength = (int) Math.floor(labelTextRatio * origTextLength) - 1;
			// add "..." at the end to mark that the text displayed does not fit
			// (it is fully visible in the label tooltip)
			String trimText = folderPath.getText().substring(0, newTextLength - 3) + "...";
			folderPath.setText(trimText);
		}
		
		// dispose created object
		gc.dispose();
	}

	private String getConfigOption(String optionName, boolean getDefaultValue) {
		String optionValue = null;

		try {
			String optionKey;
			if (getDefaultValue) {
				optionKey = "default_" + optionName;
			} else {
				optionKey = optionName;
			}
			optionValue = ConfigurationProvider.getPropertyValue(optionKey);

			if (optionName.equals("user_language")) {
				if (optionValue.equals("pl")) {
					optionValue = "Polski";
				} else {
					optionValue = "English";
				}
			} else if (optionName.equals("browser_selected")) {
				if (optionValue.equals("firefox")) {
					optionValue = "Mozilla Firefox";
				} else if (optionValue.equals("chrome")) {
					optionValue = "Google Chrome";
				} else if (optionValue.equals("iexplorer")) {
					optionValue = "Internet Explorer";
				} else {
					optionValue = "Phantom JS";
				}
			} else if (optionName.equals("user_report_folder")) {
				if (optionValue.equals("%USERHOME%")) {
					optionValue = System.getenv("HOMEDRIVE") + System.getenv("HOMEPATH");
				}
				
				optionValue = optionValue.replace("\\\\", "\\");
			}
		} catch (Exception e) {
			log.error("An exception occured while trying to get the value of the option = <" + optionName + ">! "
					+ e.getMessage());
			try {
				log.error(Utils.getExceptionStackTrace(e));
			} catch (Exception e1) {
				// do nothing here
			}
		}

		return optionValue;
	}

	private String getTextForWidget(String widgetName) {
		String widgetText = null;

		try {
			widgetText = ConfigurationProvider.getMessageByLanguage("gui_" + widgetName, userLanguage);
		} catch (Exception e) {
			log.error("An exception occured while trying to get the translation for widget = <" + widgetName
					+ ">! " + e.getMessage());
			try {
				log.error(Utils.getExceptionStackTrace(e));
			} catch (Exception e1) {
				// do nothing here
			}
		}

		return widgetText;
	}

	private void saveConfigurationChanges() throws Exception {
		boolean isSchedulerModified = false;

		ConfigurationProvider.setPropertyValue("website_url", urlAddress.getText());
		ConfigurationProvider.setPropertyValue("user_login", userLogin.getText());
		ConfigurationProvider.setPropertyValue("user_password", userPassword.getText());

		if (languageSelected.getSelectionIndex() == 0) {
			ConfigurationProvider.setPropertyValue("user_language", "en");
		} else if (languageSelected.getSelectionIndex() == 1) {
			ConfigurationProvider.setPropertyValue("user_language", "pl");
		}

		// check if the language interface setting have changed (if so, the window must be reloaded)
		if (!ConfigurationProvider.getPropertyValue("user_language").equals(userLanguage)) {
			isLanguageChanged = true;
			// reload language configuration (the version, labels will be switched after clicking OK button)
			userLanguage = ConfigurationProvider.getPropertyValue("user_language");
		}
				
		if (isBrowserVisible) {
			switch (browserSelected.getSelectionIndex()) {
			case 0:
			default:
				ConfigurationProvider.setPropertyValue("browser_selected", "firefox");
				break;
			case 1:
				ConfigurationProvider.setPropertyValue("browser_selected", "chrome");
				break;
			case 2:
				ConfigurationProvider.setPropertyValue("browser_selected", "iexplorer");
				break;
			}
		} else {
			ConfigurationProvider.setPropertyValue("browser_selected", "phantomjs");
		}

		String retriesChoice = retriesNumber.getItem(retriesNumber.getSelectionIndex());
		ConfigurationProvider.setPropertyValue("download_retries_number", retriesChoice);
		ConfigurationProvider.setPropertyValue("download_retries_minutes_delay",
				retriesTimeInterval.getText());

		String dateTimeChoice = new String(String.format("%02d", collectingDateTime.getHours()) + ":"
				+ String.format("%02d", collectingDateTime.getMinutes()));
		String frequencyChoice = collectingDaysFrequency.getItem(collectingDaysFrequency.getSelectionIndex());

		// check if the settings for collecting data scheduler have changed
		if (!ConfigurationProvider.getPropertyValue("collecting_date_time").equals(dateTimeChoice)
				|| !ConfigurationProvider.getPropertyValue("collecting_days_frequency").equals(
						frequencyChoice)
				|| !ConfigurationProvider.getPropertyValue("collecting_allow_wake_computer_to_run").equals(
						Boolean.toString(btnAllowWakeToRun.getSelection()))) {
			
			isSchedulerModified = true;
		}
		
		// save the new parameters' values for application scheduler
		ConfigurationProvider.setPropertyValue("collecting_date_time", dateTimeChoice);
		ConfigurationProvider.setPropertyValue("collecting_days_frequency", frequencyChoice);
		if (btnAllowWakeToRun.getSelection()) {
			ConfigurationProvider.setPropertyValue("collecting_allow_wake_computer_to_run", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("collecting_allow_wake_computer_to_run", OPT_FALSE_VALUE);
		}
		if (btnAllowSleepAfterRun.getSelection()) {
			ConfigurationProvider.setPropertyValue("collecting_allow_sleep_computer_after_run", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("collecting_allow_sleep_computer_after_run", OPT_FALSE_VALUE);
		}
		
		if (btnSendReportToUserEmail.getSelection()) {
			ConfigurationProvider.setPropertyValue("send_report_and_notification_email", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("send_report_and_notification_email", OPT_FALSE_VALUE);
		}
		ConfigurationProvider.setPropertyValue("user_email_address", emailAddress.getText().trim());

		if (btnSaveReportInFolder.getSelection()) {
			ConfigurationProvider.setPropertyValue("save_report_in_folder", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("save_report_in_folder", OPT_FALSE_VALUE);
		}
		ConfigurationProvider.setPropertyValue("user_report_folder", folderPathSelected.getText());

		if (selectedReportRecordsType == ChartRecordsChoice.DAYS) {
			ConfigurationProvider.setPropertyValue("selected_records_type", "days");
		} else {
			ConfigurationProvider.setPropertyValue("selected_records_type", "entries");
		}
		ConfigurationProvider.setPropertyValue("selected_records_number", selectedRecordsNumber.getText());
		
		if (selectedWholeAccountChartType == ChartWholeAccountChoice.YES) {
			ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "yes");
		} else if (selectedWholeAccountChartType == ChartWholeAccountChoice.POLICIES) {
			ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "policies");
		} else if (selectedWholeAccountChartType == ChartWholeAccountChoice.NO) {
			ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "no");
		}
		
		if (btnCreatePoliciesChart.getSelection()) {
			ConfigurationProvider.setPropertyValue("chart_generate_policies", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("chart_generate_policies", OPT_FALSE_VALUE);
		}
		if (selectedPoliciesChartType == ChartPoliciesTypeChoice.JOINED) {
			ConfigurationProvider.setPropertyValue("chart_generate_policies_type", "joined");
		} else if (selectedPoliciesChartType == ChartPoliciesTypeChoice.SEPARATED) {
			ConfigurationProvider.setPropertyValue("chart_generate_policies_type", "separated");
		} else if (selectedPoliciesChartType == ChartPoliciesTypeChoice.SUBACCOUNTS) {
			ConfigurationProvider.setPropertyValue("chart_generate_policies_type", "subaccounts");
		}
		
		if (btnCreateOnlySubAccountsChart.getSelection()) {
			ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_without_policies", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_without_policies", OPT_FALSE_VALUE);
		}
		if (btnCreateSubAccountsUFKChart.getSelection()) {
			ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk", OPT_FALSE_VALUE);
		}
		if (btnCreateSubAccountsUFKChartWithSubAccount.getSelection()) {
			ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk_with_subaccounts", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk_with_subaccounts", OPT_FALSE_VALUE);
		}
		
		if (btnCreatePoliciesPieChart.getSelection()) {
			ConfigurationProvider.setPropertyValue("chart_generate_pie_chart_for_policies", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("chart_generate_pie_chart_for_policies", OPT_FALSE_VALUE);
		}
		if (btnCreateSubAccountsPieChart.getSelection()) {
			ConfigurationProvider.setPropertyValue("chart_generate_pie_chart_for_subaccounts", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("chart_generate_pie_chart_for_subaccounts", OPT_FALSE_VALUE);
		}
		if (btnCreateSubAccountsUFKPieChart.getSelection()) {
			ConfigurationProvider.setPropertyValue("chart_generate_pie_chart_for_ufk", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("chart_generate_pie_chart_for_ufk", OPT_FALSE_VALUE);
		}
		if (btnSkipCreatingSingleItemPieChart.getSelection()) {
			ConfigurationProvider.setPropertyValue("chart_do_not_generate_single_item_pie_chart", OPT_TRUE_VALUE);
		} else {
			ConfigurationProvider.setPropertyValue("chart_do_not_generate_single_item_pie_chart", OPT_FALSE_VALUE);
		}
		
		if (selectedChartScaleType == ChartScaleTypeChoice.COMMON) {
			ConfigurationProvider.setPropertyValue("chart_combined_charts_scale_type", "common");
		} else if (selectedChartScaleType == ChartScaleTypeChoice.SEPARATED) {
			ConfigurationProvider.setPropertyValue("chart_combined_charts_scale_type", "separated");
		} else if (selectedChartScaleType == ChartScaleTypeChoice.LOGARITHMIC) {
			ConfigurationProvider.setPropertyValue("chart_combined_charts_scale_type", "logarithmic");
		}
		
		// TODO: add remaining fields

		try {
			ConfigurationProvider.saveConfigurationFile();
			log.debug("User has modified configuration file through GUI configuration.");
			if (isSchedulerModified) {
				ConfigurationProvider.manageApplicationTaskScheduler();
				log.debug("User has modified application task scheduler settings through GUI configuration.");
				String schedulerNextRun = ConfigurationProvider.getApplicationTaskSchedulerNextRun();

				// update label with the information regarding task scheduler as it was changed
				if (schedulerNextRun == null || schedulerNextRun.isEmpty()) {
					lblCollectingSchedulerInfo
							.setText(getTextForWidget("lbl_collecting_scheduler_info_no_run"));
				} else {
					lblCollectingSchedulerInfo
							.setText(getTextForWidget("lbl_collecting_scheduler_info_next_run")
									+ schedulerNextRun + ".");
				}
			} else {
				log.debug("Application task scheduler settings have not changed.");
			}
		} catch (Exception e) {
			log.error("An exception occured while trying to save configuration changes! " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}

	private void restoreDefaultConfiguration() throws Exception {
		// We do not set the default login and password here - the default values are empty, but only on the
		// first run and must be then specified by the user - pressing "Restore the default settings" will not
		// change the values for the user. It would be very inconvenient if restoring the default settings
		// would remove the proper login and password.
		urlAddress.setText(getConfigOption("website_url", true));

		int languageIndex = Arrays.asList(languageSelected.getItems()).indexOf(
				getConfigOption("user_language", true));
		languageSelected.select(languageIndex);

		int browserIndex = Arrays.asList(browserSelected.getItems()).indexOf(
				getConfigOption("browser_selected", true));		
		browserSelected.select(browserIndex);
		if (browserIndex >= 0) {
			// default browser is visible
			btnRadioShowBrowser.setSelection(true);
			btnRadioHideBrowser.setSelection(false);
			isBrowserVisible = true;
		} else {
			// default browser is invisible
			btnRadioHideBrowser.setSelection(true);
			btnRadioShowBrowser.setSelection(false);
			isBrowserVisible = false;
		}

		int retriesIndex = Arrays.asList(retriesNumber.getItems()).indexOf(
				getConfigOption("download_retries_number", true));
		retriesNumber.select(retriesIndex);
		retriesTimeInterval.setText(getConfigOption("download_retries_minutes_delay", true));

		int userHours = Integer.parseInt(getConfigOption("collecting_date_time", true).substring(0, 2));
		int userMinutes = Integer.parseInt(getConfigOption("collecting_date_time", true).substring(3, 5));
		collectingDateTime.setTime(userHours, userMinutes, 0);

		int collectingIndex = Arrays.asList(collectingDaysFrequency.getItems()).indexOf(
				getConfigOption("collecting_days_frequency", true));
		collectingDaysFrequency.select(collectingIndex);
		btnAllowWakeToRun.setSelection(Boolean.valueOf(getConfigOption("collecting_allow_wake_computer_to_run", true)));
		btnAllowSleepAfterRun.setSelection(Boolean.valueOf(getConfigOption("collecting_allow_sleep_computer_after_run", true)));
		
		btnSendReportToUserEmail.setSelection(Boolean.valueOf(getConfigOption("send_report_and_notification_email", true)));
		emailAddress.setText(getConfigOption("user_email_address", true));
		btnSaveReportInFolder.setSelection(Boolean.valueOf(getConfigOption("save_report_in_folder", true)));
		folderPathSelected.setText(getConfigOption("user_report_folder", true));		
		if (getConfigOption("selected_records_type", true).equals("days")) {
			btnNumberOfDays.setSelection(true);
			btnNumberOfRecords.setSelection(false);
			selectedReportRecordsType = ChartRecordsChoice.DAYS;
		} else {
			btnNumberOfRecords.setSelection(true);
			btnNumberOfDays.setSelection(false);
			selectedReportRecordsType = ChartRecordsChoice.ENTRIES;
		}
		selectedRecordsNumber.setText(getConfigOption("selected_records_number", true));
	
		String chartWholeAccountOptionVal = getConfigOption("chart_generate_whole_account", true);
		if (chartWholeAccountOptionVal.equals("yes")) {
			btnCreateWholeAccountChartSingle.setSelection(true);
			btnCreateWholeAccountChartPolicies.setSelection(false);
			btnCreateWholeAccountChartDisabled.setSelection(false);
			selectedWholeAccountChartType = ChartWholeAccountChoice.YES;
		} else if (chartWholeAccountOptionVal.equals("policies")) {
			btnCreateWholeAccountChartSingle.setSelection(false);
			btnCreateWholeAccountChartPolicies.setSelection(true);
			btnCreateWholeAccountChartDisabled.setSelection(false);
			selectedWholeAccountChartType = ChartWholeAccountChoice.POLICIES;
		} else if (chartWholeAccountOptionVal.equals("no")) {
			btnCreateWholeAccountChartSingle.setSelection(false);
			btnCreateWholeAccountChartPolicies.setSelection(false);
			btnCreateWholeAccountChartDisabled.setSelection(true);
			selectedWholeAccountChartType = ChartWholeAccountChoice.NO;
		}
		
		btnCreatePoliciesChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_policies", true)));
		String chartPoliciesOptionVal = getConfigOption("chart_generate_policies_type", true);
		
		if (chartPoliciesOptionVal.equals("joined")) {
			btnCreatePoliciesChartJoined.setSelection(true);
			btnCreatePoliciesChartSeparated.setSelection(false);
			btnCreatePoliciesChartSubaccounts.setSelection(false);
			selectedPoliciesChartType = ChartPoliciesTypeChoice.JOINED;
		} else if (chartPoliciesOptionVal.equals("separated")) {
			btnCreatePoliciesChartJoined.setSelection(false);
			btnCreatePoliciesChartSeparated.setSelection(true);
			btnCreatePoliciesChartSubaccounts.setSelection(false);
			selectedPoliciesChartType = ChartPoliciesTypeChoice.SEPARATED;
		} else if (chartPoliciesOptionVal.equals("subaccounts")) {
			btnCreatePoliciesChartJoined.setSelection(false);
			btnCreatePoliciesChartSeparated.setSelection(false);
			btnCreatePoliciesChartSubaccounts.setSelection(true);
			selectedPoliciesChartType = ChartPoliciesTypeChoice.SUBACCOUNTS;
		}
		
		// after restoring the settings of the buttons for policies chart options, enable or disable them
		// (depending on configuration of chart_generate_policies option (btnCreatePoliciesChart button) -
		// same action performed during buttons creation
		if (btnCreatePoliciesChart.getSelection()) {
			btnCreatePoliciesChartJoined.setEnabled(true);
			btnCreatePoliciesChartSeparated.setEnabled(true);
			btnCreatePoliciesChartSubaccounts.setEnabled(true);
		} else {
			btnCreatePoliciesChartJoined.setEnabled(false);
			btnCreatePoliciesChartSeparated.setEnabled(false);
			btnCreatePoliciesChartSubaccounts.setEnabled(false);
		}
		
		btnCreateOnlySubAccountsChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_subaccounts_without_policies", true)));
		btnCreateSubAccountsUFKChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_subaccounts_ufk", true)));
		btnCreateSubAccountsUFKChartWithSubAccount.setSelection(Boolean.valueOf(getConfigOption("chart_generate_subaccounts_ufk_with_subaccounts", true)));
		
		// after restoring the settings of the buttons for UFK chart generation, enable or disable the second
		// one (as it depends on toggling btnCreateSubAccountsUFKChart) - just like when creating them
		if (btnCreateSubAccountsUFKChart.getSelection()) {
			btnCreateSubAccountsUFKChartWithSubAccount.setEnabled(true);
		} else {
			btnCreateSubAccountsUFKChartWithSubAccount.setEnabled(false);
		}
		
		btnCreatePoliciesPieChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_pie_chart_for_policies", true)));
		btnCreateSubAccountsPieChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_pie_chart_for_subaccounts", true)));
		btnCreateSubAccountsUFKPieChart.setSelection(Boolean.valueOf(getConfigOption("chart_generate_pie_chart_for_ufk", true)));
		btnSkipCreatingSingleItemPieChart.setSelection(Boolean.valueOf(getConfigOption("chart_do_not_generate_single_item_pie_chart", true)));
		
		String combinedChartsScaleOptionVal = getConfigOption("chart_combined_charts_scale_type", true);
		if (combinedChartsScaleOptionVal.equals("common")) {
			btnCombinedChartsScaleCommon.setSelection(true);
			btnCombinedChartsScaleSeparated.setSelection(false);
			btnCombinedChartsScaleLogarithmic.setSelection(false);
			selectedChartScaleType = ChartScaleTypeChoice.COMMON;
		} else if (combinedChartsScaleOptionVal.equals("separated")) {
			btnCombinedChartsScaleCommon.setSelection(false);
			btnCombinedChartsScaleSeparated.setSelection(true);
			btnCombinedChartsScaleLogarithmic.setSelection(false);
			selectedChartScaleType = ChartScaleTypeChoice.SEPARATED;
		} else if (combinedChartsScaleOptionVal.equals("logarithmic")) {
			btnCombinedChartsScaleCommon.setSelection(false);
			btnCombinedChartsScaleSeparated.setSelection(false);
			btnCombinedChartsScaleLogarithmic.setSelection(true);
			selectedChartScaleType = ChartScaleTypeChoice.LOGARITHMIC;
		}
		// TODO: add remaining fields

		log.debug("User has chosen to restore default settings in GUI application.");
		saveConfigurationChanges();
	}

	private boolean validateApplicationForm() {
		if (urlAddress.getText().isEmpty() || !(new UrlValidator().isValid(urlAddress.getText()))) {
			return false;
		}

		if (userLogin.getText().isEmpty() || userPassword.getText().isEmpty()) {
			return false;
		}

		try {
			String retriesTimeIntervalVal = retriesTimeInterval.getText();
			if (Integer.parseInt(retriesTimeIntervalVal) < Utils.DOWNLOAD_RETRIES_TIME_INTERVAL_MIN_RANGE
					|| Integer.parseInt(retriesTimeIntervalVal) > Utils.DOWNLOAD_RETRIES_TIME_INTERVAL_MAX_RANGE) {
				return false;
			}
		} catch (NumberFormatException e) {
			return false;
		}

		if (!EmailValidator.getInstance().isValid(emailAddress.getText())) {
			if (btnSendReportToUserEmail.getSelection()) {
				return false;
			} else if (!emailAddress.getText().isEmpty()) {
				return false;
			}
		}

		try {
			String selectedRecordsNumberVal = selectedRecordsNumber.getText();
			if (Integer.parseInt(selectedRecordsNumberVal) < Utils.REPORT_RECORDS_COUNT_MIN_RANGE
					|| Integer.parseInt(selectedRecordsNumberVal) > Utils.REPORT_RECORDS_COUNT_MAX_RANGE) {
				return false;
			}
		} catch (NumberFormatException e) {
			return false;
		}
		
		return true;
	}
}
