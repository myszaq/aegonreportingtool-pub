package pl.aegon.youraccount.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;

public class YourAccountLoginPage {
	private WebDriver driver;

	@FindBy(name = "username")
	private WebElement userName;

	@FindBy(name = "password")
	private WebElement password;

	@FindBy(css = "form input.bluenButton")
	private WebElement loginButton;

	@FindBy(css = "div.aegonLogin div.errorContainer")
	private WebElement errorField;

	/**
	 * Class constructor.
	 * 
	 * @param WebDriver driver
	 */
	public YourAccountLoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Sets the user name into dedicated text box.
	 * 
	 * @param strUserName
	 */
	public void setUserName(String strUserName) {
		userName.sendKeys(strUserName);
	}

	/**
	 * Sets the password into dedicated text box.
	 * 
	 * @param strPassword
	 */
	public void setPassword(String strPassword) {
		password.sendKeys(strPassword);
	}

	/**
	 * Clicks on login button.
	 */
	public void clickLogin() {
		loginButton.click();
	}

	/**
	 * POM method used to perform login operation on Aegon Your Account web page.
	 * 
	 * @param strUserName user name
	 * @param strPasword user password
	 * @return void
	 */
	public void loginToYourAccount(String strUserName, String strPasword) {
		// fill user name
		this.setUserName(strUserName);
		// fill password
		this.setPassword(strPasword);
		// click login button
		this.clickLogin();
	}
	
	/**
	 * Gets the title of login page.
	 * 
	 * @return String
	 */
	public String getLoginPageTitle() {
		return this.driver.getTitle();
	}
	
	/**
	 * Gets the message from error field (in case of error).
	 * 
	 * @return String
	 */
	public String getErrorField() {
		return errorField.getText().replace("\n", " ");
	}
}