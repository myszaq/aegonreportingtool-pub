package pl.aegon.youraccount.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YourAccountSubAccountDetailsPage {
	private WebDriver driver;
	
	@FindBy(linkText = "Polisy na Życie")
	private WebElement policyListLink;
	
	@FindBy(linkText = "Saldo Subkonta")
	private WebElement subAccountBalanceLink;
	
	// field(s) containing value of "Ubezpieczeniowy fundusz kapitałowy"
	@FindBy(xpath = "//div[@class='box_stdC'][1]/table[@class='t_box_std twoje_polisy']//td[@class='first dark']")
	private List<WebElement> ufkNameFieldList;
	
	// field(s) containing value of "Wartość jednostki uczestnictwa (PLN)"
	@FindBy(xpath = "//div[@class='box_stdC'][1]/table[@class='t_box_std twoje_polisy']//td[@class='number light vline']")
	private List<WebElement> ufkInvestUnitValueFieldList;
	
	// field(s) containing value of "Liczba zgromadzonych jednostek uczestnictwa"
	@FindBy(xpath = "//div[@class='box_stdC'][1]/table[@class='t_box_std twoje_polisy']//td[@class='number dark vline']")
	private List<WebElement> ufkInvestUnitNumberFieldList;
	
	// field(s) containing value of "Wartość zgromadzonych jednostek uczestnictwa (PLN)"
	@FindBy(css = "table.t_box_std.twoje_polisy td#qwerty")
	private List<WebElement> ufkCollectedInvestUnitValueFieldList;
	
	/**
	 * Class constructor.
	 * 
	 * @param WebDriver driver
	 */
	public YourAccountSubAccountDetailsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Redirects to main page (with all policies/products).
	 */
	public void goToPolicyListPage() {
		policyListLink.click();
	}
	
	/**
	 * Redirects to the page with sub accounts list page (balance page).
	 */
	public void goToSubAccountBalancePage() {
		subAccountBalanceLink.click();
	}
	
	/**
	 * Gets the title of the policy data page.
	 * 
	 * @return String
	 */
	public String getSubAccountDetailsPageTitle() {
		return this.driver.getTitle();
	}

	/**
	 * Returns the link to policy list page.
	 * 
	 * @return WebElement
	 */
	public WebElement getPolicyListLink() {
		return policyListLink;
	}

	/**
	 * Returns the link to sub account balance (sub accounts list) page.
	 * 
	 * @return WebElement
	 */
	public WebElement getSubAccountBalanceLink() {
		return subAccountBalanceLink;
	}

	/**
	 * Returns the list of all UFK name visible on the current page
	 * (and assigned to the given sub account).
	 * 
	 * @return List<String>
	 */
	public List<String> getUfkNameFieldList() {
		List <String> ufkNamesList = new ArrayList<String>();
		for (WebElement item: ufkNameFieldList) {
			ufkNamesList.add(item.getText());
		}
		
		return ufkNamesList;
	}

	/**
	 * Returns the list of all UFK invest unit values visible on the current page
	 * (and assigned to the given sub account).
	 * 
	 * @return List<String>
	 */
	public List<String> getUfkInvestUnitValueFieldList() {
		List <String> ufkInvestUnitValuesList = new ArrayList<String>();
		for (WebElement item: ufkInvestUnitValueFieldList) {
			ufkInvestUnitValuesList.add(item.getText());
		}
		
		return ufkInvestUnitValuesList;
	}

	/**
	 * Returns the list of all UFK invest unit numbers visible on the current page
	 * (and assigned to the given sub account).
	 * 
	 * @return List<String>
	 */
	public List<String> getUfkInvestUnitNumberFieldList() {
		List <String> ufkInvestUnitNumbersList = new ArrayList<String>();
		for (WebElement item: ufkInvestUnitNumberFieldList) {
			ufkInvestUnitNumbersList.add(item.getText());
		}
		
		return ufkInvestUnitNumbersList;
	}

	/**
	 * Returns the list of all collected UFK invest unit values visible on the current page
	 * (and assigned to the given sub account).
	 * 
	 * @return List<String>
	 */
	public List<String> getUfkCollectedInvestUnitValueFieldList() {
		List <String> ufkCollectedInvestUnitValuesList = new ArrayList<String>();
		for (WebElement item: ufkCollectedInvestUnitValueFieldList) {
			ufkCollectedInvestUnitValuesList.add(item.getText());
		}
		
		return ufkCollectedInvestUnitValuesList;
	}
}
