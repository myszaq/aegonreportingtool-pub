package pl.aegon.youraccount.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YourAccountPolicyDataPage {
	private WebDriver driver;
	
	@FindBy(linkText = "Polisy na Życie")
	private WebElement policyListLink;
	
	@FindBy(css = "table.t_box_std tr td.dark a")
	private List<WebElement> subAccountNameFieldList;
	
	@FindBy(css = "table.t_box_std tr td.light.vline div")
	private List<WebElement> subAccountBalanceFieldList;
	
	/**
	 * Class constructor.
	 * 
	 * @param WebDriver driver
	 */
	public YourAccountPolicyDataPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Redirects to the specified sub account details page.
	 * 
	 * @param subAccountIndex index (0-based) of given account
	 * @throws IndexOutOfBoundsException when sub account with given index is invalid
	 */
	public void goToSubAccountDetailsPage(int subAccountIndex) throws IndexOutOfBoundsException {
		try {
			subAccountNameFieldList.get(subAccountIndex).click();
		} catch (IndexOutOfBoundsException e) {
			throw e;
		}
	}
	
	/**
	 * Redirects to main page (with all policies/products).
	 */
	public void goToPolicyListPage() {
		policyListLink.click();
	}
	
	/**
	 * Gets the title of the policy data page.
	 * 
	 * @return String
	 */
	public String getPolicyDataPageTitle() {
		return this.driver.getTitle();
	}

	/**
	 * Returns the link to policy list page.
	 * 
	 * @return WebElement
	 */
	public WebElement getPolicyListLink() {
		return policyListLink;
	}

	/**
	 * Returns the list of all sub account names visible on the current page.
	 * 
	 * @return List<String>
	 */
	public List<String> getSubAccountNameFieldList() {
		List <String> subAccountNamesList = new ArrayList<String>();
		for (WebElement item: subAccountNameFieldList) {
			subAccountNamesList.add(item.getText());
		}
		
		return subAccountNamesList;
	}

	/**
	 * Returns the list of all sub account balances visible on the current page.
	 *  
	 * @return List<String>
	 */
	public List<String> getSubAccountBalanceFieldList() {
		List <String> subAccountBalancesList = new ArrayList<String>();
		for (WebElement item: subAccountBalanceFieldList) {
			subAccountBalancesList.add(item.getText());
		}
		
		return subAccountBalancesList;
	}
}
