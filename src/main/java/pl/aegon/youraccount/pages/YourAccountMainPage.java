package pl.aegon.youraccount.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;

public class YourAccountMainPage {
	private WebDriver driver;
	
	@FindBy(partialLinkText = "Wyloguj")
	private WebElement logoutLink;
	
	@FindBy(css = "h2.spacetop.orange")
	private WebElement mainHeaderTitle;
	
	// field containing value of "Imię i Nazwisko"
	@FindBy(css = "div.box_RB tr:nth-child(2) td:nth-child(2)")
	private WebElement fullNameField;
	
	// field containing value of "Dane na dzień"
	@FindBy(css = "div.box_RB tr:nth-child(3) td:nth-child(2)")
	private WebElement dataOnDayField;
	
	// field containing value of "Data ostatniej aktualizacji"
	@FindBy(css = "div.box_RB tr:nth-child(4) td:nth-child(2)")
	private WebElement lastModifiedDateField;
	
	// field containing value of "Data ostatniego logowania"
	@FindBy(css = "div.box_RB tr:nth-child(5) td:nth-child(2)")
	private WebElement lastLoggedDateField;
	
	// field(s) containing value of "Nazwa Polisy"
	@FindBy(css = "tr.first.dark.r02 td.first.dark:nth-child(1) a")
	private List<WebElement> policyNameFieldList;
	
	// field(s) containing value of "Numer Polisy"
	@FindBy(css = "tr.first.dark.r02 td.first.dark:nth-child(2) a.orange_bullet_right")
	private List<WebElement> policyNumberFieldList;
	
	// field(s) containing value of "Nazwa Produktu"
	@FindBy(css = "tr.first.dark.r02 td.light.vline")
	private List<WebElement> productNameFieldList;
	
	// field(s) containing value of "Saldo"
	@FindBy(css = "tr.first.dark.r02 td.number.last.dark.vline")
	private List<WebElement> balanceFieldList;
	
	// field(s) containing value of "Razem"
	@FindBy(css = "tr.r02.higher td.number.last.dark.vline")
	private WebElement totalField;
	
	/**
	 * Class constructor.
	 * 
	 * @param WebDriver driver
	 */
	public YourAccountMainPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Logs out from the website.
	 */
	public void logoutFromYourAccount() {
		logoutLink.click();
	}
	
	/**
	 * Redirects to the page with the details of the specified policy.
	 * @param policyIndex
	 * @throws IndexOutOfBoundsException
	 */
	public void goToPolicyDetailsPage(int policyIndex) throws IndexOutOfBoundsException {
		try {
			policyNumberFieldList.get(policyIndex).click();
		} catch (IndexOutOfBoundsException e) {
			throw e;
		}
	}
	
	/**
	 * Gets the title of the main page.
	 * 
	 * @return String
	 */
	public String getMainPageTitle() {
		return this.driver.getTitle();
	}
	
	/**
	 * Returns logout link.
	 * 
	 * @return WebElement
	 */
	public WebElement getLogoutLink() {
		return logoutLink;
	}

	/**
	 * Returns the main header title
	 * @return String
	 */
	public String getMainHeaderTitle() {
		return mainHeaderTitle.getText();
	}

	/**
	 * Returns full name field value.
	 * 
	 * @return String
	 */
	public String getFullNameField() {
		return fullNameField.getText();
	}
	
	/**
	 * Returns data on day field value.
	 * 
	 * @return String
	 */
	public String getDataOnDayField() {
		return dataOnDayField.getText();
	}
	
	/**
	 * Returns last modified date field value.
	 * 
	 * @return String
	 */
	public String getLastModifiedDateField() {
		return lastModifiedDateField.getText();
	}
	
	/**
	 * Returns last logged date field value.
	 * 
	 * @return String
	 */
	public String getLastLoggedDateField() {
		return lastLoggedDateField.getText();
	}
	
	/**
	 * Returns the list of all policies' names visible on the main page.
	 * 
	 * @return List<String>
	 */
	public List<String> getPolicyNameFieldList() {
		List <String> policyNamesList = new ArrayList<String>();
		for (WebElement item: policyNameFieldList) {
			policyNamesList.add(item.getText());
		}
		
		return policyNamesList;
	}
	
	/**
	 * Returns the list of all policies' numbers visible on the main page.
	 * 
	 * @return List<String>
	 */
	public List<String> getPolicyNumberFieldList() {
		List <String> policyNumbersList = new ArrayList<String>();
		for (WebElement item: policyNumberFieldList) {
			policyNumbersList.add(item.getText());
		}
		
		return policyNumbersList;
	}
	
	/**
	 * Returns the list of all products' names visible on the main page.
	 * 
	 * @return List<String>
	 */
	public List<String> getProductNameFieldList() {
		List <String> productNamesList = new ArrayList<String>();
		for (WebElement item: productNameFieldList) {
			productNamesList.add(item.getText());
		}
		
		return productNamesList;
	}
	
	/**
	 * Returns the list of all policies' balance values visible on the main page.
	 * 
	 * @return List<String>
	 */
	public List<String> getBalanceFieldList() {
		List <String> balanceList = new ArrayList<String>();
		for (WebElement item: balanceFieldList) {
			balanceList.add(item.getText());
		}
		
		return balanceList;
	}
	
	/**
	 * Returns total balance field value.
	 * 
	 * @return String
	 */
	public String getTotalField() {
		return totalField.getText();
	}
}
