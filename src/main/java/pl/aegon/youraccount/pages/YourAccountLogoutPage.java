package pl.aegon.youraccount.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;

public class YourAccountLogoutPage {
	private WebDriver driver;

	@FindBy(css = "a.zamknijlink")
	private WebElement closeLink;

	@FindBy(css = "body div[align='center']")
	private WebElement messageBox;

	/**
	 * Class constructor.
	 * 
	 * @param WebDriver driver
	 */
	public YourAccountLogoutPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Gets the title of logout page.
	 * 
	 * @return String
	 */
	public String getLogoutPageTitle() {
		return this.driver.getTitle();
	}
	
	/**
	 * Gets the title of the close link.
	 * 
	 * @return String
	 */
	public String getCloseLink() {
		return closeLink.getText();
	}
	
	/**
	 * Gets the content of the message box.
	 * 
	 * @return String
	 */
	public String getMessageBox() {
		return messageBox.getText();
	}
}