/**
 * 
 */
package pl.aegon.youraccount.dbservice;

import java.util.List;
import java.util.ListIterator;

import pl.aegon.youraccount.config.ConfigurationProvider;
import pl.aegon.youraccount.entity.EntryLog;
import pl.aegon.youraccount.entity.Policy;
import pl.aegon.youraccount.entity.PolicyHistory;
import pl.aegon.youraccount.entity.SubAccount;
import pl.aegon.youraccount.entity.SubAccountBalanceHistory;
import pl.aegon.youraccount.entity.SubAccountHistory;
import pl.aegon.youraccount.utils.Utils;

/**
 * @author Michal_Myszkowski
 *
 */
public class DatabaseServiceProvider extends DatabaseManager {
	private static DatabaseServiceProvider classInstance = null;
	private EntryLog entryLog;
	private List<Policy> policyList;
	private List<PolicyHistory> policyHistoryList;
	private List<SubAccount> subAccountList;
	private List<SubAccountHistory> subAccountHistoryList;
	private List<SubAccountBalanceHistory> subAccountBalanceHistoryList;

	/**
	 * Default class constructor.
	 */
	protected DatabaseServiceProvider() {
		super();
	}
	
	public static DatabaseServiceProvider getInstance() {
		if(classInstance == null) {
			classInstance = new DatabaseServiceProvider();
		}
		return classInstance;
	}
	
	public void addNewEntryLogEntity() throws Exception {
		// store new EntryLog entity
		try {
			entryLog = this.addNewEntryLog(entryLog);
		} catch (Exception e) {
			log.error("An exception occured while trying to add EntryLog entity. " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	public void addOrUpdateAllPolicyEntities() throws Exception {
		// store new or update existing Policy entity (for all items in the Policy list)
		ListIterator<Policy> iterPolicy = policyList.listIterator();

		while (iterPolicy.hasNext()) {
			Policy currentItem = iterPolicy.next();
			Policy oldPolicy = null;

			try {
				// check if the current Policy from the list already exists in the database
				oldPolicy = this.getPolicyByPolicyNumber(currentItem.getPolicyNumber());
				if (oldPolicy != null) {
					// update existing Policy entity
					iterPolicy.set(this.updatePolicy(oldPolicy.getIdPolicy(), currentItem));
				} else {
					// add new Policy entity
					iterPolicy.set(this.addNewPolicy(currentItem));
				}
			} catch (Exception e) {
				log.error("An exception occured while trying to bulk add/update Policy entities. "
						+ e.getMessage());
				log.error(Utils.getExceptionStackTrace(e));
				throw e;
			}
		}
	}

	public void addAllPolicyHistoryEntities() throws Exception {
		// store new PolicyHistory entity (for all items in a list)
		ListIterator<PolicyHistory> iterPolicyHistory = policyHistoryList.listIterator();

		while (iterPolicyHistory.hasNext()) {
			PolicyHistory currentItem = iterPolicyHistory.next();
			currentItem = (PolicyHistory) updatePolicyReferenceForEntity(currentItem);

			try {
				// add latest entryLog entity and save element in the DB
				currentItem.setEntryLog(entryLog);
				iterPolicyHistory.set(this.addNewPolicyHistory(currentItem));
			} catch (Exception e) {
				log.error("An exception occured while trying to bulk add PolicyHistory entities. "
						+ e.getMessage());
				log.error(Utils.getExceptionStackTrace(e));
				throw e;
			}
		}
	}

	public void allAllSubAccountBalanceHistoryEntities() throws Exception {
		// store new SubAccountBalanceHistory entity (for all items in a list)
		ListIterator<SubAccountBalanceHistory> iterSABH = subAccountBalanceHistoryList
				.listIterator();

		while (iterSABH.hasNext()) {
			SubAccountBalanceHistory currentItem = iterSABH.next();
			currentItem = (SubAccountBalanceHistory) updateSubAccountReferenceForEntity(currentItem);

			try {
				// add latest entryLog entity and save element in the DB
				currentItem.setEntryLog(entryLog);
				iterSABH.set(this.addNewSubAccountBalanceHistory(currentItem));
			} catch (Exception e) {
				log.error("An exception occured while trying to bulk add SubAccountBalanceHistory entities. "
						+ e.getMessage());
				log.error(Utils.getExceptionStackTrace(e));
				throw e;
			}
		}
	}

	public void addAllSubAccountHistoryEntities() throws Exception {
		// store new SubAccountHistory entity (for all items in a list)
		ListIterator<SubAccountHistory> iterSAH = subAccountHistoryList.listIterator();

		while (iterSAH.hasNext()) {
			SubAccountHistory currentItem = iterSAH.next();
			currentItem = (SubAccountHistory) updateSubAccountReferenceForEntity(currentItem);

			try {
				// add latest entryLog entity and save element in the DB
				currentItem.setEntryLog(entryLog);
				iterSAH.set(this.addNewSubAccountHistory(currentItem));
			} catch (Exception e) {
				log.error("An exception occured while trying to bulk add SubAccountHistory entities. "
						+ e.getMessage());
				log.error(Utils.getExceptionStackTrace(e));
				throw e;
			}
		}
	}

	public void addOrUpdateAllSubAccountEntities() throws Exception {
		// store new or update existing SubAccount entity (for all items in a list)
		ListIterator<SubAccount> iterSubAccount = subAccountList.listIterator();

		while (iterSubAccount.hasNext()) {
			SubAccount currentItem = iterSubAccount.next();
			currentItem = (SubAccount) updatePolicyReferenceForEntity(currentItem);
			SubAccount oldSubAccount = null;

			try {
				// check if the current SubAccount from the list already exists in the database
				oldSubAccount = this.getSubAccountByPolicyAndName(currentItem.getPolicy(),
						currentItem.getSubAccountName());
				if (oldSubAccount != null) {
					// update existing SubAccount entity
					iterSubAccount.set(this.updateSubAccount(oldSubAccount.getIdSubAccount(),
							currentItem));
				} else {
					// add new SubAccount entity
					iterSubAccount.set(this.addNewSubAccount(currentItem));
				}
			} catch (Exception e) {
				log.error("An exception occured while trying to bulk add/update SubAccount entities. "
						+ e.getMessage());
				log.error(Utils.getExceptionStackTrace(e));
				throw e;
			}
		}
	}
	
	public void storeAllEntitiesInDatabase() throws Exception {
		try {
			this.addNewEntryLogEntity();
			this.addOrUpdateAllPolicyEntities();
			this.addOrUpdateAllSubAccountEntities();
			this.addAllPolicyHistoryEntities();
			this.addAllSubAccountHistoryEntities();
			this.allAllSubAccountBalanceHistoryEntities();
		} catch (Exception e) {
			String userLanguage = ConfigurationProvider.getPropertyValue("user_language");
			String trayTitle = ConfigurationProvider.getMessageByLanguage("aegon_app_name", userLanguage);
			String trayMsg = ConfigurationProvider.getMessageByLanguage("database_save_error_msg", userLanguage);
			Utils.showTrayMessage(trayTitle, trayMsg, Utils.ERROR);
			
			log.error("An exception occured while trying to store all possible entities in the database! " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}

	private Object updatePolicyReferenceForEntity(SubAccount subAccount) {
		// update current SubAccount reference to Policy (new one or existing), since
		// SubAccount is dependant of Policy entity
		for (Policy p : policyList) {
			// find Policy which is associated with the given SubAccount through policyNumber field
			if (subAccount.getPolicy().getPolicyNumber().equals(p.getPolicyNumber())) {
				subAccount.setPolicy(p);
				break;
			}
		}

		return subAccount;
	}

	private Object updatePolicyReferenceForEntity(PolicyHistory policyHistory) {
		// update current PolicyHistory reference to Policy (new one or existing), since
		// PolicyHistory is dependant of Policy entity
		for (Policy p : policyList) {
			// find Policy which is associated with the given PolicyHistory through
			// policyNumber field
			if (policyHistory.getPolicy().getPolicyNumber().equals(p.getPolicyNumber())) {
				policyHistory.setPolicy(p);
				break;
			}
		}

		return policyHistory;
	}

	private Object updateSubAccountReferenceForEntity(SubAccountHistory subAccountHistory) {
		// update current SubAccountHistory reference to SubAccount (new one or existing),
		// since SubAccountHistory is dependant of SubAccount entity
		for (SubAccount s : subAccountList) {
			// find SubAccount which is associated with the given SubAccountHistory through
			// subAccountName and underlying policyNumber field
			if (subAccountHistory.getSubAccount().getSubAccountName().equals(s.getSubAccountName())
					&& subAccountHistory.getSubAccount().getPolicy().getPolicyNumber()
							.equals(s.getPolicy().getPolicyNumber())) {
				subAccountHistory.setSubAccount(s);
				break;
			}
		}

		return subAccountHistory;
	}

	private Object updateSubAccountReferenceForEntity(
			SubAccountBalanceHistory subAccountBalanceHistory) {
		// update current SubAccountBalanceHistory reference to SubAccount (new one or
		// existing), since SubAccountBalanceHistory is dependant of SubAccount entity
		for (SubAccount s : subAccountList) {
			// find SubAccount which is associated with the given SubAccountHistory through
			// subAccountName and underlying policyNumber field
			if (subAccountBalanceHistory.getSubAccount().getSubAccountName()
					.equals(s.getSubAccountName())
					&& subAccountBalanceHistory.getSubAccount().getPolicy().getPolicyNumber()
							.equals(s.getPolicy().getPolicyNumber())) {
				subAccountBalanceHistory.setSubAccount(s);
				break;
			}
		}

		return subAccountBalanceHistory;
	}

	/**
	 * @return the entryLog
	 */
	public EntryLog getEntryLog() {
		return entryLog;
	}

	/**
	 * @param entryLog the entryLog to set
	 */
	public void setEntryLog(EntryLog entryLog) {
		this.entryLog = entryLog;
	}

	/**
	 * @return the policyList
	 */
	public List<Policy> getPolicyList() {
		return policyList;
	}

	/**
	 * @param policyList the policyList to set
	 */
	public void setPolicyList(List<Policy> policyList) {
		this.policyList = policyList;
	}

	/**
	 * @return the policyHistoryList
	 */
	public List<PolicyHistory> getPolicyHistoryList() {
		return policyHistoryList;
	}

	/**
	 * @param policyHistoryList the policyHistoryList to set
	 */
	public void setPolicyHistoryList(List<PolicyHistory> policyHistoryList) {
		this.policyHistoryList = policyHistoryList;
	}

	/**
	 * @return the subAccountList
	 */
	public List<SubAccount> getSubAccountList() {
		return subAccountList;
	}

	/**
	 * @param subAccountList the subAccountList to set
	 */
	public void setSubAccountList(List<SubAccount> subAccountList) {
		this.subAccountList = subAccountList;
	}

	/**
	 * @return the subAccountHistoryList
	 */
	public List<SubAccountHistory> getSubAccountHistoryList() {
		return subAccountHistoryList;
	}

	/**
	 * @param subAccountHistoryList the subAccountHistoryList to set
	 */
	public void setSubAccountHistoryList(List<SubAccountHistory> subAccountHistoryList) {
		this.subAccountHistoryList = subAccountHistoryList;
	}

	/**
	 * @return the subAccountBalanceHistoryList
	 */
	public List<SubAccountBalanceHistory> getSubAccountBalanceHistoryList() {
		return subAccountBalanceHistoryList;
	}

	/**
	 * @param subAccountBalanceHistoryList the subAccountBalanceHistoryList to set
	 */
	public void setSubAccountBalanceHistoryList(
			List<SubAccountBalanceHistory> subAccountBalanceHistoryList) {
		this.subAccountBalanceHistoryList = subAccountBalanceHistoryList;
	}
}
