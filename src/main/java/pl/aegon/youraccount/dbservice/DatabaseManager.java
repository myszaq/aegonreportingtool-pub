/**
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.dbservice;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.*;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import pl.aegon.youraccount.config.ConfigurationProvider;
import pl.aegon.youraccount.entity.*;
import pl.aegon.youraccount.utils.Utils;

public class DatabaseManager {
	protected static org.apache.logging.log4j.Logger log = LogManager.getLogger(DatabaseManager.class);
	private EntityManagerFactory emfactory = null;
	private EntityManager em = null;
	private static CacheManager singletonManager;
	private Cache memCache;
	private static String persistenceUnitName;
	
	public static enum PersistenceType {
		TEST_CONN, PROD_CONN
	};
	
	public DatabaseManager() {
		// if persistence unit is not set yet, use the default one - production
		if (persistenceUnitName == null || persistenceUnitName.isEmpty()) {
			persistenceUnitName = "AegonDBStorage";
		}
		
		this.emfactory = Persistence.createEntityManagerFactory(persistenceUnitName);
		this.em = emfactory.createEntityManager();
		createCache();
	}
	
	private void createCache() {
		// create a cache manager
		singletonManager = CacheManager.getInstance();
		// create in memory only cache called "memoryCache"
		Cache memCache = new Cache("memoryCache",	// the name of the cache
			500,	// the maximum number of elements in memory
			false,	// whether to use the disk store
			false,	// whether the elements in the cache are eternal, i.e. never expire
			600,	// the amount of time (in seconds) to live for an element from its creation date
			600		// the amount of time (in seconds) to live for an element from its last accessed or modified date
		);
		
		singletonManager.addCache(memCache);
		this.memCache = singletonManager.getCache("memoryCache");
	}
	
	public void startTransaction() throws Exception {
		try {
			this.em.getTransaction().begin();
		} catch (IllegalStateException e) {
			log.fatal("An exception occured during transaction beginning! " + e.getMessage());
			throw e;
		}
		
		log.info("Database transaction started successfully.");
	}
	
	public void commitTransaction(boolean displayTrayMessage) throws Exception {
		String userLanguage = ConfigurationProvider.getPropertyValue("user_language");
		String trayTitle = ConfigurationProvider.getMessageByLanguage("aegon_app_name", userLanguage);
		
		try {
			em.getTransaction().commit();
		} catch (Exception e) {
			if (displayTrayMessage) {
				String trayMsg = ConfigurationProvider.getMessageByLanguage("database_commit_error_msg",
						userLanguage);
				Utils.showTrayMessage(trayTitle, trayMsg, Utils.ERROR);
			}
			
			log.fatal("An exception occured during transaction commit! " + e.getMessage());
			throw e;
		}
		
		if (displayTrayMessage) {
			String trayMsg = ConfigurationProvider.getMessageByLanguage("database_commit_success_msg",
					userLanguage);
			Utils.showTrayMessage(trayTitle, trayMsg, Utils.INFO);
		}
		log.info("Database transaction commited successfully.");
	}
	
	/**
	 * Rolls back the current transaction if an error occured earlier.
	 * 
	 * @throws Exception if rollback operation failed
	 */
	public void rollbackTransaction() throws Exception {
		try {
			em.getTransaction().rollback();
		} catch (Exception e) {
			log.fatal("An exception occured during transaction rollback! " + e.getMessage());
			throw e;
		}
		
		log.info("Database transaction rolled back successfully.");
	}
	
	/**
	 * Closes database manager allowing to free resources and finish all db operations neatly. Method performs
	 * shutdown of cache manager and closes both EntityManagerFactory and EntityManager. This method should be
	 * called only once and only at the end of application execution!
	 */
	public void closeDatabaseManager() {
		singletonManager.shutdown();
		
		if (em.isOpen()) {
			em.close();
		}
		if (emfactory.isOpen()) {
			emfactory.close();
		}
		
		log.debug("Database manager closed successfully.");
	}
	
	public EntryLog addNewEntryLog(EntryLog entryLog) throws Exception {
		// check basic entity correctness
		if (entryLog == null || entryLog.getDataFromDayDate() == null
				|| entryLog.getDataFromDayDate().isEmpty()) {
			throw new IllegalArgumentException("EntryLog entity to persist is empty or invalid!");
		}
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentDate = dateFormat.format(new Date());
		
		try {
			if (entryLog.getReadingDate() == null || entryLog.getReadingDate().isEmpty()) {
				entryLog.setReadingDate(currentDate);
			}
			em.persist(entryLog);
			em.flush();
			log.info("New EntryLog entity has been created successfully. Created EntryLog id = "
					+ entryLog.getIdEntryLog());
		} catch (PersistenceException pe) {
			log.error("An exception occured while trying to store EntryLog entity! " + pe.getMessage());
			log.error(Utils.getExceptionStackTrace(pe));
		}
		
		return entryLog;
	}
	
	public Policy addNewPolicy(Policy policy) throws Exception {
		// check basic entity correctness
		if (policy == null || policy.getPolicyNumber() == null || policy.getPolicyNumber().isEmpty()) {
			throw new IllegalArgumentException("Policy entity to persist is empty or invalid!");
		}
		
		try {
			em.persist(policy);
			em.flush();
			log.info("New Policy entity has been created successfully. Created Policy id = "
					+ policy.getIdPolicy());
		} catch (PersistenceException pe) {
			log.error("An exception occured while trying to store Policy entity! " + pe.getMessage());
			log.error(Utils.getExceptionStackTrace(pe));
		}
		
		return policy;
	}
	
	public SubAccount addNewSubAccount(SubAccount subAccount) throws Exception {
		// check basic entity correctness
		if (subAccount == null || subAccount.getSubAccountName() == null
				|| subAccount.getSubAccountName().isEmpty()) {
			throw new IllegalArgumentException("SubAccount entity to persist is empty or invalid!");
		}
		
		try {
			em.persist(subAccount);
			em.flush();
			log.info("New SubAccount entity has been created successfully. Created SubAccount id = "
					+ subAccount.getIdSubAccount());
		} catch (PersistenceException pe) {
			log.error("An exception occured while trying to store SubAccount entity! " + pe.getMessage());
			log.error(Utils.getExceptionStackTrace(pe));
		}
		
		return subAccount;
	}
	
	public PolicyHistory addNewPolicyHistory(PolicyHistory policyHistory) throws Exception {
		// check basic entity correctness
		if (policyHistory == null || policyHistory.getPolicy() == null || policyHistory.getEntryLog() == null) {
			throw new IllegalArgumentException("PolicyHistory entity to persist is empty or invalid!");
		}
		
		try {
			em.persist(policyHistory);
			em.flush();
			log.info("New PolicyHistory entity has been created successfully. PolicyHistory id = "
					+ policyHistory.getId());
		} catch (PersistenceException pe) {
			log.error("An exception occured while trying to store PolicyHistory entity! " + pe.getMessage());
			log.error(Utils.getExceptionStackTrace(pe));
		}
		
		return policyHistory;
	}
	
	public SubAccountHistory addNewSubAccountHistory(SubAccountHistory subAccountHistory) throws Exception {
		// check basic entity correctness
		if (subAccountHistory == null || subAccountHistory.getSubAccount() == null
				|| subAccountHistory.getEntryLog() == null) {
			throw new IllegalArgumentException("SubAccountHistory entity to persist is empty or invalid!");
		}
		
		try {
			em.persist(subAccountHistory);
			em.flush();
			log.info("New SubAccountHistory entity has been created successfully. SubAccountHistory id = "
					+ subAccountHistory.getId());
		} catch (PersistenceException pe) {
			log.error("An exception occured while trying to store SubAccountHistory entity! "
					+ pe.getMessage());
			log.error(Utils.getExceptionStackTrace(pe));
		}
		
		return subAccountHistory;
	}
	
	public SubAccountBalanceHistory addNewSubAccountBalanceHistory(
			SubAccountBalanceHistory subAccountBalanceHistory) throws Exception {
		// check basic entity correctness
		if (subAccountBalanceHistory == null || subAccountBalanceHistory.getSubAccount() == null
				|| subAccountBalanceHistory.getEntryLog() == null) {
			throw new IllegalArgumentException(
					"SubAccountBalanceHistory entity to persist is empty or invalid!");
		}
		
		try {
			em.persist(subAccountBalanceHistory);
			em.flush();
			log.info("New SubAccountBalanceHistory entity has been created successfully. SubAccountBalanceHistory id = "
					+ subAccountBalanceHistory.getIdSubAccountBalanceHistory());
		} catch (PersistenceException pe) {
			log.error("An exception occured while trying to store SubAccountBalanceHistory entity! "
					+ pe.getMessage());
			log.error(Utils.getExceptionStackTrace(pe));
		}
		
		return subAccountBalanceHistory;
	}
	
	public void deletePolicy(Policy policy) throws Exception {
		// check basic entity correctness
		if (policy == null || policy.getPolicyNumber() == null) {
			throw new IllegalArgumentException("Policy entity to remove is empty or invalid!");
		}
		
		try {
			List<PolicyHistory> dependantPHList = getPolicyHistoryListByPolicy(policy);
			if (!dependantPHList.isEmpty()) {
				for (PolicyHistory item : dependantPHList) {
					deletePolicyHistory(item);
				}
			}
		} catch (Exception e) {
			log.error("An exception occured while removing dependant PolicyHistory entities for Policy entity! "
					+ e.getMessage());
			throw e;
		}
		
		try {
			List<SubAccount> dependantSAList = getSubAccountListByPolicy(policy);
			if (!dependantSAList.isEmpty()) {
				for (SubAccount item : dependantSAList) {
					deleteSubAccount(item);
				}
			}
		} catch (Exception e) {
			log.error("An exception occured while removing dependant SubAccount entities for Policy entity! "
					+ e.getMessage());
			throw e;
		}
		
		int entityId = policy.getIdPolicy();
		try {
			em.remove(policy);
			em.flush();
			log.info("Policy entity with the id = " + entityId + "has been removed successfully.");
		} catch (Exception e) {
			log.error("An exception occured while trying to remove Policy entity! " + e.getMessage());
			throw e;
		}
	}
	
	public void deletePolicyHistory(PolicyHistory policyHistory) throws Exception {
		// check basic entity correctness
		if (policyHistory == null || policyHistory.getPolicy() == null) {
			throw new IllegalArgumentException("PolicyHistory entity to remove is empty or invalid!");
		}
		
		PolicyHistoryPK entityId = policyHistory.getId();
		try {
			em.remove(policyHistory);
			em.flush();
			log.info("PolicyHistory entity with the id = " + entityId + "has been removed successfully.");
		} catch (Exception e) {
			log.error("An exception occured while trying to remove PolicyHistory entity! " + e.getMessage());
			throw e;
		}
	}
	
	public void deleteSubAccount(SubAccount subAccount) throws Exception {
		// check basic entity correctness
		if (subAccount == null || subAccount.getSubAccountName() == null) {
			throw new IllegalArgumentException("SubAccount entity to remove is empty or invalid!");
		}
		
		try {
			List<SubAccountBalanceHistory> dependantSABHList = getSubAccountBalanceHistoryListBySubAccount(subAccount);
			if (!dependantSABHList.isEmpty()) {
				for (SubAccountBalanceHistory item : dependantSABHList) {
					deleteSubAccountBalanceHistory(item);
				}
			}
		} catch (Exception e) {
			log.error("An exception occured while removing dependant SubAccountBalanceHistory entities for SubAccount entity! "
					+ e.getMessage());
			throw e;
		}
		
		try {
			List<SubAccountHistory> dependantSAHList = getSubAccountHistoryListBySubAccount(subAccount);
			if (!dependantSAHList.isEmpty()) {
				for (SubAccountHistory item : dependantSAHList) {
					deleteSubAccountHistory(item);
				}
			}
		} catch (Exception e) {
			log.error("An exception occured while removing dependant SubAccountHistory entities for SubAccount entity! "
					+ e.getMessage());
			throw e;
		}
		
		int entityId = subAccount.getIdSubAccount();
		try {
			em.remove(subAccount);
			em.flush();
			log.info("SubAccount entity with the id = " + entityId + "has been removed successfully.");
		} catch (Exception e) {
			log.error("An exception occured while trying to remove SubAccount entity! " + e.getMessage());
			throw e;
		}
	}
	
	public void deleteSubAccountHistory(SubAccountHistory subAccountHistory) throws Exception {
		// check basic entity correctness
		if (subAccountHistory == null || subAccountHistory.getSubAccount() == null) {
			throw new IllegalArgumentException("SubAccountHistory entity to remove is empty or invalid!");
		}
		
		SubAccountHistoryPK entityId = subAccountHistory.getId();
		try {
			em.remove(subAccountHistory);
			em.flush();
			log.info("SubAccountHistory entity with the id = " + entityId + "has been removed successfully.");
		} catch (Exception e) {
			log.error("An exception occured while trying to remove SubAccountHistory entity! "
					+ e.getMessage());
			throw e;
		}
	}
	
	public void deleteSubAccountBalanceHistory(SubAccountBalanceHistory subAccountBalanceHistory)
			throws Exception {
		// check basic entity correctness
		if (subAccountBalanceHistory == null || subAccountBalanceHistory.getSubAccount() == null
				|| subAccountBalanceHistory.getEntryLog() == null) {
			throw new IllegalArgumentException(
					"SubAccountBalanceHistory entity to remove is empty or invalid!");
		}
		
		int entityId = subAccountBalanceHistory.getIdSubAccountBalanceHistory();
		try {
			em.remove(subAccountBalanceHistory);
			em.flush();
			log.info("SubAccountBalanceHistory entity with the id = " + entityId
					+ "has been removed successfully.");
		} catch (Exception e) {
			log.error("An exception occured while trying to remove SubAccountBalanceHistory entity! "
					+ e.getMessage());
			throw e;
		}
	}
	
	public List<EntryLog> getEntryLogListByLimit(int limit) throws Exception {
		List<EntryLog> result = null;
		String cacheKey = "EntryLog.findWithLimit_" + limit;
		
		// if the requested data is already in the cache, get the data from it
		if (this.memCache.isKeyInCache(cacheKey)) {
			log.debug("EntryLog list obtained from cache under key = " + cacheKey);
			
			// the code below is to avoid the warning
			// "Type safety: Unchecked cast from Object to List<EntryLog>"
			// using simple check of the objects in the obtained list we make the safe cast
			List<?> cacheResult = (List<?>) memCache.get(cacheKey).getObjectValue();
			List<EntryLog> tmpList = new ArrayList<EntryLog>();
			
			for (Object obj : cacheResult) {
				if (obj instanceof EntryLog) {
					tmpList.add((EntryLog) obj);
				}
			}
			
			result = tmpList;
		} else {
			try {
				// use entity manager to retrieve named query
				TypedQuery<EntryLog> query = em.createNamedQuery("EntryLog.findWithLimit", EntryLog.class);
				// set max result (counterpart of SQL LIMIT)
				query.setMaxResults(limit);
				// execute query and get results
				result = query.getResultList();
			} catch (Exception e) {
				log.error("An exception occured while trying to get EntryLog entities! " + e.getMessage());
				throw e;
			}
			
			// store the data from the database in cache for future use
			this.memCache.put(new Element(cacheKey, result));
			log.debug("EntryLog list added to cache under key = " + cacheKey);
		}
		
		return result;
	}
	
	public List<EntryLog> getEntryLogListByTimeRange(String minDateTime, String maxDateTime) throws Exception {
		List<EntryLog> result = null;
		String cacheKey = "EntryLog.findByTimeRange_" + minDateTime + "_" + maxDateTime;
		
		// if maxDateTime was passed null or empty, set the default value to current datetime (in the
		// appropriate format)
		if (maxDateTime == null || maxDateTime.isEmpty()) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			maxDateTime = dateFormat.format(new Date());
		}
		
		// if the requested data is already in the cache, get the data from it
		if (this.memCache.isKeyInCache(cacheKey)) {
			log.debug("EntryLog list obtained from cache under key = " + cacheKey);
			
			// the code below is to avoid the warning
			// "Type safety: Unchecked cast from Object to List<EntryLog>"
			// using simple check of the objects in the obtained list we make the safe cast
			List<?> cacheResult = (List<?>) memCache.get(cacheKey).getObjectValue();
			List<EntryLog> tmpList = new ArrayList<EntryLog>();
			
			for (Object obj : cacheResult) {
				if (obj instanceof EntryLog) {
					tmpList.add((EntryLog) obj);
				}
			}
			
			result = tmpList;
		} else {
			try {
				// use entity manager to retrieve named query
				TypedQuery<EntryLog> query = em.createNamedQuery("EntryLog.findByTimeRange", EntryLog.class);
				// set dynamic data for query
				query.setParameter("minDateTime", minDateTime);
				query.setParameter("maxDateTime", maxDateTime);
				// execute query and get results
				result = query.getResultList();
			} catch (Exception e) {
				log.error("An exception occured while trying to get EntryLog entities! " + e.getMessage());
				throw e;
			}
			
			// store the data from the database in cache for future use
			this.memCache.put(new Element(cacheKey, result));
			log.debug("EntryLog list added to cache under key = " + cacheKey);
		}
		
		return result;
	}
	
	public List<Policy> getAllPolicyList() throws Exception {
		List<Policy> result = null;
		String cacheKey = "Policy.findAll";
		
		// if the requested data is already in the cache, get the data from it
		if (this.memCache.isKeyInCache(cacheKey)) {
			log.debug("Policy list obtained from cache under key = " + cacheKey);
			
			// the code below is to avoid the warning "Type safety: Unchecked cast from Object to List<...>"
			// using simple check of the objects in the obtained list we make the safe cast
			List<?> cacheResult = (List<?>) memCache.get(cacheKey).getObjectValue();
			List<Policy> tmpList = new ArrayList<Policy>();
			
			for (Object obj : cacheResult) {
				if (obj instanceof Policy) {
					tmpList.add((Policy) obj);
				}
			}
			
			result = tmpList;
		} else {
			try {
				// use entity manager to retrieve named query
				TypedQuery<Policy> query = em.createNamedQuery("Policy.findAll", Policy.class);
				// execute query and get results
				result = query.getResultList();
			} catch (Exception e) {
				log.error("An exception occured while trying to get Policy entities! " + e.getMessage());
				throw e;
			}
			
			// store the data from the database in cache for future use
			this.memCache.put(new Element(cacheKey, result));
			log.debug("Policy list added to cache under key = " + cacheKey);
		}
		
		return result;
	}
	
	public Policy getPolicyByPolicyNumber(String policyNumber) throws Exception {
		Policy result = null;
		
		try {
			// use entity manager to retrieve named query
			Query query = em.createNamedQuery("Policy.findByNumber");
			// set dynamic data for query
			query.setParameter("number", policyNumber);
			// execute query and get results
			result = (Policy) query.getSingleResult();
		} catch (NoResultException nre) {
			log.warn("No Policy found in getPolicyByPolicyNumber method.");
		} catch (Exception e) {
			log.error("An exception occured while trying to get Policy entity! " + e.getMessage());
			throw e;
		}
		
		return result;
	}
	
	public List<SubAccount> getAllSubAccountList() throws Exception {
		List<SubAccount> result = null;
		String cacheKey = "SubAccount.findAll";
		
		// if the requested data is already in the cache, get the data from it
		if (this.memCache.isKeyInCache(cacheKey)) {
			log.debug("SubAccount list obtained from cache under key = " + cacheKey);
			
			// the code below is to avoid the warning "Type safety: Unchecked cast from Object to List<...>"
			// using simple check of the objects in the obtained list we make the safe cast
			List<?> cacheResult = (List<?>) memCache.get(cacheKey).getObjectValue();
			List<SubAccount> tmpList = new ArrayList<SubAccount>();
			
			for (Object obj : cacheResult) {
				if (obj instanceof SubAccount) {
					tmpList.add((SubAccount) obj);
				}
			}
			
			result = tmpList;
		} else {
			try {
				// use entity manager to retrieve named query
				TypedQuery<SubAccount> query = em.createNamedQuery("SubAccount.findAll", SubAccount.class);
				// execute query and get results
				result = query.getResultList();
			} catch (Exception e) {
				log.error("An exception occured while trying to get SubAccount entities! " + e.getMessage());
				throw e;
			}
			
			// store the data from the database in cache for future use
			this.memCache.put(new Element(cacheKey, result));
			log.debug("SubAccount list added to cache under key = " + cacheKey);
		}
		
		return result;
	}
	
	public List<SubAccount> getSubAccountListByPolicy(Policy policy) throws Exception {
		List<SubAccount> result = null;
		String cacheKey = "SubAccount.findByPolicy_" + policy.getIdPolicy();
		
		// if the requested data is already in the cache, get the data from it
		if (this.memCache.isKeyInCache(cacheKey)) {
			log.debug("SubAccount list obtained from cache under key = " + cacheKey);
			
			// the code below is to avoid the warning "Type safety: Unchecked cast from Object to List<...>"
			// using simple check of the objects in the obtained list we make the safe cast
			List<?> cacheResult = (List<?>) memCache.get(cacheKey).getObjectValue();
			List<SubAccount> tmpList = new ArrayList<SubAccount>();
			
			for (Object obj : cacheResult) {
				if (obj instanceof SubAccount) {
					tmpList.add((SubAccount) obj);
				}
			}
			
			result = tmpList;
		} else {
			try {
				// use entity manager to retrieve named query
				TypedQuery<SubAccount> query = em.createNamedQuery("SubAccount.findByPolicy",
						SubAccount.class);
				// set dynamic data for query
				query.setParameter("policy", policy);
				// execute query and get results
				result = query.getResultList();
			} catch (Exception e) {
				log.error("An exception occured while trying to get SubAccount entities! " + e.getMessage());
				throw e;
			}
			
			// store the data from the database in cache for future use
			this.memCache.put(new Element(cacheKey, result));
			log.debug("SubAccount list added to cache under key = " + cacheKey);
		}
		
		return result;
	}
	
	public SubAccount getSubAccountByPolicyAndName(Policy policy, String subAccountName) throws Exception {
		SubAccount result = null;
		
		try {
			// use entity manager to retrieve named query
			Query query = em.createNamedQuery("SubAccount.findByPolicyAndName");
			// set dynamic data for query
			query.setParameter("policy", policy);
			query.setParameter("subAccountName", subAccountName);
			// execute query and get results
			result = (SubAccount) query.getSingleResult();
		} catch (NoResultException nre) {
			log.warn("No subAccount found in getSubAccountByPolicyAndName method.");
		} catch (Exception e) {
			log.error("An exception occured while trying to get SubAccount entity! " + e.getMessage());
			throw e;
		}
		
		return result;
	}
	
	public List<PolicyHistory> getPolicyHistoryListByPolicy(Policy policy) throws Exception {
		List<PolicyHistory> result = null;
		
		try {
			// use entity manager to retrieve named query
			TypedQuery<PolicyHistory> query = em.createNamedQuery("PolicyHistory.findByPolicy",
					PolicyHistory.class);
			// set dynamic data for query, execute it and get results
			query.setParameter("policy", policy);
			result = query.getResultList();
		} catch (Exception e) {
			log.error("An exception occured while trying to get PolicyHistory entities! " + e.getMessage());
			throw e;
		}
		
		return result;
	}
	
	public List<PolicyHistory> getPolicyHistoryListByEntryLogRange(int idPolicy, EntryLog minEntryLog,
			EntryLog maxEntryLog) throws Exception {
		List<PolicyHistory> result = null;
		
		if (minEntryLog == null || minEntryLog.getIdEntryLog() == 0 || maxEntryLog == null
				|| maxEntryLog.getIdEntryLog() == 0) {
			throw new IllegalArgumentException(
					"EntryLog entity used in PolicyHistory search is empty or invalid!");
		}
		
		String cacheKey = "PolicyHistory.findByIdAndEntryLogRange_" + idPolicy + "_"
				+ minEntryLog.getIdEntryLog() + "_" + maxEntryLog.getIdEntryLog();
		// if the requested data is already in the cache, get the data from it
		if (this.memCache.isKeyInCache(cacheKey)) {
			log.debug("PolicyHistory list obtained from cache under key = " + cacheKey);
			
			// the code below is to avoid the warning
			// "Type safety: Unchecked cast from Object to List<...>"
			// using simple check of the objects in the obtained list we make the safe cast
			List<?> cacheResult = (List<?>) memCache.get(cacheKey).getObjectValue();
			List<PolicyHistory> tmpList = new ArrayList<PolicyHistory>();
			
			for (Object obj : cacheResult) {
				if (obj instanceof PolicyHistory) {
					tmpList.add((PolicyHistory) obj);
				}
			}
			
			result = tmpList;
		} else {
			try {
				// use entity manager to retrieve named query
				TypedQuery<PolicyHistory> query = em.createNamedQuery(
						"PolicyHistory.findByIdAndEntryLogRange", PolicyHistory.class);
				// set dynamic data for query, execute it and get results
				query.setParameter("idPolicy", idPolicy);
				query.setParameter("idEntryLogMin", minEntryLog.getIdEntryLog());
				query.setParameter("idEntryLogMax", maxEntryLog.getIdEntryLog());
				result = query.getResultList();
			} catch (Exception e) {
				log.error("An exception occured while trying to get PolicyHistory entities! "
						+ e.getMessage());
				throw e;
			}
			
			// store the data from the database in cache for future use
			this.memCache.put(new Element(cacheKey, result));
			log.debug("PolicyHistory list added to cache under key = " + cacheKey);
		}
		
		return result;
	}
	
	public List<SubAccountHistory> getSubAccountHistoryListBySubAccount(SubAccount subAccount)
			throws Exception {
		List<SubAccountHistory> result = null;
		
		try {
			// use entity manager to retrieve named query
			TypedQuery<SubAccountHistory> query = em.createNamedQuery("SubAccountHistory.findBySubAccount",
					SubAccountHistory.class);
			// set dynamic data for query, execute it and get results
			query.setParameter("subAccount", subAccount);
			result = query.getResultList();
		} catch (Exception e) {
			log.error("An exception occured while trying to get SubAccountHistory entities! "
					+ e.getMessage());
			throw e;
		}
		
		return result;
	}
	
	public List<SubAccountHistory> getSubAccountHistoryListByEntryLogRange(int idSubAccount,
			EntryLog minEntryLog, EntryLog maxEntryLog) throws Exception {
		List<SubAccountHistory> result = null;
		
		if (minEntryLog == null || minEntryLog.getIdEntryLog() == 0 || maxEntryLog == null
				|| maxEntryLog.getIdEntryLog() == 0) {
			throw new IllegalArgumentException(
					"EntryLog entity used in SubAccountHistory search is empty or invalid!");
		}
		
		String cacheKey = "SubAccountHistory.findByIdAndEntryLogRange_" + idSubAccount + "_"
				+ minEntryLog.getIdEntryLog() + "_" + maxEntryLog.getIdEntryLog();
		// if the requested data is already in the cache, get the data from it
		if (this.memCache.isKeyInCache(cacheKey)) {
			log.debug("SubAccountHistory list obtained from cache under key = " + cacheKey);
			
			// the code below is to avoid the warning
			// "Type safety: Unchecked cast from Object to List<...>"
			// using simple check of the objects in the obtained list we make the safe cast
			List<?> cacheResult = (List<?>) memCache.get(cacheKey).getObjectValue();
			List<SubAccountHistory> tmpList = new ArrayList<SubAccountHistory>();
			
			for (Object obj : cacheResult) {
				if (obj instanceof SubAccountHistory) {
					tmpList.add((SubAccountHistory) obj);
				}
			}
			
			result = tmpList;
		} else {
			try {
				// use entity manager to retrieve named query
				TypedQuery<SubAccountHistory> query = em.createNamedQuery(
						"SubAccountHistory.findByIdAndEntryLogRange", SubAccountHistory.class);
				// set dynamic data for query, execute it and get results
				query.setParameter("idSubAccount", idSubAccount);
				query.setParameter("idEntryLogMin", minEntryLog.getIdEntryLog());
				query.setParameter("idEntryLogMax", maxEntryLog.getIdEntryLog());
				result = query.getResultList();
			} catch (Exception e) {
				log.error("An exception occured while trying to get SubAccountHistory entities! "
						+ e.getMessage());
				throw e;
			}
			
			// store the data from the database in cache for future use
			this.memCache.put(new Element(cacheKey, result));
			log.debug("SubAccountHistory list added to cache under key = " + cacheKey);
		}
		
		return result;
	}
	
	public List<SubAccountBalanceHistory> getSubAccountBalanceHistoryListBySubAccount(SubAccount subAccount)
			throws Exception {
		List<SubAccountBalanceHistory> result = null;
		
		try {
			// use entity manager to retrieve named query
			TypedQuery<SubAccountBalanceHistory> query = em.createNamedQuery(
					"SubAccountBalanceHistory.findBySubAccount", SubAccountBalanceHistory.class);
			// set dynamic data for query, execute it and get results
			query.setParameter("subAccount", subAccount);
			result = query.getResultList();
		} catch (Exception e) {
			log.error("An exception occured while trying to get SubAccountBalanceHistory entities! "
					+ e.getMessage());
			throw e;
		}
		
		return result;
	}
	
	public List<String> getSubAccountBalanceHistoryUFKNameListBySubAccount(SubAccount subAccount)
			throws Exception {
		List<String> result = null;
		String cacheKey = "SubAccountBalanceHistory.findUfkNamesBySubAccount_" + subAccount.getIdSubAccount();
		
		// if the requested data is already in the cache, get the data from it
		if (this.memCache.isKeyInCache(cacheKey)) {
			log.debug("UFKName list obtained from cache under key = " + cacheKey);
			
			// the code below is to avoid the warning "Type safety: Unchecked cast from Object to List<...>"
			// using simple check of the objects in the obtained list we make the safe cast
			List<?> cacheResult = (List<?>) memCache.get(cacheKey).getObjectValue();
			List<String> tmpList = new ArrayList<String>();
			
			for (Object obj : cacheResult) {
				if (obj instanceof String) {
					tmpList.add((String) obj);
				}
			}
			
			result = tmpList;
		} else {
			try {
				// use entity manager to retrieve named query
				TypedQuery<String> query = em.createNamedQuery(
						"SubAccountBalanceHistory.findUfkNamesBySubAccount", String.class);
				// set dynamic data for query, execute it and get results
				query.setParameter("subAccount", subAccount);
				result = query.getResultList();
			} catch (Exception e) {
				log.error("An exception occured while trying to get UFK names from SubAccountBalanceHistory entities! "
						+ e.getMessage());
				throw e;
			}
			
			// store the data from the database in cache for future use
			this.memCache.put(new Element(cacheKey, result));
			log.debug("UFKName list added to cache under key = " + cacheKey);
		}
		
		return result;
	}
	
	public List<SubAccountBalanceHistory> getSubAccountBalanceHistoryListByEntryLogRange(int idSubAccount,
			EntryLog minEntryLog, EntryLog maxEntryLog) throws Exception {
		List<SubAccountBalanceHistory> result = null;
		
		if (minEntryLog == null || minEntryLog.getIdEntryLog() == 0 || maxEntryLog == null
				|| maxEntryLog.getIdEntryLog() == 0) {
			throw new IllegalArgumentException(
					"EntryLog entity used in SubAccountBalanceHistory search is empty or invalid!");
		}
		
		String cacheKey = "SubAccountBalanceHistory.findByIdAndEntryLogRange_" + idSubAccount + "_"
				+ minEntryLog.getIdEntryLog() + "_" + maxEntryLog.getIdEntryLog();
		// if the requested data is already in the cache, get the data from it
		if (this.memCache.isKeyInCache(cacheKey)) {
			log.debug("SubAccountBalanceHistory list obtained from cache under key = " + cacheKey);
			
			// the code below is to avoid the warning
			// "Type safety: Unchecked cast from Object to List<...>"
			// using simple check of the objects in the obtained list we make the safe cast
			List<?> cacheResult = (List<?>) memCache.get(cacheKey).getObjectValue();
			List<SubAccountBalanceHistory> tmpList = new ArrayList<SubAccountBalanceHistory>();
			
			for (Object obj : cacheResult) {
				if (obj instanceof SubAccountBalanceHistory) {
					tmpList.add((SubAccountBalanceHistory) obj);
				}
			}
			
			result = tmpList;
		} else {
			try {
				// use entity manager to retrieve named query
				TypedQuery<SubAccountBalanceHistory> query = em.createNamedQuery(
						"SubAccountBalanceHistory.findByIdAndEntryLogRange", SubAccountBalanceHistory.class);
				// set dynamic data for query, execute it and get results
				query.setParameter("idSubAccount", idSubAccount);
				query.setParameter("idEntryLogMin", minEntryLog.getIdEntryLog());
				query.setParameter("idEntryLogMax", maxEntryLog.getIdEntryLog());
				result = query.getResultList();
			} catch (Exception e) {
				log.error("An exception occured while trying to get SubAccountBalanceHistory entities! "
						+ e.getMessage());
				throw e;
			}
			
			// store the data from the database in cache for future use
			this.memCache.put(new Element(cacheKey, result));
			log.debug("SubAccountBalanceHistory list added to cache under key = " + cacheKey);
		}
		
		return result;
	}
	
	public Policy updatePolicy(int idPolicy, Policy policy) throws Exception {
		// check basic entity correctness
		if (policy == null || policy.getPolicyNumber() == null) {
			throw new IllegalArgumentException("Policy entity to update is empty or invalid!");
		}
		Policy oldPolicy = null;
		
		try {
			oldPolicy = em.find(Policy.class, idPolicy);
		} catch (IllegalArgumentException e) {
			log.error("An exception occured while trying to get Policy entity with id = " + idPolicy + "!");
			try {
				log.error(Utils.getExceptionStackTrace(e));
			} catch (Exception ex) {
				log.error("An exception occured! " + ex.getMessage());
				ex.printStackTrace();
			}
			throw e;
		}
		
		if (oldPolicy != null) {
			// set updatable fields from a new Policy entity to the existing one
			oldPolicy.setCurrentBalance(policy.getCurrentBalance());
			oldPolicy.setPolicyName(policy.getPolicyName());
			oldPolicy.setProductName(policy.getProductName());
			
			try {
				em.merge(oldPolicy);
				em.flush();
				log.info("Policy entity has been updated successfully. Updated Policy id = "
						+ oldPolicy.getIdPolicy());
			} catch (PersistenceException pe) {
				log.error("An exception occured while trying to update Policy entity! " + pe.getMessage());
				log.error(Utils.getExceptionStackTrace(pe));
			}
		}
		
		return oldPolicy;
	}
	
	public SubAccount updateSubAccount(int idSubAccount, SubAccount subAccount) throws Exception {
		// check basic entity correctness
		if (subAccount == null || subAccount.getSubAccountName() == null || subAccount.getPolicy() == null) {
			throw new IllegalArgumentException("SubAccount entity to update is empty or invalid!");
		}
		SubAccount oldSubAccount = null;
		
		try {
			oldSubAccount = em.find(SubAccount.class, idSubAccount);
		} catch (IllegalArgumentException e) {
			log.error("An exception occured while trying to get SubAccount entity with id = " + idSubAccount
					+ "!");
			try {
				log.error(Utils.getExceptionStackTrace(e));
			} catch (Exception ex) {
				log.error("An exception occured! " + ex.getMessage());
				ex.printStackTrace();
			}
			throw e;
		}
		
		if (oldSubAccount != null) {
			// set updatable fields from a new SubAccount entity to the existing one
			oldSubAccount.setCurrentBalance(subAccount.getCurrentBalance());
			oldSubAccount.setSubAccountName(subAccount.getSubAccountName());
			
			try {
				em.merge(oldSubAccount);
				em.flush();
				log.info("SubAccount entity has been updated successfully. Updated SubAccount id = "
						+ oldSubAccount.getIdSubAccount());
			} catch (PersistenceException pe) {
				log.error("An exception occured while trying to update SubAccount entity! " + pe.getMessage());
				log.error(Utils.getExceptionStackTrace(pe));
			}
		}
		
		return oldSubAccount;
	}
	
	/**
	 * Gets the name of persistence unit used to connect to database.
	 * 
	 * @return String
	 */
	public static String getPersistenceUnitName() {
		return persistenceUnitName;
	}
	
	/**
	 * Sets the value of persistence unit name used to connect to database.
	 * 
	 * @param persistenceType the type of persistence unit defining connection
	 */
	public static void setPersistenceUnitName(PersistenceType persistenceType) {
		if (persistenceType == PersistenceType.PROD_CONN) {
			persistenceUnitName = "AegonDBStorage";
		} else if (persistenceType == PersistenceType.TEST_CONN) {
			persistenceUnitName = "AegonDBStorageTest";
		}
	}
}
