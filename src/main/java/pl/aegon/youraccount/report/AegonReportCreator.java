package pl.aegon.youraccount.report;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesDataItem;

public class AegonReportCreator {
	
	private double getMinimumValueFromTimeSeries(TimeSeries timeSeries) {
		double minValue = timeSeries.getMinY();
		
		BigDecimal bd = new BigDecimal(minValue).setScale(2, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	
	private double getMaximumValueFromTimeSeries(TimeSeries timeSeries) {
		double maxValue = timeSeries.getMaxY();
		
		BigDecimal bd = new BigDecimal(maxValue).setScale(2, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	
	private double getStartValueFromTimeSeries(TimeSeries timeSeries) {
		double startValue = timeSeries.getDataItem(0).getValue().doubleValue();
		
		BigDecimal bd = new BigDecimal(startValue).setScale(2, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	
	private double getEndValueFromTimeSeries(TimeSeries timeSeries) {
		int itemsCount = timeSeries.getItemCount();
		double endValue = timeSeries.getDataItem(itemsCount - 1).getValue().doubleValue();
		
		BigDecimal bd = new BigDecimal(endValue).setScale(2, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	
	private double getAverageValueFromTimeSeries(TimeSeries timeSeries) {
		double sum = 0;
		double averageValue;
		
		for (int i = 0; i < timeSeries.getItemCount(); i++) {
			double tsValue = timeSeries.getDataItem(i).getValue().doubleValue();
			sum += tsValue;
		}
		
		averageValue = sum / timeSeries.getItemCount();
		BigDecimal bd = new BigDecimal(averageValue).setScale(2, RoundingMode.HALF_UP);
		
		return bd.doubleValue();
	}
	
	private double[] getLargestGrowthInTimeSeries(TimeSeries timeSeries) {
		double maxDiff = 0;
		double maxPercentDiff = 0;
		
		for (int i = 1; i < timeSeries.getItemCount(); i++) {
			double nextValue = timeSeries.getDataItem(i).getValue().doubleValue();
			double prevValue = timeSeries.getDataItem(i - 1).getValue().doubleValue();
			
			double diff = nextValue - prevValue;
			if (diff > maxDiff) {
				maxDiff = diff;
				maxPercentDiff = (diff / prevValue) * 100;
			}
		}
		
		BigDecimal bd = new BigDecimal(maxDiff).setScale(2, RoundingMode.HALF_UP);
		maxDiff = bd.doubleValue();
		
		bd = new BigDecimal(maxPercentDiff).setScale(2, RoundingMode.HALF_UP);
		maxPercentDiff = bd.doubleValue();
		
		double[] resultArr = new double[2];
		resultArr[0] = maxDiff;
		resultArr[1] = maxPercentDiff;

		return resultArr;
	}
	
	private double[] getLargestPercentageChangeInTimeSeries(TimeSeries timeSeries, boolean getGrowth) {
		double maxDiff = 0;
		double maxPercentDiff = 0;
		boolean isChanged = false;
		
		for (int i = 1; i < timeSeries.getItemCount(); i++) {
			double nextValue = timeSeries.getDataItem(i).getValue().doubleValue();
			double prevValue = timeSeries.getDataItem(i - 1).getValue().doubleValue();
			
			double diff = nextValue - prevValue;
			if (getGrowth) {
				if (diff > maxDiff) {
					isChanged = true;
				} 
			} else {
				if (diff < maxDiff) {
					isChanged = true;
				}
			}
			
			if (isChanged) {
				maxDiff = diff;
				maxPercentDiff = (diff / prevValue) * 100;
				isChanged = false;
			}
		}
		
		BigDecimal bd = new BigDecimal(maxDiff).setScale(2, RoundingMode.HALF_UP);
		maxDiff = bd.doubleValue();
		
		bd = new BigDecimal(maxPercentDiff).setScale(2, RoundingMode.HALF_UP);
		maxPercentDiff = bd.doubleValue();
		
		double[] resultArr = new double[2];
		resultArr[0] = maxDiff;
		resultArr[1] = maxPercentDiff;

		return resultArr;
	}
}
