/**
 * The LineChartsGenerator class, as the name suggests, provides the interface for generating line charts (other name possible
 * is XY-charts or TimeSeries charts, since they are based on TimeSeries entries) presenting the visual change of balances in
 * time (for given entities) for Aegon report creation purposes. The process of creating line charts is based on the JFreeChart
 * library which provides all necessary, lower lever functionality. The generated line charts represent
 * the following entities:
 * - Whole account (the list of EntryLog records) with or without policies,
 * - Policy (the list of policies inside user's Aegon account) with or without their sub accounts,
 * - SubAccount (the list of sub accounts),
 * - SubAccountBalanceHistory (the list of UFKs inside every sub account) with or without sub accounts, to which they belong.
 * 
 * Created line charts are saved by the class in the image files (in designated temporary folder) and later available
 * as the paths to these files through the appropriate methods provided.
 * The class also provides the methods to retrieve TimeSeries objects for every entity mentioned, which store the actual
 * data used to plot every chart.
 * There are multiple settings available which define what and how the charts are generated - they are available through
 * the class {@link GUIApplicationConfigurator}.
 * 
 * For more details, see the comments of each method.
 * 
 * @see <a href="http://www.jfree.org/jfreechart/">http://www.jfree.org/jfreechart/</a>
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.report;

import org.apache.logging.log4j.*;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.time.*;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleInsets;

import pl.aegon.youraccount.config.ConfigurationProvider;
import pl.aegon.youraccount.dbservice.*;
import pl.aegon.youraccount.entity.*;
import pl.aegon.youraccount.utils.Utils;
import pl.aegon.youraccount.utils.Utils.*;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LineChartsGenerator {
	private static org.apache.logging.log4j.Logger log = LogManager.getLogger(LineChartsGenerator.class);
	private TimeSeriesHelper tsHelper;
	private DatabaseServiceProvider dbsp;
	private static HashMap<Utils.ChartEntityType, HashMap<Integer, String>> chartsFileNamesMap;
	private static HashMap<Utils.ChartEntityType, HashMap<Object, TimeSeries>> entitiesTimeSeriesMap;
	
	// properties corresponding to configuration entries related to line charts creation
	private ChartRecordsChoice reportRecordsType;
	private ChartWholeAccountChoice wholeAccountChartType;
	private ChartPoliciesTypeChoice policiesChartType;
	private ChartScaleTypeChoice chartScaleType;
	private String dateTimeFrom;
	private String dateTimeTo;
	private int selectedRecordsCount;
	private boolean generatePoliciesCharts;
	private boolean generateOnlySubAccountsCharts;
	private boolean generateSubAccountsUfkCharts;
	private boolean generateSubAccountsUfkChartsWithSubaccounts;
	private boolean useSmoothedLineForChart;
	
	// properties used to set different parameters of visual side of the pie charts
	private final static int MAX_ITEMS_COUNT_FOR_BASELINE = 30;
	private final static int CHART_IMAGE_WIDTH = 780;
	private final static int CHART_IMAGE_HEIGHT = 450;
	private final static Color CHART_BACKGROUND_COLOR = Color.WHITE;
	private final static Color CHART_GRIDLINE_COLOR = Color.DARK_GRAY;
	private final static Font AXIS_LABEL_FONT = new Font("Calibri", Font.TRUETYPE_FONT | Font.BOLD, 15);
	private final static Font AXIS_TICK_LABEL_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 11);
	private final static Font CHART_TITLE_FONT = new Font("Tahoma", Font.BOLD, 19);
	private final static Color CHART_TITLE_COLOR = new Color(0x17375E);
	private final static Stroke PLOT_SERIES_STROKE = new BasicStroke(2.5f);
	private final static Shape PLOT_SERIES_SHAPE = new Ellipse2D.Double(-2.5, -2.5, 5.0, 5.0);
	
	/**
	 * Default class constructor. Initializes all internal class properties.
	 * 
	 * @throws Exception when configuration for charts generation was incorrect (NumberFormatException,
	 * NoSuchElementException and other)
	 */
	public LineChartsGenerator() throws Exception {
		// initialize class internal objects and properties
		this.tsHelper = new TimeSeriesHelper();
		this.dbsp = DatabaseServiceProvider.getInstance();
		chartsFileNamesMap = new HashMap<Utils.ChartEntityType, HashMap<Integer, String>>();
		entitiesTimeSeriesMap = new HashMap<Utils.ChartEntityType, HashMap<Object, TimeSeries>>();
		
		// set the type of DB records collecting (whether to generate report for last n entries
		// or for last n entries)
		String recordsType = ConfigurationProvider.getPropertyValue("selected_records_type");
		if (recordsType.equals("days")) {
			this.reportRecordsType = ChartRecordsChoice.DAYS;
		} else if (recordsType.equals("entries")) {
			this.reportRecordsType = ChartRecordsChoice.ENTRIES;
		} else {
			// in case of error set the default value (instead of throwing exception and
			// terminating execution)
			log.error("Invalid records selection type parameter! Value: " + recordsType);
			log.warn("The default value of " + ChartRecordsChoice.DAYS + " will be set instead.");
			this.reportRecordsType = ChartRecordsChoice.DAYS;
		}
		
		// get the value of n (number of entries or days to read)
		try {
			this.selectedRecordsCount = Integer.parseInt(ConfigurationProvider
					.getPropertyValue("selected_records_number"));
			this.dateTimeFrom = tsHelper.calculateDateTimeFromDays(this.selectedRecordsCount);
			// by default end datetime is set to null (its value can be changed using dedicated setter)
			this.dateTimeTo = null;
		} catch (Exception e) {
			log.error("Could not get the value of selected records number! Exception message: "
					+ e.getMessage());
			throw e;
		}
		
		// get configuration of the chart type for whole account
		try {
			String wholeAccountChartTypeValue = ConfigurationProvider
					.getPropertyValue("chart_generate_whole_account");
			if (wholeAccountChartTypeValue.equals("yes")) {
				this.wholeAccountChartType = ChartWholeAccountChoice.YES;
			} else if (wholeAccountChartTypeValue.equals("policies")) {
				this.wholeAccountChartType = ChartWholeAccountChoice.POLICIES;
			} else if (wholeAccountChartTypeValue.equals("no")) {
				this.wholeAccountChartType = ChartWholeAccountChoice.NO;
			}
		} catch (Exception e) {
			log.error("Could not get the value of whole account chart type! Exception message: "
					+ e.getMessage());
			throw e;
		}
		
		// get configuration of the chart type for policies
		try {
			String policiesChartTypeValue = ConfigurationProvider
					.getPropertyValue("chart_generate_policies_type");
			if (policiesChartTypeValue.equals("joined")) {
				this.policiesChartType = ChartPoliciesTypeChoice.JOINED;
			} else if (policiesChartTypeValue.equals("separated")) {
				this.policiesChartType = ChartPoliciesTypeChoice.SEPARATED;
			} else if (policiesChartTypeValue.equals("subaccounts")) {
				this.policiesChartType = ChartPoliciesTypeChoice.SUBACCOUNTS;
			}
		} catch (Exception e) {
			log.error("Could not get the value of policies chart type! Exception message: " + e.getMessage());
			throw e;
		}
		
		// get configuration of the scale type for combined charts
		try {
			String chartScaleTypeValue = ConfigurationProvider
					.getPropertyValue("chart_combined_charts_scale_type");
			if (chartScaleTypeValue.equals("common")) {
				this.chartScaleType = ChartScaleTypeChoice.COMMON;
			} else if (chartScaleTypeValue.equals("separated")) {
				this.chartScaleType = ChartScaleTypeChoice.SEPARATED;
			} else if (chartScaleTypeValue.equals("logarithmic")) {
				this.chartScaleType = ChartScaleTypeChoice.LOGARITHMIC;
			}
		} catch (Exception e) {
			log.error("Could not get the value of combined charts scale type! Exception message: "
					+ e.getMessage());
			throw e;
		}
		
		// remaining values for policies, sub acccounts, UFK and other charts configuration (boolean values)
		try {
			this.generatePoliciesCharts = Boolean.parseBoolean(ConfigurationProvider
					.getPropertyValue("chart_generate_policies"));
			this.generateOnlySubAccountsCharts = Boolean.parseBoolean(ConfigurationProvider
					.getPropertyValue("chart_generate_subaccounts_without_policies"));
			this.generateSubAccountsUfkCharts = Boolean.parseBoolean(ConfigurationProvider
					.getPropertyValue("chart_generate_subaccounts_ufk"));
			this.generateSubAccountsUfkChartsWithSubaccounts = Boolean.parseBoolean(ConfigurationProvider
					.getPropertyValue("chart_generate_subaccounts_ufk_with_subaccounts"));
			this.useSmoothedLineForChart = Boolean.parseBoolean(ConfigurationProvider
					.getPropertyValue("chart_use_plot_smoothed_line"));
		} catch (Exception e) {
			log.error("Could not get the values of remaining charts generation properties! Exception message: "
					+ e.getMessage());
			throw e;
		}
	}
	
	/**
	 * Helper method which gets the translated message (for user's selected language) of the given property
	 * key (from languages translation hash map). This is essentially the wrapper for ConfigurationProvider
	 * class methods.
	 * 
	 * @param langPropertyKey the name of the property to be translated
	 * @return the text translation corresponding to given property key
	 */
	private static String getPropertyTranslation(String langPropertyKey) {
		String userLanguage = ConfigurationProvider.getPropertyValue("user_language");
		String translation = ConfigurationProvider.getMessageByLanguage(langPropertyKey, userLanguage);
		
		return translation;
	}
	
	/**
	 * Helper method used to convert the given dataset into the list of datasets by splitting multiple
	 * TimeSeries from source dataset into the separate datasets. So the input dataset with x TimeSeries
	 * elements is converted into the list of x datasets (where each dataset holds exactly one TimeSeries
	 * element).
	 * 
	 * @param the source dataset to be converted
	 * @return the list of datasets (TimeSeriesCollection) with single TimeSeries elements
	 */
	private List<XYDataset> convertTimeSeriesCollectionToDatasetsList(XYDataset dataset) {
		// the list of data sets (collections of TimeSeries)
		List<XYDataset> datasetsList = new ArrayList<XYDataset>();
		
		for (int i = 0; i < dataset.getSeriesCount(); i++) {
			TimeSeriesCollection currentDataset = new TimeSeriesCollection();
			
			currentDataset.addSeries(((TimeSeriesCollection) dataset).getSeries(i));
			datasetsList.add(currentDataset);
		}
		
		return datasetsList;
	}
	
	/**
	 * Main method generating TimeSeries (line, plot) chart from provided data and applying visual settings
	 * depending on its type and configuration. Created chart can be in a form of single or multiple plots
	 * with one or more scales and later can be saved as an image.
	 * 
	 * @param chartTitle the title of the created line chart
	 * @param primaryDataset the dataset containing the main data for the chart (TimeSeriesCollection for the
	 * primary line plotted on the chart) (null value not allowed)
	 * @param additionalDataset the dataset with additional, optional data for the chart (TimeSeriesCollection
	 * for the secondary line or lines plotted on the chart) (null value allowed)
	 * @return created line/TimeSeries chart ready to be saved
	 */
	private JFreeChart buildTimeSeriesChart(String chartTitle, XYDataset primaryDataset,
			XYDataset additionalDataset) {
		// list of all datasets to be displayed on the plot (for multiple axes mode)
		List<XYDataset> datasetList = null;
		
		// prepare the data to be displayed on the chart
		if (additionalDataset != null) {
			if (chartScaleType == ChartScaleTypeChoice.COMMON) {
				for (int i = 0; i < additionalDataset.getSeriesCount(); i++) {
					((TimeSeriesCollection) primaryDataset)
							.addSeries(((TimeSeriesCollection) additionalDataset).getSeries(i));
				}
			} else if (chartScaleType == ChartScaleTypeChoice.SEPARATED) {
				datasetList = convertTimeSeriesCollectionToDatasetsList(additionalDataset);
				datasetList.add(0, primaryDataset);
			}
		} else {
			if (chartScaleType == ChartScaleTypeChoice.SEPARATED && primaryDataset.getSeriesCount() > 1) {
				datasetList = convertTimeSeriesCollectionToDatasetsList(primaryDataset);
				primaryDataset = datasetList.get(0);
			}
		}
		
		// create basic TimeSeries chart (subset of XYPlot) from the JFreeChart factory
		JFreeChart chart = ChartFactory.createTimeSeriesChart(null, // title
			getPropertyTranslation("time_axis_label_name"),		// x-axis label (time)
			getPropertyTranslation("value_axis_label_name"),	// y-axis label (value)
			primaryDataset,		// the default dataset for the chart
			true,				// a flag for creating the legend
			true, 				// configure chart to generate tooltips?
			false 				// configure chart to generate URLs?
		);
		
		// get the plot from the chart which corresponds to visual presentation of that chart
		XYPlot plot = (XYPlot) chart.getPlot();
		// for smoothed lines of the chart we need to set a different renderer for the plot
		// (the default is XYLineAndShapeRenderer)
		if (useSmoothedLineForChart) {
			plot.setRenderer(new XYSplineRenderer());
		}
		int chartRecordsCount = plot.getDataset().getItemCount(0);
		
		// for separated scale we have to apply range axis and renderer settings
		// for all datasets (overwriting default settings for the primary dataset)
		if (chartScaleType == ChartScaleTypeChoice.SEPARATED && datasetList != null) {
			int dsIndex = 0;
			for (XYDataset currentDataset : datasetList) {
				// get the paint (color) for the next dataset from the sequence (to have the same
				// color of number axis label and the line drawn on the plot)
				Paint axisPaint = DefaultDrawingSupplier.DEFAULT_PAINT_SEQUENCE[dsIndex];
				
				// set up number axis for each dataset
				NumberAxis numberAxis = new NumberAxis(null);
				numberAxis.setAutoRangeIncludesZero(false);
				// for the first dataset (primary), do not transform the axis label (set the original params)
				if (dsIndex == 0) {
					numberAxis.setLabel(getPropertyTranslation("value_axis_label_name"));
					numberAxis.setLabelPaint(plot.getRangeAxis().getLabelPaint()); // set default label color
				} else {
					numberAxis.setLabel("");
					numberAxis.setLabelPaint(axisPaint);
				}
				numberAxis.setNumberFormatOverride(new DecimalFormat("###"));
				numberAxis.setLabelFont(AXIS_LABEL_FONT);
				numberAxis.setTickLabelPaint(axisPaint);
				numberAxis.setTickLabelFont(AXIS_TICK_LABEL_FONT);
				
				// set dataset and the range (number) axis on the plot
				plot.setDataset(dsIndex, currentDataset);
				plot.setRangeAxis(dsIndex, numberAxis);
				plot.mapDatasetToRangeAxis(dsIndex, dsIndex);
				
				// create the renderer which displays the line and (optionally) base shapes
				XYLineAndShapeRenderer renderer = null;
				if (useSmoothedLineForChart) {
					// if smoothed lines are enabled, create the renderer which uses natural cubic splines
					renderer = new XYSplineRenderer();
				} else {
					renderer = new XYLineAndShapeRenderer();
				}
				
				// set custom stroke (thickness of the plot line)
				renderer.setSeriesStroke(0, PLOT_SERIES_STROKE);
				// by default do not display the base shapes
				renderer.setBaseShapesVisible(false);
				
				// if the number of items (records on range axis) is small enough, set base shapes visible
				// for the plot - these are dots marking particular points on the plot
				if (chartRecordsCount <= MAX_ITEMS_COUNT_FOR_BASELINE) {
					// set the shape to be in the form of a small circle
					renderer.setBaseShapesVisible(true);
					renderer.setSeriesShape(0, PLOT_SERIES_SHAPE);
				}
				
				plot.setRenderer(dsIndex, renderer);
				dsIndex++;
			} // for (XYDataset currentDataset : datasetList) {
		} else {
			// for common scale (or for separated but with one plot line only) apply similar settings
			// as in case of separated scale - stroke and (optionally) base shapes for the displayed
			// plot lines
			
			// create the renderer which displays the line and (optionally) base shapes
			XYLineAndShapeRenderer renderer = null;
			if (useSmoothedLineForChart) {
				// if smoothed lines are enabled, get XYSplineRenderer (which uses cubic splines)
				renderer = (XYSplineRenderer) plot.getRenderer();
			} else {
				renderer = (XYLineAndShapeRenderer) plot.getRenderer();
			}
			
			// by default do not display the base shapes
			renderer.setBaseShapesVisible(false);
			
			// set custom stroke (thickness of the plot line)
			for (int i = 0; i < plot.getSeriesCount(); i++) {
				renderer.setSeriesStroke(i, PLOT_SERIES_STROKE);
			}
			if (chartRecordsCount <= MAX_ITEMS_COUNT_FOR_BASELINE) {
				renderer.setBaseShapesVisible(true);
				// set the shape to be in the form of a small circle
				for (int i = 0; i < plot.getSeriesCount(); i++) {
					renderer.setSeriesShape(i, PLOT_SERIES_SHAPE);
				}
			}
		} // if (chartScaleType == ChartScaleTypeChoice.SEPARATED && datasetList != null) {
		
		// prepare and display the text for the chart title
		TextTitle chartTextTitle = new TextTitle(chartTitle, CHART_TITLE_FONT);
		chartTextTitle.setPaint(CHART_TITLE_COLOR);
		chart.setTitle(chartTextTitle);
		chart.setBackgroundPaint(CHART_BACKGROUND_COLOR);
		
		// set the colors for the chart
		plot.setBackgroundPaint(CHART_BACKGROUND_COLOR);
		plot.setRangeGridlinePaint(CHART_GRIDLINE_COLOR);
		plot.setDomainGridlinePaint(CHART_GRIDLINE_COLOR);
		// set the offset for both axes (gap between the data area and the axes)
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		
		// build the main range (value) axis
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setNumberFormatOverride(new DecimalFormat("###"));
		rangeAxis.setLabelFont(AXIS_LABEL_FONT);
		rangeAxis.setAutoRange(true);
		rangeAxis.setAutoTickUnitSelection(true);
		
		// build the domain (date) axis
		DateAxis domainAxis = (DateAxis) plot.getDomainAxis();
		domainAxis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));
		domainAxis.setLabelFont(AXIS_LABEL_FONT);
		domainAxis.setVerticalTickLabels(true);
		domainAxis.setAutoRange(true);
		domainAxis.setAutoTickUnitSelection(true);
		
		return chart;
	}
	
	/**
	 * Helper method returning the list of TimeSeries objects for each sub account belonging to given policy,
	 * identified by its entity.
	 * 
	 * @param policy the Policy entity for which to get the list of sub accounts TimeSeries
	 * @return the list of TimeSeries objects for sub accounts in given policy
	 * @throws Exception when retrieving TimeSeries objects for SubAccount list failed or if another error
	 * occured
	 */
	private List<TimeSeries> getSubAccountTimeSeriesListForPolicy(Policy policy) throws Exception {
		List<TimeSeries> subAccountTimeSeriesList = null;
		TimeSeries subAccountSeries = null;
		List<SubAccount> subAccountList = null;
		// the internal map linking sub account (by id) with its TimeSeries object
		HashMap<Object, TimeSeries> timeSeriesMap = new HashMap<Object, TimeSeries>();
		
		// get the list of all sub accounts for given policy
		try {
			subAccountList = dbsp.getSubAccountListByPolicy(policy);
		} catch (Exception e) {
			log.error("Could not get sub accounts for Policy with id = " + policy.getIdPolicy()
					+ "! Exception message: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		if (subAccountList != null && !subAccountList.isEmpty()) {
			subAccountTimeSeriesList = new ArrayList<TimeSeries>();
			try {
				// get the TimeSeries object for each sub account based on user's settings
				for (SubAccount subAccount : subAccountList) {
					if (this.reportRecordsType == ChartRecordsChoice.DAYS) {
						subAccountSeries = tsHelper.getSubAccountHistoryRecordsForChart(
								subAccount.getIdSubAccount(), this.dateTimeFrom, null);
					} else {
						subAccountSeries = tsHelper.getSubAccountHistoryRecordsForChart(
								subAccount.getIdSubAccount(), this.selectedRecordsCount);
					}
					
					subAccountSeries.setKey(subAccount.getSubAccountName());
					subAccountTimeSeriesList.add(subAccountSeries);
					
					// save TimeSeries object for the current sub account in the map
					timeSeriesMap.put(new Integer(subAccount.getIdSubAccount()), subAccountSeries);
				}
				
				// save retrieved TimeSeries items map for sub accounts list so that it can be accessed
				// again without the need to rerun the current method
				if (!entitiesTimeSeriesMap.containsKey(Utils.ChartEntityType.SUBACCOUNTS)) {
					entitiesTimeSeriesMap.put(Utils.ChartEntityType.SUBACCOUNTS, timeSeriesMap);
				} else {
					entitiesTimeSeriesMap.get(Utils.ChartEntityType.SUBACCOUNTS).putAll(timeSeriesMap);
				}
			} catch (Exception e) {
				log.error("An exception occured while trying to get SubAccountHistory records! "
						+ e.getMessage());
				throw e;
			}
		}
		
		return subAccountTimeSeriesList;
	}
	
	/**
	 * Helper method returning the list of TimeSeries objects for each UFK belonging to given sub account,
	 * identified by its entity.
	 * 
	 * @param subAccount the SubAccount entity for which to get the list of UFKs TimeSeries
	 * @return the list of TimeSeries objects for UFKs in given sub account
	 * @throws Exception when retrieving TimeSeries objects for UFK (SubAccountBalanceHistory) list failed or
	 * if another error occured
	 */
	private List<TimeSeries> getSubAccountBalanceHistoryTimeSeriesListForSubAccount(SubAccount subAccount)
			throws Exception {
		List<TimeSeries> subAccountUFKTimeSeriesList = null;
		TimeSeries subAccountUFKSeries = null;
		List<String> ufkNameList = null;
		// the internal map linking sub account UFK (by pseudo-id) with its TimeSeries object
		HashMap<Object, TimeSeries> timeSeriesMap = new HashMap<Object, TimeSeries>();
		
		// get the list of all UFKs for given sub account
		try {
			ufkNameList = dbsp.getSubAccountBalanceHistoryUFKNameListBySubAccount(subAccount);
		} catch (Exception e) {
			log.error("Could not get UFK names for SubAccount with id = " + subAccount.getIdSubAccount()
					+ "! Exception message: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		if (ufkNameList != null && !ufkNameList.isEmpty()) {
			subAccountUFKTimeSeriesList = new ArrayList<TimeSeries>();
			try {
				// get the TimeSeries object for each UFK based on user's settings
				for (String ufkName : ufkNameList) {
					if (this.reportRecordsType == ChartRecordsChoice.DAYS) {
						subAccountUFKSeries = tsHelper.getSubAccountBalanceHistoryRecordsForChart(
								subAccount.getIdSubAccount(), ufkName, this.dateTimeFrom, this.dateTimeTo);
					} else {
						subAccountUFKSeries = tsHelper.getSubAccountBalanceHistoryRecordsForChart(
								subAccount.getIdSubAccount(), ufkName, this.selectedRecordsCount);
					}
					
					subAccountUFKSeries.setKey(ufkName);
					subAccountUFKTimeSeriesList.add(subAccountUFKSeries);
					
					// save TimeSeries object for the current sub account UFK in the map
					// since UFKs are not identified by unique ids, we create the key as the String below
					Object timeSeriesKey = subAccount.getIdSubAccount() + "_" + ufkName;
					timeSeriesMap.put(timeSeriesKey, subAccountUFKSeries);
				}
				
				// save retrieved TimeSeries items map for sub accounts UFK list so that it can be
				// accessed again without the need to rerun the current method
				if (!entitiesTimeSeriesMap.containsKey(Utils.ChartEntityType.UFKS)) {
					entitiesTimeSeriesMap.put(Utils.ChartEntityType.UFKS, timeSeriesMap);
				} else {
					entitiesTimeSeriesMap.get(Utils.ChartEntityType.UFKS).putAll(timeSeriesMap);
				}
			} catch (Exception e) {
				log.error("An exception occured while trying to get SubAccountBalanceHistory records! "
						+ e.getMessage());
				throw e;
			}
		}
		
		return subAccountUFKTimeSeriesList;
	}
	
	/**
	 * Method creating and returning the dataset for EntryLog records list. This dataset is later used to
	 * build the chart for the whole account.
	 * 
	 * @return the dataset (TimeSeriesCollection) of single TimeSeries for EntryLog records list
	 * @throws Exception when retrieving TimeSeries for EntryLog records list failed
	 */
	private TimeSeriesCollection getDatasetForEntryLogList() throws Exception {
		// the data set (collection of TimeSeries) for all EntryLog balances (whole account)
		TimeSeriesCollection entryLogDataset = new TimeSeriesCollection();
		TimeSeries entryLogSeries;
		
		try {
			// get the TimeSeries object of the whole account based on user's settings
			if (this.reportRecordsType == ChartRecordsChoice.DAYS) {
				entryLogSeries = tsHelper.getWholeAccountHistoryRecordsForChart(this.dateTimeFrom,
						this.dateTimeTo);
			} else {
				entryLogSeries = tsHelper.getWholeAccountHistoryRecordsForChart(this.selectedRecordsCount);
			}
			
			String accountName = getPropertyTranslation("account_name");
			entryLogSeries.setKey(accountName);
			entryLogDataset.addSeries(entryLogSeries);
			
			// save retrieved TimeSeries for EntryLog records list in the internal map so that it can
			// be accessed again without the need to rerun the current method
			HashMap<Object, TimeSeries> timeSeriesMap = new HashMap<Object, TimeSeries>();
			// since there is only one TimeSeries item for EntryLog list, the id (the key of internal
			// HashMap) is just set to 1
			timeSeriesMap.put(new Integer(1), entryLogSeries);
			entitiesTimeSeriesMap.put(Utils.ChartEntityType.ACCOUNT, timeSeriesMap);
		} catch (Exception e) {
			log.error("Could not get the list of EntryLog records! Exception message: " + e.getMessage());
			throw e;
		}
		
		return entryLogDataset;
	}
	
	/**
	 * Method creating and returning the dataset for policies (PolicyHistory records) list. The dataset
	 * contains one or more TimeSeries objects (identified by policy name), where each TimeSeries item is
	 * bound to exactly one policy. This dataset is later used to build the chart(s) for all policies.
	 * 
	 * @return the dataset (TimeSeriesCollection) of multiple TimeSeries for all policies list
	 * @throws Exception when retrieving TimeSeries objects for Policy list failed or if another error occured
	 */
	private TimeSeriesCollection getDatasetForPolicyList() throws Exception {
		List<Policy> policyList = new ArrayList<Policy>();
		// the data set (collection of TimeSeries) for all PolicyHistory balances
		TimeSeriesCollection policyDataset = new TimeSeriesCollection();
		// the internal map linking policy (by id) with its TimeSeries object
		HashMap<Object, TimeSeries> timeSeriesMap = new HashMap<Object, TimeSeries>();
		
		try {
			policyList = dbsp.getAllPolicyList();
		} catch (Exception e) {
			log.error("Could not get the list of all policies! Exception message: " + e.getMessage());
			throw e;
		}
		
		try {
			for (Policy policy : policyList) {
				TimeSeries policySeries;
				
				// get the TimeSeries object for the given Policy based on user's settings
				try {
					if (this.reportRecordsType == ChartRecordsChoice.DAYS) {
						policySeries = tsHelper.getPolicyHistoryRecordsForChart(policy.getIdPolicy(),
								this.dateTimeFrom, this.dateTimeTo);
					} else {
						policySeries = tsHelper.getPolicyHistoryRecordsForChart(policy.getIdPolicy(),
								this.selectedRecordsCount);
					}
				} catch (Exception e) {
					log.error("Could not get the list of PolicyHistory records! Exception message: "
							+ e.getMessage());
					throw e;
				}
				
				String policyName = getPropertyTranslation("policy_name");
				// the key looks like "Polisa EFExxx..."
				policySeries.setKey(policyName + " " + policy.getPolicyNumber());
				policySeries.setDescription(policy.getPolicyNumber());
				policyDataset.addSeries(policySeries);
				
				// save TimeSeries object for the current policy in the map
				timeSeriesMap.put(new Integer(policy.getIdPolicy()), policySeries);
			} // for (Policy policy : policyList) {
			
			// save retrieved TimeSeries items map for policies list so that it can be accessed
			// again without the need to rerun the current method
			entitiesTimeSeriesMap.put(Utils.ChartEntityType.POLICIES, timeSeriesMap);
		} catch (Exception e) {
			log.error("Could not generate the dataset for PolicyHistory list! Exception message: "
					+ e.getMessage());
			throw e;
		}
		
		return policyDataset;
	}
	
	/**
	 * Method creating and returning the list of datasets for sub accounts (SubAccountHistory records) list.
	 * Each element of the returned list corresponds to one policy (identified by policy name) and contains
	 * TimeSeries objects list (each TimeSeries item is bound to exactly one sub account) for sub accounts
	 * belonging to that policy. This list of datasets is later used to build the charts for all sub accounts.
	 * 
	 * @return the list of datasets (TimeSeriesCollection items) containing multiple TimeSeries for all sub
	 * accounts
	 * @throws Exception when retrieving policies list or TimeSeries objects for SubAccount list failed
	 */
	private List<XYDataset> getDatasetsForSubAccountList() throws Exception {
		// the list of all data sets (collections of TimeSeries) for every sub account balance (per each
		// policy)
		List<XYDataset> subAccountDatasets = new ArrayList<XYDataset>();
		List<Policy> policyList = new ArrayList<Policy>();
		
		try {
			policyList = dbsp.getAllPolicyList();
		} catch (Exception e) {
			log.error("Could not get the list of all policies! Exception message: " + e.getMessage());
			throw e;
		}
		
		List<TimeSeries> subAccountTimeSeriesList;
		for (Policy policy : policyList) {
			TimeSeriesCollection subAccountDataset = new TimeSeriesCollection();
			// get the TimeSeries list for sub accounts
			try {
				subAccountTimeSeriesList = getSubAccountTimeSeriesListForPolicy(policy);
			} catch (Exception e) {
				log.error("An exception occured while trying to get SubAccount TimeSeries list! "
						+ e.getMessage());
				throw e;
			}
			
			// put all TimeSeries objects into one dataset
			if (subAccountTimeSeriesList != null && !subAccountTimeSeriesList.isEmpty()) {
				for (TimeSeries ts : subAccountTimeSeriesList) {
					subAccountDataset.addSeries(ts);
				}
			}
			
			// the dataset is identified by its groupId - policy number
			subAccountDataset.setGroup(new DatasetGroup(policy.getPolicyNumber()));
			subAccountDatasets.add(subAccountDataset);
		}
		
		return subAccountDatasets;
	}
	
	/**
	 * Method creating and returning the list of datasets for sub accounts UFKs (SubAccountBalanceHistory
	 * records) list. Each element of the returned list corresponds to one sub account (identified by sub
	 * account name) and contains TimeSeries objects list (each TimeSeries item is bound to exactly one UFK)
	 * for UFKs belonging to that sub account. This list of datasets is later used to build the charts for all
	 * UFKs.
	 * 
	 * @return the list of datasets (TimeSeriesCollection items) containing multiple TimeSeries for all UFKs
	 * @throws Exception when retrieving sub accounts list or TimeSeries objects for UFK list failed
	 */
	private List<XYDataset> getDatasetsForSubAccountBalanceHistoryList() throws Exception {
		// the list of all data sets (collections of TimeSeries) for every sub account balance history
		// (UFK) (per each sub account)
		List<XYDataset> subAccountUFKDatasets = new ArrayList<XYDataset>();
		List<SubAccount> subAccountList = new ArrayList<SubAccount>();
		
		try {
			subAccountList = dbsp.getAllSubAccountList();
		} catch (Exception e) {
			log.error("Could not get the list of all sub accounts! Exception message: " + e.getMessage());
			throw e;
		}
		
		List<TimeSeries> subAccountUFKTimeSeriesList;
		for (SubAccount subAccount : subAccountList) {
			TimeSeriesCollection subAccountUFKDataset = new TimeSeriesCollection();
			// get the TimeSeries list for sub accounts UFK
			try {
				subAccountUFKTimeSeriesList = getSubAccountBalanceHistoryTimeSeriesListForSubAccount(subAccount);
			} catch (Exception e) {
				log.error("An exception occured while trying to get SubAccountBalanceHistory TimeSeries list! "
						+ e.getMessage());
				throw e;
			}
			
			// put all TimeSeries objects into one dataset
			if (subAccountUFKTimeSeriesList != null && !subAccountUFKTimeSeriesList.isEmpty()) {
				for (TimeSeries ts : subAccountUFKTimeSeriesList) {
					subAccountUFKDataset.addSeries(ts);
				}
			}
			
			// the dataset is identified by its groupId - sub account name
			subAccountUFKDataset.setGroup(new DatasetGroup(subAccount.getSubAccountName()));
			subAccountUFKDatasets.add(subAccountUFKDataset);
		}
		
		return subAccountUFKDatasets;
	}
	
	/**
	 * Method creating the line/TimeSeries chart for the whole Aegon account. It is possible that the chart is
	 * not generated at all, as it may be skipped by the user. Optionally it may contain also the plots for
	 * policies in the account. Generated chart is saved as an image file, path to which can be obtained from
	 * the method {@link #getImageFilePathToWholeAccountChart()}.
	 * 
	 * @throws Exception if generating line chart for whole account (with or without policies) failed
	 */
	public void generateChartForWholeAccount() throws Exception {
		// an internal HashMap which maps path to the file (created chart image)
		// under the specific key/id
		HashMap<Integer, String> chartFilePathMap = new HashMap<Integer, String>();
		
		// check if whole account chart should be produced (user can disable such chart)
		if (this.wholeAccountChartType == ChartWholeAccountChoice.NO) {
			log.debug("[LineChartsGenerator] Chart creation for the whole account is skipped by user.");
			
			// since no chart is created, we set the whole map as null
			chartsFileNamesMap.put(Utils.ChartEntityType.ACCOUNT, null);
			// same situation for TimeSeries map
			entitiesTimeSeriesMap.put(Utils.ChartEntityType.ACCOUNT, null);
			return;
		}
		
		try {
			JFreeChart chart = null;
			TimeSeriesCollection accountDataset = this.getDatasetForEntryLogList();
			TimeSeriesCollection policiesDataset = null;
			String chartTitle;
			
			// if the chart is generated with policies balances, provide policies data to the chart
			if (this.wholeAccountChartType == ChartWholeAccountChoice.POLICIES) {
				chartTitle = getPropertyTranslation("whole_account_policies_chart_title");
				policiesDataset = this.getDatasetForPolicyList();
			} else {
				chartTitle = getPropertyTranslation("whole_account_chart_title");
			}
			
			// create the chart with provided data and save it in the appropriate file
			try {
				chart = this.buildTimeSeriesChart(chartTitle, accountDataset, policiesDataset);
				String chartFilePath = Utils.getChartImageFilePath("whole_account", null, false);
				
				// since there is only one chart for whole account, the id (the key of
				// internal HashMap) is just set to 1
				chartFilePathMap.put(new Integer(1), chartFilePath);
				chartsFileNamesMap.put(Utils.ChartEntityType.ACCOUNT, chartFilePathMap);
				
				ChartUtilities.saveChartAsPNG(new File(chartFilePath), chart, CHART_IMAGE_WIDTH,
						CHART_IMAGE_HEIGHT);
				log.info("[LineChartsGenerator] Chart for the whole account generated successfully.");
			} catch (Exception e) {
				throw e;
			}
		} catch (Exception e) {
			log.error("[LineChartsGenerator] Generating chart for the whole account failed! Exception message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Method creating the line/TimeSeries charts for all policies in the account. It is possible that the
	 * charts are not generated at all, as they may be skipped by the user. Optionally charts my contain also
	 * the plots for sub accounts in each policy. The number of charts created corresponds to the number of
	 * policies in the account if the charts are generated separately, otherwise it is one chart. Generated
	 * charts (or single chart) are saved as the image files, paths to which can be obtained from the method
	 * {@link #getImageFilePathsToPoliciesCharts()}.
	 * 
	 * @throws Exception if generating some of the line charts for policies (with or without sub accounts)
	 * failed
	 */
	public void generateChartsForAllPolicies() throws Exception {
		// an internal HashMap which maps paths to the files (created charts images)
		// under the specific key/id
		HashMap<Integer, String> chartFilePathsMap = new HashMap<Integer, String>();
		
		// check if policies chart(s) should be produced (user can disable such chart)
		if (!this.generatePoliciesCharts) {
			log.debug("[LineChartsGenerator] Charts creation for policies in the account is skipped by user.");
			
			// since no chart is created, we set the whole map as null
			chartsFileNamesMap.put(Utils.ChartEntityType.POLICIES, null);
			return;
		}
		
		try {
			JFreeChart chart = null;
			TimeSeriesCollection policiesDataset = this.getDatasetForPolicyList();
			String chartTitle;
			String chartFilePath;
			
			// create one chart with all policies balances displayed on it
			if (this.policiesChartType == ChartPoliciesTypeChoice.JOINED) {
				chartTitle = getPropertyTranslation("all_policies_joined_chart_title");
				
				// create the chart with provided data and save it in the appropriate file
				try {
					chart = this.buildTimeSeriesChart(chartTitle, policiesDataset, null);
					chartFilePath = Utils.getChartImageFilePath("policies_joined", null, false);
					
					// in this case there is only one chart for policies, so the id
					// (the key of internal HashMap) is just set to 1
					chartFilePathsMap.put(new Integer(1), chartFilePath);
					chartsFileNamesMap.put(Utils.ChartEntityType.POLICIES, chartFilePathsMap);
					
					ChartUtilities.saveChartAsPNG(new File(chartFilePath), chart, CHART_IMAGE_WIDTH,
							CHART_IMAGE_HEIGHT);
					log.info("[LineChartsGenerator] Chart for all policies in the account generated successfully.");
				} catch (Exception e) {
					throw e;
				}
			} else {
				// create the separate chart for each policy (with sub accounts or not)
				List<XYDataset> policiesDatasetList = this
						.convertTimeSeriesCollectionToDatasetsList(policiesDataset);
				List<XYDataset> subAccountsDatasetList = null;
				TimeSeriesCollection subAccountsDataset = null;
				
				String chartType = "policies_separated";
				if (this.policiesChartType == ChartPoliciesTypeChoice.SUBACCOUNTS) {
					subAccountsDatasetList = getDatasetsForSubAccountList();
					chartType = "policies_subaccounts";
				}
				
				for (XYDataset policyDataset : policiesDatasetList) {
					String policyNumber = ((TimeSeriesCollection) policyDataset).getSeries(0)
							.getDescription();
					Integer policyId = new Integer(dbsp.getPolicyByPolicyNumber(policyNumber).getIdPolicy());
					
					// set up chart title and the dataset for sub account(s) belonging to the currently
					// processed policy
					if (this.policiesChartType == ChartPoliciesTypeChoice.SEPARATED) {
						chartTitle = getPropertyTranslation("all_policies_separated_chart_title");
						chartTitle += " " + policyNumber;
					} else {
						chartTitle = getPropertyTranslation("all_policies_subaccounts_chart_title");
						for (XYDataset currentDataset : subAccountsDatasetList) {
							if (currentDataset.getGroup().getID() == policyNumber) {
								subAccountsDataset = (TimeSeriesCollection) currentDataset;
								break;
							}
						}
					}
					
					// create the chart with provided data and save it in the appropriate file
					try {
						chart = this.buildTimeSeriesChart(chartTitle, policyDataset, subAccountsDataset);
						chartFilePath = Utils.getChartImageFilePath(chartType, policyNumber, false);
						// save the path to the file in the map
						chartFilePathsMap.put(policyId, chartFilePath);
						
						ChartUtilities.saveChartAsPNG(new File(chartFilePath), chart, CHART_IMAGE_WIDTH,
								CHART_IMAGE_HEIGHT);
						log.info("[LineChartsGenerator] Chart for policy <" + policyNumber
								+ "> generated successfully.");
					} catch (Exception e) {
						throw e;
					}
				} // for (XYDataset policyDataset : policiesDatasetList) {
				
				// save the result map with all policies charts files paths
				chartsFileNamesMap.put(Utils.ChartEntityType.POLICIES, chartFilePathsMap);
			} // if (this.policiesChartType == ChartPoliciesTypeChoice.JOINED) {
		} catch (Exception e) {
			log.error("[LineChartsGenerator] Generating chart(s) for policies in the account failed! Exception message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Method creating the line/TimeSeries charts for all sub accounts in every policy. It is possible that
	 * the charts are not generated at all, as they may be skipped by the user. The number of charts created
	 * corresponds to the number of policies in the account and each chart displays plots for sub accounts in
	 * the given policy. Generated charts are saved as the image files, paths to which can be obtained from
	 * the method {@link #getImageFilePathsToSubAccountsCharts()}.
	 * 
	 * @throws Exception if generating some of the line charts for sub accounts failed
	 */
	public void generateChartsForSubAccountsWithoutPolicies() throws Exception {
		// an internal HashMap which maps paths to the files (created charts images)
		// under the specific key/id
		HashMap<Integer, String> chartFilePathsMap = new HashMap<Integer, String>();
		
		// check if sub accounts charts should be produced (user can disable such charts)
		if (!this.generateOnlySubAccountsCharts) {
			log.debug("[LineChartsGenerator] Charts creation for sub accounts without policies is skipped by user.");
			
			// since no chart is created, we set the whole map as null
			chartsFileNamesMap.put(Utils.ChartEntityType.SUBACCOUNTS, null);
			return;
		}
		
		// these charts (for sub accounts only) cannot be also created if the setting below
		// is active (this code should never be executed)
		if (this.policiesChartType == ChartPoliciesTypeChoice.SUBACCOUNTS) {
			log.warn("[LineChartsGenerator] Charts creation for sub accounts without policies is skipped "
					+ "because of incompatible settings detected! Sub accounts charts are already "
					+ "generated with their policies.");
			
			// since no chart is created here as well, we set the whole map as null
			chartsFileNamesMap.put(Utils.ChartEntityType.SUBACCOUNTS, null);
			return;
		}
		
		JFreeChart chart = null;
		String chartTitle;
		String chartFilePath;
		List<XYDataset> subAccountsDatasetList = getDatasetsForSubAccountList();
		
		try {
			// create the separate chart for each sub account (taken from its dataset)
			for (XYDataset subAccountsDataset : subAccountsDatasetList) {
				String policyNumber = subAccountsDataset.getGroup().getID();
				Integer policyId = new Integer(dbsp.getPolicyByPolicyNumber(policyNumber).getIdPolicy());
				
				chartTitle = getPropertyTranslation("subaccounts_without_policies_chart_title");
				chartTitle += " " + policyNumber;
				
				// create the chart with provided data and save it in the appropriate file
				try {
					chart = this.buildTimeSeriesChart(chartTitle, subAccountsDataset, null);
					chartFilePath = Utils.getChartImageFilePath("subaccounts_only", policyNumber, false);
					// save the path to the file in the map
					chartFilePathsMap.put(policyId, chartFilePath);
					
					ChartUtilities.saveChartAsPNG(new File(chartFilePath), chart, CHART_IMAGE_WIDTH,
							CHART_IMAGE_HEIGHT);
					log.info("[LineChartsGenerator] Chart for sub accounts from policy <" + policyNumber
							+ "> generated successfully.");
				} catch (Exception e) {
					throw e;
				}
			}
			
			// save the result map with all sub accounts charts files paths
			chartsFileNamesMap.put(Utils.ChartEntityType.SUBACCOUNTS, chartFilePathsMap);
		} catch (Exception e) {
			log.error("[LineChartsGenerator] Generating charts for sub accounts (without policies) failed! Exception message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Method creating the line/TimeSeries charts for all UFKs in every sub account. It is possible that the
	 * charts are not generated at all, as they may be skipped by the user. The number of charts created
	 * corresponds to the number of sub accounts and each chart displays plots for UFKs in the given sub
	 * account. Optionally they my contain also the plots for sub account which contains given UFKs. Generated
	 * charts are saved as the image files, paths to which can be obtained from the method
	 * {@link #getImageFilePathsToSubAccountsUFKCharts()}.
	 * 
	 * @throws Exception if generating some of the line charts for UFKs (with or without sub accounts) failed
	 */
	public void generateChartsForSubAccountsUFK() throws Exception {
		// an internal HashMap which maps paths to the files (created charts images)
		// under the specific key/id
		HashMap<Integer, String> chartFilePathsMap = new HashMap<Integer, String>();
		
		// check if UFK charts should be produced (user can disable such charts)
		if (!this.generateSubAccountsUfkCharts) {
			log.debug("[LineChartsGenerator] Charts creation for sub accounts UFKs is skipped by user.");
			
			// since no chart is created, we set the whole map as null
			chartsFileNamesMap.put(Utils.ChartEntityType.UFKS, null);
			return;
		}
		
		JFreeChart chart = null;
		String chartTitle;
		String chartFilePath;
		List<XYDataset> subAccountsDatasetList = null;
		List<XYDataset> subAccountsUFKDatasetList = getDatasetsForSubAccountBalanceHistoryList();
		List<SubAccount> subAccountList = new ArrayList<SubAccount>();
		
		// get the list of all sub accounts (needed to know their ids to name the chart files uniquely)
		try {
			subAccountList = dbsp.getAllSubAccountList();
		} catch (Exception e) {
			log.error("Could not get the list of all sub accounts! Exception message: " + e.getMessage());
			throw e;
		}
		
		// rebuild the list of sub accounts datasets (grouped by policy) to have the list of flat datasets
		// (so that one dataset holds exactly one collection of TimeSeries for given sub account)
		subAccountsDatasetList = new ArrayList<XYDataset>();
		for (XYDataset subAccountDataset : getDatasetsForSubAccountList()) {
			List<XYDataset> splitSubAccountsDatasets = this
					.convertTimeSeriesCollectionToDatasetsList(subAccountDataset);
			subAccountsDatasetList.addAll(splitSubAccountsDatasets);
		}
		
		try {
			int dsIndex = 0; // current dataset index
			String chartType;
			
			// traverse through all UFKs datasets
			for (XYDataset subAccountsUFKDataset : subAccountsUFKDatasetList) {
				XYDataset primaryDataset = null;
				XYDataset secondaryDataset = null;
				
				String subAccountId = String.valueOf((subAccountList.get(dsIndex).getIdSubAccount()));
				// if sub accounts are also displayed on the chart get the dataset for current sub account
				if (this.generateSubAccountsUfkChartsWithSubaccounts) {
					chartTitle = getPropertyTranslation("subaccounts_ufk_with_subaccount_chart_title");
					chartType = "subaccounts_ufk_with_subaccount";
					XYDataset subAccountDataset = subAccountsDatasetList.get(dsIndex);
					
					// in such case the primary dataset is sub account, and addditional one
					// holds all UFK datasets for this sub account
					primaryDataset = subAccountDataset;
					secondaryDataset = subAccountsUFKDataset;
				} else {
					String subAccountName = subAccountsUFKDataset.getGroup().getID();
					chartTitle = getPropertyTranslation("subaccounts_ufk_chart_title");
					chartTitle += " " + subAccountName;
					chartType = "subaccounts_ufk";
					
					// if no sub account is displayed, only UFK dataset (with all UFKs combined) is used
					primaryDataset = subAccountsUFKDataset;
				}
				
				// create the chart with provided data and save it in the appropriate file
				try {
					chart = this.buildTimeSeriesChart(chartTitle, primaryDataset, secondaryDataset);
					chartFilePath = Utils.getChartImageFilePath(chartType, subAccountId, false);
					// save the path to the file in the map
					chartFilePathsMap.put(new Integer(subAccountId), chartFilePath);
					
					ChartUtilities.saveChartAsPNG(new File(chartFilePath), chart, CHART_IMAGE_WIDTH,
							CHART_IMAGE_HEIGHT);
					log.info("[LineChartsGenerator] Chart for UFKs from sub account <ID=" + subAccountId
							+ "> generated successfully.");
				} catch (Exception e) {
					throw e;
				}
				
				dsIndex++;
			} // for (XYDataset subAccountsUFKDataset : subAccountsUFKDatasetList) {
			
			// save the result map with all UFKs charts files paths
			chartsFileNamesMap.put(Utils.ChartEntityType.UFKS, chartFilePathsMap);
		} catch (Exception e) {
			log.error("[LineChartsGenerator] Generating charts for sub accounts UFKs failed! Exception message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Main method creating all possible line charts: for the whole account, policies, sub accounts and UFKs
	 * stored in the database. It is the main interface and the main purpose of the LineChartsGenerator class.
	 * The method is essentially the wrapper for other public methods generating more specific line charts for
	 * mentioned entities. Every chart created is saved as a separate image file, path to which is available
	 * through the public methods getImageFilePath...().
	 * 
	 * @throws Exception if generating any of the line charts failed
	 */
	public void generateAllCharts() throws Exception {
		try {
			this.generateChartForWholeAccount();
			this.generateChartsForAllPolicies();
			this.generateChartsForSubAccountsWithoutPolicies();
			this.generateChartsForSubAccountsUFK();
			
			log.info("[LineChartsGenerator] All possible line charts have been generated successfully.");
		} catch (Exception e) {
			log.error("[LineChartsGenerator] Generating one or more line charts for all items failed!"
					+ " Exception message: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Gets the path to the image file with generated chart for whole account.
	 * 
	 * @return path to the image file with created chart of the whole account or null if the chart was not
	 * generated (if it was skipped, generating chart was not run or an error has earlier occured)
	 */
	public String getImageFilePathToWholeAccountChart() {
		String imageFilePath = null;
		
		if (!chartsFileNamesMap.containsKey(Utils.ChartEntityType.ACCOUNT)) {
			log.error("[LineChartsGenerator] The chart for whole account has not been created yet or the process has failed!");
		} else {
			if (chartsFileNamesMap.get(Utils.ChartEntityType.ACCOUNT) != null) {
				imageFilePath = chartsFileNamesMap.get(Utils.ChartEntityType.ACCOUNT).get(new Integer(1));
			}
		}
		
		return imageFilePath;
	}
	
	/**
	 * Gets the HashMap of images files (mapped by key being policy id) with generated charts for policies.
	 * Notice: if there is generated only one chart with all policies (that is when policies generation type
	 * is set to ChartPoliciesTypeChoice.JOINED), then the key of the policies is equal to 1.
	 * 
	 * @return HashMap mapping the id of the policy to the image file with created policy chart or null if the
	 * charts were not generated (if generating charts was not run or an error has earlier occured)
	 */
	public HashMap<Integer, String> getImageFilePathsToPoliciesCharts() {
		HashMap<Integer, String> imageFilePathsMap = null;
		
		if (!chartsFileNamesMap.containsKey(Utils.ChartEntityType.POLICIES)) {
			log.error("[LineChartsGenerator] The charts for policies have not been created yet or the process has failed!");
		} else {
			imageFilePathsMap = chartsFileNamesMap.get(Utils.ChartEntityType.POLICIES);
		}
		
		return imageFilePathsMap;
	}
	
	/**
	 * Gets the HashMap of images files (mapped by key being policy id) with generated charts for sub accounts
	 * (per policy containing these sub accounts).
	 * 
	 * @return HashMap mapping the id of the policy to the image file with created sub accounts chart (per
	 * each policy) or null if the charts were not generated (if generating charts was not run or an error has
	 * earlier occured)
	 */
	public HashMap<Integer, String> getImageFilePathsToSubAccountsCharts() {
		HashMap<Integer, String> imageFilePathsMap = null;
		
		if (!chartsFileNamesMap.containsKey(Utils.ChartEntityType.SUBACCOUNTS)) {
			log.error("[LineChartsGenerator] The charts for sub accounts have not been created yet or the process has failed!");
		} else {
			imageFilePathsMap = chartsFileNamesMap.get(Utils.ChartEntityType.SUBACCOUNTS);
		}
		
		return imageFilePathsMap;
	}
	
	/**
	 * Gets the HashMap of images files (mapped by key being sub account id) with generated charts for
	 * UFKs/ICFs (per sub account containing these UFKs).
	 * 
	 * @return HashMap mapping the id of the sub account to the image file with created UFKs chart (per each
	 * sub account) or null if the charts were not generated (if generating charts was not run or an error has
	 * earlier occured)
	 */
	public HashMap<Integer, String> getImageFilePathsToSubAccountsUFKCharts() {
		HashMap<Integer, String> imageFilePathsMap = null;
		
		if (!chartsFileNamesMap.containsKey(Utils.ChartEntityType.UFKS)) {
			log.error("[LineChartsGenerator] The charts for UFKs have not been created yet or the process has failed!");
		} else {
			imageFilePathsMap = chartsFileNamesMap.get(Utils.ChartEntityType.UFKS);
		}
		
		return imageFilePathsMap;
	}
	
	/**
	 * Gets the TimeSeries object (with periods and balances) for the whole account. The TimeSeries can be
	 * retrieved only if such data was needed, that is only when the generation of the chart for the whole
	 * account took place.
	 * 
	 * @return TimeSeries object for the whole account or null if the TimeSeries was not retrieved (if the
	 * whole account chart generation was skipped, it was not run yet or an error has earlier occured)
	 */
	public TimeSeries getTimeSeriesForWholeAccount() {
		TimeSeries timeSeriesObj = null;
		
		if (!entitiesTimeSeriesMap.containsKey(Utils.ChartEntityType.ACCOUNT)) {
			log.warn("[LineChartsGenerator] The TimeSeries for whole account has not been retrieved yet"
					+ " or the process has failed!");
		} else {
			timeSeriesObj = entitiesTimeSeriesMap.get(Utils.ChartEntityType.ACCOUNT).get(new Integer(1));
		}
		
		return timeSeriesObj;
	}
	
	/**
	 * Gets the HashMap of TimeSeries objects (with periods and balances) for all policies. The returned
	 * HashMap is mapped by the key being policy id.
	 * 
	 * @return HashMap mapping the id of the policy to the TimeSeries object for this policy or null if the
	 * TimeSeries items were not retrieved (if policies charts generation was skipped, it was not run yet or
	 * an error has earlier occured)
	 */
	public HashMap<Integer, TimeSeries> getTimeSeriesListForPolicies() {
		HashMap<Object, TimeSeries> timeSeriesObjMap = null;
		HashMap<Integer, TimeSeries> timeSeriesMap = null;
		
		if (!entitiesTimeSeriesMap.containsKey(Utils.ChartEntityType.POLICIES)) {
			log.warn("[LineChartsGenerator] The TimeSeries list for policies has not been retrieved yet"
					+ " or the process has failed!");
		} else {
			timeSeriesObjMap = entitiesTimeSeriesMap.get(Utils.ChartEntityType.POLICIES);
		}
		
		// because the keys of the policies TimeSeries map are always their numeric Ids (Integer), we can
		// convert the map to be returned from HashMap<Object, TimeSeries> to HashMap<Integer, TimeSeries>
		if (timeSeriesObjMap != null && !timeSeriesObjMap.isEmpty()) {
			timeSeriesMap = new HashMap<Integer, TimeSeries>();
			for (Map.Entry<Object, TimeSeries> entry : timeSeriesObjMap.entrySet()) {
				timeSeriesMap.put((Integer) entry.getKey(), entry.getValue());
			}
		}
		
		return timeSeriesMap;
	}
	
	/**
	 * Gets the HashMap of TimeSeries objects (with periods and balances) for all sub accounts. The returned
	 * HashMap is mapped by the key being sub account id.
	 * 
	 * @return HashMap mapping the id of the sub account to the TimeSeries object for this sub account or null
	 * if the TimeSeries items were not retrieved (if sub accounts charts generation was skipped, it was not
	 * run yet or an error has earlier occured)
	 */
	public HashMap<Integer, TimeSeries> getTimeSeriesListForSubAccounts() {
		HashMap<Object, TimeSeries> timeSeriesObjMap = null;
		HashMap<Integer, TimeSeries> timeSeriesMap = null;
		
		if (!entitiesTimeSeriesMap.containsKey(Utils.ChartEntityType.SUBACCOUNTS)) {
			log.warn("[LineChartsGenerator] The TimeSeries list for sub accounts has not been"
					+ " retrieved yet or the process has failed!");
		} else {
			timeSeriesObjMap = entitiesTimeSeriesMap.get(Utils.ChartEntityType.SUBACCOUNTS);
		}
		
		// because the keys of the sub accounts TimeSeries map are always their numeric Ids (Integer), we can
		// convert the map to be returned from HashMap<Object, TimeSeries> to HashMap<Integer, TimeSeries>
		if (timeSeriesObjMap != null && !timeSeriesObjMap.isEmpty()) {
			timeSeriesMap = new HashMap<Integer, TimeSeries>();
			for (Map.Entry<Object, TimeSeries> entry : timeSeriesObjMap.entrySet()) {
				timeSeriesMap.put((Integer) entry.getKey(), entry.getValue());
			}
		}
		
		return timeSeriesMap;
	}
	
	/**
	 * Gets the HashMap of TimeSeries objects (with periods and balances) for all UFKs. The returned HashMap
	 * is mapped by the key being a pseudo-id of UFK - that is the id takes the form of: &lt;SubAccount id (which
	 * contains given UFK)&gt; + "_" + &lt;UFK name&gt;.
	 * 
	 * @return HashMap mapping the pseudo-id of the UFK to the TimeSeries object for this UFK or null if the
	 * TimeSeries items were not retrieved (if UFKs charts generation was skipped, it was not run yet or an
	 * error has earlier occured)
	 */
	public HashMap<String, TimeSeries> getTimeSeriesListForSubAccountsUFK() {
		HashMap<Object, TimeSeries> timeSeriesObjMap = null;
		HashMap<String, TimeSeries> timeSeriesMap = null;
		
		if (!entitiesTimeSeriesMap.containsKey(Utils.ChartEntityType.UFKS)) {
			log.warn("[LineChartsGenerator] The TimeSeries list for UFKs has not been retrieved yet"
					+ " or the process has failed!");
		} else {
			timeSeriesObjMap = entitiesTimeSeriesMap.get(Utils.ChartEntityType.UFKS);
		}
		
		// because the keys of the sub accounts UFK TimeSeries map are built as a String
		// (idSubAccount + "_" + ufkName), we can convert the map to be returned from
		// HashMap<Object, TimeSeries> to HashMap<String, TimeSeries>
		if (timeSeriesObjMap != null && !timeSeriesObjMap.isEmpty()) {
			timeSeriesMap = new HashMap<String, TimeSeries>();
			for (Map.Entry<Object, TimeSeries> entry : timeSeriesObjMap.entrySet()) {
				timeSeriesMap.put(entry.getKey().toString(), entry.getValue());
			}
		}
		
		return timeSeriesMap;
	}
	
	/**
	 * Sets the start datetime which is used to select the time period for charts generation.
	 * 
	 * @param dateTimeFrom start date time to be set (in format YYYY-mm-dd HH:mm:ss)
	 */
	public void setDateTimeFrom(String dateTimeFrom) {
		this.dateTimeFrom = dateTimeFrom;
	}
	
	/**
	 * Sets the end datetime which is used to select the time period for charts generation.
	 * 
	 * @param dateTimeTo end date time to be set (in format YYYY-mm-dd HH:mm:ss)
	 */
	public void setDateTimeTo(String dateTimeTo) {
		this.dateTimeTo = dateTimeTo;
	}
	
	/**
	 * Sets the number of records for all entities to be used in charts generation. This number determines the
	 * size of created TimeSeries for all charts.
	 * 
	 * @param selectedRecordsCount number of entities records to be set
	 */
	public void setSelectedRecordsCount(int selectedRecordsCount) {
		this.selectedRecordsCount = selectedRecordsCount;
	}
	
	/**
	 * Method deleting all image files for created charts after they are no longer needed (it must be called
	 * explicitly in another class).
	 */
	public void deleteChartsImagesFiles() {
		for (HashMap<Integer, String> mapKeys : chartsFileNamesMap.values()) {
			if (mapKeys != null && !mapKeys.isEmpty()) {
				for (String filePath : mapKeys.values()) {
					if (filePath == null) {
						continue;
					}
					try {
						new File(filePath).delete();
					} catch (Exception e) {
						log.error("[LineChartsGenerator] An error occured while deleting image file <"
								+ filePath + ">! Exception message: " + e.getMessage());
						log.error(Utils.getExceptionStackTrace(e));
					}
				}
			}
		}
	}
}
