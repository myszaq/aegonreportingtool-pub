/**
 * The PieChartsGenerator class, as the name suggests, provides the interface for generating pie charts (displaying the
 * distribution of given entities' recent balances) for Aegon report creation purposes. The process of creating pie charts
 * is based on the JFreeChart library which provides all necessary, lower lever functionality. The generated pie charts
 * represent the following entities:
 * - Policy (that is the percentage/ratio of policies inside user's Aegon account),
 * - SubAccount (that is the percentage/ratio of sub accounts inside every policy),
 * - SubAccountBalanceHistory (that is the percentage/ratio of UFKs inside every sub account).
 * 
 * Created pie charts are saved by the class in the image files (in designated temporary folder) and later available
 * as the paths to these files through the appropriate methods provided.
 * The way how the pie charts are generated depends on user's configuration, but basically 2 modes are supported:
 * - standard pie chart,
 * - 3D pie chart.
 * 
 * For more details, see the comments of each method.
 * 
 * @see <a href="http://www.jfree.org/jfreechart/">http://www.jfree.org/jfreechart/</a>
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.report;

import org.apache.logging.log4j.LogManager;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PieLabelLinkStyle;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

import pl.aegon.youraccount.config.ConfigurationProvider;
import pl.aegon.youraccount.dbservice.DatabaseServiceProvider;
import pl.aegon.youraccount.entity.*;
import pl.aegon.youraccount.utils.Utils;

import java.awt.*;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PieChartsGenerator {
	private static org.apache.logging.log4j.Logger log = LogManager.getLogger(PieChartsGenerator.class);
	private DatabaseServiceProvider dbsp;
	private static HashMap<Utils.ChartEntityType, HashMap<Integer, String>> pieChartsFileNamesMap;
	
	// properties used to set different parameters of visual side of the pie charts
	private final static int CHART_IMAGE_WIDTH = 450;
	private final static int CHART_IMAGE_HEIGHT = 320;
	private final static Color CHART_BACKGROUND_COLOR = Color.WHITE;
	private final static Color CHART_TITLE_COLOR = new Color(0x17375E);
	private final static Font CHART_TITLE_FONT = new Font("Tahoma", Font.BOLD, 15);
	private final static Font CHART_LABEL_FONT = new Font("Calibri", Font.TRUETYPE_FONT, 13);
	private final static Stroke LABEL_LINK_STROKE = new BasicStroke(1.0f);
	private final static Shape LEGEND_ITEM_SHAPE = new Rectangle(9, 9);
	
	// properties corresponding to configuration entries related to pie charts creation
	private boolean generatePoliciesPieChart;
	private boolean generateSubAccountsPieChart;
	private boolean generateUfkChart;
	private boolean skipSingleItemPieChart;
	private boolean generate3DPieCharts;
	
	/**
	 * Class constructor. Initializes all internal class properties and reads pie charts configuration.
	 * 
	 * @throws Exception when configuration for charts generation was incorrect (NoSuchElementException)
	 */
	public PieChartsGenerator() throws Exception {
		this.dbsp = DatabaseServiceProvider.getInstance();
		pieChartsFileNamesMap = new HashMap<Utils.ChartEntityType, HashMap<Integer, String>>();
		
		// get the configuration of pie charts creation (boolean values only)
		try {
			this.generatePoliciesPieChart = Boolean.parseBoolean(ConfigurationProvider
					.getPropertyValue("chart_generate_pie_chart_for_policies"));
			this.generateSubAccountsPieChart = Boolean.parseBoolean(ConfigurationProvider
					.getPropertyValue("chart_generate_pie_chart_for_subaccounts"));
			this.generateUfkChart = Boolean.parseBoolean(ConfigurationProvider
					.getPropertyValue("chart_generate_pie_chart_for_ufk"));
			this.skipSingleItemPieChart = Boolean.parseBoolean(ConfigurationProvider
					.getPropertyValue("chart_do_not_generate_single_item_pie_chart"));
			this.generate3DPieCharts = Boolean.parseBoolean(ConfigurationProvider
					.getPropertyValue("chart_generate_3D_pie_charts"));
		} catch (Exception e) {
			log.error("Could not get the values of pie charts generation properties! Exception message: "
					+ e.getMessage());
			throw e;
		}
	}
	
	/**
	 * Helper method which gets the translated message (for user's selected language) of the given property
	 * key (from languages translation hash map). This is essentially the wrapper for ConfigurationProvider
	 * class methods.
	 * 
	 * @param langPropertyKey the name of the property to be translated
	 * @return the text translation corresponding to given property key
	 */
	private static String getPropertyTranslation(String langPropertyKey) {
		String userLanguage = ConfigurationProvider.getPropertyValue("user_language");
		String translation = ConfigurationProvider.getMessageByLanguage(langPropertyKey, userLanguage);
		
		return translation;
	}
	
	/**
	 * Main method generating pie chart from provided data and applying visual settings depending on its type
	 * and configuration. Created chart can be in a form of standard pie chart or 3D pie chart and later can
	 * be saved as an image.
	 * 
	 * @param chartTitle the title of the created pie chart
	 * @param pieDataset the dataset containing data for the pie chart
	 * @return created pie chart object ready to be saved
	 */
	private JFreeChart buildPieChart(String chartTitle, DefaultPieDataset pieDataset) {
		JFreeChart chart;
		PiePlot plot; // plot displaying data in the form of a pie chart
		
		// build 3D or standard pie chart
		if (this.generate3DPieCharts) {
			chart = ChartFactory.createPieChart3D(null, // title
				pieDataset,	// the default dataset for the chart
				true,		// flag specifying whether legend is created
				true,		// configure chart to generate tooltips?
				false		// configure chart to generate URLs?
			);
		} else {
			chart = ChartFactory.createPieChart(null, // title
				pieDataset,	// the default dataset for the chart
				true,		// flag specifying whether legend is created
				true,		// configure chart to generate tooltips?
				false		// configure chart to generate URLs?
			);
		}
		
		// apply appropriate settings for the plot depending of its type
		if (this.generate3DPieCharts) {
			plot = (PiePlot3D) chart.getPlot();
			plot.setDirection(Rotation.CLOCKWISE);
			plot.setForegroundAlpha(0.55f); // transparency of the pie chart
			plot.setCircular(false);
		} else {
			plot = (PiePlot) chart.getPlot();
			plot.setCircular(true);
		}
		
		// configure the content of the labels to display value and percentage of each section
		StandardPieSectionLabelGenerator labelGenerator = new StandardPieSectionLabelGenerator(
				" {1} PLN \r\n({2})", new DecimalFormat("###.##"), // the format object for the values
				new DecimalFormat("###.#%") // the format object for the percentages
		);
		
		// general visual settings for the whole pie chart
		plot.setBackgroundPaint(CHART_BACKGROUND_COLOR);
		plot.setOutlineVisible(false);
		plot.setNoDataMessage(getPropertyTranslation("pie_chart_no_data"));
		plot.setLegendItemShape(LEGEND_ITEM_SHAPE);
		
		// visual settings for the labels of the sections
		plot.setLabelFont(CHART_LABEL_FONT);
		plot.setLabelGap(0.03);
		plot.setLabelGenerator(labelGenerator);
		plot.setLabelLinkStyle(PieLabelLinkStyle.STANDARD);
		plot.setLabelLinkStroke(LABEL_LINK_STROKE);
		
		// prepare and display the text for the chart title
		TextTitle chartTextTitle = new TextTitle(chartTitle, CHART_TITLE_FONT);
		chartTextTitle.setPaint(CHART_TITLE_COLOR);
		chart.setTitle(chartTextTitle);
		
		return chart;
	}
	
	/**
	 * Method creating and returning the pie dataset for all policies in the Aegon account. The dataset is
	 * composed of values - policies' current balances and keys - policies' names associated with them.
	 * 
	 * @return the pie dataset containing pairs of <name, current balance> for all policies in the account
	 * @throws Exception if policies list could not be retrieved or if dataset creation failed
	 */
	private DefaultPieDataset getDatasetForPolicyList() throws Exception {
		// the pie dataset for all Policy items (current balances)
		DefaultPieDataset policyDataset = new DefaultPieDataset();
		List<Policy> policyList = new ArrayList<Policy>();
		
		try {
			policyList = dbsp.getAllPolicyList();
		} catch (Exception e) {
			log.error("Could not get the list of all policies! Exception message: " + e.getMessage());
			throw e;
		}
		
		// get the current balance and policy name for each policy - these pairs construct the pie dataset
		try {
			String policyName = getPropertyTranslation("policy_name");
			for (Policy policy : policyList) {
				String datasetKey = policyName + " " + policy.getPolicyNumber();
				policyDataset.setValue(datasetKey, new Double(policy.getCurrentBalance()));
			}
		} catch (Exception e) {
			log.error("Could not generate the pie dataset for Policy list! Exception message: "
					+ e.getMessage());
			throw e;
		}
		
		return policyDataset;
	}
	
	/**
	 * Method creating and returning the list of pie datasets for all sub accounts - one element per each
	 * policy in the Aegon account. The dataset is composed of values - sub accounts' current balances and
	 * keys - sub accounts' names associated with them.
	 * 
	 * @return the list of pie datasets (containing pairs of <name, current balance>) for all sub accounts
	 * where each element holds the dataset for sub accounts in every policy in the account
	 * @throws Exception if policies or sub accounts list could not be retrieved or if any dataset creation
	 * failed
	 */
	private List<DefaultPieDataset> getDatasetsForSubAccountList() throws Exception {
		// the list of pie datasets for all SubAccount items (per each policy) (current balances)
		List<DefaultPieDataset> subAccountsDatasets = new ArrayList<DefaultPieDataset>();
		List<Policy> policyList = new ArrayList<Policy>();
		
		try {
			policyList = dbsp.getAllPolicyList();
		} catch (Exception e) {
			log.error("Could not get the list of all policies! Exception message: " + e.getMessage());
			throw e;
		}
		
		try {
			DefaultPieDataset subAccountsDataset;
			for (Policy policy : policyList) {
				try {
					subAccountsDataset = this.getSubAccountsDatasetForPolicy(policy);
				} catch (Exception e) {
					log.error("An exception occured while trying to get SubAccounts dataset! "
							+ e.getMessage());
					throw e;
				}
				
				if (subAccountsDataset != null) {
					// to identify each dataset in the list, assign for it the policy number as the group ID
					subAccountsDataset.setGroup(new DatasetGroup(policy.getPolicyNumber()));
					subAccountsDatasets.add(subAccountsDataset);
				}
			}
		} catch (Exception e) {
			log.error("Could not generate the pie datasets for SubAccount list! Exception message: "
					+ e.getMessage());
			throw e;
		}
		
		return subAccountsDatasets;
	}
	
	/**
	 * Method creating and returning the list of pie datasets for all sub account balance history (UFK/ICF -
	 * Investment Capital Fund) items in the latest data reading - one element of the list per each sub
	 * account. Since ICFs are not unique and only their history is stored in the database, only the last set
	 * (that means for the last day when Aegon data were read) of ICFs composing each sub account is taken
	 * into account and put in every dataset of the result list. The dataset is composed of values - UFKs'
	 * invest unit collect value and keys - UFKs' names associated with them.
	 * 
	 * @return the list of pie datasets (containing pairs of <name, invest unit collect value>) for all UFK
	 * items in sub accounts where each element holds the dataset for UFKs in every sub account
	 * @throws Exception if sub accounts or UFK list could not be retrieved or if any dataset creation failed
	 */
	private List<DefaultPieDataset> getDatasetsForUFKList() throws Exception {
		// the list of pie datasets for all SubAccountBalanceHistory (UFK) items
		// (per each sub account) (current balances)
		List<DefaultPieDataset> subAccountsUFKDatasets = new ArrayList<DefaultPieDataset>();
		List<SubAccount> subAccountList = new ArrayList<SubAccount>();
		
		try {
			subAccountList = dbsp.getAllSubAccountList();
		} catch (Exception e) {
			log.error("Could not get the list of all sub accounts! Exception message: " + e.getMessage());
			throw e;
		}
		
		try {
			DefaultPieDataset subAccountsUFKDataset;
			for (SubAccount subAccount : subAccountList) {
				try {
					subAccountsUFKDataset = this.getUFKDatasetForSubAccount(subAccount);
				} catch (Exception e) {
					log.error("An exception occured while trying to get UFK dataset! " + e.getMessage());
					throw e;
				}
				
				if (subAccountsUFKDataset != null) {
					// to identify each dataset in the list, assign for it the sub account
					// name as the group ID
					subAccountsUFKDataset.setGroup(new DatasetGroup(subAccount.getSubAccountName()));
					subAccountsUFKDatasets.add(subAccountsUFKDataset);
				}
			}
		} catch (Exception e) {
			log.error("Could not generate the pie datasets for SubAccountBalanceHistory list! Exception message: "
					+ e.getMessage());
			throw e;
		}
		
		return subAccountsUFKDatasets;
	}
	
	/**
	 * Method creating and returning the pie dataset for all sub accounts in specified policy. The dataset is
	 * composed of values - sub accounts' current balances and keys - sub accounts' names associated with
	 * them.
	 * 
	 * @param policy the entity representing policy whose sub accounts should be included in the dataset
	 * @return the pie dataset containing pairs of <name, current balance> for all sub accounts in the given
	 * policy
	 * @throws Exception if sub account list could not be retrieved or if dataset creation failed
	 */
	private DefaultPieDataset getSubAccountsDatasetForPolicy(Policy policy) throws Exception {
		// the pie dataset for all SubAccount items (current balances)
		DefaultPieDataset subAccountsDataset = null;
		List<SubAccount> subAccountList = null;
		
		try {
			subAccountList = dbsp.getSubAccountListByPolicy(policy);
		} catch (Exception e) {
			log.error("Could not get SubAccount list for Policy with id = " + policy.getIdPolicy()
					+ "! Exception message: " + e.getMessage());
		}
		
		// get the current balance and sub account name for each sub account in given policy - these pairs
		// construct the pie dataset
		try {
			if (subAccountList != null && !subAccountList.isEmpty()) {
				subAccountsDataset = new DefaultPieDataset();
				
				for (SubAccount subAccount : subAccountList) {
					subAccountsDataset.setValue(subAccount.getSubAccountName(),
							new Double(subAccount.getCurrentBalance()));
				}
			}
		} catch (Exception e) {
			log.error("Could not generate the pie dataset for SubAccount list! Exception message: "
					+ e.getMessage());
			throw e;
		}
		
		return subAccountsDataset;
	}
	
	/**
	 * Method creating and returning the pie dataset for all recent UFKs (that is UFKs composing given sub
	 * account in the last day) in specified sub account. The dataset is composed of values - UFKs' invest
	 * unit collect values and keys - UFKs' names associated with them.
	 * 
	 * @param subAccount the entity representing sub account whose UFKs should be included in the dataset
	 * @return the pie dataset containing pairs of <name, invest unit collect value> for all recent UFKs in
	 * the given sub account
	 * @throws Exception if UFK list could not be retrieved or if dataset creation failed
	 */
	private DefaultPieDataset getUFKDatasetForSubAccount(SubAccount subAccount) throws Exception {
		// the pie dataset for all sub account UFK (SubAccountBalanceHistory) items (current balances)
		DefaultPieDataset subAccountsUFKDataset = null;
		List<SubAccountBalanceHistory> subAccountBalanceHistoryList = null;
		
		try {
			// first get the last entry log (that is why we set limit to 1) and extract it from the list
			EntryLog lastEntryLog = dbsp.getEntryLogListByLimit(1).get(0);
			
			// get SubAccountBalanceHistory (UFK) items for the last EntryLog
			subAccountBalanceHistoryList = dbsp.getSubAccountBalanceHistoryListByEntryLogRange(
					subAccount.getIdSubAccount(), lastEntryLog, lastEntryLog);
		} catch (Exception e) {
			log.error("Could not get SubAccountBalanceHistory list for SubAccount with id = "
					+ subAccount.getIdSubAccount() + "! Exception message: " + e.getMessage());
		}
		
		// get the invest unit collect value and UFK name for each UFK in given sub account - these pairs
		// construct the pie dataset
		try {
			if (subAccountBalanceHistoryList != null && !subAccountBalanceHistoryList.isEmpty()) {
				subAccountsUFKDataset = new DefaultPieDataset();
				
				for (SubAccountBalanceHistory subAccountUFK : subAccountBalanceHistoryList) {
					subAccountsUFKDataset.setValue(subAccountUFK.getUfkName(),
							new Double(subAccountUFK.getUfkInvestUnitCollectValue()));
				}
			}
		} catch (Exception e) {
			log.error("Could not generate the pie dataset for SubAccountBalanceHistory list! Exception message: "
					+ e.getMessage());
			throw e;
		}
		
		return subAccountsUFKDataset;
	}
	
	/**
	 * Method creates the pie chart for all policies in the account. The pie chart is saved as an image and
	 * represents the percentage (the distribution) of policies latest balances.
	 * 
	 * @throws Exception if generating pie chart for policies failed
	 */
	public void generatePieChartForPolicies() throws Exception {
		// an internal HashMap which maps path to the file (containing created chart image)
		// under the specific key/id
		HashMap<Integer, String> chartFilePathMap = new HashMap<Integer, String>();
		
		// check if policies pie chart should be produced (user can disable such chart)
		if (!this.generatePoliciesPieChart) {
			log.debug("[PieChartsGenerator] Pie chart creation for policies in the account is skipped by user.");
			
			// since no chart is created, we set the whole map as null
			pieChartsFileNamesMap.put(Utils.ChartEntityType.POLICIES, null);
			return;
		}
		
		try {
			JFreeChart chart = null;
			DefaultPieDataset policiesDataset = this.getDatasetForPolicyList();
			
			// skip pie chart creation if there is only one item in the dataset
			// and skipping single item is enabled
			if (this.skipSingleItemPieChart && policiesDataset.getItemCount() == 1) {
				log.debug("[PieChartsGenerator] Pie chart creation for policies in the account is skipped "
						+ "because the chart contains one element only.");
				
				// since no chart is created, we set the whole map as null
				pieChartsFileNamesMap.put(Utils.ChartEntityType.POLICIES, null);
				return;
			}
			
			// create pie chart with provided policies data and save it in the appropriate file
			try {
				String chartTitle = getPropertyTranslation("policies_in_account_pie_chart_title");
				chart = this.buildPieChart(chartTitle, policiesDataset);
				String chartFilePath = Utils.getChartImageFilePath("policies_in_account", null, true);
				
				// since there is only one pie chart for policies, the id (the key of internal HashMap)
				// is just set to 1
				chartFilePathMap.put(new Integer(1), chartFilePath);
				pieChartsFileNamesMap.put(Utils.ChartEntityType.POLICIES, chartFilePathMap);
				
				ChartUtilities.saveChartAsPNG(new File(chartFilePath), chart, CHART_IMAGE_WIDTH,
						CHART_IMAGE_HEIGHT);
				log.info("[PieChartsGenerator] Pie chart for policies in the account generated successfully.");
			} catch (Exception e) {
				throw e;
			}
		} catch (Exception e) {
			log.error("[PieChartsGenerator] Generating pie chart for policies in the account failed! Exception message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Method creates the pie charts for all sub accounts per each policy in the account. The number of
	 * created pie charts corresponds to the number of policies in the account (unless some pie chart
	 * generation is skipped because it contains only one element), each one of them is saved as a separate
	 * image and represents the percentage (the distribution) of sub accounts' latest balances inside every
	 * policy.
	 * 
	 * @throws Exception if generating at least one pie chart for sub accounts failed
	 */
	public void generatePieChartsForSubAccounts() throws Exception {
		// an internal HashMap which maps paths to the files (containing created charts images)
		// under the specific key/id
		HashMap<Integer, String> chartFilePathsMap = new HashMap<Integer, String>();
		
		// check if sub accounts pie chart should be produced (user can disable such chart)
		if (!this.generateSubAccountsPieChart) {
			log.debug("[PieChartsGenerator] Pie charts creation for sub accounts in the policies is skipped by user.");
			
			// since no chart is created, we set the whole map as null
			pieChartsFileNamesMap.put(Utils.ChartEntityType.SUBACCOUNTS, null);
			return;
		}
		
		JFreeChart chart = null;
		String chartTitle;
		String chartFilePath;
		
		try {
			List<DefaultPieDataset> subAccountsDatasetList = this.getDatasetsForSubAccountList();
			for (DefaultPieDataset subAccountsDataset : subAccountsDatasetList) {
				String policyNumber = subAccountsDataset.getGroup().getID();
				Integer policyId = new Integer(dbsp.getPolicyByPolicyNumber(policyNumber).getIdPolicy());
				
				// skip pie chart creation if there is only one item in the dataset
				// and skipping single item is enabled
				if (this.skipSingleItemPieChart && subAccountsDataset.getItemCount() == 1) {
					log.debug("[PieChartsGenerator] Pie chart creation for sub accounts in policy <"
							+ policyNumber + "> is skipped because the chart contains one element only.");
					
					// set path to the chart file as null since it's not generated for that sub accounts set
					chartFilePathsMap.put(policyId, null);
					continue;
				}
				
				chartTitle = getPropertyTranslation("subaccounts_in_policy_pie_chart_title");
				chartTitle += " " + policyNumber;
				
				// create pie chart with provided sub accounts data and save it in the appropriate file
				try {
					chart = this.buildPieChart(chartTitle, subAccountsDataset);
					chartFilePath = Utils.getChartImageFilePath("sub_accounts_in_policy", policyNumber, true);
					// save the path to the file in the map
					chartFilePathsMap.put(policyId, chartFilePath);
					
					ChartUtilities.saveChartAsPNG(new File(chartFilePath), chart, CHART_IMAGE_WIDTH,
							CHART_IMAGE_HEIGHT);
					log.info("[PieChartsGenerator] Pie chart for sub accounts in policy <" + policyNumber
							+ "> generated successfully.");
				} catch (Exception e) {
					throw e;
				}
			} // for (DefaultPieDataset subAccountsDataset : subAccountsDatasetList) {
			
			// save the result map with all charts' files paths
			pieChartsFileNamesMap.put(Utils.ChartEntityType.SUBACCOUNTS, chartFilePathsMap);
		} catch (Exception e) {
			log.error("[PieChartsGenerator] Generating pie charts for sub accounts in the policies failed! Exception message: "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Method creates the pie charts for all UFKs/ICFs per each user's sub account. The number of created pie
	 * charts corresponds to the number of sub accounts (unless some pie chart generation is skipped because
	 * it contains only one element), each one of them is saved as a separate image and represents the
	 * percentage (the distribution) of UFKs'/ICFs' latest balances inside every sub account.
	 * 
	 * @throws Exception if generating at least one pie chart for UFKs failed
	 */
	public void generatePieChartsForSubAccountsUFK() throws Exception {
		// an internal HashMap which maps paths to the files (containing created charts images)
		// under the specific key/id
		HashMap<Integer, String> chartFilePathsMap = new HashMap<Integer, String>();
		
		// check if UFK pie chart should be produced (user can disable such chart)
		if (!this.generateUfkChart) {
			// since no chart is created, we set the whole map as null
			pieChartsFileNamesMap.put(Utils.ChartEntityType.UFKS, null);
			
			log.debug("[PieChartsGenerator] Pie charts creation for UFKs in the sub accounts is skipped by user.");
			return;
		}
		
		// get the list of all sub accounts (needed to know their IDs to name the chart files uniquely)
		List<SubAccount> subAccountList = null;
		try {
			subAccountList = dbsp.getAllSubAccountList();
		} catch (Exception e) {
			log.error("Could not get the list of all sub accounts! Exception message: " + e.getMessage());
			throw e;
		}
		
		JFreeChart chart = null;
		String chartTitle;
		String chartFilePath;
		
		try {
			int dsIndex = 0; // current dataset index
			List<DefaultPieDataset> subAccountsUFKDatasetList = this.getDatasetsForUFKList();
			
			for (DefaultPieDataset subAccountsUFKDataset : subAccountsUFKDatasetList) {
				String subAccountId = String.valueOf((subAccountList.get(dsIndex).getIdSubAccount()));
				dsIndex++;
				
				// skip pie chart creation if there is only one item in the dataset
				// and skipping single item is enabled
				if (this.skipSingleItemPieChart && subAccountsUFKDataset.getItemCount() == 1) {
					log.debug("[PieChartsGenerator] Pie chart creation for UFKs in sub account <ID="
							+ subAccountId + "> is skipped because the chart contains one element only.");
					
					// set path to the chart file as null since it's not generated for that UFKs set
					chartFilePathsMap.put(new Integer(subAccountId), null);
					continue;
				}
				
				String subAccountName = subAccountsUFKDataset.getGroup().getID();
				chartTitle = getPropertyTranslation("ufk_in_subaccount_pie_chart_title");
				chartTitle += " " + subAccountName;
				
				// create pie chart with provided UFKs data and save it in the appropriate file
				try {
					chart = this.buildPieChart(chartTitle, subAccountsUFKDataset);
					chartFilePath = Utils.getChartImageFilePath("ufk_in_subaccount", subAccountId, true);
					// save the path to the file in the map
					chartFilePathsMap.put(new Integer(subAccountId), chartFilePath);
					
					ChartUtilities.saveChartAsPNG(new File(chartFilePath), chart, CHART_IMAGE_WIDTH,
							CHART_IMAGE_HEIGHT);
					log.info("[PieChartsGenerator] Pie chart for UFKs in sub account <ID=" + subAccountId
							+ "> generated successfully.");
				} catch (Exception e) {
					throw e;
				}
			} // for (DefaultPieDataset subAccountsUFKDataset : subAccountsUFKDatasetList) {
			
			// save the result map with all charts' files paths
			pieChartsFileNamesMap.put(Utils.ChartEntityType.UFKS, chartFilePathsMap);
		} catch (Exception e) {
			log.error("[PieChartsGenerator] Generating pie charts for UFKs in the sub accounts failed! "
					+ "Exception message: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Main method creating all possible pie charts: for policies, sub accounts and UFKs stored in the
	 * database. It is the main interface and the main purpose of the PieChartsGenerator class. The method is
	 * essentially the wrapper for other public methods generating more specific pie charts for mentioned
	 * entities. Every chart created is saved as a separate image, path to which is available through the
	 * public methods getImageFilePath...().
	 * 
	 * @throws Exception if generating any of the pie charts failed
	 */
	public void generateAllPieCharts() throws Exception {
		try {
			this.generatePieChartForPolicies();
			this.generatePieChartsForSubAccounts();
			this.generatePieChartsForSubAccountsUFK();
			
			log.info("[PieChartsGenerator] All possible pie charts have been generated successfully.");
		} catch (Exception e) {
			log.error("[PieChartsGenerator] Generating one or more pie charts for all items failed!"
					+ " Exception message: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			throw e;
		}
	}
	
	/**
	 * Gets the path to the image file with generated pie chart for policies.
	 * 
	 * @return path to the image file with created policies' pie chart or null if the chart was not generated
	 * (if it was skipped, generating pie chart was not run or an error has earlier occured)
	 */
	public String getImageFilePathToPoliciesPieChart() {
		String imageFilePath = null;
		
		if (!pieChartsFileNamesMap.containsKey(Utils.ChartEntityType.POLICIES)) {
			log.error("[PieChartsGenerator] The pie chart for policies has not been created yet or the process has failed!");
		} else {
			imageFilePath = pieChartsFileNamesMap.get(Utils.ChartEntityType.POLICIES).get(
					new Integer(1));
		}
		
		return imageFilePath;
	}
	
	/**
	 * Gets the HashMap of images files (mapped by key being policy ID) with generated pie charts for sub
	 * accounts (per policy containing these sub accounts).
	 * It is possible that though the returned map is not null, the value under some key is null.
	 * In that case it means that for given policy ID the sub accounts pie chart was not generated
	 * (because it was skipped).
	 * 
	 * @return HashMap mapping the id of the policy to the image file with created sub accounts' pie chart
	 * (per each policy) or null if the charts were not generated (if it was skipped, generating pie charts
	 * was not run or an error has earlier occured)
	 */
	public HashMap<Integer, String> getImageFilePathsToSubAccountsPieCharts() {
		HashMap<Integer, String> imageFilePathsMap = null;
		
		if (!pieChartsFileNamesMap.containsKey(Utils.ChartEntityType.SUBACCOUNTS)) {
			log.error("[PieChartsGenerator] The pie charts for sub accounts have not been created yet or the process has failed!");
		} else {
			imageFilePathsMap = pieChartsFileNamesMap.get(Utils.ChartEntityType.SUBACCOUNTS);
		}
		
		return imageFilePathsMap;
	}
	
	/**
	 * Gets the HashMap of images files (mapped by key being sub account ID) with generated pie charts for
	 * UFKs/ICFs (per sub account containing these UFKs).
	 * It is possible that though the returned map is not null, the value under some key is null.
	 * In that case it means that for given sub account ID the UFKs pie chart was not generated
	 * (because it was skipped).
	 * 
	 * @return HashMap mapping the id of the sub account to the image file with created UFKs' pie chart (per
	 * each sub account) or null if the charts were not generated (if it was skipped, generating pie charts
	 * was not run or an error has earlier occured)
	 */
	public HashMap<Integer, String> getImageFilePathsToSubAccountsUFK() {
		HashMap<Integer, String> imageFilePathsMap = null;
		
		if (!pieChartsFileNamesMap.containsKey(Utils.ChartEntityType.UFKS)) {
			log.error("[PieChartsGenerator] The pie charts for UFKs have not been created yet or the process has failed!");
		} else {
			imageFilePathsMap = pieChartsFileNamesMap.get(Utils.ChartEntityType.UFKS);
		}
		
		return imageFilePathsMap;
	}
	
	/**
	 * Method deleting all image files for created pie charts after they are no longer needed (it must be
	 * called explicitly in another class).
	 */
	public void deletePieChartsImagesFiles() {
		for (HashMap<Integer, String> mapKeys : pieChartsFileNamesMap.values()) {
			if (mapKeys != null && !mapKeys.isEmpty()) {
				for (String filePath : mapKeys.values()) {					
					if (filePath == null) {
						continue;
					}
					try {
						new File(filePath).delete();
					} catch (Exception e) {
						log.error("[PieChartsGenerator] An error occured while deleting image file <"
								+ filePath + ">! Exception message: " + e.getMessage());
						log.error(Utils.getExceptionStackTrace(e));
					}
				}
			}
		}
	}
}