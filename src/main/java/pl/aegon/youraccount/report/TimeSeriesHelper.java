/**
 * The TimeSeriesHelper class provides additional, helper methods used for charts generation process.
 * The methods available are defined with default package access, so this class can only be used by
 * ChartGenerator class or other classes from the package pl.aegon.youraccount.report.
 * 
 * The methods implemented here help to build TimeSeries objects under given criteria for the main entities
 * included in Aegon report generation:
 * - Whole account (by collecting EntryLog entities),
 * - Policy (by collecting PolicyHistory entities),
 * - SubAccount (by collecting SubAccountHistory entities),
 * - UFK (by collecting SubAccountBalanceHistory entities) - although this is not strictly an entity
 * (basically here UFK is the visible and meaningful counterpart for any SubAccountBalanceHistory entity).
 * 
 * For more details, see the comments of each method.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.*;
import org.jfree.data.time.*;

import pl.aegon.youraccount.dbservice.DatabaseServiceProvider;
import pl.aegon.youraccount.entity.*;
import pl.aegon.youraccount.utils.Utils;

public class TimeSeriesHelper {
	private static org.apache.logging.log4j.Logger log = LogManager.getLogger(TimeSeriesHelper.class);
	private DatabaseServiceProvider dbsp = DatabaseServiceProvider.getInstance();
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * Helper method to validate the specified datetime range, that is if lower range bound is before upper
	 * bound.
	 * 
	 * @param minDateTime minimum datetime - lower bound of datetime range (in YYYY-mm-dd HH:mm:ss format)
	 * @param maxDateTime maximum datetime - upper bound of datetime range (in YYYY-mm-dd HH:mm:ss format) or
	 * null for now
	 * @throws Exception if validation failed
	 */
	private void validateDatetimeRange(String minDateTime, String maxDateTime) throws Exception {
		try {
			Date firstDate = dateFormat.parse(minDateTime);
			
			// check the maxDateTime only if it's not null (if null, then it will be set by default
			// to current datetime)
			if (maxDateTime != null && !maxDateTime.isEmpty()) {
				Date lastDate = dateFormat.parse(maxDateTime);
				
				// this should never happen!
				if (!firstDate.before(lastDate)) {
					throw new IllegalArgumentException(
							"Invalid minimum and/or maximum datetime passed - minimum datetime is not before maximum!");
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Helper method creating (or selecting if you prefer) TimeSeries object with the data pairs (datetime,
	 * balance) for EntryLog entities corresponding to the given list of EntryLog records.
	 * 
	 * {@link} http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param entryLogItems list of EntryLogs entities for which the datetimes apply
	 * @return TimeSeries object with the data <datetime, balance> for EntryLog entities
	 * @throws Exception when retrieval of EntryLog entities failed
	 * @throws NullPointerException when no whole account (EntryLog) balance series were created/selected
	 */
	private TimeSeries selectWholeAccountBalanceSeries(List<EntryLog> entryLogItems) throws Exception {
		TimeSeries wholeAccountBalanceSeries = null;
		
		// create TimeSeries object for EntryLog balance series
		if (entryLogItems != null && !entryLogItems.isEmpty()) {
			wholeAccountBalanceSeries = new TimeSeries("EntryLog balance");
			
			for (EntryLog currentItem : entryLogItems) {
				// build the time series of EntryLog datetime and the balance
				Date entryLogDate = dateFormat.parse(currentItem.getReadingDate());
				wholeAccountBalanceSeries.addOrUpdate(new Day(entryLogDate),
						(double) currentItem.getCurrentBalance());
			}
		}
		
		// before returning policyBalanceSeries we must make sure it's not null nor empty
		if (wholeAccountBalanceSeries == null || wholeAccountBalanceSeries.isEmpty()) {
			log.error("Could not select any whole account balance series! Probably data is corrupted "
					+ "or not enough data is available.");
			throw new NullPointerException("Whole account balance series could not be created! "
					+ "Please check configuration or verify data provided.");
		}
		
		return wholeAccountBalanceSeries;
	}
	
	/**
	 * Helper method creating (or selecting if you prefer) TimeSeries object with the data pairs (datetime,
	 * balance) for PolicyHistory entities corresponding to the specified criteria - Policy and time range.
	 * The criteria are defined by the id of Policy entity and list of EntryLog entities.
	 * 
	 * {@link} http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param idPolicy id of the policy for which to get the PolicyHistory entities
	 * @param entryLogItems list of EntryLogs entities for which the datetimes apply
	 * @return TimeSeries object with the data <datetime, balance> for PolicyHistory entities
	 * @throws Exception when retrieval of PolicyHistory entities failed
	 * @throws NullPointerException when no policy balance series were created/selected
	 */
	private TimeSeries selectPolicyBalanceSeries(int idPolicy, List<EntryLog> entryLogItems) throws Exception {
		TimeSeries policyBalanceSeries = null;
		List<PolicyHistory> policyHistoryItems = null;
		
		try {
			// get the first and the last EntryLog from the EntryLog range list
			if (entryLogItems != null && !entryLogItems.isEmpty()) {
				EntryLog firstItem = entryLogItems.get(entryLogItems.size() - 1);
				EntryLog lastItem = entryLogItems.get(0);
				
				// get PolicyHistory items for given idPolicy within defined range of first and last EntryLog
				policyHistoryItems = dbsp.getPolicyHistoryListByEntryLogRange(idPolicy, firstItem, lastItem);
			}
		} catch (Exception e) {
			log.error("An exception occured while trying to select PolicyHistory entities! " + e.getMessage());
			throw e;
		}
		
		// create TimeSeries object for Policy balance series
		if (policyHistoryItems != null && !policyHistoryItems.isEmpty()) {
			policyBalanceSeries = new TimeSeries("Policy balance");
			
			// build the time series of PolicyHistory datetime and the balance
			for (PolicyHistory currentItem : policyHistoryItems) {
				Date policyHistoryDate = dateFormat.parse(currentItem.getEntryLog().getReadingDate());
				policyBalanceSeries
						.addOrUpdate(new Day(policyHistoryDate), (double) currentItem.getBalance());
			}
		}
		
		// before returning policyBalanceSeries we must make sure it's not null nor empty
		if (policyBalanceSeries == null || policyBalanceSeries.isEmpty()) {
			log.error("Could not select any policy balance series! Probably data is corrupted "
					+ "or not enough data is available.");
			throw new NullPointerException("Policy balance series could not be created! "
					+ "Please check configuration or verify data provided.");
		}
		
		return policyBalanceSeries;
	}
	
	/**
	 * Helper method creating (or selecting if you prefer) TimeSeries object with the data pairs (datetime,
	 * balance) for SubAccountHistory entities corresponding to the specified criteria - SubAccount and time
	 * range. The criteria are defined by the id of SubAccount entity and list of EntryLog entities.
	 * 
	 * {@link} http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param idSubAccount id of the sub account for which to get the SubAccountHistory entities
	 * @param entryLogItems list of EntryLogs entities for which the datetimes apply
	 * @return TimeSeries object with the data <datetime, balance> for SubAccountHistory entities
	 * @throws Exception when retrieval of SubAccountHistory entities failed
	 * @throws NullPointerException when no sub account balance series were created/selected
	 */
	private TimeSeries selectSubAccountBalanceSeries(int idSubAccount, List<EntryLog> entryLogItems)
			throws Exception {
		TimeSeries subAccountBalanceSeries = null;
		List<SubAccountHistory> subAccountHistoryItems = null;
		
		try {
			// get the first and the last EntryLog from the EntryLog range list
			if (entryLogItems != null && !entryLogItems.isEmpty()) {
				EntryLog firstItem = entryLogItems.get(entryLogItems.size() - 1);
				EntryLog lastItem = entryLogItems.get(0);
				
				// get SubAccountHistory items for given idSubAccount within defined range of first and last
				// EntryLog
				subAccountHistoryItems = dbsp.getSubAccountHistoryListByEntryLogRange(idSubAccount,
						firstItem, lastItem);
			}
		} catch (Exception e) {
			log.error("An exception occured while trying to select SubAccountHistory entities! "
					+ e.getMessage());
			throw e;
		}
		
		// create TimeSeries object for SubAccount balance series
		if (subAccountHistoryItems != null && !subAccountHistoryItems.isEmpty()) {
			subAccountBalanceSeries = new TimeSeries("SubAccount balance");
			
			// build the time series of SubAccountHistory datetime and the balance
			for (SubAccountHistory currentItem : subAccountHistoryItems) {
				Date subAccountHistoryDate = dateFormat.parse(currentItem.getEntryLog().getReadingDate());
				subAccountBalanceSeries.addOrUpdate(new Day(subAccountHistoryDate),
						(double) currentItem.getBalance());
			}
		}
		
		// before returning subAccountBalanceSeries we must make sure it's not null nor empty
		if (subAccountBalanceSeries == null || subAccountBalanceSeries.isEmpty()) {
			log.error("Could not select any sub account balance series! Probably data is corrupted "
					+ "or not enough data is available.");
			throw new NullPointerException("Sub account balance series could not be created! "
					+ "Please check configuration or verify data provided.");
		}
		
		return subAccountBalanceSeries;
	}
	
	/**
	 * Helper method creating (or selecting if you prefer) TimeSeries object with the data pairs (datetime,
	 * collected invest unit value) for SubAccountBalanceHistory entities corresponding to the specified
	 * criteria - SubAccount, UFK name and time range. The criteria are defined by the id of SubAccount
	 * entity, name of the UFK and list of EntryLog entities.
	 * 
	 * {@link} http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param idSubAccount id of the sub account for which to get the SubAccountBalanceHistory entities
	 * @param ufkName the name of the UFK for which to get the SubAccountBalanceHistory entities
	 * @param entryLogItems list of EntryLogs entities for which the datetimes apply
	 * @return TimeSeries object with the data <datetime, collected invest unit value> for
	 * SubAccountBalanceHistory entities
	 * @throws Exception when retrieval of SubAccountBalanceHistory entities failed
	 * @throws NullPointerException when no sub account balance history balance series were created/selected
	 */
	private TimeSeries selectSubAccountBalanceHistoryBalanceSeries(int idSubAccount, String ufkName,
			List<EntryLog> entryLogItems) throws Exception {
		TimeSeries subAccountBalanceHistoryBalanceSeries = null;
		List<SubAccountBalanceHistory> subAccountBalanceHistoryItems = null;
		
		try {
			// get the first and the last EntryLog from the EntryLog range list
			if (entryLogItems != null && !entryLogItems.isEmpty()) {
				EntryLog firstItem = entryLogItems.get(entryLogItems.size() - 1);
				EntryLog lastItem = entryLogItems.get(0);
				
				// get SubAccountBalanceHistory items within defined range of first and last EntryLog
				subAccountBalanceHistoryItems = dbsp.getSubAccountBalanceHistoryListByEntryLogRange(
						idSubAccount, firstItem, lastItem);
			}
		} catch (Exception e) {
			log.error("An exception occured while trying to select SubAccountBalanceHistory entities! "
					+ e.getMessage());
			throw e;
		}
		
		// create TimeSeries object for SubAccountBalanceHistory balance series
		if (subAccountBalanceHistoryItems != null && !subAccountBalanceHistoryItems.isEmpty()) {
			subAccountBalanceHistoryBalanceSeries = new TimeSeries("SubAccountBalanceHistory balance");
			
			// extract only these SubAccountBalanceHistory items which are corresponding to the given UFK name
			for (SubAccountBalanceHistory currentItem : subAccountBalanceHistoryItems) {
				// build the time series of SubAccountBalanceHistory datetime and the balance (where the
				// balance is collected invest unit value for given UFK)
				if (currentItem.getUfkName().equals(ufkName)) {
					Date subAccountBalanceHistoryDate = dateFormat.parse(currentItem.getEntryLog()
							.getReadingDate());
					subAccountBalanceHistoryBalanceSeries.add(new Day(subAccountBalanceHistoryDate),
							currentItem.getUfkInvestUnitCollectValue());
				}
			}
		}
		
		// before returning subAccountyBalanceHistoryBalanceSeries we must make sure it's not null nor empty
		if (subAccountBalanceHistoryBalanceSeries == null || subAccountBalanceHistoryBalanceSeries.isEmpty()) {
			log.error("Could not select any sub account balance history (UFK) balance series! Probably data is corrupted "
					+ "or not enough data is available.");
			throw new NullPointerException(
					"Sub account balance history (UFK) balance series could not be created! "
							+ "Please check configuration or verify data provided.");
		}
		
		return subAccountBalanceHistoryBalanceSeries;
	}
	
	/**
	 * The helper method used to get the datetime from the specified days before current datetime.
	 * 
	 * @param daysNumber number of days to go back in time
	 * @return formatted datetime from the past
	 */
	String calculateDateTimeFromDays(int daysNumber) {
		// get timestamp of the date from <daysNumber> days before now
		long timestampBefore = new Date().getTime() - daysNumber * 24 * 3600 * 1000l;
		
		// format the timestamp back to String
		return dateFormat.format(new Date(timestampBefore));
	}
	
	/**
	 * Method returning TimeSeries object with the data pairs (datetime, balance) for EntryLog entities
	 * corresponding to the specified criteria, which are defined by the number of last EntryLog entities to
	 * take into account.
	 * 
	 * {@link}http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param limit number of last EntryLog entities (ordered by creation time) to use for datetime range
	 * @return TimeSeries object with the data <datetime, balance> for EntryLog entities or null if nothing
	 * was retrieved
	 * @throws Exception when the input parameter was invalid or if another error occured
	 */
	TimeSeries getWholeAccountHistoryRecordsForChart(int limit) throws Exception {
		// validate the input parameter
		if (limit < Utils.REPORT_RECORDS_COUNT_MIN_RANGE || limit > Utils.REPORT_RECORDS_COUNT_MAX_RANGE) {
			throw new IllegalArgumentException("Limit for WholeAccount is beyond the allowed range <"
					+ Utils.REPORT_RECORDS_COUNT_MIN_RANGE + " - " + Utils.REPORT_RECORDS_COUNT_MAX_RANGE
					+ ">!");
		}
		
		TimeSeries result = null;
		try {
			// get the list of EntryLogs with the specified limit of elements (that is last <limit> EntryLog
			// entities ordered by creation time)
			List<EntryLog> entryLogItems = dbsp.getEntryLogListByLimit(limit);
			// get the time series for specified time range
			result = selectWholeAccountBalanceSeries(entryLogItems);
		} catch (Exception e) {
			log.error("An exception occured while trying to get criteria-based EntryLog records! "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		return result;
	}
	
	/**
	 * Method returning TimeSeries object with the data pairs (datetime, balance) for EntryLog entities
	 * corresponding to the specified criteria, which are defined by the range of min-max datetimes.
	 * 
	 * {@link}http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param minDateTime minimum datetime - lower bound of datetime range (in YYYY-mm-dd HH:mm:ss format)
	 * @param maxDateTime maximum datetime - upper bound of datetime range (in YYYY-mm-dd HH:mm:ss format) or
	 * null for now
	 * @return TimeSeries object with the data <datetime, balance> for EntryLog entities or null if nothing
	 * was retrieved
	 * @throws Exception when the input parameters were invalid or if another error occured
	 */
	TimeSeries getWholeAccountHistoryRecordsForChart(String minDateTime, String maxDateTime) throws Exception {
		// validate the input parameters
		try {
			validateDatetimeRange(minDateTime, maxDateTime);
		} catch (ParseException e) {
			log.error("Datetime validation error! Exception message: " + e.getMessage());
			throw e;
		} catch (IllegalArgumentException ie) {
			log.error("Datetime validation error! Exception message: " + ie.getMessage());
			throw ie;
		}
		
		TimeSeries result = null;
		try {
			// get the list of EntryLogs within the specified time range
			List<EntryLog> entryLogItems = dbsp.getEntryLogListByTimeRange(minDateTime, maxDateTime);
			// get the time series for specified time range
			result = selectWholeAccountBalanceSeries(entryLogItems);
		} catch (Exception e) {
			log.error("An exception occured while trying to get criteria-based EntryLog records! "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		return result;
	}
	
	/**
	 * Method returning TimeSeries object with the data pairs (datetime, balance) for PolicyHistory entities
	 * corresponding to the specified criteria - Policy and time limit backwards. The criteria are defined by
	 * the id of Policy entity and the number of last EntryLog entities to take into account.
	 * 
	 * {@link}http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param idPolicy id of the policy for which to get the PolicyHistory entities
	 * @param limit number of last EntryLog entities (ordered by creation time) to use for datetime range
	 * @return TimeSeries object with the data <datetime, balance> for PolicyHistory entities or null if
	 * nothing was retrieved
	 * @throws Exception when the input parameters were invalid or if another error occured
	 */
	TimeSeries getPolicyHistoryRecordsForChart(int idPolicy, int limit) throws Exception {
		// validate the input parameters
		if (idPolicy < 1) {
			throw new IllegalArgumentException("Invalid Policy id passed as an input parameter!");
		}
		if (limit < Utils.REPORT_RECORDS_COUNT_MIN_RANGE || limit > Utils.REPORT_RECORDS_COUNT_MAX_RANGE) {
			throw new IllegalArgumentException("Limit for PolicyHistory is beyond the allowed range <"
					+ Utils.REPORT_RECORDS_COUNT_MIN_RANGE + " - " + Utils.REPORT_RECORDS_COUNT_MAX_RANGE
					+ ">!");
		}
		
		TimeSeries result = null;
		try {
			// get the list of EntryLogs with the specified limit of elements (that is last <limit> EntryLog
			// entities ordered by creation time)
			List<EntryLog> entryLogItems = dbsp.getEntryLogListByLimit(limit);
			// get the time series for specified Policy and time range
			result = selectPolicyBalanceSeries(idPolicy, entryLogItems);
		} catch (Exception e) {
			log.error("An exception occured while trying to get criteria-based PolicyHistory records! "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		return result;
	}
	
	/**
	 * Method returning TimeSeries object with the data pairs (datetime, balance) for PolicyHistory entities
	 * corresponding to the specified criteria - Policy and time range. The criteria are defined by the id of
	 * Policy entity and the range of min-max datetimes.
	 * 
	 * {@link}http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param idPolicy id of the policy for which to get the PolicyHistory entities
	 * @param minDateTime minimum datetime - lower bound of datetime range (in YYYY-mm-dd HH:mm:ss format)
	 * @param maxDateTime maximum datetime - upper bound of datetime range (in YYYY-mm-dd HH:mm:ss format) or
	 * null for now
	 * @return TimeSeries object with the data <datetime, balance> for PolicyHistory entities or null if
	 * nothing was retrieved
	 * @throws Exception when the input parameters were invalid or if another error occured
	 */
	TimeSeries getPolicyHistoryRecordsForChart(int idPolicy, String minDateTime, String maxDateTime)
			throws Exception {
		// validate the input parameters
		if (idPolicy < 1) {
			throw new IllegalArgumentException("Invalid Policy id passed as an input parameter!");
		}
		
		try {
			validateDatetimeRange(minDateTime, maxDateTime);
		} catch (ParseException e) {
			log.error("Datetime validation error! Exception message: " + e.getMessage());
			throw e;
		} catch (IllegalArgumentException ie) {
			log.error("Datetime validation error! Exception message: " + ie.getMessage());
			throw ie;
		}
		
		TimeSeries result = null;
		try {
			// get the list of EntryLogs within the specified time range
			List<EntryLog> entryLogItems = dbsp.getEntryLogListByTimeRange(minDateTime, maxDateTime);
			// get the time series for specified Policy and time range
			result = selectPolicyBalanceSeries(idPolicy, entryLogItems);
		} catch (Exception e) {
			log.error("An exception occured while trying to get criteria-based PolicyHistory records! "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		return result;
	}
	
	/**
	 * Method returning TimeSeries object with the data pairs (datetime, collected invest unit value) for
	 * SubAccountBalanceHistory entities corresponding to the specified criteria - SubAccount, UFK name and
	 * time limit backwards. The criteria are defined by the id of SubAccount entity, name of the UFK and the
	 * number of last EntryLog entities to take into account.
	 * 
	 * {@link}http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param idSubAccount id of the sub account for which to get the SubAccountBalanceHistory entities
	 * @param ufkName the name of the UFK for which to get the SubAccountBalanceHistory entities
	 * @param limit number of last EntryLog entities (ordered by creation time) to use for datetime range
	 * @return TimeSeries object with the data <datetime, collected invest unit value> for
	 * SubAccountBalanceHistory entities or null if nothing was retrieved
	 * @throws Exception when the input parameters were invalid or if another error occured
	 */
	TimeSeries getSubAccountBalanceHistoryRecordsForChart(int idSubAccount, String ufkName, int limit)
			throws Exception {
		// validate the input parameters
		if (idSubAccount < 1) {
			throw new IllegalArgumentException("Invalid SubAccount id passed as an input parameter!");
		}
		if (ufkName == null || ufkName.isEmpty()) {
			throw new IllegalArgumentException("Invalid UFK name passed as an input parameter!");
		}
		if (limit < Utils.REPORT_RECORDS_COUNT_MIN_RANGE || limit > Utils.REPORT_RECORDS_COUNT_MAX_RANGE) {
			throw new IllegalArgumentException(
					"Limit for SubAccountBalanceHistory is beyond the allowed range <"
							+ Utils.REPORT_RECORDS_COUNT_MIN_RANGE + " - "
							+ Utils.REPORT_RECORDS_COUNT_MAX_RANGE + ">!");
		}
		
		TimeSeries result = null;
		try {
			// get the list of EntryLogs with the specified limit of elements (that is last <limit> EntryLog
			// entities ordered by creation time)
			List<EntryLog> entryLogItems = dbsp.getEntryLogListByLimit(limit);
			// get the time series for specified SubAccount, UFK name and time range
			result = selectSubAccountBalanceHistoryBalanceSeries(idSubAccount, ufkName, entryLogItems);
		} catch (Exception e) {
			log.error("An exception occured while trying to get criteria-based SubAccountBalanceHistory records! "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		return result;
	}
	
	/**
	 * Method returning TimeSeries object with the data pairs (datetime, collected invest unit value) for
	 * SubAccountBalanceHistory entities corresponding to the specified criteria - SubAccount, UFK name and
	 * time range. The criteria are defined by the id of SubAccount entity, name of the UFK and the range of
	 * min-max datetimes.
	 * 
	 * {@link}http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param idSubAccount id of the sub account for which to get the SubAccountBalanceHistory entities
	 * @param ufkName the name of the UFK for which to get the SubAccountBalanceHistory entities
	 * @param minDateTime minimum datetime - lower bound of datetime range (in YYYY-mm-dd HH:mm:ss format)
	 * @param maxDateTime maximum datetime - upper bound of datetime range (in YYYY-mm-dd HH:mm:ss format) or
	 * null for now
	 * @return TimeSeries object with the data <datetime, collected invest unit value> for
	 * SubAccountBalanceHistory entities or null if nothing was retrieved
	 * @throws Exception when the input parameters were invalid or if another error occured
	 */
	TimeSeries getSubAccountBalanceHistoryRecordsForChart(int idSubAccount, String ufkName,
			String minDateTime, String maxDateTime) throws Exception {
		// validate the input parameters
		if (idSubAccount < 1) {
			throw new IllegalArgumentException("Invalid SubAccount id passed as an input parameter!");
		}
		if (ufkName == null || ufkName.isEmpty()) {
			throw new IllegalArgumentException("Invalid UFK name passed as an input parameter!");
		}
		
		try {
			validateDatetimeRange(minDateTime, maxDateTime);
		} catch (ParseException e) {
			log.error("Datetime validation error! Exception message: " + e.getMessage());
			throw e;
		} catch (IllegalArgumentException ie) {
			log.error("Datetime validation error! Exception message: " + ie.getMessage());
			throw ie;
		}
		
		TimeSeries result = null;
		try {
			// get the list of EntryLogs within the specified time range
			List<EntryLog> entryLogItems = dbsp.getEntryLogListByTimeRange(minDateTime, maxDateTime);
			// get the time series for specified SubAccount, UFK name and time range
			result = selectSubAccountBalanceHistoryBalanceSeries(idSubAccount, ufkName, entryLogItems);
		} catch (Exception e) {
			log.error("An exception occured while trying to get criteria-based SubAccountBalanceHistory records! "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		return result;
	}
	
	/**
	 * Method returning TimeSeries object with the data pairs (datetime, balance) for SubAccountHistory
	 * entities corresponding to the specified criteria - SubAccount and time limit backwards. The criteria
	 * are defined by the id of SubAccount entity and the number of last EntryLog entities to take into
	 * account.
	 * 
	 * {@link}http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param idSubAccount id of the sub account for which to get the SubAccountHistory entities
	 * @param limit number of last EntryLog entities (ordered by creation time) to use for datetime range
	 * @return TimeSeries object with the data <datetime, balance> for SubAccountHistory entities or null if
	 * nothing was retrieved
	 * @throws Exception when the input parameters were invalid or if another error occured
	 */
	TimeSeries getSubAccountHistoryRecordsForChart(int idSubAccount, int limit) throws Exception {
		// validate the input parameters
		if (idSubAccount < 1) {
			throw new IllegalArgumentException("Invalid SubAccount id passed as an input parameter!");
		}
		if (limit < Utils.REPORT_RECORDS_COUNT_MIN_RANGE || limit > Utils.REPORT_RECORDS_COUNT_MAX_RANGE) {
			throw new IllegalArgumentException("Limit for SubAccountHistory is beyond the allowed range <"
					+ Utils.REPORT_RECORDS_COUNT_MIN_RANGE + " - " + Utils.REPORT_RECORDS_COUNT_MAX_RANGE
					+ ">!");
		}
		
		TimeSeries result = null;
		try {
			// get the list of EntryLogs with the specified limit of elements (that is last <limit>
			// EntryLog entities ordered by creation time)
			List<EntryLog> entryLogItems = dbsp.getEntryLogListByLimit(limit);
			// get the time series for specified SubAccount and time range
			result = selectSubAccountBalanceSeries(idSubAccount, entryLogItems);
		} catch (Exception e) {
			log.error("An exception occured while trying to get criteria-based SubAccountHistory records! "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		return result;
	}
	
	/**
	 * Method returning TimeSeries object with the data pairs (datetime, balance) for SubAccountHistory
	 * entities corresponding to the specified criteria - SubAccount and time range. The criteria are defined
	 * by the id of SubAccount entity and the range of min-max datetimes.
	 * 
	 * {@link}http://www.jfree.org/jfreechart/api/javadoc/org/jfree/data/time/TimeSeries.html
	 * 
	 * @param idSubAccount id of the sub account for which to get the SubAccountHistory entities
	 * @param minDateTime minimum datetime - lower bound of datetime range (in YYYY-mm-dd HH:mm:ss format)
	 * @param maxDateTime maximum datetime - upper bound of datetime range (in YYYY-mm-dd HH:mm:ss format) or
	 * null for now
	 * @return TimeSeries object with the data <datetime, balance> for SubAccountHistory entities or null if
	 * nothing was retrieved
	 * @throws Exception when the input parameters were invalid or if another error occured
	 */
	TimeSeries getSubAccountHistoryRecordsForChart(int idSubAccount, String minDateTime, String maxDateTime)
			throws Exception {
		// validate the input parameters
		if (idSubAccount < 1) {
			throw new IllegalArgumentException("Invalid SubAccount id passed as an input parameter!");
		}
		
		try {
			validateDatetimeRange(minDateTime, maxDateTime);
		} catch (ParseException e) {
			log.error("Datetime validation error! Exception message: " + e.getMessage());
			throw e;
		} catch (IllegalArgumentException ie) {
			log.error("Datetime validation error! Exception message: " + ie.getMessage());
			throw ie;
		}
		
		TimeSeries result = null;
		try {
			// get the list of EntryLogs within the specified time range
			List<EntryLog> entryLogItems = dbsp.getEntryLogListByTimeRange(minDateTime, maxDateTime);
			// get the time series for specified SubAccount and time range
			result = selectSubAccountBalanceSeries(idSubAccount, entryLogItems);
		} catch (Exception e) {
			log.error("An exception occured while trying to get criteria-based SubAccountHistory records! "
					+ e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		return result;
	}
}
