/**
 * WebSiteHandler class used to manage all Aegon Your Account online application.
 * The class methods perform various operations related with the Aegon website, such as:
 * - handling web browser,
 * - logging user in,
 * - veryfing the correctness of current web page,
 * - managing websites workflow,
 * - collecting all policy data (sub accounts, UFK, investments),
 * - providing methods to get collected data,
 * - logging user out,
 * - handling errors,
 * - logging various information.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */

package pl.aegon.youraccount.website;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import pl.aegon.youraccount.entity.*;
import pl.aegon.youraccount.pages.*;
import pl.aegon.youraccount.utils.Utils;
import pl.aegon.youraccount.config.ConfigurationProvider;
import org.apache.logging.log4j.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import com.google.common.base.Stopwatch;

public class WebSiteHandler {
	private static org.apache.logging.log4j.Logger log = LogManager.getLogger(WebSiteHandler.class);
	private WebDriver driver;
	private String customUrl;
	private String userLanguage;
	
	// Aegon web pages objects
	private YourAccountLoginPage loginPage;
	private YourAccountLogoutPage logoutPage;
	private YourAccountMainPage mainPage;
	private YourAccountPolicyDataPage policyDataPage;
	private YourAccountSubAccountDetailsPage subAccountDetailsPage;

	// Aegon entity objects and their lists
	private EntryLog entryLogEntity = null;
	private List<Policy> policyEntityList = null;
	private List<PolicyHistory> policyHistoryEntityList = null;
	private List<SubAccount> subAccountEntityList = null;
	private List<SubAccountHistory> subAccountHistoryEntityList = null;
	private List<SubAccountBalanceHistory> subAccountBalanceHistoryEntityList = null;

	/**
	 * Default class constructor.
	 */
	public WebSiteHandler() {
		this.userLanguage = ConfigurationProvider.getPropertyValue("user_language");
	}
	
	/**
	 * This method initializes web browser (based on user choice) and verifies the correctness of
	 * its selection. If the browser is started properly, the method loads main page of Aegon
	 * website according to url configuration.
	 * The method can be accessed only within current package (package-private access).
	 * 
	 * @throws Exception when selected browser was not initialized or if another error occured
	 */
	void setUpBrowser() throws Exception {
		String browserName = ConfigurationProvider.getPropertyValue("browser_selected");
		String browserPropertyPath;

		try {
			switch (browserName) {
			case "firefox":
				// disable flash plugin for firefox
				FirefoxProfile firefoxProfile = new FirefoxProfile();
				firefoxProfile.setPreference("plugin.state.flash", 0);
				// to avoid issues with predefined page titles, make sure that Firefox will use
				// polish language only
				firefoxProfile.setPreference("intl.accept_languages", "pl");
				this.driver = new FirefoxDriver(firefoxProfile);

				log.info("User selected browser is Firefox.");
				break;
			case "chrome":
				browserPropertyPath = System.getProperty("user.dir")
						+ "\\bin\\drivers\\chromedriver.exe";
				if (Utils.checkIfBrowserPathValid(browserPropertyPath)) {
					System.setProperty("webdriver.chrome.driver", browserPropertyPath);
					// to avoid issues with predefined page titles, make sure that Chrome will use
					// polish language only
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--lang=es");
					this.driver = new ChromeDriver(options);
				}

				log.info("User selected browser is Chrome.");
				break;
			case "iexplorer":
				browserPropertyPath = System.getProperty("user.dir")
						+ "\\bin\\drivers\\IEDriverServer.exe";
				if (Utils.checkIfBrowserPathValid(browserPropertyPath)) {
					System.setProperty("webdriver.ie.driver", browserPropertyPath);
					this.driver = new InternetExplorerDriver();
				}

				log.info("User selected browser is Internet Explorer.");
				break;
			case "phantomjs":
				browserPropertyPath = System.getProperty("user.dir")
						+ "\\bin\\drivers\\phantomjs.exe";
				if (Utils.checkIfBrowserPathValid(browserPropertyPath)) {
					System.setProperty("phantomjs.binary.path", browserPropertyPath);
					this.driver = new PhantomJSDriver();
				}

				log.info("User selected browser is PhantomJS.");
				break;
			default:
				// if no browser passed throw exception
				log.error("Selected browser is not supported!");
				throw new IllegalArgumentException("Browser name is not correct!");
			}

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			if (this.customUrl != null && !this.customUrl.isEmpty()) {
				driver.get(customUrl);
			} else {
				driver.get(ConfigurationProvider.getPropertyValue("website_url"));
			}
		} catch (Exception e) {
			log.error("An exception occured! Exception: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));

			if (this.driver == null) {
				log.error("Could not initialize " + browserName + " browser!");
				log.error("Program needs to terminate now.");
				
				String trayTitle = ConfigurationProvider.getMessageByLanguage("aegon_app_name", userLanguage);
				String trayMsg = ConfigurationProvider.getMessageByLanguage("browser_error_msg", userLanguage);
				Utils.showTrayMessage(trayTitle, trayMsg, Utils.ERROR);
			}

			throw e;
		}
	}

	/**
	 * The method closing web browser after it is not needed anymore.
	 * The method can be accessed only within current package (package-private access).
	 * 
	 * @throws Exception when the browser was not initialized before at all
	 */
	void tearDownBrowser() throws Exception {
		if (driver != null) {
			driver.quit();
		} else {
			log.error("Web browser driver was not initialized!");
			throw new IllegalArgumentException("Web browser driver was not initialized!");
		}
	}

	/**
	 * This method checks the first Aegon web page - that is login page - if it has the correct
	 * title (comparing to expected one). This is important to make sure that the login form
	 * is available and that logging to Aegon application will take place.
	 * 
	 * @throws Exception when login page title is not as expected or if another error occured
	 */
	private void checkLoginPage() throws Exception {
		String logMessage;
		loginPage = new YourAccountLoginPage(this.driver);

		// verify login page title
		try {
			String actLoginTitle = loginPage.getLoginPageTitle();
			String expLoginTitle = ConfigurationProvider.getPropertyValue("expect_login_title");

			/* If it happens, it can mean 3 scenarios:
				1) there was an error in retrieval of expected title (for example problem with encoding),
				2) Selenium WebDriver returned invalid title (tests show that this is only possible for Internet Explorer),
				3) the title of the Aegon login page has really changed and the expected value is out of date.
			*/
			if (!actLoginTitle.equalsIgnoreCase(expLoginTitle)) {
				logMessage = "Login page title is different than expected! Retrieved value is: ["
						+ actLoginTitle + "] I will try to continue...";
				log.warn(logMessage);
				throw new WebDriverException("Login web page title is different than expected!");
			}
		} catch (WebDriverException e) {
			log.warn(Utils.getExceptionStackTrace(e));
			throw e;
		} catch (Exception ex) {
			log.error("An exception occured! " + ex.getMessage());
			log.error(Utils.getExceptionStackTrace(ex));
			throw ex;
		}
	}

	/**
	 * This methods performs logging user to Aegon application and checks if logging was successful
	 * by verifying the landing page (main page). If there was any error, the method throws the
	 * exception which can be caught at higher call level (bubbling up exception).
	 * 
	 * @throws Exception when logging failed, the main page title is not as expected or if another
	 * error occured
	 */
	public void loginToApplicationAndCheckMainPage() throws Exception {
		String logMessage;

		this.checkLoginPage();
		mainPage = new YourAccountMainPage(this.driver);

		try {
			log.info("Started logging to Aegon Your Account application...");
			String login = ConfigurationProvider.getPropertyValue("user_login");
			String password = ConfigurationProvider.getPropertyValue("user_password");
			loginPage.loginToYourAccount(login, password);

			String actMainTitle = mainPage.getMainPageTitle();
			String expMainTitle = ConfigurationProvider.getPropertyValue("expect_main_title");

			// if main page title is the title of login page, it means we are still on login page,
			// so logging failed
			if (actMainTitle.equals(ConfigurationProvider.getPropertyValue("expect_login_title"))) {
				// try to get the error message from login page
				String loginError = null;
				try {
					loginError = loginPage.getErrorField();
				} catch (NoSuchElementException e) {
					log.warn("An exception occured: login error message does not exist!");
					log.warn(Utils.getExceptionStackTrace(e));
				}

				if (loginError != null && !loginError.isEmpty()) {
					logMessage = "Logging operation failed! Website returned message: " + loginError;
					log.error(logMessage);
					String trayTitle = ConfigurationProvider.getMessageByLanguage("aegon_app_name", userLanguage);
					String trayMsg = ConfigurationProvider.getMessageByLanguage("login_error_msg", userLanguage);
					Utils.showTrayMessage(trayTitle, trayMsg + loginError, Utils.ERROR);
					this.tearDownBrowser();
					throw new Exception(logMessage);
					//System.exit(1);
				} else {
					logMessage = "Unexpected situation occured! Logging operation failed but could not determine the reason of it.";
					throw new Exception(logMessage);
				}
			}

			String actMainHeader = mainPage.getMainHeaderTitle();
			String expMainHeader = ConfigurationProvider.getPropertyValue("expect_main_header");

			// verify main page title
			if (!actMainTitle.equalsIgnoreCase(expMainTitle)) {
				logMessage = "Main page title is different than expected! Retrieved value is: ["
						+ actMainTitle + "] I will try to continue...";
				log.warn(logMessage);
				throw new WebDriverException(
						"Main web page title is different than expected! Current page is probably wrong.");
			}

			// verify main page header
			if (!actMainHeader.equals(expMainHeader)) {
				logMessage = "Main page header is different than expected! Retrieved value is: ["
						+ actMainHeader + "] I will try to continue...";
				log.warn(logMessage);
				throw new WebDriverException(
						"Main web header  is different than expected! Current page is probably wrong.");
			}

			log.info("User has been logged in successfully!");
		} catch (WebDriverException e) {
			log.error(Utils.getExceptionStackTrace(e));
		} catch (Exception ex) {
			log.error("An exception occured! " + ex.getMessage());
			log.error(Utils.getExceptionStackTrace(ex));
			throw ex;
		}
	}

	/**
	 * The methods collects all general data (name and surname, last update and last logging dates,
	 * date of "data from day") from main Aegon web page ("Twoje Polisy") and creates a new EntryLog
	 * entity.
	 * 
	 * @throws Exception when reading and/or storing data in the entity failed
	 */
	public void collectAndStoreGeneralData() throws Exception {
		try {
			// get general values from the main page and build the DB entity
			String fullName = mainPage.getFullNameField().trim();
			log.info("Started collecting general data for Aegon user [" + fullName + "]");

			String dataOnDay = mainPage.getDataOnDayField().trim();
			String lastModifiedDate = mainPage.getLastModifiedDateField().trim();
			String lastLoggedDate = mainPage.getLastLoggedDateField().trim();
			float totalField = Utils.getFloatFromStringField(mainPage.getTotalField());

			EntryLog entryLog = new EntryLog();
			entryLog.setCurrentBalance(totalField);
			entryLog.setDataFromDayDate(dataOnDay);
			entryLog.setLastLoginDate(lastLoggedDate);
			entryLog.setLastModifiedDate(lastModifiedDate);
			this.entryLogEntity = entryLog;

			log.info("Finished collecting general data.");
		} catch (Exception e) {
			log.error("Collecting general data on the main page failed! Exception raised: " + e.getMessage());
			throw e;
		}
	}

	/**
	 * The method collects all policies data (policy name, number, product name and ballance) from
	 * the main Aegon web page and stores them in newly created Policy entity (one for each policy
	 * found). Additionaly, for each policy a PolicyHistory entity is created.
	 * 
	 * @throws Exception when there was incoherence in collected data or reading and/or storing data
	 * in the entities failed
	 */
	public void collectAndStorePolicyData() throws Exception {
		try {
			// get detailed policy values from the main page and build the DB
			// entities
			log.info("Started collecting policy data for Aegon user ["
					+ mainPage.getFullNameField().trim() + "]");

			List<String> policyNameList = mainPage.getPolicyNameFieldList();
			List<String> policyNumberList = mainPage.getPolicyNumberFieldList();
			List<String> productNameList = mainPage.getProductNameFieldList();
			List<String> balanceList = mainPage.getBalanceFieldList();

			// check if all the lists are not empty
			if (policyNameList.isEmpty()) {
				throw new NoSuchElementException("No policy name found on the main page!");
			}
			if (policyNumberList.isEmpty()) {
				throw new NoSuchElementException("No policy number found on the main page!");
			}
			if (productNameList.isEmpty()) {
				throw new NoSuchElementException("No product name found on the main page!");
			}
			if (balanceList.isEmpty()) {
				throw new NoSuchElementException("No policy balance found on the main page!");
			}

			assert policyNameList.size() == policyNumberList.size();
			assert productNameList.size() == balanceList.size();
			assert policyNameList.size() == productNameList.size();

			// iterate through all the lists and assign their elements in the
			// right order to Policy and PolicyHistory entities
			// all lists should have the same size
			this.policyEntityList = new ArrayList<Policy>();
			this.policyHistoryEntityList = new ArrayList<PolicyHistory>();

			for (int i = 0; i < policyNameList.size(); i++) {
				float currentBalance = Utils.getFloatFromStringField(balanceList.get(i));
				Policy policy = new Policy();
				policy.setCurrentBalance(currentBalance);
				policy.setPolicyName(policyNameList.get(i));
				policy.setPolicyNumber(policyNumberList.get(i));
				policy.setProductName(productNameList.get(i));

				// add new Policy to policies list
				this.policyEntityList.add(policy);

				// add new PolicyHistory to policy histories list
				PolicyHistory policyHistory = new PolicyHistory(currentBalance,
						this.entryLogEntity, policy);
				this.policyHistoryEntityList.add(policyHistory);
			}

			log.info("Finished collecting policy data. Number of collected policies: "
					+ this.policyEntityList.size());
		} catch (Exception e) {
			log.error("Collecting policy data on the main page failed! Exception raised: "
					+ e.getMessage());
			throw e;
		}
	}

	/**
	 * The method collects general sub account data (name and balance) for all sub accounts assigned
	 * to the given policy. The data are read from the Aegon policy details web page
	 * ("Wybierz subkonto") and stored in a newly created SubAccount entity (for each sub account
	 * found). Additionally, for each sub account a SubAccountHistory entity is created.
	 * 
	 * @param policyIndex the index of the policy (in policyEntityList list) for which to get and
	 * read sub accounts data
	 * @throws Exception when there was incoherence in collected data or reading and/or storing data
	 * in the entities failed
	 */
	public void collectAndStoreSubAccountData(int policyIndex) throws Exception {
		try {
			// first try to get to the page with specified policy details
			mainPage.goToPolicyDetailsPage(policyIndex);
			policyDataPage = new YourAccountPolicyDataPage(this.driver);

			// verify policy details page title
			String actPolicyTitle = policyDataPage.getPolicyDataPageTitle();
			String expPolicyTitle = ConfigurationProvider.getPropertyValue("expect_policy_title");

			if (!actPolicyTitle.equalsIgnoreCase(expPolicyTitle)) {
				String logMessage = "Policy page title is different than expected! Retrieved value is: ["
						+ actPolicyTitle + "] I will try to continue...";
				log.warn(logMessage);
				throw new WebDriverException(
						"Policy web page title is different than expected! Current page is probably wrong.");
			}
		} catch (WebDriverException e) {
			log.warn(Utils.getExceptionStackTrace(e));
		} catch (Exception ex) {
			log.error("An exception occured! " + ex.getMessage());
			log.error(Utils.getExceptionStackTrace(ex));
		}

		try {
			String currentPolicyName = this.policyEntityList.get(policyIndex).getPolicyName();
			// get general sub account values from the policy page and build the DB entities
			log.info("Started collecting sub account general data for policy [" + currentPolicyName
					+ "]");

			List<String> subAccountNameList = policyDataPage.getSubAccountNameFieldList();
			List<String> subAccountBalanceList = policyDataPage.getSubAccountBalanceFieldList();

			// check if all the lists are not empty
			if (subAccountNameList.isEmpty()) {
				throw new NoSuchElementException("No sub account name found on the policy page!");
			}
			if (subAccountBalanceList.isEmpty()) {
				throw new NoSuchElementException("No sub account balance found on the policy page!");
			}

			assert subAccountNameList.size() == subAccountBalanceList.size();

			// iterate through all the lists and assign their elements in the
			// right order to SubAccount and SubAccountHistory entities
			// all lists should have the same size
			if (this.subAccountEntityList == null) {
				this.subAccountEntityList = new ArrayList<SubAccount>();
			}
			if (this.subAccountHistoryEntityList == null) {
				this.subAccountHistoryEntityList = new ArrayList<SubAccountHistory>();
			}

			for (int i = 0; i < subAccountNameList.size(); i++) {
				float currentBalance = Utils.getFloatFromStringField(subAccountBalanceList.get(i));
				SubAccount subAccount = new SubAccount();
				subAccount.setCurrentBalance(currentBalance);
				subAccount.setSubAccountName(subAccountNameList.get(i));
				subAccount.setPolicy(this.policyEntityList.get(policyIndex));

				// add new SubAccount to sub accounts list
				this.subAccountEntityList.add(subAccount);

				// add new SubAccountHistory to sub account histories list
				SubAccountHistory subAccountHistory = new SubAccountHistory(currentBalance,
						this.entryLogEntity, subAccount);
				this.subAccountHistoryEntityList.add(subAccountHistory);
			}

			log.info("Finished collecting sub account general data. Number of collected sub accounts: "
					+ subAccountNameList.size());
		} catch (Exception e) {
			log.error("Collecting policy data on the policy page failed! Exception raised: "
					+ e.getMessage());
			throw e;
		}
	}

	/**
	 * The method collects detailed sub account data (UFK name, invest unit value, invest unit count
	 * and collected invest units value) for all UFKs (Insurance capital funds) found for given sub
	 * account. The data are read from the Aegon sub account details web page ("Saldo Subkonta") and
	 * stored in a newly created SubAccountBalanceHistory entity (for each UFK found).
	 * 
	 * @param policySubAccountIndex the index of current sub account across all collected sub
	 * accounts (stored in subAccountEntityList list) - this is used to access the
	 * element in subAccountEntityList for which the sub accounts details (UFK data) are
	 * retrieved
	 * @param currentSubAccountIndex the index of current sub account for given policy (starting
	 * from 0) on the policy data page - this is used to navigate to sub account details
	 * page to read UFK data from it
	 * @throws Exception when sub account details page title is not as expected or when there was
	 * incoherence in collected data
	 */
	public void collectAndStoreSubAccountDetailsData(int policySubAccountIndex, int currentSubAccountIndex) throws Exception {
		try {
			// first try to get to the page with specified sub account details (based on the sub account index on the current page ("Wybierz subkonto")
			policyDataPage.goToSubAccountDetailsPage(currentSubAccountIndex);
			subAccountDetailsPage = new YourAccountSubAccountDetailsPage(this.driver);

			// verify sub account details page title
			String actSubAccountTitle = subAccountDetailsPage.getSubAccountDetailsPageTitle();
			String expSubAccountTitle = ConfigurationProvider
					.getPropertyValue("expect_subaccount_title");

			if (!actSubAccountTitle.equalsIgnoreCase(expSubAccountTitle)) {
				String logMessage = "Sub account details page title is different than expected! Retrieved value is: ["
						+ actSubAccountTitle + "] I will try to continue...";
				log.warn(logMessage);
				throw new WebDriverException(
						"Sub account details page title is different than expected! Current page is probably wrong.");
			}
		} catch (WebDriverException e) {
			log.warn(Utils.getExceptionStackTrace(e));
		} catch (Exception ex) {
			log.error("An exception occured! " + ex.getMessage());
			log.error(Utils.getExceptionStackTrace(ex));
		}

		try {
			// get the name of the sub account for which the details are going to be retrieved
			String currentSubAccountName = this.subAccountEntityList.get(policySubAccountIndex)
					.getSubAccountName();
			// get detailed sub account values from the sub account details page and build the DB entities
			log.info("Started collecting sub account details for sub account [" + currentSubAccountName + "]");

			List<String> ufkNameList = subAccountDetailsPage.getUfkNameFieldList();
			List<String> ufkInvestUnitValueList = subAccountDetailsPage
					.getUfkInvestUnitValueFieldList();
			List<String> ufkInvestUnitNumberList = subAccountDetailsPage
					.getUfkInvestUnitNumberFieldList();
			List<String> ufkCollectedInvestUnitValueList = subAccountDetailsPage
					.getUfkCollectedInvestUnitValueFieldList();

			// check if all the lists are not empty
			if (ufkNameList.isEmpty()) {
				throw new NoSuchElementException(
						"No UFK name found on the sub account details page!");
			}
			if (ufkInvestUnitValueList.isEmpty()) {
				throw new NoSuchElementException(
						"No UFK invest unit value found on the sub account details page!");
			}
			if (ufkInvestUnitNumberList.isEmpty()) {
				throw new NoSuchElementException(
						"No UFK invest unit number found on the sub account details page!");
			}
			if (ufkCollectedInvestUnitValueList.isEmpty()) {
				throw new NoSuchElementException(
						"No UFK collected invest unit value found on the sub account details page!");
			}

			assert ufkNameList.size() == ufkInvestUnitValueList.size();
			assert ufkInvestUnitNumberList.size() == ufkCollectedInvestUnitValueList.size();
			assert ufkNameList.size() == ufkInvestUnitNumberList.size();

			// iterate through all the lists and assign their elements in the right order to
			// SubAccountBalanceHistory entity
			// all lists should have the same size
			if (this.subAccountBalanceHistoryEntityList == null) {
				this.subAccountBalanceHistoryEntityList = new ArrayList<SubAccountBalanceHistory>();
			}

			for (int i = 0; i < ufkNameList.size(); i++) {
				SubAccountBalanceHistory subAccountBalanceHistory = new SubAccountBalanceHistory();
				subAccountBalanceHistory.setUfkInvestUnitCollectValue(Utils
						.getFloatFromStringField(ufkCollectedInvestUnitValueList.get(i)));
				subAccountBalanceHistory.setUfkInvestUnitCount(Utils
						.getDoubleFromStringField(ufkInvestUnitNumberList.get(i)));
				subAccountBalanceHistory.setUfkInvestUnitValue(Utils
						.getDoubleFromStringField(ufkInvestUnitValueList.get(i)));
				subAccountBalanceHistory.setUfkName(ufkNameList.get(i));
				subAccountBalanceHistory.setEntryLog(this.entryLogEntity);
				subAccountBalanceHistory.setSubAccount(this.subAccountEntityList
						.get(policySubAccountIndex));

				// add new SubAccountBalanceHistory to sub account balance
				// history list
				this.subAccountBalanceHistoryEntityList.add(subAccountBalanceHistory);
			}

			log.info("Finished collecting sub account details. Number of collected sub account UFKs: "
					+ ufkNameList.size());

			// return to policy details page (sub account list page)
			subAccountDetailsPage.goToSubAccountBalancePage();
		} catch (Exception e) {
			log.error("Collecting sub account details on the sub account page failed! Exception raised: "
					+ e.getMessage());
			throw e;
		}
	}

	/**
	 * The method logs out the user from Aegon application. Before this happens the main and the logout
	 * pages are checked under the angle of page title correctness.
	 * 
	 * @throws Exception when main page/logout page title is not as expected or if an error occured
	 * during log out operation
	 */
	public void logoutFromApplication() throws Exception {
		String logMessage;
		String actMainTitle = mainPage.getMainPageTitle();
		String expMainTitle = ConfigurationProvider.getPropertyValue("expect_main_title");

		String actMainHeader = mainPage.getMainHeaderTitle();
		String expMainHeader = ConfigurationProvider.getPropertyValue("expect_main_header");

		try {
			log.info("Started logging out from Aegon Your Account application...");
			if (!actMainTitle.equalsIgnoreCase(expMainTitle)) {
				logMessage = "Main page title is different than expected! Retrieved value is: ["
						+ actMainTitle + "] I will try to continue...";
				log.warn(logMessage);
				throw new WebDriverException(
						"Main web page title is different than expected! Current page is probably wrong.");
			}

			if (!actMainHeader.equals(expMainHeader)) {
				logMessage = "Main page header is different than expected! Retrieved value is: ["
						+ actMainHeader + "] I will try to continue...";
				log.warn(logMessage);
				throw new WebDriverException(
						"Main web header  is different than expected! Current page is probably wrong.");
			}

			mainPage.logoutFromYourAccount();

			logoutPage = new YourAccountLogoutPage(this.driver);
			String actLogoutTitle = logoutPage.getLogoutPageTitle();
			String expLogoutTitle = ConfigurationProvider.getPropertyValue("expect_logout_title");
			String actLogoutMsg = logoutPage.getMessageBox();
			String expLogoutMsg = ConfigurationProvider.getPropertyValue("expect_logout_msg");

			if (!actLogoutTitle.equalsIgnoreCase(expLogoutTitle)) {
				logMessage = "Logout page title is different than expected! Retrieved value is: ["
						+ actLogoutTitle + "] I will try to continue...";
				log.warn(logMessage);
				throw new Exception(
						"Logout web page title is different than expected! Logging out may have failed.");
			}

			if (!actLogoutMsg.contains(expLogoutMsg)) {
				logMessage = "Logout page message is different than expected! Retrieved value is: ["
						+ actLogoutMsg + "] I will try to continue...";
				log.warn(logMessage);
				throw new Exception(
						"Logout web page message is different than expected! Logging out may have failed.");
			}

			log.info("User has been logged out successfully!");
		} catch (WebDriverException e) {
			log.error(Utils.getExceptionStackTrace(e));
		} catch (Exception ex) {
			log.error("An exception occured! " + ex.getMessage());
			log.error(Utils.getExceptionStackTrace(ex));
			throw ex;
		}
	}

	/**
	 * This is the main method in the class which should be used as the only one for bulk collecting
	 * of all Aegon data in the external calls. The method implements all the actual logic - starts
	 * the browser, logs the user, traverses through all web pages and collects (with storing in
	 * internal entities lists) all relevant data linked to the whole Aegon investment (which is
	 * available through the application). When all necessary data are retrieved, the method logs
	 * out the user and closes the browser.
	 * 
	 * @throws Exception when there was any error in the collecting process or when one of the
	 * underlying methods failed
	 */
	public void collectAllRelevantDataFromApplication() throws Exception {
		// set the timer to measure the time of downloading all Aegon data
		Stopwatch timer = Stopwatch.createUnstarted();
		timer.start();
		log.info("Collecting all relevant data from the webpage started.");

		String trayTitle = ConfigurationProvider.getMessageByLanguage("aegon_app_name", userLanguage);
		String trayMsg = ConfigurationProvider.getMessageByLanguage("download_start_msg", userLanguage);
		Utils.showTrayMessage(trayTitle, trayMsg, Utils.INFO);
		
		// start the session and open browser
		try {
			this.setUpBrowser();
		} catch (Exception e) {
			log.fatal("Fatal exception occured! Exception: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			System.exit(1);
		}

		try {
			// try to login to Aegon application
			// loginPage and mainPage initialized
			this.loginToApplicationAndCheckMainPage();
			// EntryLog data retrieved
			this.collectAndStoreGeneralData();
			// Policy and PolicyHistory data retrieved
			this.collectAndStorePolicyData();

			// the index of previous subAccountEntityList list size, used to access the proper sub
			// account index (when there is more than one policy) from all retrieved sub accounts
			// list
			int lastSubAccountSizeIdx = 0;
			// for each policy found get the sub account(s) assigned to it
			for (int i = 0; i < this.policyEntityList.size(); i++) {
				// policyDataPage initialized
				// SubAccount and SubAccountHistory data retrieved
				this.collectAndStoreSubAccountData(i);

				// get the sub account details for each sub account found in the current policy
				// the range of <for> loop is taking into account only this part of all sub accounts
				// list,
				// which are corresponding to i-th policy
				for (int j = lastSubAccountSizeIdx; j < this.subAccountEntityList.size(); j++) {
					// the index of the currently processed sub account in terms of given policy
					// (starting from 0)
					int currentSubAccountIndex = j - lastSubAccountSizeIdx;
					// subAccountDetailsPage initialized
					// SubAccountBalanceHistory data retrieved
					this.collectAndStoreSubAccountDetailsData(j, currentSubAccountIndex);
				}

				// update the variable
				lastSubAccountSizeIdx = this.subAccountEntityList.size();

				// return to all policies list page (main page)
				policyDataPage.goToPolicyListPage();
			}

			// log summary data
			log.info("Finished collecting all sub accounts general data. Number of total collected sub accounts: "
					+ this.subAccountEntityList.size());
			log.info("Finished collecting all sub accounts details. Number of total collected sub account UFKs: "
					+ this.subAccountBalanceHistoryEntityList.size());

			// try to logout from Aegon application
			// logoutPage initialized
			this.logoutFromApplication();
			// finish the session and close browser
			this.tearDownBrowser();

			trayMsg = ConfigurationProvider.getMessageByLanguage("download_stop_msg", userLanguage);
			Utils.showTrayMessage(trayTitle, trayMsg, Utils.INFO);
			log.info("Collecting all relevant data from the webpage finished successfully.");
		} catch (Exception ex) {
			log.fatal("Fatal exception occured! Exception: " + ex.getMessage());
			log.error(Utils.getExceptionStackTrace(ex));
			trayMsg = ConfigurationProvider.getMessageByLanguage("download_error_msg", userLanguage);
			Utils.showTrayMessage(trayTitle, trayMsg, Utils.ERROR);

			// finish the session and close browser even in case of exception
			this.tearDownBrowser();
			throw ex;
		}

		timer.stop();
		log.info("Collecting all relevant data took: " + timer.toString());
	}

	/**
	 * Returns the EntryLog entity object with data collected from the Aegon main page.
	 * 
	 * @return the entryLogEntity
	 */
	public EntryLog getEntryLogEntity() {
		return this.entryLogEntity;
	}

	/**
	 * Returns the List (possibly one element only) of Policy entity objects with data collected
	 * from the Aegon main page.
	 * 
	 * @return the policyEntityList
	 */
	public List<Policy> getPolicyEntityList() {
		return this.policyEntityList;
	}

	/**
	 * Returns the List (possibly one element only) of PolicyHistory entity objects with data
	 * collected from the Aegon main page.
	 * 
	 * @return the policyHistoryEntityList
	 */
	public List<PolicyHistory> getPolicyHistoryEntityList() {
		return this.policyHistoryEntityList;
	}

	/**
	 * Returns the List (possibly one element only) of SubAccount entity objects with data collected
	 * from the Aegon policy data page.
	 * 
	 * @return the subAccountEntityList
	 */
	public List<SubAccount> getSubAccountEntityList() {
		return this.subAccountEntityList;
	}

	/**
	 * Returns the List (possibly one element only) of SubAccountHistory entity objects with data
	 * collected from the Aegon policy data page.
	 * 
	 * @return the subAccountHistoryEntityList
	 */
	public List<SubAccountHistory> getSubAccountHistoryEntityList() {
		return this.subAccountHistoryEntityList;
	}

	/**
	 * Returns the List (rather more than one element) of SubAccountBalanceHistory entity objects
	 * with data collected from the Aegon sub account details page.
	 * 
	 * @return the subAccountBalanceHistoryEntityList
	 */
	public List<SubAccountBalanceHistory> getSubAccountBalanceHistoryEntityList() {
		return this.subAccountBalanceHistoryEntityList;
	}

	/**
	 * Sets custom url to use as Aegon main page (used for testing purposes).
	 * 
	 * @param url custom url to set
	 * @throws Exception when the given url is invalid or malformed
	 */
	public void setCustomUrl(String url) throws Exception {
		if (url != null && !url.isEmpty()) {
			this.customUrl = url;
		} else {
			throw new IllegalArgumentException("Invalid custom url passed as an argument!");
		}

		try {
			URLConnection conn = new URL(url).openConnection();
			conn.connect();
		} catch (MalformedURLException e) {
			throw e;
		}
	}

}
