/**
 * Class MailSender provides a simple and basic inteface for sending emails (planned to be notification emails)
 * in Aegon application. It uses JavaMail API for creating and sending email and Gmail account as SMTP server.
 * For more details, see the comments of each method.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.utils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import pl.aegon.youraccount.config.ConfigurationProvider;

public class MailSender {
	private String userName;
	private String password;
	private String senderName;
	private String subject;
	private String body;
	private List<String> recipientsList;
	private List<String> attachmentsList;
	private Session mailSession = null;
	
	private static org.apache.logging.log4j.Logger log = LogManager.getLogger(MailSender.class);
	private static final String EMAIL_HOST_ADDRESS = "smtp.gmail.com";
	private static final String EMAIL_SMTP_PORT = "587";
	private static final String EMAIL_SMTP_AUTH = "true";
	
	/**
	 * Default class constructor. Initializes some variables and creates the default session for sending
	 * emails.
	 */
	public MailSender() {
		try {
			// get username, password and sender name from configuration file (base64 encoded)
			String userName = ConfigurationProvider.getPropertyValue("email_username");
			byte[] decodedUserName = Base64.decodeBase64(userName);
			this.userName = new String(decodedUserName, "UTF-8");
			
			String password = ConfigurationProvider.getPropertyValue("email_password");
			byte[] decodedPassword = Base64.decodeBase64(password);
			this.password = new String(decodedPassword, "UTF-8");
			
			String senderName = ConfigurationProvider.getPropertyValue("email_sender_name");
			byte[] decodedSenderName = Base64.decodeBase64(senderName);
			this.senderName = new String(decodedSenderName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.error("An error occured while trying to decode configuration parameters! "
					+ "Exception message: " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		// setup the properties of the smtp server necessary to send emails
		Properties props = System.getProperties();
		props.put("mail.smtp.host", EMAIL_HOST_ADDRESS);
		props.put("mail.smtp.user", this.userName);
		props.put("mail.smtp.password", this.password);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", EMAIL_SMTP_PORT);
		props.put("mail.smtp.auth", EMAIL_SMTP_AUTH);
		
		// create the default session for mailing purposes
		this.mailSession = Session.getDefaultInstance(props);
	}
	
	/**
	 * Main method used to send the email to previously defined recipient(s) with configured subject, body and
	 * optional attachment(s).
	 * 
	 * @param sendAsHTML whether to send the email as text/html MIME type (depends on the body)
	 * @throws Exception when there was an error while creating or sending email
	 */
	public void sendEmail(boolean sendAsHTML) throws Exception {
		MimeMessage message = new MimeMessage(this.mailSession);
		
		try {
			if (this.userName == null || this.password == null) {
				throw new NullPointerException("Email credentials are not configured! Email cannot be sent.");
			}
			
			// set the sender, recipient(s) and subject of the message
			message.setFrom(new InternetAddress(this.userName, this.senderName));
			if (recipientsList != null && !recipientsList.isEmpty()) {
				for (String recipient : recipientsList) {
					InternetAddress recipientAddress = new InternetAddress(recipient);
					message.addRecipient(Message.RecipientType.TO, recipientAddress);
				}
			} else {
				throw new IllegalArgumentException("Recipients list cannot be null nor empty!");
			}
			message.setSubject(this.subject, "UTF-8");
			
			// create the message part and set the text/body
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			if (sendAsHTML) {
				messageBodyPart.setContent(this.body, "text/html; charset=utf-8");
			} else {
				messageBodyPart.setText(this.body, "UTF-8");
			}
			
			// create multi-part of the message and add body part to it
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			
			// add attachments if they are available
			if (this.attachmentsList != null && !this.attachmentsList.isEmpty()) {
				for (String filePath : this.attachmentsList) {
					MimeBodyPart attachmentPart = new MimeBodyPart();
					
					attachmentPart.attachFile(filePath);
					multipart.addBodyPart(attachmentPart);
				}
			}
			
			// set the multi-part as e-mail's content before sending
			message.setContent(multipart);
			
			// finally create message transport and send the message
			Transport transport = this.mailSession.getTransport("smtp");
			transport.connect(EMAIL_HOST_ADDRESS, this.userName, this.password);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			log.debug("Email has been sent successfully to the address: <" + recipientsList.get(0) + ">.");
		} catch (AddressException ae) {
			log.error("An error occured while trying to configure email addresses! Exception message: "
					+ ae.getMessage());
			log.error(Utils.getExceptionStackTrace(ae));
		} catch (MessagingException me) {
			log.error("An error occured while trying to build or send email! Exception message: "
					+ me.getMessage());
			log.error(Utils.getExceptionStackTrace(me));
			// this exception is most probable to happen, that's why we throw only this one
			throw me;
		} catch (IOException ioe) {
			log.error("An error occured while trying to access attachment file(s)! Exception message: "
					+ ioe.getMessage());
			log.error(Utils.getExceptionStackTrace(ioe));
		}
	}
	
	/**
	 * Sets the subject of the message to be sent.
	 * 
	 * @param subject subject of the message
	 * @throws NullPointerException when the subject is null or empty
	 */
	public void setSubject(String subject) throws NullPointerException {
		if (subject != null && !subject.isEmpty()) {
			this.subject = subject;
		} else {
			throw new NullPointerException("Email subject cannot be null nor empty!");
		}
	}
	
	/**
	 * Sets the content (body) of the message to be sent.
	 * 
	 * @param body the content of the message (as html or plain text)
	 * @throws NullPointerException when the body is null or empty
	 */
	public void setBody(String body) throws NullPointerException {
		if (body != null && !body.isEmpty()) {
			this.body = body;
		} else {
			throw new NullPointerException("Email body cannot be null nor empty!");
		}
	}
	
	/**
	 * Adds a new recipient of the message to be sent. This class allows to send the email to multiple
	 * recipients and each of them must be added by the call to this method.
	 * 
	 * @param recipient the email address of the recipient (with optional name of the recipient)
	 * @throws NullPointerException when recipient is null or empty
	 */
	public void addRecipient(String recipient) throws NullPointerException {
		if (recipient != null & !recipient.isEmpty()) {
			if (this.recipientsList == null) {
				this.recipientsList = new ArrayList<String>();
			}
			this.recipientsList.add(recipient);
		} else {
			throw new NullPointerException("Email recipient cannot be null nor empty!");
		}
	}
	
	/**
	 * Adds a new attachment file to be sent. This class allows to add multiple attachments and each of them
	 * must be added by the call to this method.
	 * 
	 * @param filePath the full path to the attachment file
	 * @throws NullPointerException when the file path is null or empty
	 * @throws IOException when the file denoted by path does not exist or is not a file
	 */
	public void addAttachment(String filePath) throws NullPointerException, IOException {
		if (filePath == null || filePath.isEmpty()) {
			throw new NullPointerException("The file path to attachment does not exist!");
		}
		
		// check if file denoted by path exists and is available for sending
		File attachFile = new File(filePath);
		if (!attachFile.exists() || !attachFile.isFile()) {
			throw new IOException("Cannot access attachment file or the file does not exist!");
		}
		
		if (this.attachmentsList == null) {
			this.attachmentsList = new ArrayList<String>();
		}
		this.attachmentsList.add(filePath);
	}
}
