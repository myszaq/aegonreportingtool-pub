/**
 * Utils class providing different helper methods used in AegonYourAccount project.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.*;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;

public class Utils {
	private static org.apache.logging.log4j.Logger log = LogManager.getLogger(Utils.class);
	private static TrayIcon trayIcon = null;
	public static MessageType INFO = MessageType.INFO;
	public static MessageType WARNING = MessageType.WARNING;
	public static MessageType ERROR = MessageType.ERROR;
	
	final public static int DOWNLOAD_RETRIES_TIME_INTERVAL_MIN_RANGE = 1;
	final public static int DOWNLOAD_RETRIES_TIME_INTERVAL_MAX_RANGE = 60;
	final public static int REPORT_RECORDS_COUNT_MIN_RANGE = 10;
	final public static int REPORT_RECORDS_COUNT_MAX_RANGE = 365;
	
	static public enum ChartRecordsChoice {
		DAYS, ENTRIES
	};
	
	static public enum ChartWholeAccountChoice {
		YES, NO, POLICIES
	};
	
	static public enum ChartPoliciesTypeChoice {
		JOINED, SEPARATED, SUBACCOUNTS
	};
	
	static public enum ChartScaleTypeChoice {
		COMMON, SEPARATED, LOGARITHMIC
	};
	
	static public enum ChartEntityType {
		ACCOUNT, POLICIES, SUBACCOUNTS, UFKS
	};
	
	/**
	 * Returns double value of the floating point number stored as a String.
	 * 
	 * @param number the number in String to convert
	 * @return number converted to double
	 * @throws IllegalArgumentException when given number is null or empty
	 */
	public static double getDoubleFromStringField(String number) throws IllegalArgumentException {
		if (number == null || number.isEmpty()) {
			throw new IllegalArgumentException("Wrong number passed to convert to double value!");
		}
		
		number = number.trim();
		number = number.replaceAll("[\\sA-Za-z\\.]", "");
		number = number.replace(",", ".");
		
		return Double.parseDouble(number);
	}
	
	/**
	 * Returns float value of the floating point number stored as a String.
	 * 
	 * @param number the number in String to convert
	 * @return number converted to float
	 * @throws IllegalArgumentException when given number is null or empty
	 */
	public static float getFloatFromStringField(String number) throws IllegalArgumentException {
		if (number == null || number.isEmpty()) {
			throw new IllegalArgumentException("Wrong number passed to convert to float value!");
		}
		
		number = number.trim();
		number = number.replaceAll("[\\sA-Za-z\\.]", "");
		number = number.replace(",", ".");
		
		return Float.parseFloat(number);
	}
	
	/**
	 * Takes the screenshot of the current web page (pointed by the WebDriver instance) and saves it
	 * into a file.
	 * 
	 * @param driver the instance of WebDriver
	 * @param filePath the file path where to save the screenshot image
	 * @throws Exception when there was some internal error during execution
	 */
	public static void takeSnapShot(WebDriver driver, String filePath) throws Exception {
		// convert web driver object to TakeScreenshot
		TakesScreenshot scrShot = ((TakesScreenshot) driver);
		// call getScreenshotAs method to create image file
		File srcFile = scrShot.getScreenshotAs(OutputType.FILE);
		
		// move image file to new destination
		File destFile = new File(filePath);
		// copy file at destination
		FileUtils.copyFile(srcFile, destFile);
	}
	
	/**
	 * Checks if the given path for a web browser (driver, library or something else) exists and is correct.
	 * 
	 * @param filePath the path pointing to a browser-specific file
	 * @return true if the path specified is correct
	 * @throws Exception when given file path does not exist or it does not point to a file
	 */
	public static boolean checkIfBrowserPathValid(String filePath) throws Exception {
		if (filePath == null || filePath.isEmpty()) {
			throw new IllegalArgumentException("Wrong filePath passed as browser path parameter!");
		}
		
		try {
			File tmpFile = new File(filePath);
			if (!tmpFile.exists()) {
				throw new FileNotFoundException("File denoted by path: \"" + filePath + "\" does not exist!");
			}
			if (!tmpFile.isFile()) {
				throw new SecurityException("File denoted by path: \"" + filePath + "\" is not readable!");
			}
			return true;
		} catch (Exception e) {
			log.error("An exception occured! " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
			
			return false;
		}
	}
	
	/**
	 * Returns the path to the system temporary folder.
	 * 
	 * @return path to system temp folder
	 */
	public static String getTempFolderPath() {
		String tempFolderPath = null;
		
		String[] possibleTmpDirs = {
			System.getenv("TEMP"),
			System.getProperty("java.io.tmpdir"),
			System.getProperty("user.dir")
		};
		
		for (int i = 0; i < possibleTmpDirs.length; i++) {
			File tmpDir = new File(possibleTmpDirs[i]);
			if (tmpDir.exists() && tmpDir.isDirectory()) {
				tempFolderPath = possibleTmpDirs[i];
				break;
			}
		}
		
		return tempFolderPath;
	}
	
	/**
	 * Builds and returns the full path to the file in which generated chart image will be saved.
	 * 
	 * @param chartType the type of the chart (such as for whole account, policy etc.); first prefix to the
	 * file name
	 * @param chartSubset the subset of the chart (such as the number of the policy); second prefix to the
	 * file name
	 * @param isPieChart whether the image file is for the pie chart or not
	 * @return the full path to the image file for generated chart
	 * @throws Exception when creation of the directory for the file failed
	 */
	public static String getChartImageFilePath(String chartType, String chartSubset, boolean isPieChart)
			throws Exception {
		String tempPath = Utils.getTempFolderPath();
		String chartsFolderPath = tempPath + "\\aegon_charts_files";
		String chartFileName;
		
		try {
			if (!Files.isDirectory(Paths.get(chartsFolderPath))) {
				if (!new File(chartsFolderPath).mkdir()) {
					throw new Exception("Creation of the directory <" + chartsFolderPath + "> failed!");
				}
			}
		} catch (Exception e) {
			log.error("Could not retrieve the directory for charts files! Exception message: "
					+ e.getMessage());
			throw e;
		}
		
		// create the file name such as chart_whole_account_2016-01-04-21-40-50.png
		if (isPieChart) {
			chartFileName = "pie_chart_";
		} else {
			chartFileName = "chart_";
		}
		chartFileName += chartType + "_";
		
		if (chartSubset != null) {
			chartFileName += chartSubset + "_";
		}
		chartFileName += new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
		chartFileName += ".png";
		
		return chartsFolderPath + "\\" + chartFileName;
	}
	
	/**
	 * Returns the stack trace of specified exception in a String variable.
	 * 
	 * @param e Exception whose stack trace is to be retrieved
	 * @return String value of exception stack trace
	 * @throws IllegalArgumentException when passed exception does not exist
	 */
	public static String getExceptionStackTrace(Exception e) throws IllegalArgumentException {
		if (e != null) {
			return ExceptionUtils.getStackTrace(e);
		} else {
			throw new IllegalArgumentException("Passed exception does not exist or is null!");
		}
	}
	
	/**
	 * The method used to display given text in a system tray together with a tray icon. That function can be
	 * used for notifying user about various actions ongoing. The main part of the method which is displaying
	 * the message in a tooltip is executed inside executor - as a separate thread. The method uses therefore
	 * java.util.concurrent functions.
	 *
	 * @param trayTitle the title of the tray to be displayed
	 * @param trayMessage the message to be displayed in a tray
	 * @param messageType type of the displayed message (INFO, WARNING or ERROR)
	 * @throws Exception when there was some internal error during execution
	 */
	public static void showTrayMessage(String trayTitle, String trayMessage, MessageType messageType)
			throws Exception {
		String imageIconPath = System.getProperty("user.dir") + "\\res\\img\\icon.gif";
		
		// check if the SystemTray is supported by the system
		if (!SystemTray.isSupported()) {
			log.error("SystemTray is not supported on this system! SystemTray could not be initialized.");
			return;
		}
		
		try {
			// check if the file with a tray icon exists and is accessible
			File tmpFile = new File(imageIconPath);
			if (!tmpFile.exists()) {
				throw new FileNotFoundException("Icon file denoted by path: \"" + imageIconPath
						+ "\" does not exist!");
			}
			if (!tmpFile.isFile()) {
				throw new SecurityException("Icon file denoted by path: \"" + imageIconPath
						+ "\" is not readable!");
			}
		} catch (Exception e) {
			log.error("An exception occured! " + e.getMessage());
			log.error(Utils.getExceptionStackTrace(e));
		}
		
		// run the code which will be executed as a separate thread inside executor
		// this internal code is adding the tray icon and displaying the message in that tray
		ExecutorService executor = Executors.newSingleThreadExecutor();
		executor.submit(() -> {
			// read the image icon from the file and create TrayIcon object
			Image image = Toolkit.getDefaultToolkit().getImage(imageIconPath);
			if (trayIcon == null) {
				trayIcon = new TrayIcon(image);
			}
			
			// get the system tray and add icon if it doesn't exist yet in the tray
			final SystemTray tray = SystemTray.getSystemTray();
			if (!Arrays.asList(tray.getTrayIcons()).contains(trayIcon)) {
				// finally try to add the icon to tray and display the message
				try {
					tray.add(trayIcon);
				} catch (AWTException e) {
					log.error("TrayIcon could not be added! Exception message: " + e.getMessage());
					try {
						log.error(Utils.getExceptionStackTrace(e));
						throw e;
					} catch (Exception e1) {
						// can't use Utils.getExceptionStackTrace - unless we would create another
						// nested try/catch...
						e1.printStackTrace();
					}
				}
			}
			trayIcon.displayMessage(trayTitle, trayMessage, messageType);
			
			// after displaying the message wait for some time, even if the main program finished,
			// to make sure that the tooltip with the message will be visible for the end user
			// (and not disappear too fast)
			try {
				TimeUnit.MILLISECONDS.sleep(4500);
			} catch (InterruptedException e) {
				log.error("An exception occured! " + e.getMessage());
				try {
					log.error(Utils.getExceptionStackTrace(e));
					throw e;
				} catch (Exception e1) {
					// can't use Utils.getExceptionStackTrace - unless we would create another
					// nested try/catch...
					e1.printStackTrace();
				}
			}
		}); // end of the executor body
		
		// finish the executor operations
		executor.shutdown();
		// maximum time to wait for the executor termination
		executor.awaitTermination(6, TimeUnit.SECONDS);
	}
}
