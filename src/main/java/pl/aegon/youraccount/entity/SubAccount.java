package pl.aegon.youraccount.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;

/**
 * The persistent class for the aegon_sub_account database table.
 * JPA implementation here is based on EclipseLink v.2.1.6.
 * 
 * @author Michal Myszkowski <myszaq85@gmail.com>
 * 
 */

@Entity
@Table(name = "aegon_sub_account")
@NamedQueries({
	@NamedQuery(name = "SubAccount.findAll", query = "SELECT s FROM SubAccount s"),
	@NamedQuery(name = "SubAccount.findByPolicy", query = "SELECT s FROM SubAccount s WHERE s.policy = :policy"),
	@NamedQuery(name = "SubAccount.findByPolicyAndName", query = "SELECT s FROM SubAccount s WHERE s.policy = :policy "
			+ "AND s.subAccountName = :subAccountName")
})
public class SubAccount implements Serializable {
	private static final long serialVersionUID = 5260056951225463161L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_sub_account")
	private int idSubAccount;

	@Column(columnDefinition = "Decimal(10,2)", name = "current_balance", nullable = false)
	private float currentBalance;

	@Column(name = "sub_account_name", nullable = false, length = 100)
	private String subAccountName;

	// bi-directional many-to-one association to Policy
	@ManyToOne
	@JoinColumn(name = "id_policy", nullable = false)
	private Policy policy;

	// bi-directional many-to-one association to SubAccountBalanceHistory
	@OneToMany(mappedBy = "subAccount")
	private List<SubAccountBalanceHistory> subAccountBalanceHistoryList;

	// bi-directional many-to-one association to SubAccountHistory
	@OneToMany(mappedBy = "subAccount")
	private List<SubAccountHistory> subAccountHistoryList;

	/**
	 * Class constructor with specified fields values.
	 * 
	 * @param idSubAccount
	 * @param currentBalance
	 * @param subAccountName
	 * @param policy
	 */
	public SubAccount(int idSubAccount, float currentBalance, String subAccountName, Policy policy) {
		super();
		this.idSubAccount = idSubAccount;
		this.currentBalance = currentBalance;
		this.subAccountName = subAccountName;
		this.policy = policy;
	}

	/**
	 * Default class constructor.
	 */
	public SubAccount() {
		super();
	}

	/**
	 * @return the idSubAccount
	 */
	public int getIdSubAccount() {
		return this.idSubAccount;
	}

	/**
	 * @param idSubAccount the idSubAccount to set
	 */
	public void setIdSubAccount(int idSubAccount) {
		this.idSubAccount = idSubAccount;
	}

	/**
	 * @return the currentBalance
	 */
	public float getCurrentBalance() {
		return this.currentBalance;
	}

	/**
	 * @param currentBalance the currentBalance to set
	 */
	public void setCurrentBalance(float currentBalance) {
		this.currentBalance = currentBalance;
	}

	/**
	 * @return the subAccountName
	 */
	public String getSubAccountName() {
		return this.subAccountName;
	}

	/**
	 * @param subAccountName the subAccountName to set
	 */
	public void setSubAccountName(String subAccountName) {
		this.subAccountName = subAccountName;
	}

	/**
	 * @return the policy
	 */
	public Policy getPolicy() {
		return this.policy;
	}

	/**
	 * @param policy the policy to set
	 */
	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	/**
	 * @return the subAccountBalanceHistoryList
	 */
	public List<SubAccountBalanceHistory> getSubAccountBalanceHistoryList() {
		return this.subAccountBalanceHistoryList;
	}

	/**
	 * @param subAccountBalanceHistoryList the subAccountBalanceHistoryList to set
	 */
	public void setSubAccountBalanceHistoryList(
			List<SubAccountBalanceHistory> subAccountBalanceHistoryList) {
		this.subAccountBalanceHistoryList = subAccountBalanceHistoryList;
	}

	/**
	 * @return the subAccountHistoryList
	 */
	public List<SubAccountHistory> getSubAccountHistoryList() {
		return this.subAccountHistoryList;
	}

	/**
	 * @param subAccountHistoryList the subAccountHistoryList to set
	 */
	public void setSubAccountHistoryList(List<SubAccountHistory> subAccountHistoryList) {
		this.subAccountHistoryList = subAccountHistoryList;
	}

	/**
	 * Adds the new SubAccountBalanceHistory entity to subAccountBalanceHistory list.
	 * 
	 * @param subAccountBalanceHistory the subAccountBalanceHistory to add
	 * @return SubAccountBalanceHistory with the new element
	 */
	public SubAccountBalanceHistory addSubAccountBalanceHistory(
			SubAccountBalanceHistory subAccountBalanceHistory) {
		getSubAccountBalanceHistoryList().add(subAccountBalanceHistory);
		subAccountBalanceHistory.setSubAccount(this);

		return subAccountBalanceHistory;
	}

	/**
	 * Removes the SubAccountBalanceHistory entity from subAccountBalanceHistory list.
	 * 
	 * @param subAccountBalanceHistory the subAccountBalanceHistory to remove
	 * @return updated SubAccountBalanceHistory list
	 */
	public SubAccountBalanceHistory removeSubAccountBalanceHistory(
			SubAccountBalanceHistory subAccountBalanceHistory) {
		getSubAccountBalanceHistoryList().remove(subAccountBalanceHistory);
		subAccountBalanceHistory.setSubAccount(null);

		return subAccountBalanceHistory;
	}

	/**
	 * Adds the new SubAccountHistory entity to subAccountHistory list.
	 * 
	 * @param subAccountHistory the subAccountHistory to add
	 * @return SubAccountHistory with the new element
	 */
	public SubAccountHistory addSubAccountHistory(SubAccountHistory subAccountHistory) {
		getSubAccountHistoryList().add(subAccountHistory);
		subAccountHistory.setSubAccount(this);

		return subAccountHistory;
	}

	/**
	 * Removes the SubAccountHistory entity from subAccountHistory list.
	 * 
	 * @param subAccountHistory the subAccountHistory to remove
	 * @return updated SubAccountHistory list
	 */
	public SubAccountHistory removeSubAccountHistory(SubAccountHistory subAccountHistory) {
		getSubAccountHistoryList().remove(subAccountHistory);
		subAccountHistory.setSubAccount(null);

		return subAccountHistory;
	}

	@Override
	public String toString() {
		return "SubAccount [idSubAccount = " + idSubAccount + "\nsubAccountName = "
				+ subAccountName + "\ncurrentBalance = " + String.format("%.2f", currentBalance)
				+ "\npolicy = " + policy.getIdPolicy() + "]";
	}
}
