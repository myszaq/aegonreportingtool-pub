package pl.aegon.youraccount.entity;

import java.io.Serializable;
import javax.persistence.*;
import org.eclipse.persistence.annotations.Index;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;

/**
 * The persistence class for the aegon_entry_log database table.
 * JPA implementation here is based on EclipseLink v.2.1.6.
 * 
 * @author Michal Myszkowski <myszaq85@gmail.com>
 */

@Entity
@Table(name = "aegon_entry_log")
@NamedQueries({
	@NamedQuery(name = "EntryLog.findAll", query = "SELECT e FROM EntryLog e"),
	@NamedQuery(name = "EntryLog.findWithLimit", query = "SELECT e FROM EntryLog e ORDER BY e.readingDate DESC"),
	@NamedQuery(name = "EntryLog.findByTimeRange", query = "SELECT e FROM EntryLog e WHERE e.readingDate >= :minDateTime "
			+ "AND e.readingDate <= :maxDateTime ORDER BY e.readingDate ASC")
})
public class EntryLog implements Serializable {
	private static final long serialVersionUID = 5615934464581903952L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_entry_log")
	private int idEntryLog;

	@Column(columnDefinition = "Decimal(10,2)", name = "current_balance", nullable = false)
	private float currentBalance;

	@Index(name = "data_from_day_date")
	@Column(name = "data_from_day_date", nullable = false, length = 100)
	private String dataFromDayDate;

	@Column(name = "last_login_date", nullable = false, length = 100)
	private String lastLoginDate;

	@Column(name = "last_modified_date", nullable = false, length = 100)
	private String lastModifiedDate;

	@Column(name = "reading_date", unique = true, nullable = false, length = 100)
	private String readingDate;

	// bi-directional many-to-one association to PolicyHistory
	@OneToMany(mappedBy = "entryLog")
	private List<PolicyHistory> policyHistoryList;

	// bi-directional many-to-one association to SubAccountBalanceHistory
	@OneToMany(mappedBy = "entryLog")
	private List<SubAccountBalanceHistory> subAccountBalanceHistoryList;

	// bi-directional many-to-one association to SubAccountHistory
	@OneToMany(mappedBy = "entryLog")
	private List<SubAccountHistory> subAccountHistoryList;

	/**
	 * Class constructor with specified fields values.
	 * 
	 * @param idEntryLog
	 * @param currentBalance
	 * @param dataFromDayDate
	 * @param lastLoginDate
	 * @param lastModifiedDate
	 */
	public EntryLog(int idEntryLog, float currentBalance, String dataFromDayDate,
			String lastLoginDate, String lastModifiedDate) {
		super();
		this.idEntryLog = idEntryLog;
		this.currentBalance = currentBalance;
		this.dataFromDayDate = dataFromDayDate;
		this.lastLoginDate = lastLoginDate;
		this.lastModifiedDate = lastModifiedDate;

		// set current date time in the appropriate format
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		this.readingDate = dateFormat.format(date);
	}

	/**
	 * Default class constructor.
	 */
	public EntryLog() {
		super();
	}

	/**
	 * @return the idEntryLog
	 */
	public int getIdEntryLog() {
		return this.idEntryLog;
	}

	/**
	 * @param idEntryLog the idEntryLog to set
	 */
	public void setIdEntryLog(int idEntryLog) {
		this.idEntryLog = idEntryLog;
	}

	/**
	 * @return the currentBalance
	 */
	public float getCurrentBalance() {
		return this.currentBalance;
	}

	/**
	 * @param currentBalance the currentBalance to set
	 */
	public void setCurrentBalance(float currentBalance) {
		this.currentBalance = currentBalance;
	}

	/**
	 * @return the dataFromDayDate
	 */
	public String getDataFromDayDate() {
		return this.dataFromDayDate;
	}

	/**
	 * @param dataFromDayDate the dataFromDayDate to set
	 */
	public void setDataFromDayDate(String dataFromDayDate) {
		this.dataFromDayDate = dataFromDayDate;
	}

	/**
	 * @return the lastLoginDate
	 */
	public String getLastLoginDate() {
		return this.lastLoginDate;
	}

	/**
	 * @param lastLoginDate the lastLoginDate to set
	 */
	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	/**
	 * @return the lastModifiedDate
	 */
	public String getLastModifiedDate() {
		return this.lastModifiedDate;
	}

	/**
	 * @param lastModifiedDate the lastModifiedDate to set
	 */
	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * @return the readingDate
	 */
	public String getReadingDate() {
		return this.readingDate;
	}

	/**
	 * @param readingDate the readingDate to set
	 */
	public void setReadingDate(String readingDate) {
		this.readingDate = readingDate;
	}

	/**
	 * @return the policyHistoryList
	 */
	public List<PolicyHistory> getPolicyHistoryList() {
		return this.policyHistoryList;
	}

	/**
	 * @param policyHistoryList the policyHistoryList to set
	 */
	public void setPolicyHistoryList(List<PolicyHistory> policyHistoryList) {
		this.policyHistoryList = policyHistoryList;
	}

	/**
	 * @return the subAccountBalanceHistoryList
	 */
	public List<SubAccountBalanceHistory> getSubAccountBalanceHistoryList() {
		return this.subAccountBalanceHistoryList;
	}

	/**
	 * @param subAccountBalanceHistoryList the subAccountBalanceHistoryList to set
	 */
	public void setSubAccountBalanceHistoryList(
			List<SubAccountBalanceHistory> subAccountBalanceHistoryList) {
		this.subAccountBalanceHistoryList = subAccountBalanceHistoryList;
	}

	/**
	 * @return the subAccountHistoryList
	 */
	public List<SubAccountHistory> getSubAccountHistoryList() {
		return this.subAccountHistoryList;
	}

	/**
	 * @param subAccountHistoryList the subAccountHistoryList to set
	 */
	public void setSubAccountHistoryList(List<SubAccountHistory> subAccountHistoryList) {
		this.subAccountHistoryList = subAccountHistoryList;
	}

	/**
	 * Adds the new PolicyHistory entity to policyHistory list.
	 * 
	 * @param policyHistory the policyHistory to add
	 * @return PolicyHistory with the new element
	 */
	public PolicyHistory addPolicyHistory(PolicyHistory policyHistory) {
		getPolicyHistoryList().add(policyHistory);
		policyHistory.setEntryLog(this);

		return policyHistory;
	}

	/**
	 * Removes the PolicyHistory entity from policyHistory list.
	 * 
	 * @param policyHistory the policyHistory to remove
	 * @return updated PolicyHistory list
	 */
	public PolicyHistory removePolicyHistory(PolicyHistory policyHistory) {
		getPolicyHistoryList().remove(policyHistory);
		policyHistory.setEntryLog(null);

		return policyHistory;
	}

	/**
	 * Adds the new SubAccountBalanceHistory entity to subAccountBalanceHistory list.
	 * 
	 * @param subAccountBalanceHistory the subAccountBalanceHistory to add
	 * @return SubAccountBalanceHistory with the new element
	 */
	public SubAccountBalanceHistory addSubAccountBalanceHistory(
			SubAccountBalanceHistory subAccountBalanceHistory) {
		getSubAccountBalanceHistoryList().add(subAccountBalanceHistory);
		subAccountBalanceHistory.setEntryLog(this);

		return subAccountBalanceHistory;
	}

	/**
	 * Removes the SubAccountBalanceHistory entity from subAccountBalanceHistory list.
	 * 
	 * @param subAccountBalanceHistory the subAccountBalanceHistory to remove
	 * @return updated SubAccountBalanceHistory list
	 */
	public SubAccountBalanceHistory removeSubAccountBalanceHistory(
			SubAccountBalanceHistory subAccountBalanceHistory) {
		getSubAccountBalanceHistoryList().remove(subAccountBalanceHistory);
		subAccountBalanceHistory.setEntryLog(null);

		return subAccountBalanceHistory;
	}

	/**
	 * Adds the new SubAccountHistory entity to subAccountHistory list.
	 * 
	 * @param subAccountHistory the subAccountHistory to add
	 * @return SubAccountHistory with the new element
	 */
	public SubAccountHistory addSubAccountHistory(SubAccountHistory subAccountHistory) {
		getSubAccountHistoryList().add(subAccountHistory);
		subAccountHistory.setEntryLog(this);

		return subAccountHistory;
	}

	/**
	 * Removes the SubAccountHistory entity from subAccountHistory list.
	 * 
	 * @param subAccountHistory the SubAccountHistory to remove
	 * @return updated SubAccountHistory list
	 */
	public SubAccountHistory removeSubAccountHistory(SubAccountHistory subAccountHistory) {
		getSubAccountBalanceHistoryList().remove(subAccountHistory);
		subAccountHistory.setEntryLog(null);

		return subAccountHistory;
	}

	@Override
	public String toString() {
		return "EntryLog [idEntryLog = " + idEntryLog + "\ndataFromDayDate = " + dataFromDayDate
				+ "\nlastModifiedDate = " + lastModifiedDate + "\nlastLoginDate = " + lastLoginDate
				+ "\nreadingDate = " + readingDate + "\ncurrentBalance = "
				+ String.format("%.2f", currentBalance) + "]";
	}
}
