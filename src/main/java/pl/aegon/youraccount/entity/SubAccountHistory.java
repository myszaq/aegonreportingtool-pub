package pl.aegon.youraccount.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the aegon_sub_account_history database table.
 * JPA implementation here is based on EclipseLink v.2.1.6.
 * 
 * @author Michal Myszkowski <myszaq85@gmail.com>
 */

@Entity
@Table(name = "aegon_sub_account_history")
@NamedQueries({
	@NamedQuery(name = "SubAccountHistory.findAll", query = "SELECT s FROM SubAccountHistory s"),
	@NamedQuery(name = "SubAccountHistory.findBySubAccount", query = "SELECT s FROM SubAccountHistory s WHERE s.subAccount = :subAccount"),
	@NamedQuery(name = "SubAccountHistory.findByIdAndEntryLogRange", query = "SELECT s FROM SubAccountHistory s INNER JOIN s.entryLog e "
			+ "WHERE s.subAccount.idSubAccount = :idSubAccount AND e.idEntryLog >= :idEntryLogMin AND e.idEntryLog <= :idEntryLogMax")
})
public class SubAccountHistory implements Serializable {
	private static final long serialVersionUID = 1496067347610590547L;

	@EmbeddedId
	private SubAccountHistoryPK id;

	@Column(columnDefinition = "Decimal(10,2)", name = "balance", nullable = false)
	private float balance;

	// bi-directional one-to-one association to EntryLog
	@ManyToOne
	@JoinColumn(name = "id_entry_log", insertable = true, updatable = false)
	private EntryLog entryLog;

	// bi-directional many-to-one association to SubAccount
	@ManyToOne
	@JoinColumn(name = "id_sub_account", insertable = true, updatable = false)
	private SubAccount subAccount;

	/**
	 * Class constructor with specified fields values.
	 * 
	 * @param balance
	 * @param entryLog
	 * @param subAccount
	 */
	public SubAccountHistory(float balance, EntryLog entryLog, SubAccount subAccount) {
		super();
		this.balance = balance;
		this.entryLog = entryLog;
		this.subAccount = subAccount;
	}

	/**
	 * Default class constructor.
	 */
	public SubAccountHistory() {
		super();
	}

	/**
	 * @return the id
	 */
	public SubAccountHistoryPK getId() {
		return this.id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(SubAccountHistoryPK id) {
		this.id = id;
	}

	/**
	 * @return the balance
	 */
	public float getBalance() {
		return this.balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(float balance) {
		this.balance = balance;
	}

	/**
	 * @return the entryLog
	 */
	public EntryLog getEntryLog() {
		return this.entryLog;
	}

	/**
	 * @param entryLog the entryLog to set
	 */
	public void setEntryLog(EntryLog entryLog) {
		this.entryLog = entryLog;
	}

	/**
	 * @return the subAccount
	 */
	public SubAccount getSubAccount() {
		return this.subAccount;
	}

	/**
	 * @param subAccount the subAccount to set
	 */
	public void setSubAccount(SubAccount subAccount) {
		this.subAccount = subAccount;
	}

	@Override
	public String toString() {
		return "SubAccountHistory [balance = " + String.format("%.2f", balance) + "\nidEntryLog = "
				+ entryLog.getIdEntryLog() + "\nidSubAccount = " + subAccount.getIdSubAccount()
				+ "]";
	}
}
