package pl.aegon.youraccount.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the aegon_policy database table.
 * JPA implementation here is based on EclipseLink v.2.1.6.
 * 
 * @author Michal Myszkowski <myszaq85@gmail.com>
 */

@Entity
@Table(name = "aegon_policy")
@NamedQueries({
	@NamedQuery(name = "Policy.findAll", query = "SELECT p FROM Policy p"),
	@NamedQuery(name = "Policy.findByNumber", query = "SELECT p FROM Policy p WHERE p.policyNumber = :number")
})
public class Policy implements Serializable {
	private static final long serialVersionUID = 3894705459254746268L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_policy")
	private int idPolicy;

	@Column(columnDefinition = "Decimal(10,2)", name = "current_balance", nullable = false)
	private float currentBalance;

	@Column(name = "policy_name", nullable = false, length = 100)
	private String policyName;

	@Column(name = "policy_number", unique = true, nullable = false, length = 100)
	private String policyNumber;

	@Column(name = "product_name", nullable = false, length = 100)
	private String productName;

	// bi-directional many-to-one association to PolicyHistory
	@OneToMany(mappedBy = "policy")
	private List<PolicyHistory> policyHistoryList;

	// bi-directional many-to-one association to SubAccount
	@OneToMany(mappedBy = "policy")
	private List<SubAccount> subAccountList;

	/**
	 * Class constructor with specified fields values.
	 * 
	 * @param idPolicy
	 * @param currentBalance
	 * @param policyName
	 * @param policyNumber
	 * @param productName
	 */
	public Policy(int idPolicy, float currentBalance, String policyName, String policyNumber,
			String productName) {
		super();
		this.idPolicy = idPolicy;
		this.currentBalance = currentBalance;
		this.policyName = policyName;
		this.policyNumber = policyNumber;
		this.productName = productName;
	}

	/**
	 * Default class constructor.
	 */
	public Policy() {
		super();
	}

	/**
	 * @return the idPolicy
	 */
	public int getIdPolicy() {
		return this.idPolicy;
	}

	/**
	 * @param idPolicy the idPolicy to set
	 */
	public void setId(int idPolicy) {
		this.idPolicy = idPolicy;
	}

	/**
	 * @return the currentBalance
	 */
	public float getCurrentBalance() {
		return this.currentBalance;
	}

	/**
	 * @param currentBalance the currentBalance to set
	 */
	public void setCurrentBalance(float currentBalance) {
		this.currentBalance = currentBalance;
	}

	/**
	 * @return the policyName
	 */
	public String getPolicyName() {
		return this.policyName;
	}

	/**
	 * @param policyName the policyName to set
	 */
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	/**
	 * @return the policyNumber
	 */
	public String getPolicyNumber() {
		return this.policyNumber;
	}

	/**
	 * @param policyNumber the policyNumber to set
	 */
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return this.productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the policyHistoryList
	 */
	public List<PolicyHistory> getPolicyHistoryList() {
		return this.policyHistoryList;
	}

	/**
	 * @param policyHistoryList the policyHistoryList to set
	 */
	public void setPolicyHistoryList(List<PolicyHistory> policyHistoryList) {
		this.policyHistoryList = policyHistoryList;
	}

	/**
	 * @return the subAccountList
	 */
	public List<SubAccount> getSubAccountList() {
		return subAccountList;
	}

	/**
	 * @param subAccountList the subAccountList to set
	 */
	public void setSubAccountList(List<SubAccount> subAccountList) {
		this.subAccountList = subAccountList;
	}

	/**
	 * Adds the new PolicyHistory entity to policyHistory list.
	 * 
	 * @param policyHistory the policyHistory to add
	 * @return PolicyHistory with the new element
	 */
	public PolicyHistory addPolicyHistory(PolicyHistory policyHistory) {
		getPolicyHistoryList().add(policyHistory);
		policyHistory.setPolicy(this);

		return policyHistory;
	}

	/**
	 * Removes the PolicyHistory entity from policyHistory list.
	 * 
	 * @param policyHistory the policyHistory to remove
	 * @return updated PolicyHistory list
	 */
	public PolicyHistory removePolicyHistory(PolicyHistory policyHistory) {
		getPolicyHistoryList().remove(policyHistory);
		policyHistory.setPolicy(null);

		return policyHistory;
	}

	/**
	 * Adds the new SubAccount entity to subAccount list.
	 * 
	 * @param subAccount the subAccount to add
	 * @return SubAccount with the new element
	 */
	public SubAccount addSubAccount(SubAccount subAccount) {
		getSubAccountList().add(subAccount);
		subAccount.setPolicy(this);

		return subAccount;
	}

	/**
	 * Removes the SubAccount entity from subAccount list.
	 * 
	 * @param subAccount the subAccount to remove
	 * @return updated SubAccount list
	 */
	public SubAccount removeSubAccount(SubAccount subAccount) {
		getSubAccountList().remove(subAccount);
		subAccount.setPolicy(null);

		return subAccount;
	}

	@Override
	public String toString() {
		return "Policy [idPolicy = " + idPolicy + "\npolicyName = " + policyName
				+ "\npolicyNumber = " + policyNumber + "\nproductName = " + productName
				+ "\ncurrentBalance = " + String.format("%.2f", currentBalance) + "]";
	}
}