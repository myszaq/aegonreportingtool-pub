package pl.aegon.youraccount.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the aegon_sub_account_balance_history database table.
 * JPA implementation here is based on EclipseLink v.2.1.6.
 * 
 * @author Michal Myszkowski <myszaq85@gmail.com>
 */

@Entity
@Table(name = "aegon_sub_account_balance_history")
@NamedQueries({
	@NamedQuery(name = "SubAccountBalanceHistory.findAll", query = "SELECT s FROM SubAccountBalanceHistory s"),
	@NamedQuery(name = "SubAccountBalanceHistory.findBySubAccount", query = "SELECT s FROM SubAccountBalanceHistory s WHERE s.subAccount = :subAccount"),
	@NamedQuery(name = "SubAccountBalanceHistory.findByIdAndEntryLogRange", query = "SELECT s FROM SubAccountBalanceHistory s "
			+ "INNER JOIN s.entryLog e WHERE s.subAccount.idSubAccount = :idSubAccount "
			+ "AND e.idEntryLog >= :idEntryLogMin AND e.idEntryLog <= :idEntryLogMax"),
	@NamedQuery(name = "SubAccountBalanceHistory.findUfkNamesBySubAccount", query = "SELECT DISTINCT s.ufkName FROM SubAccountBalanceHistory s "
			+ "WHERE s.subAccount = :subAccount")
})
public class SubAccountBalanceHistory implements Serializable {
	private static final long serialVersionUID = 2493865296942661082L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_sub_account_balance_history")
	private int idSubAccountBalanceHistory;

	@Column(columnDefinition = "Decimal(10,2)", name = "ufk_invest_unit_collect_value", nullable = false)
	private float ufkInvestUnitCollectValue;

	@Column(name = "ufk_invest_unit_count", nullable = false)
	private double ufkInvestUnitCount;

	@Column(name = "ufk_invest_unit_value", nullable = false)
	private double ufkInvestUnitValue;

	@Column(name = "ufk_name", nullable = false, length = 100)
	private String ufkName;

	// bi-directional many-to-one association to EntryLog
	@ManyToOne
	@JoinColumn(name = "id_entry_log")
	private EntryLog entryLog;

	// bi-directional many-to-one association to SubAccount
	@ManyToOne
	@JoinColumn(name = "id_sub_account")
	private SubAccount subAccount;

	/**
	 * Class constructor with specified fields values.
	 * 
	 * @param ufkInvestUnitCollectValue
	 * @param ufkInvestUnitCount
	 * @param ufkInvestUnitValue
	 * @param ufkName
	 * @param entryLog
	 * @param subAccount
	 */
	public SubAccountBalanceHistory(float ufkInvestUnitCollectValue, float ufkInvestUnitCount,
			float ufkInvestUnitValue, String ufkName, EntryLog entryLog, SubAccount subAccount) {
		super();
		this.ufkInvestUnitCollectValue = ufkInvestUnitCollectValue;
		this.ufkInvestUnitCount = ufkInvestUnitCount;
		this.ufkInvestUnitValue = ufkInvestUnitValue;
		this.ufkName = ufkName;
		this.entryLog = entryLog;
		this.subAccount = subAccount;
	}

	/**
	 * Default class constructor.
	 */
	public SubAccountBalanceHistory() {
		super();
	}

	/**
	 * @return the idSubAccountBalanceHistory
	 */
	public int getIdSubAccountBalanceHistory() {
		return this.idSubAccountBalanceHistory;
	}

	/**
	 * @param idSubAccountBalanceHistory the idSubAccountBalanceHistory to set
	 */
	public void setIdSubAccountBalanceHistory(int idSubAccountBalanceHistory) {
		this.idSubAccountBalanceHistory = idSubAccountBalanceHistory;
	}

	/**
	 * @return the ufkInvestUnitCollectValue
	 */
	public float getUfkInvestUnitCollectValue() {
		return this.ufkInvestUnitCollectValue;
	}

	/**
	 * @param ufkInvestUnitCollectValue the ufkInvestUnitCollectValue to set
	 */
	public void setUfkInvestUnitCollectValue(float ufkInvestUnitCollectValue) {
		this.ufkInvestUnitCollectValue = ufkInvestUnitCollectValue;
	}

	/**
	 * @return the ufkInvestUnitCount
	 */
	public double getUfkInvestUnitCount() {
		return this.ufkInvestUnitCount;
	}

	/**
	 * @param ufkInvestUnitCount the ufkInvestUnitCount to set
	 */
	public void setUfkInvestUnitCount(double ufkInvestUnitCount) {
		this.ufkInvestUnitCount = ufkInvestUnitCount;
	}

	/**
	 * @return the ufkInvestUnitValue
	 */
	public double getUfkInvestUnitValue() {
		return this.ufkInvestUnitValue;
	}

	/**
	 * @param ufkInvestUnitValue the ufkInvestUnitValue to set
	 */
	public void setUfkInvestUnitValue(double ufkInvestUnitValue) {
		this.ufkInvestUnitValue = ufkInvestUnitValue;
	}

	/**
	 * @return the ufkName
	 */
	public String getUfkName() {
		return this.ufkName;
	}

	/**
	 * @param ufkName the ufkName to set
	 */
	public void setUfkName(String ufkName) {
		this.ufkName = ufkName;
	}

	/**
	 * @return the entryLog
	 */
	public EntryLog getEntryLog() {
		return this.entryLog;
	}

	/**
	 * @param entryLog the entryLog to set
	 */
	public void setEntryLog(EntryLog entryLog) {
		this.entryLog = entryLog;
	}

	/**
	 * @return the subAccount
	 */
	public SubAccount getSubAccount() {
		return this.subAccount;
	}

	/**
	 * @param subAccount the subAccount to set
	 */
	public void setSubAccount(SubAccount subAccount) {
		this.subAccount = subAccount;
	}

	@Override
	public String toString() {
		return "SubAccountBalanceHistory [idSubAccountBalanceHistory = "
				+ idSubAccountBalanceHistory + "\nufkName = " + ufkName + "\nufkInvestUnitValue = "
				+ ufkInvestUnitValue + "\nufkInvestUnitCount = " + ufkInvestUnitCount
				+ "\nufkInvestUnitCollectValue = " + ufkInvestUnitCollectValue + "\nidEntryLog = "
				+ entryLog.getIdEntryLog() + "\nidSubAccount = " + subAccount.getIdSubAccount()
				+ "]";
	}
}
