package pl.aegon.youraccount.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the aegon_policy_history database table.
 * JPA implementation here is based on EclipseLink v.2.1.6.
 * 
 * @author Michal Myszkowski <myszaq85@gmail.com>
 */

@Entity
@Table(name = "aegon_policy_history")
@NamedQueries({
	@NamedQuery(name = "PolicyHistory.findAll", query = "SELECT p FROM PolicyHistory p"),
	@NamedQuery(name = "PolicyHistory.findByPolicy", query = "SELECT p FROM PolicyHistory p WHERE p.policy = :policy"),
	@NamedQuery(name = "PolicyHistory.findByIdAndEntryLogRange", query = "SELECT p FROM PolicyHistory p INNER JOIN p.entryLog e "
			+ "WHERE p.policy.idPolicy = :idPolicy AND e.idEntryLog >= :idEntryLogMin AND e.idEntryLog <= :idEntryLogMax")
})
public class PolicyHistory implements Serializable {
	private static final long serialVersionUID = 7717520202650819239L;

	@EmbeddedId
	private PolicyHistoryPK id;

	@Column(columnDefinition = "Decimal(10,2)", name = "balance", nullable = false)
	private float balance;

	// bi-directional many-to-one association to EntryLog
	@ManyToOne
	@JoinColumn(name = "id_entry_log")
	private EntryLog entryLog;

	// bi-directional many-to-one association to Policy
	@ManyToOne
	@JoinColumn(name = "id_policy", nullable = false)
	private Policy policy;

	/**
	 * Class constructor with specified fields values.
	 * 
	 * @param balance
	 * @param entryLog
	 * @param policy
	 */
	public PolicyHistory(float balance, EntryLog entryLog, Policy policy) {
		super();
		this.balance = balance;
		this.entryLog = entryLog;
		this.policy = policy;
	}

	/**
	 * Default class constructor.
	 */
	public PolicyHistory() {
		super();
	}

	/**
	 * @return the id
	 */
	public PolicyHistoryPK getId() {
		return this.id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(PolicyHistoryPK id) {
		this.id = id;
	}

	/**
	 * @return the balance
	 */
	public float getBalance() {
		return this.balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(float balance) {
		this.balance = balance;
	}

	/**
	 * @return the entryLog
	 */
	public EntryLog getEntryLog() {
		return this.entryLog;
	}

	/**
	 * @param entryLog the entryLog to set
	 */
	public void setEntryLog(EntryLog entryLog) {
		this.entryLog = entryLog;
	}

	/**
	 * @return the policy
	 */
	public Policy getPolicy() {
		return this.policy;
	}

	/**
	 * @param policy the policy to set
	 */
	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	@Override
	public String toString() {
		return "PolicyHistory [balance = " + String.format("%.2f", balance) + "\nidEntryLog = "
				+ entryLog.getIdEntryLog() + "\nidPolicy = " + policy.getIdPolicy() + "]";
	}
}
