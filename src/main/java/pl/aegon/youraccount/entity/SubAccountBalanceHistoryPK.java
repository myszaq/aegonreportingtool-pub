package pl.aegon.youraccount.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the aegon_sub_account_balance_history database table.
 * JPA implementation here is based on EclipseLink v.2.1.6.
 * 
 * @author Michal Myszkowski <myszaq85@gmail.com>
 */

@Embeddable
public class SubAccountBalanceHistoryPK implements Serializable {
	// default serial version id, required for serializable classes
	private static final long serialVersionUID = 2976770391990431203L;

	@Column(name = "id_entry_log", insertable = false, updatable = false)
	private int idEntryLog;

	@Column(name = "id_sub_account", insertable = false, updatable = false)
	private int idSubAccount;

	public SubAccountBalanceHistoryPK() {
		super();
	}

	/**
	 * @return the idEntryLog
	 */
	public int getIdEntryLog() {
		return idEntryLog;
	}

	/**
	 * @param idEntryLog the idEntryLog to set
	 */
	public void setIdEntryLog(int idEntryLog) {
		this.idEntryLog = idEntryLog;
	}

	/**
	 * @return the idSubAccount
	 */
	public int getIdSubAccount() {
		return idSubAccount;
	}

	/**
	 * @param idSubAccount the idSubAccount to set
	 */
	public void setIdSubAccount(int idSubAccount) {
		this.idSubAccount = idSubAccount;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SubAccountBalanceHistoryPK) {
			SubAccountBalanceHistoryPK subAccountBalanceHistoryPK = (SubAccountBalanceHistoryPK) obj;

			if (subAccountBalanceHistoryPK.getIdSubAccount() != this.idSubAccount) {
				return false;
			}
			if (subAccountBalanceHistoryPK.getIdEntryLog() != this.idEntryLog) {
				return false;
			}

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idSubAccount;
		hash = hash * prime + this.idEntryLog;

		return hash;
	}
}
