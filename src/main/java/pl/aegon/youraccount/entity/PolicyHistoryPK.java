package pl.aegon.youraccount.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The primary key class for the aegon_policy_history database table.
 * JPA implementation here is based on EclipseLink v.2.1.6.
 * 
 * @author Michal Myszkowski <myszaq85@gmail.com>
 */

@Embeddable
public class PolicyHistoryPK implements Serializable {
	// default serial version id, required for serializable classes
	private static final long serialVersionUID = 2233377448630550940L;

	@Column(name = "id_entry_log", insertable = false, updatable = false)
	private int idEntryLog;

	@Column(name = "id_policy", insertable = false, updatable = false)
	private int idPolicy;

	/**
	 * Default class constructor.
	 */
	public PolicyHistoryPK() {
		super();
	}

	/**
	 * @return the idEntryLog
	 */
	public int getIdEntryLog() {
		return this.idEntryLog;
	}

	/**
	 * @param idEntryLog the idEntryLog to set
	 */
	public void setIdEntryLog(int idEntryLog) {
		this.idEntryLog = idEntryLog;
	}

	/**
	 * @return the idPolicy
	 */
	public int getIdPolicy() {
		return this.idPolicy;
	}

	/**
	 * @param idPolicy the idPolicy to set
	 */
	public void setIdPolicy(int idPolicy) {
		this.idPolicy = idPolicy;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PolicyHistoryPK) {
			PolicyHistoryPK policyHistoryPK = (PolicyHistoryPK) obj;

			if (policyHistoryPK.getIdEntryLog() != this.idEntryLog) {
				return false;
			}
			if (policyHistoryPK.getIdPolicy() != this.idPolicy) {
				return false;
			}

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idPolicy;
		hash = hash * prime + this.idEntryLog;

		return hash;
	}
	
	@Override
	public String toString() {
		return "(idEntryLog = " + this.idEntryLog + ", idPolicy = " + this.idPolicy + ")"; 
	}
}
