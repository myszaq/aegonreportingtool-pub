/**
 * Utility class for reading and returning content of the excel file with test data
 * for various test classes.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	public static final int TEST_NAME = 0;
	public static final int INPUT_INDEX = 1;
	public static final int PARAMETER_NAME = 2;
	public static final int PARAMETER_VALUE = 3;
	
	/**
	 * Gets and returns the map of tests data from input Excel file for those tests which have Integer index.
	 * For more details see the structure of the Excel test data file.
	 * 
	 * @param filePath path to Excel file containing test data
	 * @param sheetName name of the Excel sheet from which to read data
	 * @return HashMap containing tests data, where key is the test name and value is another HashMap mapping
	 * given test index with particular parameters (map in form of: test_param_key => test_param_value)
	 * @throws Exception when reading Excel file failed or if an error occured during input data
	 * transformation
	 */
	public static HashMap<String, HashMap<Integer, HashMap<String, Object>>> getTestDataWithIntegerIndexFromExcel(
			String filePath, String sheetName) throws Exception {
		Sheet sheet = null;
		try {
			// access the required test data sheet
			sheet = readExcel(filePath, sheetName);
		} catch (IOException e) {
			System.err.println("Could not read the Excel sheet!");
			e.printStackTrace();
			throw e;
		}
		
		String paramName = null, paramValue = null;
		HashMap<String, Object> testParams = new HashMap<String, Object>();
		HashMap<Integer, HashMap<String, Object>> testParamsCollection = new HashMap<Integer, HashMap<String, Object>>();
		HashMap<String, HashMap<Integer, HashMap<String, Object>>> testDataMap = new HashMap<String, HashMap<Integer, HashMap<String, Object>>>();
		
		// find number of rows in excel file
		int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
		// loop over all the rows of excel file to read it
		for (int i = 1; i <= rowCount; i++) {
			Row row = sheet.getRow(i);
			
			// if there is a new input index (corresponding cell is not empty), clear the previous params, but
			// only if the loop has not just started
			// (so we don't remove the params from the first row, where the new input index begins)
			if (!row.getCell(INPUT_INDEX).toString().isEmpty() && i != 1) {
				// reset hash map
				testParams = new HashMap<String, Object>();
			}
			
			// if there is a new test name (corresponding cell is not empty), clear the previous params and
			// input indexes, but only if the loop has not just started
			// (so we don't remove the params and indexes from the first row, where the new test name begins)
			if (!row.getCell(TEST_NAME).toString().isEmpty() && i != 1) {
				// reset hash maps
				testParams = new HashMap<String, Object>();
				testParamsCollection = new HashMap<Integer, HashMap<String, Object>>();
			}
			
			// if there is a parameter and its value, store it in the testParams map (they should be always
			// available)
			paramName = row.getCell(PARAMETER_NAME).toString();
			paramValue = row.getCell(PARAMETER_VALUE).toString();
			if (!paramName.isEmpty() && !paramValue.isEmpty()) {
				Object objValue = null;
				// split the parameter value into list if possible (when there is more than one value)
				if (paramValue.contains("\n")) {
					objValue = new ArrayList<String>();
					objValue = Arrays.asList(paramValue.split("\n"));
				} else {
					objValue = paramValue;
				}
				
				testParams.put(paramName, objValue);
			}
			
			Cell rowCell = row.getCell(INPUT_INDEX);
			// if there is a new input index, add previously collected testParams to the collection
			if (!rowCell.toString().isEmpty() && rowCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				// add hash map with test params to the collection
				testParamsCollection.put((int) rowCell.getNumericCellValue(), testParams);
			}
			
			// finally, if there is a new test name, add the test params collection to the test data map
			if (!row.getCell(TEST_NAME).toString().isEmpty()) {
				String testName = row.getCell(TEST_NAME).toString();
				// add collection of test params to the hash map of the ultimate test data
				testDataMap.put(testName, testParamsCollection);
			}
		}
		
		return testDataMap;
	}
	
	/**
	 * Gets and returns the map of tests data from input Excel file for those tests which have String index.
	 * For more details see the structure of the Excel test data file.
	 * 
	 * @param filePath path to Excel file containing test data
	 * @param sheetName name of the Excel sheet from which to read data
	 * @return HashMap containing tests data, where key is the test name and value is another HashMap mapping
	 * given test index with particular parameters (map in form of: test_param_key => test_param_value)
	 * @throws Exception when reading Excel file failed or if an error occured during input data
	 * transformation
	 */
	public static HashMap<String, HashMap<String, HashMap<String, Object>>> getTestDataWithStringIndexFromExcel(
			String filePath, String sheetName) throws Exception {
		Sheet sheet = null;
		try {
			// access the required test data sheet
			sheet = readExcel(filePath, sheetName);
		} catch (IOException e) {
			System.out.println("Could not read the Excel sheet!");
			e.printStackTrace();
			throw e;
		}
		
		String paramName = null, paramValue = null;
		HashMap<String, Object> testParams = new HashMap<String, Object>();
		HashMap<String, HashMap<String, Object>> testParamsCollection = new HashMap<String, HashMap<String, Object>>();
		HashMap<String, HashMap<String, HashMap<String, Object>>> testDataMap = new HashMap<String, HashMap<String, HashMap<String, Object>>>();
		
		// find number of rows in excel file
		int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
		// loop over all the rows of excel file to read it
		for (int i = 1; i <= rowCount; i++) {
			Row row = sheet.getRow(i);
			
			// if there is a new input index (corresponding cell is not empty), clear the previous params, but
			// only if the loop has not just started
			// (so we don't remove the params from the first row, where the new input index begins)
			if (!row.getCell(INPUT_INDEX).toString().isEmpty() && i != 1) {
				// reset hash map
				testParams = new HashMap<String, Object>();
			}
			
			// if there is a new test name (corresponding cell is not empty), clear the previous params and
			// input indexes, but only if the loop has not just started
			// (so we don't remove the params and indexes from the first row, where the new test name begins)
			if (!row.getCell(TEST_NAME).toString().isEmpty() && i != 1) {
				// reset hash maps
				testParams = new HashMap<String, Object>();
				testParamsCollection = new HashMap<String, HashMap<String, Object>>();
			}
			
			// if there is a parameter and its value, store it in the testParams map (they should be always
			// available)
			paramName = row.getCell(PARAMETER_NAME).toString();
			paramValue = row.getCell(PARAMETER_VALUE).toString();
			if (!paramName.isEmpty() && !paramValue.isEmpty()) {
				Object objValue = null;
				// split the parameter value into list if possible (when there is more than one value)
				if (paramValue.contains("\n")) {
					objValue = new ArrayList<String>();
					objValue = Arrays.asList(paramValue.split("\n"));
				} else {
					objValue = paramValue;
				}
				
				testParams.put(paramName, objValue);
			}
			
			// if there is a new input index, add previously collected testParams to the collection
			if (!row.getCell(INPUT_INDEX).toString().isEmpty()) {
				// add hash map with test params to the collection
				testParamsCollection.put(row.getCell(INPUT_INDEX).toString(), testParams);
			}
			
			// finally, if there is a new test name, add the test params collection to the test data map
			if (!row.getCell(TEST_NAME).toString().isEmpty()) {
				String testName = row.getCell(TEST_NAME).toString();
				// add collection of test params to the hash map of the ultimate test data
				testDataMap.put(testName, testParamsCollection);
			}
		}
		
		return testDataMap;
	}
	
	/**
	 * Helper method reading Excel file with tests data used in many tests.
	 * 
	 * @param filePath path to Excel file
	 * @param sheetName name of the Excel sheet from which to read data
	 * @return Sheet object with the data of the given Excel sheet
	 * @throws IOException when reading Excel file failed
	 */
	public static Sheet readExcel(String filePath, String sheetName) throws IOException {
		// create an object of File and FileInputStream class to open xlsx file
		File file = new File(filePath);
		FileInputStream inputStream = new FileInputStream(file);
		Workbook workbook = new XSSFWorkbook(inputStream);
		
		// read sheet inside the workbook by its name
		Sheet sheet = workbook.getSheet(sheetName);
		// close resources
		workbook.close();
		inputStream.close();
		
		return sheet;
	}
}