/**
 * This utility class implements DataProvider for TestNG tests with methods providing test data.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package utils;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.DataProvider;

public class TestDataProvider {
	private static String filePath = System.getProperty("user.dir")
			+ "\\src\\test\\java\\utils\\TestDataProvisioning.xlsx";
	private static String sheetName = "TestData";
	
	/**
	 * Main method providing test data for all the tests which declare DataProvider of that name
	 * (<b>AegonTestDataProvider</b>). This data provider is used for tests which are based on Integer index.
	 * See the structure of Excel file containing test data for more details.
	 * 
	 * @param m test method which calls this data provider method
	 * @return HashMap of test data designed for given test method
	 * @throws Exception when there is no test data for given method
	 */
	@DataProvider(name = "AegonTestDataProvider")
	public static Object[][] getDataFromDataProvider(Method m) throws Exception {
		Object[][] result = null;
		String methodName = m.getName();
		HashMap<String, HashMap<Integer, HashMap<String, Object>>> testDataMap = ExcelReader
				.getTestDataWithIntegerIndexFromExcel(filePath, sheetName);
		
		// take the test data from the excel file by method name (method name = test name)
		if (!testDataMap.containsKey(methodName)) {
			throw new IllegalArgumentException("There is no defined test data for the test method <"
					+ methodName + ">");
		}
		HashMap<Integer, HashMap<String, Object>> testData = testDataMap.get(methodName);
		
		// build final result data used in a test, depending on the test method
		switch (methodName) {
		case "testCollectAndStoreSubAccountData":
		case "testCollectAndStoreSubAccountDetailsData":
			result = new Object[testData.size()][];
			for (Integer key : testData.keySet()) {
				result[key] = new Object[] { key, testData.get(key) };
			}
			break;
		case "testCollectAllRelevantDataFromApplication":
			List<String> includedTests = Arrays.asList("testCollectAndStoreSubAccountDetailsData",
					"testCollectAndStoreSubAccountData", "testCollectAndStorePolicyData",
					"testCollectAndStoreGeneralData");
			
			HashMap<String, HashMap<Integer, HashMap<String, Object>>> multipleTestsData = new HashMap<String, HashMap<Integer, HashMap<String, Object>>>();
			for (String testName : includedTests) {
				if (testDataMap.containsKey(testName)) {
					multipleTestsData.put(testName, testDataMap.get(testName));
				}
			}
			
			result = new Object[1][];
			result[0] = new Object[] { multipleTestsData };
			break;
		case "testGeneralDataCorrectness":
		case "testLogoutAndLogoutPage":
		case "testPolicyDataCorrectness":
		case "testRedirectionToPolicyData":
		case "testRedirectionToSubAccountPage":
		case "testErrorFieldPresence":
		case "testLoginCorrect":
		case "testLoginIncorrect":
		case "testGetDatasetForPolicyList":
			result = new Object[1][];
			result[0] = new Object[] { testData.get(0) };
			break;
		default:
			result = new Object[1][];
			result[0] = new Object[] { testData };
			break;
		}
		return result;
	}
	
	/**
	 * Main method providing test data for all the tests which declare DataProvider of that name
	 * (<b>AegonTestDataProvider-StringInputIndex</b>). This data provider is used for tests which are based
	 * on String index. See the structure of Excel file containing test data for more details.
	 * 
	 * @param m test method which calls this data provider method
	 * @return HashMap of test data designed for given test method
	 * @throws Exception when there is no test data for given method
	 */
	@DataProvider(name = "AegonTestDataProvider-StringInputIndex")
	public static Object[][] getDataWithStringIndexFromDataProvider(Method m) throws Exception {
		Object[][] result = null;
		String methodName = m.getName();
		HashMap<String, HashMap<String, HashMap<String, Object>>> testDataMap = ExcelReader
				.getTestDataWithStringIndexFromExcel(filePath, sheetName);
		
		// take the test data from the excel file by method name (method name = test name)
		if (!testDataMap.containsKey(methodName)) {
			throw new IllegalArgumentException("There is no defined test data for the test method <"
					+ methodName + ">");
		}
		HashMap<String, HashMap<String, Object>> testData = testDataMap.get(methodName);
		
		result = new Object[1][];
		result[0] = new Object[] { testData };
		
		return result;
	}
}
