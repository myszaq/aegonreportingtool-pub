/**
 * Simple class providing basic access to the test database (MySQL) through the methods
 * which allow to send SQL queries to that database and retrieve the results.
 * This class is intended for testing purposes only (the main interface for accessing DB
 * is based on JPA) and uses JDBC engine to access the database.
 * For more details, see the comments of each method.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package utils;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class DBSimpleService {
	private Connection connection = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	
	/**
	 * Default class constructor. Reads the connection parameters from properties file and creates database
	 * connection.
	 */
	public DBSimpleService() {
		Properties prop = new Properties();
		InputStream inputStream = null;
		
		// try to get connection params from properties file
		try {
			inputStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("dbConnection.properties");
			prop.load(inputStream);
			inputStream.close();
		} catch (Exception e) {
			System.err.println("Could not read dbConnection.properties file! Exception message: "
					+ e.getMessage());
			e.printStackTrace();
		}
		
		try {
			String driver = prop.getProperty("jdbc.driver");
			String connectionURL = prop.getProperty("jdbc.url");
			String username = prop.getProperty("jdbc.username");
			String password = prop.getProperty("jdbc.password");
			
			// this will load the MySQL driver, each DB has its own driver
			Class.forName(driver);
			// setup the connection with the DB
			connection = DriverManager.getConnection(connectionURL, username, password);
		} catch (Exception e) {
			System.err.println("Could not create connection with database! Exception message: "
					+ e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the result of SQL select query as the list of rows retrieved from the database.
	 * Method uses JDBC Statement to issue query to the database.
	 * 
	 * @param sqlQuery String containing SQL query (select) to be executed
	 * @return SQL query result as a list of rows retrieved
	 * @see #convertResultSetToMapList()
	 */
	public List<HashMap<String, Object>> getSqlQueryResult(String sqlQuery) {
		List<HashMap<String, Object>> resultsList = null;
		
		try {
			// create Statement which allows to issue SQL queries to the database
			statement = connection.createStatement();
			// get the result of the SQL query
			resultSet = statement.executeQuery(sqlQuery);
			// convert result to more versatile list of HashMap
			resultsList = convertResultSetToMapList();
		} catch (SQLException e) {
			System.err.println("An error occured while trying to execute query! Exception message: "
					+ e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				resultSet.close();
				statement.close();
			} catch (SQLException e) {
				System.err.println("An error occured while trying to close statement!"
						+ " Exception message: " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		return resultsList;
	}
	
	/**
	 * Gets the result of SQL select query as the list of rows retrieved from the database. Method uses JDBC
	 * PreparedStatement to issue query (with optional variables) to the database.
	 * 
	 * @param sqlQuery String containing SQL query (select) to be executed
	 * @param paramsList the list of values corresponding to the variables used in prepared statement (null
	 * allowed)
	 * @return SQL query result as a list of rows retrieved
	 * @see #convertResultSetToMapList()
	 */
	public List<HashMap<String, Object>> getPreparedSqlQueryResult(String sqlQuery, List<Object> paramsList) {
		List<HashMap<String, Object>> resultsList = null;
		
		try {
			// PreparedStatement can use variables and is more efficient
			preparedStatement = connection.prepareStatement(sqlQuery);
			
			// set parameters for prepared statement (if the list is not empty)
			if (paramsList != null && !paramsList.isEmpty()) {
				int i = 1;
				for (Object param : paramsList) {
					preparedStatement.setObject(i++, param);
				}
			}
			
			resultSet = preparedStatement.executeQuery();
			resultsList = convertResultSetToMapList();
		} catch (SQLException e) {
			System.err.println("An error occured while trying to execute query! Exception message: "
					+ e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
			} catch (SQLException e) {
				System.err.println("An error occured while trying to close preparedStatement!"
						+ " Exception message: " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		return resultsList;
	}
	
	/**
	 * Closes open connection resource.
	 */
	public void closeConnection() {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (Exception e) {
			System.err.println("An error occured while trying to close connection! Exception message: "
					+ e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Helper method converting resultSet with the result of SQL query to the list of HashMap items
	 * corresponding to values retrieved from database.
	 * 
	 * @return list of elements retrieved from database, where each item in the list corresponds to the row of
	 * the query result and item contains the mapping of sql columns (keys) with their values stored as
	 * objects (values)
	 * @throws SQLException when a database access error occurs for any operation on JDBC resultSet
	 */
	private List<HashMap<String, Object>> convertResultSetToMapList() throws SQLException {
		ResultSetMetaData rsMetaData = resultSet.getMetaData();
		int columnCount = rsMetaData.getColumnCount();
		List<HashMap<String, Object>> resultsList = new ArrayList<HashMap<String, Object>>();
		
		// convert the result set into the list of maps (map element for every row)
		while (resultSet.next()) {
			HashMap<String, Object> resultRow = new HashMap<String, Object>(columnCount);
			// add a new element (the row from the database) to the map, where key is the column
			// and value is the value of the column stored as an object
			for (int i = 1; i <= columnCount; i++) {
				resultRow.put(rsMetaData.getColumnLabel(i), resultSet.getObject(i));
			}
			
			resultsList.add(resultRow);
		}
		
		return resultsList;
	}
}
