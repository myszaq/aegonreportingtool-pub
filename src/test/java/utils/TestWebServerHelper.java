/**
 * Very simple class providing 2 methods for running and stopping Moongoose v.5.6 web server on a local computer.
 * This webserver is needed for the purposes of running tests using offline version of Aegon application
 * (which must be hosted through HTTP channel). For more details see https://code.google.com/p/mongoose/.
 * 
 * @author @author Michał Myszkowski <myszaq85@gmail.com>
 */
package utils;

public class TestWebServerHelper {
	private static String executablePath = null;
	private static Process serverProcess = null;

	// initialize the path to Moongoose web server executable file
	static {
		ClassLoader classLoader = TestWebServerHelper.class.getClassLoader();
		executablePath = classLoader.getResource("offline_test_version/mongoose-free-5.6.exe").getPath();
	}
	
	/**
	 * Helper method used to start Moongoose web server before website tests.
	 * 
	 * @throws Exception when launching program failed
	 */
	public static void startLocalWebServer() throws Exception {
		// protection from running more than one instance of Moongoose web server
		if (serverProcess != null) {
			System.out.println("Server has already started by another process. You can ignore this message.");
			return;
		}
		
		ProcessBuilder pb = new ProcessBuilder(executablePath);
		try {
			serverProcess = pb.start();

			if (serverProcess.isAlive()) {
				System.out.println("Moongoose web server v.5.6 has been started successfully.");
			}
		} catch (Exception e) {
			System.out.println("An exception has occured while trying to run mongoose-free-5.6.exe! "
					+ e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Helper method used to stop/shutdown Moongoose web server after website tests.
	 */
	public static void stopLocalWebServer() {
		if (serverProcess != null) {
			serverProcess.destroy();

			System.out.println("Moongoose web server v.5.6 has been stopped successfully.");
		}
	}
}
