/**
 * This is MailSenderTest class with the test methods covering all public methods
 * of the tested class - MailSender.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */

package pl.aegon.youraccount.utils;

import java.io.IOException;
import javax.mail.MessagingException;
import org.testng.Assert;
import org.testng.annotations.*;
import pl.aegon.youraccount.config.ConfigurationProvider;

public class MailSenderTest {
	// the reference to tested class object
	private MailSender mailSender;
	private static String recipient1 = "bitingshoe@mailinator.com";
	private static String recipient2 = "titan_cris@mailinator.com";
	private static String attachment1FilePath = null;
	private static String attachment2FilePath = null;
	
	/**
	 * Helper method loading file paths for exemplary atttachments.
	 */
	@BeforeTest
	public void setUpTests() {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		
		attachment1FilePath = cl.getResource("test_attachments/testimage1.gif").getPath();
		attachment2FilePath = cl.getResource("test_attachments/testimage2.jpg").getPath();
	}
	
	/**
	 * Tests valid behavior of adding attachment to the email.
	 */
	@Test
	public void testValidAddAttachment() {
		try {
			mailSender = new MailSender();
			mailSender.addRecipient(recipient1);
			mailSender.setSubject("Hello! This is a test subject.");
			mailSender.setBody("Test message");
			
			try {
				mailSender.addAttachment(attachment1FilePath);
			} catch (NullPointerException e) {
				Assert.fail("There was unexpected exception!");
			}
			
			mailSender.sendEmail(false);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
	}
	
	/**
	 * Tests invalid behavior of adding null/invalid attachment to the email.
	 */
	@Test(expectedExceptions = { NullPointerException.class, IOException.class })
	public void testInvalidAddAttachment() {
		Exception ex = null;
		mailSender = new MailSender();
		
		// 1st case - attachment is null
		try {
			mailSender.addAttachment(null);
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex, "An exception was expected!");
		
		// 2nd case - attachment is invalid (does not exist)
		ex = null;
		try {
			mailSender.addAttachment("some/really/fake/path/");
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex, "An exception was expected!");
		
		throw new NullPointerException(ex.getMessage());
	}
	
	/**
	 * Tests valid behavior of adding recipient of the email.
	 */
	@Test
	public void testValidAddRecipient() {
		try {
			mailSender = new MailSender();
			try {
				mailSender.addRecipient(recipient1);
			} catch (NullPointerException e) {
				Assert.fail("There was unexpected exception!");
			}
			// add one more recipient
			mailSender.addRecipient(recipient2);
			
			mailSender.setSubject("Test subject.");
			mailSender.setBody("<b>Test message</b>");
			mailSender.sendEmail(true);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
	}
	
	/**
	 * Tests invalid behavior of adding null/empty recipient of the email.
	 */
	@Test(expectedExceptions = NullPointerException.class)
	public void testInvalidAddRecipient() {
		Exception ex = null;
		mailSender = new MailSender();
		
		// 1st case - recipient is null
		try {
			mailSender.addRecipient(null);
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex, "An exception was expected!");
		
		// 2nd case - recipient is empty
		ex = null;
		try {
			mailSender.addRecipient("");
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex, "An exception was expected!");
		
		throw new NullPointerException(ex.getMessage());
	}
	
	/**
	 * Tests valid behavior of setting email message.
	 */
	@Test
	public void testValidSendEmail() {
		try {
			mailSender = new MailSender();
			// add 2 recipients
			mailSender.addRecipient(recipient1);
			mailSender.addRecipient(recipient2);
			mailSender.setSubject("Some test subject");
			mailSender.setBody("Some test message. <b>How are you?</b>");
			// add 2 attachments
			mailSender.addAttachment(attachment1FilePath);
			mailSender.addAttachment(attachment2FilePath);
			
			// first send email as plain text, then as html
			mailSender.sendEmail(false);
			mailSender.sendEmail(true);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception! " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Tests invalid behavior of setting email message for few fail-scenarios.
	 */
	@Test
	public void testInvalidSendEmail() {
		Exception ex = null;
		mailSender = new MailSender();
		
		// #1 case - no recipient, no subject and no body
		try {
			mailSender.sendEmail(false);
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex, "An exception was expected!");
		Assert.assertTrue(ex instanceof IllegalArgumentException);
		
		// #2 case - no subject and no body
		ex = null;
		mailSender.addRecipient("fake_email@host.somewhere.mail.com");
		try {
			mailSender.sendEmail(false);
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex, "An exception was expected!");
		
		// #3 case - invalid recipient
		ex = null;
		mailSender.setSubject("fake/subject");
		mailSender.setBody("/fake/body/");
		try {
			mailSender.addRecipient("that_is_not_email");
			mailSender.sendEmail(true);
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex);
		Assert.assertTrue(ex instanceof MessagingException);
		
		// #4 case - attachment type not accepted by Gmail (exe file)
		ex = null;
		mailSender = new MailSender();
		try {
			mailSender.setSubject("fake/subject");
			mailSender.setBody("/fake/body/");
			mailSender.addRecipient(recipient1);
			mailSender.addAttachment(System.getProperty("user.dir") + "\\bin\\drivers\\chromedriver.exe");
			mailSender.sendEmail(false);
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex);
		Assert.assertTrue(ex instanceof MessagingException);
		
		// #5 case - authentication error (wrong username)
		ex = null;
		// save original value of the username from configuration
		String origUserName = ConfigurationProvider.getPropertyValue("email_username");
		ConfigurationProvider.setPropertyValue("email_username", "bad_username_for_email");
		
		mailSender = new MailSender();
		try {
			mailSender.setSubject("fake/subject");
			mailSender.setBody("/fake/body/");
			mailSender.addRecipient(recipient1);
			mailSender.sendEmail(false);
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex);
		Assert.assertTrue(ex instanceof MessagingException);
		
		// go back to original value of username (without it other tests would fail)
		ConfigurationProvider.setPropertyValue("email_username", origUserName);
	}
	
	/**
	 * Tests valid behavior of setting body for the email.
	 */
	@Test
	public void testValidSetBody() {
		try {
			mailSender = new MailSender();
			mailSender.addRecipient(recipient1);
			mailSender.setSubject("Test subject");
			
			try {
				mailSender.setBody("Hello! This is a test message body!");
			} catch (NullPointerException e) {
				Assert.fail("There was unexpected exception!");
			}
			
			mailSender.sendEmail(false);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
	}
	
	/**
	 * Tests invalid behavior of setting null/empty body for the email.
	 */
	@Test(expectedExceptions = NullPointerException.class)
	public void testInvalidSetBody() {
		Exception ex = null;
		mailSender = new MailSender();
		
		// 1st case - body is null
		try {
			mailSender.setBody(null);
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex, "An exception was expected!");
		
		// 2nd case - body is empty
		ex = null;
		try {
			mailSender.setBody("");
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex, "An exception was expected!");
		
		throw new NullPointerException(ex.getMessage());
	}
	
	/**
	 * Tests valid behavior of setting subject for the email.
	 */
	@Test
	public void testValidSetSubject() {
		try {
			mailSender = new MailSender();
			mailSender.addRecipient(recipient2);
			
			try {
				mailSender.setSubject("Hello! This is a test subject.");
			} catch (NullPointerException e) {
				Assert.fail("There was unexpected exception!");
			}
			
			mailSender.setBody("Test message");
			mailSender.sendEmail(false);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
	}
	
	/**
	 * Tests invalid behavior of setting null/empty subject for the email.
	 */
	@Test(expectedExceptions = NullPointerException.class)
	public void testInvalidSetSubject() {
		Exception ex = null;
		mailSender = new MailSender();
		
		// 1st case - subject is null
		try {
			mailSender.setSubject(null);
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex, "An exception was expected!");
		
		// 2nd case - subject is empty
		ex = null;
		try {
			mailSender.setSubject("");
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNotNull(ex, "An exception was expected!");
		
		throw new NullPointerException(ex.getMessage());
	}
}
