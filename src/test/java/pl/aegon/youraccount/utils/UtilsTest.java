/**
 * This is UtilsTest class with the test methods covering all relevant public methods
 * in the tested class - Utils.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.testng.Assert;
import org.testng.annotations.*;

public class UtilsTest {
	
	/**
	 * Test for valid behavior of the method getDoubleFromStringField.
	 */
	@Test
	public void testValidGetDoubleFromStringField() {
		String testValue = " Current balance is 2154,36897 PLN";
		double expectedValue = 2154.36897d;
		double actualValue = Utils.getDoubleFromStringField(testValue);
		
		Assert.assertEquals(actualValue, expectedValue);
	}
	
	/**
	 * Test for invalid behavior of the method getDoubleFromStringField.
	 */
	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testInvalidGetDoubleFromStringField() {
		Utils.getDoubleFromStringField(null);
	}
	
	/**
	 * Test for valid behavior of the method getFloatFromStringField.
	 */
	@Test
	public void testValidGetFloatFromStringField() {
		String testValue = " Current balance is 1794,70489 PLN";
		float expectedValue = 1794.70489f;
		float actualValue = Utils.getFloatFromStringField(testValue);
		
		Assert.assertEquals(actualValue, expectedValue);
	}
	
	/**
	 * Test for invalid behavior of the method getFloatFromStringField.
	 */
	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testInvalidGetFloatFromStringField() {
		Utils.getFloatFromStringField(null);
	}
	
	/**
	 * Test for normal, valid behavior of the method checkIfBrowserPathValid.
	 */
	@Test
	public void testValidCheckIfBrowserPathValid() {
		String testDriverPath = System.getProperty("user.dir") + "\\bin\\drivers\\chromedriver.exe";
		
		try {
			Assert.assertEquals(Utils.checkIfBrowserPathValid(testDriverPath), true);
		} catch (Exception e) {
			Assert.assertNull(e, "There was unexpected exception!");
		}
	}
	
	/**
	 * Test for valid behavior of the method checkIfBrowserPathValid when it throws exception.
	 */
	@Test
	public void testValidWrongPathCheckIfBrowserPathValid() {
		String testDriverPath = "nosuchfile.exe";
		
		try {
			Assert.assertEquals(Utils.checkIfBrowserPathValid(testDriverPath), false);
		} catch (Exception e) {
			Assert.assertNotNull(e, "There should have been an exception!");
		}
	}
	
	/**
	 * Test for invalid behavior of the method checkIfBrowserPathValid, where it throws expected Exception.
	 * 
	 * @throws Exception
	 */
	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testInvalidCheckIfBrowserPathValid() throws Exception {
		Utils.checkIfBrowserPathValid(null);
	}
	
	/**
	 * Test for valid behavior of the method getTempFolderPath.
	 */
	@Test
	public void testGetTempFolderPath() {
		String actualPath = Utils.getTempFolderPath();
		
		Assert.assertNotNull(actualPath);
		Assert.assertTrue((new File(actualPath)).isDirectory());
	}
	
	/**
	 * Test for valid behavior of the method getChartImageFilePath.
	 */
	@Test
	public void testGetChartImageFilePath() {
		String expectedPath = "chart_test_policy_123_"
				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		
		try {
			String actualPath = Utils.getChartImageFilePath("test_policy", "123", false);
			Assert.assertTrue(actualPath.contains(expectedPath));
			
			File file = new File(actualPath);
			Assert.assertFalse(file.exists());
		} catch (Exception e) {
			Assert.assertNull(e, "There was unexpected exception!");
		}
	}
	
	/**
	 * Test for valid behavior of the method getExceptionStackTrace.
	 */
	@Test
	public void testValidGetExceptionStackTrace() {
		try {
			String filePath = null;
			new File(filePath);
		} catch (Exception e) {
			String stackTrace = Utils.getExceptionStackTrace(e);
			Assert.assertNotNull(stackTrace);
		}
	}
	
	/**
	 * Test for invalid behavior of the method getExceptionStackTrace when it throws Exception.
	 */
	@Test(expectedExceptions = IllegalArgumentException.class)
	public void testInvalidGetExceptionStackTrace() {
		Exception ex = null;
		try {
			// perfectly valid operation
			System.getProperty("user.dir");
		} catch (Exception e) {
			ex = e;
			Assert.assertNull(e, "There was unexpected exception");
		}
		
		Utils.getExceptionStackTrace(ex);
	}
	
	/**
	 * Test for valid behavior of the method showTrayMessage.
	 */
	@Test
	public void testShowTrayMessage() {
		try {
			Utils.showTrayMessage("Test notification", "Test info message", Utils.INFO);
			Utils.showTrayMessage("Test notification", "Test warning message", Utils.WARNING);
			Utils.showTrayMessage("Test notification", "Test error message", Utils.ERROR);
		} catch (Exception e) {
			Assert.assertNull(e, "There was unexpected exception");
		}
	}
}
