package pl.aegon.youraccount.dbservice;

import java.util.List;
import pl.aegon.youraccount.config.ConfigurationProvider;
import pl.aegon.youraccount.entity.*;
import pl.aegon.youraccount.website.WebSiteHandler;

public class DBHandlerServiceTest {

	public static void main(String[] args) {
		// System.exit(1);
		DatabaseServiceProvider dbManager = new DatabaseServiceProvider();
		
		try {
			ConfigurationProvider.setPropertyValue("user_login", "xxxxxxxxxxxxxx");
			String login = ConfigurationProvider.getPropertyValue("user_login");
			if (login.isEmpty()) {
				System.out.println("login is empty");
			}
			ConfigurationProvider.setPropertyValue("user_password", "xxxxxxxxxxxxxx");
			ConfigurationProvider.setPropertyValue("generate_ufk_balance_chart", "true");
			ConfigurationProvider.saveConfigurationFile();
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		WebSiteHandler handler = new WebSiteHandler();
		try {
			handler.setCustomUrl("http://localhost:8080/AEGON Online Logowanie.html");
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		try {
			handler.collectAllRelevantDataFromApplication();
		} catch (Exception e) {
			System.out.println("Caught exception: " + e.getMessage());
			e.printStackTrace();
		}

		EntryLog entryLog = handler.getEntryLogEntity();
		List<Policy> policyList = handler.getPolicyEntityList();
		List<PolicyHistory> policyHistoryList = handler.getPolicyHistoryEntityList();
		List<SubAccount> subAccountList = handler.getSubAccountEntityList();
		List<SubAccountHistory> subAccountHistoryList = handler.getSubAccountHistoryEntityList();
		List<SubAccountBalanceHistory> subAccountBalanceHistoryList = handler
				.getSubAccountBalanceHistoryEntityList();

		System.out.println(entryLog.toString() + "\n");
		for (Policy p : policyList) {
			System.out.println(p.toString() + "\n");
			System.out.println("policy SubAccounts: " + p.getSubAccountList());
		}
		for (PolicyHistory ph : policyHistoryList) {
			System.out.println(ph.toString() + "\n");
		}
		for (SubAccount sa : subAccountList) {
			System.out.println(sa.toString() + "\n");
			System.out.println("SubAccount policy: " + sa.getPolicy());
		}
		for (SubAccountHistory sah : subAccountHistoryList) {
			System.out.println(sah.toString() + "\n");
		}
		for (SubAccountBalanceHistory sabh : subAccountBalanceHistoryList) {
			System.out.println(sabh.toString() + "\n");
		}

		try {
			dbManager.setEntryLog(entryLog);
			dbManager.setPolicyList(policyList);
			dbManager.setPolicyHistoryList(policyHistoryList);
			dbManager.setSubAccountList(subAccountList);
			dbManager.setSubAccountHistoryList(subAccountHistoryList);
			dbManager.setSubAccountBalanceHistoryList(subAccountBalanceHistoryList);
			
			dbManager.startTransaction();
			dbManager.storeAllEntitiesInDatabase();
			dbManager.commitTransaction(true);
			System.exit(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			System.exit(1);
		}

	}
}
