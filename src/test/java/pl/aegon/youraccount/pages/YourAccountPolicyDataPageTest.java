/**
 * YourAccountPolicyDataPageTest class with the test methods covering all public methods
 * in the tested class - YourAccountPolicyDataPage.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.pages;

import java.util.HashMap;
import org.testng.Assert;
import org.testng.annotations.*;

public class YourAccountPolicyDataPageTest extends YourAccountAbstractPage {
	private static String pageUrl = "http://localhost:8080/AEGON Online Wybierz subkonto P1.html";
	private YourAccountMainPage objMainPage;
	private YourAccountPolicyDataPage objPolicyDataPage;

	/**
	 * Test of reading all policy details (sub accounts) data on the policy page and their correctness.
	 * 
	 * @param subAccountData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testPolicyDetailsDataCorrectness(HashMap<Integer, HashMap<String, Object>> subAccountData) {
		this.driver.get("http://localhost:8080/AEGON Online Twoje Polisy.html");
		objMainPage = new YourAccountMainPage(driver);

		for (int i = 0; i < objMainPage.getPolicyNumberFieldList().size(); i++) {
			objMainPage.goToPolicyDetailsPage(i);

			objPolicyDataPage = new YourAccountPolicyDataPage(driver);
			Assert.assertEquals(objPolicyDataPage.getSubAccountNameFieldList(),
					subAccountData.get(i).get("expectedSubAccountNames"));
			Assert.assertEquals(objPolicyDataPage.getSubAccountBalanceFieldList(),
					subAccountData.get(i).get("expectedBalances"));
			objPolicyDataPage.goToPolicyListPage();
		}

		// test using only one test policy
		objMainPage.goToPolicyDetailsPage(0);
		Assert.assertEquals(objPolicyDataPage.getPolicyDataPageTitle(),
				subAccountData.get(2).get("expectedPolicyPageTitle"));
		Assert.assertEquals(objPolicyDataPage.getPolicyListLink().getText(),
				subAccountData.get(2).get("expectedPolicyListLinkText"));
	}

	/**
	 * Test of redirecting from policy data page to the given sub account data page.
	 * 
	 * @param testData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testRedirectionToSubAccountPage(HashMap<String, Object> testData) {
		this.driver.get(pageUrl);
		objPolicyDataPage = new YourAccountPolicyDataPage(driver);

		YourAccountSubAccountDetailsPage objSubAccountPage;
		for (int i = 0; i < objPolicyDataPage.getSubAccountNameFieldList().size(); i++) {
			objPolicyDataPage.goToSubAccountDetailsPage(i);

			objSubAccountPage = new YourAccountSubAccountDetailsPage(driver);
			Assert.assertEquals(objSubAccountPage.getSubAccountDetailsPageTitle(),
					testData.get("expectedSubAccountPageTitle"));
			objSubAccountPage.goToSubAccountBalancePage();
		}
	}
}
