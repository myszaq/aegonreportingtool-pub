/**
 * YourAccountSubAccountDetailsPageTest class with the test method covering all public methods
 * in the tested class - YourAccountSubAccountDetailsPage.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.pages;

import java.util.Arrays;
import java.util.HashMap;
import org.testng.Assert;
import org.testng.annotations.*;

public class YourAccountSubAccountDetailsPageTest extends YourAccountAbstractPage {
	private static String pageUrl = "http://localhost:8080/AEGON Online Saldo Subkonta (P1_S1).html";
	private YourAccountSubAccountDetailsPage objSubAccountPage;
	private YourAccountPolicyDataPage objPolicyDataPage;

	/**
	 * Test of reading all policy sub account details (UFKs) data on the sub account details page and their
	 * correctness.
	 * 
	 * @param ufkData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testSubAccountDetailsDataCorrectness(HashMap<Integer, HashMap<String, Object>> ufkData) {
		this.driver.get("http://localhost:8080/AEGON Online Twoje Polisy.html");
		YourAccountMainPage objMainPage = new YourAccountMainPage(driver);

		int subAccountIdx = 0;
		// iterate through all policies on the main page
		for (int i = 0; i < objMainPage.getPolicyNumberFieldList().size(); i++) {
			objMainPage.goToPolicyDetailsPage(i);

			objPolicyDataPage = new YourAccountPolicyDataPage(driver);
			// for each policy iterate through every sub account page
			for (int j = 0; j < objPolicyDataPage.getSubAccountNameFieldList().size(); j++) {
				objPolicyDataPage.goToSubAccountDetailsPage(j);

				// verify that the data displayed on given sub account details page are correct
				objSubAccountPage = new YourAccountSubAccountDetailsPage(driver);
				Object expectedData = new Object();

				// if the current test data is the string, create the list from it (needed for comparison) and
				// repeat this step for all data types
				if (ufkData.get(subAccountIdx).get("expectedUfkNames") instanceof String) {
					expectedData = Arrays.asList(ufkData.get(subAccountIdx).get("expectedUfkNames"));
				} else {
					expectedData = ufkData.get(subAccountIdx).get("expectedUfkNames");
				}
				Assert.assertEquals(objSubAccountPage.getUfkNameFieldList(), expectedData);

				if (ufkData.get(subAccountIdx).get("expectedUfkInvestUnitValues") instanceof String) {
					expectedData = Arrays.asList(ufkData.get(subAccountIdx)
							.get("expectedUfkInvestUnitValues"));
				} else {
					expectedData = ufkData.get(subAccountIdx).get("expectedUfkInvestUnitValues");
				}
				Assert.assertEquals(objSubAccountPage.getUfkInvestUnitValueFieldList(), expectedData);

				if (ufkData.get(subAccountIdx).get("expectedUfkInvestUnitCounts") instanceof String) {
					expectedData = Arrays.asList(ufkData.get(subAccountIdx)
							.get("expectedUfkInvestUnitCounts"));
				} else {
					expectedData = ufkData.get(subAccountIdx).get("expectedUfkInvestUnitCounts");
				}
				Assert.assertEquals(objSubAccountPage.getUfkInvestUnitNumberFieldList(), expectedData);

				if (ufkData.get(subAccountIdx).get("expectedUfkInvestUnitCollectedValues") instanceof String) {
					expectedData = Arrays.asList(ufkData.get(subAccountIdx).get(
							"expectedUfkInvestUnitCollectedValues"));
				} else {
					expectedData = ufkData.get(subAccountIdx).get("expectedUfkInvestUnitCollectedValues");
				}
				Assert.assertEquals(objSubAccountPage.getUfkCollectedInvestUnitValueFieldList(), expectedData);

				subAccountIdx++;
				// go back to sub accounts list page (policy details page)
				objSubAccountPage.goToSubAccountBalancePage();
			}
			objSubAccountPage.goToPolicyListPage();
		}

		// navigate directly to the first sub account page (hardcoded for simplicity - to avoid
		// navigating from the main page)
		this.driver.get(pageUrl);
		objSubAccountPage = new YourAccountSubAccountDetailsPage(driver);

		Assert.assertEquals(objSubAccountPage.getSubAccountDetailsPageTitle(),
				ufkData.get(5).get("expectedSubAccountPageTitle"));
		Assert.assertEquals(objSubAccountPage.getSubAccountBalanceLink().getText(),
				ufkData.get(5).get("expectedBalanceLinkText"));
		Assert.assertEquals(objSubAccountPage.getPolicyListLink().getText(),
				ufkData.get(5).get("expectedPolicyListLinkText"));
	}
}
