/**
 * YourAccountLoginPageTest class with the test methods covering all public methods
 * in the tested class - YourAccountLoginPage.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.pages;

import java.util.HashMap;
import org.testng.Assert;
import org.testng.annotations.*;

public class YourAccountLoginPageTest extends YourAccountAbstractPage {
	private static String pageUrl = "http://localhost:8080/AEGON Online Logowanie.html";
	private YourAccountLoginPage objLoginPage;
	private YourAccountMainPage objMainPage;

	/**
	 * Test whether the error field appears and displays a correct message in case of failed login.
	 * 
	 * @param testData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testErrorFieldPresence(HashMap<String, Object> testData) {
		this.driver.get("http://localhost:8080/AEGON Online Logowanie - Blad.html");

		objLoginPage = new YourAccountLoginPage(driver);
		String errorField = objLoginPage.getErrorField();
		String loginPageTitle = objLoginPage.getLoginPageTitle();

		Assert.assertEquals(errorField, testData.get("errorFieldText"));
		Assert.assertEquals(loginPageTitle, testData.get("expectedLoginPageTitle"));
	}

	/**
	 * Test of correct login behaviour - whether the login form works properly.
	 * 
	 * @param testData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testLoginCorrect(HashMap<String, Object> testData) {
		this.driver.get(pageUrl);
		objLoginPage = new YourAccountLoginPage(driver);
		objMainPage = new YourAccountMainPage(driver);

		Assert.assertEquals(objLoginPage.getLoginPageTitle(), testData.get("expectedLoginPageTitle"));

		// test two ways to login to the application
		objLoginPage.loginToYourAccount((String) testData.get("userNameValid"), "fakePassword");
		Assert.assertEquals(objMainPage.getMainPageTitle(), testData.get("expectedMainPageTitle"));

		this.driver.get(pageUrl);
		objLoginPage.setUserName((String) testData.get("userNameValid"));
		objLoginPage.setPassword("fakePassword");
		objLoginPage.clickLogin();

		// login to application
		Assert.assertEquals(objMainPage.getMainPageTitle(), testData.get("expectedMainPageTitle"));
	}

	/**
	 * Test of incorrect login behaviour - whether the error field with the message appears correctly.
	 * 
	 * @param testData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testLoginIncorrect(HashMap<String, Object> testData) {
		this.driver.get(pageUrl);
		objLoginPage = new YourAccountLoginPage(driver);
		objMainPage = new YourAccountMainPage(driver);

		Assert.assertEquals(objLoginPage.getLoginPageTitle(), testData.get("expectedLoginPageTitle"));
		objLoginPage.loginToYourAccount("badUsername", "fakePassword");

		Assert.assertEquals(objMainPage.getMainPageTitle(), testData.get("expectedMainPageTitle"));
		Assert.assertNotNull(objLoginPage.getErrorField());
		Assert.assertFalse(objLoginPage.getErrorField().isEmpty());
	}
}
