/**
 * Abstract class containing methods for setting up and tearing down all concrete test classes
 * (which inherit after this class) in the current package.
 *  
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import utils.TestWebServerHelper;

public abstract class YourAccountAbstractPage {
	protected WebDriver driver = null;

	/**
	 * Helper method starting Moongoose webserver at the beginning of the test suite.
	 */
	@BeforeSuite
	public void setUpWebServer() {
		Exception ex = null;
		try {
			TestWebServerHelper.startLocalWebServer();
		} catch (Exception e) {
			ex = e;
		}

		// assert that there was no exception
		Assert.assertNull(ex);
	}
	
	/**
	 * Helper method stopping Moongoose webserver at the end of the test suite.
	 */
	@AfterSuite
	public void tearDownWebServer() {
		TestWebServerHelper.stopLocalWebServer();
	}
	
	/**
	 * Method launched before any of the tests becomes executed. Used to
	 * initialize web browser. 
	 * 
	 * @param browser name of the browser to run
	 * @throws Exception when the invalid browser name was passed
	 */
	@BeforeClass
	@Parameters("browser")
	public void setUp(@Optional String browser) throws IllegalArgumentException {
		// default browser is firefox
		if (browser == null || browser.isEmpty()) {
			browser = "firefox";
		}
		
		switch (browser) {
		case "firefox":
			// disable flash plugin for firefox
			FirefoxProfile firefoxProfile = new FirefoxProfile();
			firefoxProfile.setPreference("plugin.state.flash", 0);
			// to avoid issues with predefined page titles, make sure that Firefox will use
			// polish language only
			firefoxProfile.setPreference("intl.accept_languages", "pl");
			this.driver = new FirefoxDriver(firefoxProfile);
			break;
		case "chrome":
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
					+ "\\bin\\drivers\\chromedriver.exe");
			// to avoid issues with predefined page titles, make sure that Chrome will use polish
			// language only
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--lang=pl");
			this.driver = new ChromeDriver(options);
			break;
		case "iexplorer":
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")
					+ "\\bin\\drivers\\IEDriverServer.exe");
			this.driver = new InternetExplorerDriver();
			break;
		case "phantomjs":
			System.setProperty("phantomjs.binary.path", System.getProperty("user.dir")
					+ "\\bin\\drivers\\phantomjs.exe");
			this.driver = new PhantomJSDriver();
			break;
		default:
			throw new IllegalArgumentException("Browser name is not correct!");
		}

		this.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	/**
	 * Method launched after all tests were executed. Used to close the web browser.
	 */
	@AfterClass
	public void tearDown() {
		this.driver.quit();
	}
}
