/**
 * YourAccountMainPageTest class with the test methods covering all public methods in the tested class
 *  - YourAccountMainPage (additionally also YourAccountLogoutPage methods are covered).
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.pages;

import java.util.HashMap;
import org.testng.Assert;
import org.testng.annotations.*;

public class YourAccountMainPageTest extends YourAccountAbstractPage {
	private static String pageUrl = "http://localhost:8080/AEGON Online Twoje Polisy.html";
	private YourAccountMainPage objMainPage;
	private YourAccountLogoutPage objLogoutPage;

	/**
	 * Test of reading all policy data on the main page and their correctness.
	 * 
	 * @param policyData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testPolicyDataCorrectness(HashMap<String, Object> policyData) {
		this.driver.get(pageUrl);
		objMainPage = new YourAccountMainPage(driver);

		Assert.assertEquals(objMainPage.getBalanceFieldList(), policyData.get("expectedBalances"));
		Assert.assertEquals(objMainPage.getPolicyNameFieldList(), policyData.get("expectedPolicyNames"));
		Assert.assertEquals(objMainPage.getPolicyNumberFieldList(), policyData.get("expectedPolicyNumbers"));
		Assert.assertEquals(objMainPage.getProductNameFieldList(), policyData.get("expectedProductNames"));
		Assert.assertEquals(objMainPage.getTotalField(), policyData.get("expectedTotalField"));
	}

	/**
	 * Test of redirecting from main page to the given policy data page.
	 * 
	 * @param testData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testRedirectionToPolicyData(HashMap<String, String> testData) {
		this.driver.get(pageUrl);
		objMainPage = new YourAccountMainPage(driver);

		YourAccountPolicyDataPage objPolicyPage;
		for (int i = 0; i < objMainPage.getPolicyNumberFieldList().size(); i++) {
			objMainPage.goToPolicyDetailsPage(i);

			objPolicyPage = new YourAccountPolicyDataPage(driver);
			Assert.assertEquals(objPolicyPage.getPolicyDataPageTitle(),
					testData.get("expectedPolicyPageTitle"));
			objPolicyPage.goToPolicyListPage();
			Assert.assertEquals(objMainPage.getMainPageTitle(), testData.get("expectedMainPageTitle"));
		}
	}

	/**
	 * Test of logging out from the application and the logout page itself.
	 * 
	 * @param testData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testLogoutAndLogoutPage(HashMap<String, String> testData) {
		this.driver.get(pageUrl);
		objMainPage = new YourAccountMainPage(driver);
		objLogoutPage = new YourAccountLogoutPage(driver);

		// logout from application
		Assert.assertTrue(objMainPage.getLogoutLink().getText()
				.equals(testData.get("expectedLogoutLinkText")));
		objMainPage.logoutFromYourAccount();

		Assert.assertEquals(objLogoutPage.getLogoutPageTitle(), testData.get("expectedLogoutPageTitle"));
		Assert.assertTrue(objLogoutPage.getMessageBox().contains(testData.get("messageBoxText")));
		Assert.assertEquals(objLogoutPage.getCloseLink(), testData.get("closeLinkText"));
	}

	/**
	 * Test of reading general data on the main page and their correctness.
	 * 
	 * @param generalData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testGeneralDataCorrectness(HashMap<String, String> generalData) {
		this.driver.get(pageUrl);
		objMainPage = new YourAccountMainPage(driver);

		Assert.assertEquals(objMainPage.getMainPageTitle(), generalData.get("expectedMainPageTitle"));
		Assert.assertEquals(objMainPage.getMainHeaderTitle(), generalData.get("expectedMainHeaderTitle"));

		Assert.assertEquals(objMainPage.getDataOnDayField(), generalData.get("expectedDataOnDay"));
		Assert.assertEquals(objMainPage.getFullNameField(), generalData.get("expectedFullName"));
		Assert.assertEquals(objMainPage.getLastLoggedDateField(), generalData.get("expectedLastLoggedDate"));
		Assert.assertEquals(objMainPage.getLastModifiedDateField(),
				generalData.get("expectedLastModifiedDate"));
	}
}
