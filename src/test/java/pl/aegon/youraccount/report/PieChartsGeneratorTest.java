/**
 * This is PieChartsGeneratorTest class with the test methods covering all public
 * and few private methods in the tested class - PieChartsGenerator.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.report;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;

import com.google.common.io.Files;
import org.jfree.data.general.DefaultPieDataset;
import org.testng.Assert;
import org.testng.annotations.*;

import pl.aegon.youraccount.config.ConfigurationProvider;
import pl.aegon.youraccount.dbservice.*;

public class PieChartsGeneratorTest {
	// the reference to tested class object
	private PieChartsGenerator pcGenerator;
	private static String imagesResourcesPath;
	
	/**
	 * Helper method run before any of the tests is executed. It prepares the class for all of the tests.
	 * 
	 * @throws Exception when initialization of the tested object failed
	 */
	@BeforeTest
	public void setUpTests() throws Exception {
		// set the database to use test connection
		DatabaseServiceProvider.setPersistenceUnitName(DatabaseManager.PersistenceType.TEST_CONN);
		
		// set up the properties for pie charts generation (even if the settings are the same,
		// to make sure that exactly this configuration will be active)
		ConfigurationProvider.setPropertyValue("chart_generate_pie_chart_for_policies", "true");
		ConfigurationProvider.setPropertyValue("chart_generate_pie_chart_for_subaccounts", "true");
		ConfigurationProvider.setPropertyValue("chart_generate_pie_chart_for_ufk", "true");
		ConfigurationProvider.setPropertyValue("chart_do_not_generate_single_item_pie_chart", "true");
		// just in case set the language to polish (model pie charts images are created in polish)
		ConfigurationProvider.setPropertyValue("user_language", "pl");
		
		imagesResourcesPath = new File("src/test/resources/charts_test_images").getAbsolutePath();
		this.pcGenerator = new PieChartsGenerator();
	}
	
	/**
	 * Helper method run after execution of each test method. Deletes created pie charts images.
	 */
	@AfterMethod()
	public void cleanAfterTests() {
		this.pcGenerator.deletePieChartsImagesFiles();
	}
	
	/**
	 * Test for correctness of creating policies dataset. Notice: the tested method is private!
	 */
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testGetDatasetForPolicyList(HashMap<String, Object> policyData) {
		Exception ex = null;
		DefaultPieDataset policiesDataset = null;
		
		// since getDatasetForPolicyList() is a private method, we use reflection to access it
		try {
			Method method = this.pcGenerator.getClass().getDeclaredMethod("getDatasetForPolicyList",
					(Class<?>[]) null);
			method.setAccessible(true);
			// call the method without the parameters
			Object invokeResult = method.invoke(this.pcGenerator, (Object[]) null);
			
			if (invokeResult instanceof DefaultPieDataset) {
				policiesDataset = (DefaultPieDataset) invokeResult;
			} else {
				throw new Exception("Method getDatasetForPolicyList did not return DefaultPieDataset type!");
			}
		} catch (Exception e) {
			ex = e;
		}
		
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		Assert.assertNotNull(policiesDataset, "Policies dataset is null!");
		
		// verify the correctness of the dataset
		// first, extract the right data (list of policy numbers and their current balances) from data source
		List<String> expPolicyNumbers = (List<String>) policyData.get("expectedPolicyNumbers");
		List<String> expCurrentBalances = (List<String>) policyData.get("expectedCurrentBalances");
		
		for (int i = 0; i < policiesDataset.getItemCount(); i++) {
			// get exact policy number by retrieving the second token
			// (original string looks like "Polisa EFExxx")
			String tmpStr = ((String) policiesDataset.getKey(i));
			String policyNumber = tmpStr.split(" ")[1];
			
			// get the value of current balance as double with 2 digits precision
			double rawBalance = policiesDataset.getValue(i).doubleValue();
			BigDecimal bd = new BigDecimal(rawBalance).setScale(2, RoundingMode.HALF_UP);
			double currentBalance = bd.doubleValue();
			
			// finally compare actual values with expected
			Assert.assertEquals(policyNumber, expPolicyNumbers.get(i));
			Assert.assertEquals(currentBalance, Double.parseDouble(expCurrentBalances.get(i)));
		}
	}
	
	/**
	 * Test for correctness of creating sub accounts datasets list. Notice: the tested method is private!
	 */
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testGetDatasetsForSubAccountList(HashMap<Integer, HashMap<String, Object>> subAccountsData) {
		Exception ex = null;
		List<DefaultPieDataset> subAccountsDatasetsList = null;
		
		// since getDatasetsForSubAccountList() is a private method, we use reflection to access it
		try {
			Method method = this.pcGenerator.getClass().getDeclaredMethod("getDatasetsForSubAccountList",
					(Class<?>[]) null);
			method.setAccessible(true);
			// call the method without the parameters
			Object invokeResult = method.invoke(this.pcGenerator, (Object[]) null);
			
			if (invokeResult instanceof List<?>) {
				subAccountsDatasetsList = (List<DefaultPieDataset>) invokeResult;
			} else {
				throw new Exception("Method getDatasetsForSubAccountList did not return List type!");
			}
		} catch (Exception e) {
			ex = e;
		}
		
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		Assert.assertNotNull(subAccountsDatasetsList, "Sub accounts datasets list is null!");
		
		for (Integer mapKey : subAccountsData.keySet()) {
			DefaultPieDataset subAccountDataset = subAccountsDatasetsList.get(mapKey.intValue());
			
			// first, extract the right data (list of sub account names and their current balances)
			// from data source
			List<String> expSubAccountNames = (List<String>) subAccountsData.get(mapKey).get(
					"expectedSubAccountNames");
			List<String> expCurrentBalances = (List<String>) subAccountsData.get(mapKey).get(
					"expectedCurrentBalances");
			
			// verify the correctness of each sub accounts dataset
			for (int i = 0; i < subAccountDataset.getItemCount(); i++) {
				String subAccountName = (String) subAccountDataset.getKey(i);
				
				// get the value of current balance as double with 2 digits precision
				double rawBalance = subAccountDataset.getValue(i).doubleValue();
				BigDecimal bd = new BigDecimal(rawBalance).setScale(2, RoundingMode.HALF_UP);
				double currentBalance = bd.doubleValue();
				
				// finally compare actual values with expected
				Assert.assertEquals(subAccountName, expSubAccountNames.get(i));
				Assert.assertEquals(currentBalance, Double.parseDouble(expCurrentBalances.get(i)));
			}
		}
	}
	
	/**
	 * Test for correctness of creating UFKs datasets list. Notice: the tested method is private!
	 */
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testGetDatasetsForUFKList(HashMap<Integer, HashMap<String, Object>> subAccountsUFKData) {
		Exception ex = null;
		List<DefaultPieDataset> subAccountsUFKDatasetsList = null;
		
		// since getDatasetsForUFKList() is a private method, we use reflection to access it
		try {
			Method method = this.pcGenerator.getClass().getDeclaredMethod("getDatasetsForUFKList",
					(Class<?>[]) null);
			method.setAccessible(true);
			// call the method without the parameters
			Object invokeResult = method.invoke(this.pcGenerator, (Object[]) null);
			
			if (invokeResult instanceof List<?>) {
				subAccountsUFKDatasetsList = (List<DefaultPieDataset>) invokeResult;
			} else {
				throw new Exception("Method getDatasetsForUFKList did not return List type!");
			}
		} catch (Exception e) {
			ex = e;
		}
		
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		Assert.assertNotNull(subAccountsUFKDatasetsList, "Sub accounts UFK datasets list is null!");
		
		for (Integer mapKey : subAccountsUFKData.keySet()) {
			DefaultPieDataset subAccountUFKDataset = subAccountsUFKDatasetsList.get(mapKey.intValue());
			
			// first, extract the right data (list of UFK names and their invest unit collect values)
			// from data source
			List<String> expUfkNames = (List<String>) subAccountsUFKData.get(mapKey).get("expectedUfkNames");
			List<String> expInvestUnitCollectValues = (List<String>) subAccountsUFKData.get(mapKey).get(
					"expectedInvestUnitCollectValues");
			
			// verify the correctness of each UFK dataset
			for (int i = 0; i < subAccountUFKDataset.getItemCount(); i++) {
				String subAccountName = (String) subAccountUFKDataset.getKey(i);
				
				// get the value of current balance as double with 2 digits precision
				double rawBalance = subAccountUFKDataset.getValue(i).doubleValue();
				BigDecimal bd = new BigDecimal(rawBalance).setScale(2, RoundingMode.HALF_UP);
				double currentBalance = bd.doubleValue();
				
				// finally compare actual values with expected
				Assert.assertEquals(subAccountName, expUfkNames.get(i));
				Assert.assertEquals(currentBalance, Double.parseDouble(expInvestUnitCollectValues.get(i)));
			}
		}
	}
	
	/**
	 * Test for correctness of generating 2D pie chart for policies in the account.
	 * 
	 * @param testImagePaths map containing relative file path to image of policies pie chart
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testGeneratePieChartForPolicies2DVersion(
			HashMap<Integer, HashMap<String, String>> testImagePaths) {
		try {
			this.setPieCharts3DGenerationMode(false);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
		
		this.runPieChartGenerationForPolicies(testImagePaths);
	}
	
	/**
	 * Test for correctness of generating 3D pie chart for policies in the account.
	 * 
	 * @param testImagePaths map containing relative file path to image of policies pie chart
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testGeneratePieChartForPolicies3DVersion(
			HashMap<Integer, HashMap<String, String>> testImagePaths) {
		try {
			this.setPieCharts3DGenerationMode(true);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
		
		this.runPieChartGenerationForPolicies(testImagePaths);
	}
	
	/**
	 * Test for correctness of generating 2D pie charts for sub accounts.
	 * 
	 * @param testImagePaths map containing relative file paths to images of sub accounts pie charts
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testGeneratePieChartsForSubAccounts2DVersion(
			HashMap<Integer, HashMap<String, String>> testImagePaths) {
		try {
			this.setPieCharts3DGenerationMode(false);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
		
		this.runPieChartsGenerationForSubAccounts(testImagePaths);
	}
	
	/**
	 * Test for correctness of generating 3D pie charts for sub accounts.
	 * 
	 * @param testImagePaths map containing relative file paths to images of sub accounts pie charts
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testGeneratePieChartsForSubAccounts3DVersion(
			HashMap<Integer, HashMap<String, String>> testImagePaths) {
		try {
			this.setPieCharts3DGenerationMode(true);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
		
		this.runPieChartsGenerationForSubAccounts(testImagePaths);
	}
	
	/**
	 * Test for correctness of generating 2D pie charts for sub accounts UFK.
	 * 
	 * @param testImagePaths map containing relative file paths to images of UFK pie charts
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testGeneratePieChartsForSubAccountsUFK2DVersion(
			HashMap<Integer, HashMap<String, String>> testImagePaths) {
		try {
			this.setPieCharts3DGenerationMode(false);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
		
		this.runPieChartsGenerationForUFK(testImagePaths);
	}
	
	/**
	 * Test for correctness of generating 3D pie charts for sub accounts UFK.
	 * 
	 * @param testImagePaths map containing relative file paths to images of UFK pie charts
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testGeneratePieChartsForSubAccountsUFK3DVersion(
			HashMap<Integer, HashMap<String, String>> testImagePaths) {
		try {
			this.setPieCharts3DGenerationMode(true);
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
		
		this.runPieChartsGenerationForUFK(testImagePaths);
	}
	
	/**
	 * Test for correctness of generating all pie chart images.
	 */
	@Test
	public void testGenerateAllPieCharts() {
		try {
			// since tested method is just the wrapper for specific methods,
			// we only verify if all the image file paths are available
			this.pcGenerator.generateAllPieCharts();
			
			Assert.assertNotNull(this.pcGenerator.getImageFilePathToPoliciesPieChart(),
					"Image file paths map does not exist for policies pie charts!");
			Assert.assertNotNull(this.pcGenerator.getImageFilePathsToSubAccountsUFK(),
					"Image file paths map does not exist for sub accounts pie charts!");
			Assert.assertNotNull(this.pcGenerator.getImageFilePathsToSubAccountsUFK(),
					"Image file paths map does not exist for UFK pie charts!");
			
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
	}
	
	/**
	 * Helper method running actual test of pie chart generation for policies in the account. The pie chart is
	 * generated and then image created for it is compared with model image file.
	 * 
	 * @param testImagePaths map containing relative file path to image of policies pie chart
	 */
	private void runPieChartGenerationForPolicies(HashMap<Integer, HashMap<String, String>> testImagePaths) {
		Exception ex = null;
		// get the path to the test image to be compared with the actual one
		String expectedFilePath = testImagePaths.get(0).get("expectedImageFilePath");
		expectedFilePath = imagesResourcesPath + expectedFilePath;
		String actualFilePath = null;
		
		try {
			this.pcGenerator.generatePieChartForPolicies();
			actualFilePath = this.pcGenerator.getImageFilePathToPoliciesPieChart();
		} catch (Exception e) {
			ex = e;
		}
		
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		Assert.assertNotNull(actualFilePath, "Actual file path does not exist!");
		
		// last but not least, compare the files
		try {
			boolean result = this.compareFilesBinary(actualFilePath, expectedFilePath);
			Assert.assertTrue(result, "Actual policies pie chart image file differs from expected!");
		} catch (IOException e) {
			Assert.fail("There was unexpected exception!");
		}
	}
	
	/**
	 * Helper method running actual test of pie charts generation for sub accounts. The pie charts are
	 * generated and then images created for them are compared with model image files.
	 * 
	 * @param testImagePaths map containing relative file paths to images of sub accounts pie charts
	 */
	private void runPieChartsGenerationForSubAccounts(HashMap<Integer, HashMap<String, String>> testImagePaths) {
		Exception ex = null;
		HashMap<Integer, String> actualFilePaths = null;
		
		try {
			this.pcGenerator.generatePieChartsForSubAccounts();
			actualFilePaths = this.pcGenerator.getImageFilePathsToSubAccountsPieCharts();
		} catch (Exception e) {
			ex = e;
		}
		
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		Assert.assertNotNull(actualFilePaths, "Actual file paths map does not exist!");
		
		// finally compare all the image files
		try {
			int i = 0;
			for (String currentPath : actualFilePaths.values()) {
				// if the actual path was not null, it means the creation of given pie chart image was skipped
				// (so we skip the current execution)
				if (currentPath == null) {
					continue;
				}
				
				// get the path to the test image to be compared with the actual one
				String expectedFilePath = testImagePaths.get(i).get("expectedImageFilePath");
				expectedFilePath = imagesResourcesPath + expectedFilePath;
				
				boolean result = this.compareFilesBinary(currentPath, expectedFilePath);
				Assert.assertTrue(result, "Actual sub accounts pie chart image files differ from expected!");
				i++;
			}
		} catch (IOException e) {
			Assert.fail("There was unexpected exception!");
		}
	}
	
	/**
	 * Helper method running actual test of pie charts generation for UFKs. The pie charts are generated and
	 * then images created for them are compared with model image files.
	 * 
	 * @param testImagePaths map containing relative file paths to images of UFK pie charts
	 */
	private void runPieChartsGenerationForUFK(HashMap<Integer, HashMap<String, String>> testImagePaths) {
		Exception ex = null;
		HashMap<Integer, String> actualFilePaths = null;
		
		try {
			this.pcGenerator.generatePieChartsForSubAccountsUFK();
			actualFilePaths = this.pcGenerator.getImageFilePathsToSubAccountsUFK();
		} catch (Exception e) {
			ex = e;
		}
		
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		Assert.assertNotNull(actualFilePaths, "Actual file paths map does not exist!");
		
		// finally compare all the image files
		try {
			int i = 0;
			for (String currentPath : actualFilePaths.values()) {
				// if the actual path was not null, it means the creation of given pie chart image was skipped
				// (so we skip the current execution)
				if (currentPath == null) {
					continue;
				}
				
				// get the path to the test image to be compared with the actual one
				String expectedFilePath = testImagePaths.get(i).get("expectedImageFilePath");
				expectedFilePath = imagesResourcesPath + expectedFilePath;
				
				boolean result = this.compareFilesBinary(currentPath, expectedFilePath);
				Assert.assertTrue(result, "Actual UFKs pie chart image files differ from expected!");
				i++;
			}
		} catch (IOException e) {
			Assert.fail("There was unexpected exception!");
		}
	}
	
	/**
	 * Helper method setting the mode of generating 3D pie charts.
	 * 
	 * @param generateIn3D whether to set the mode to 3D (if set to true) or standard 2D
	 * @throws Exception when initialization of the tested object failed
	 */
	private void setPieCharts3DGenerationMode(boolean generateIn3D) throws Exception {
		ConfigurationProvider
				.setPropertyValue("chart_generate_3D_pie_charts", Boolean.toString(generateIn3D));
		this.pcGenerator = new PieChartsGenerator();
	}
	
	/**
	 * Helper method used to check if two given files denoted by their paths are identical.
	 * 
	 * @param srcFilePath the path to the first, source file to compare (from resources)
	 * @param destFilePath the path to the second, destination file to compare (from test execution)
	 * @return true if the files are binary identical, false otherwise
	 * @throws IOException when comparing files failed
	 */
	private boolean compareFilesBinary(String srcFilePath, String destFilePath) throws IOException {
		Exception ex = null;
		boolean result = false;
		File srcFile = null, destFile = null;
		
		try {
			srcFile = new File(srcFilePath);
			if (!srcFile.exists() || !srcFile.isFile()) {
				throw new IOException("First file pointed by path <" + srcFilePath
						+ "> apparently is not a file!");
			}
		} catch (Exception e) {
			ex = e;
		}
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		
		try {
			destFile = new File(destFilePath);
			if (!destFile.exists() || !destFile.isFile()) {
				throw new IOException("First file pointed by path <" + destFilePath
						+ "> apparently is not a file!");
			}
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNull(ex, "There was unexpected exception!");
		
		try {
			result = Files.equal(srcFile, destFile);
		} catch (IOException e) {
			ex = e;
			throw e;
		}
		Assert.assertNull(ex, "There was unexpected exception!");
		
		return result;
	}
}
