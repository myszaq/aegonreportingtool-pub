/**
 * This is LineChartsGeneratorTest class with the test methods covering all public methods
 * in the tested class - LineChartsGenerator.
 * For more details, see the comments of each method.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */

package pl.aegon.youraccount.report;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.HashMap;

import org.jfree.data.time.TimeSeries;
import org.testng.Assert;
import org.testng.annotations.*;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import pl.aegon.youraccount.config.ConfigurationProvider;
import pl.aegon.youraccount.dbservice.*;
import utils.DBSimpleService;

public class LineChartsGeneratorTest {
	// the reference to tested class object
	private LineChartsGenerator chartsGenerator;
	private static String imagesResourcesPath;
	private String rendererType;
	private int recordsCount;
	private String scaleType;
	
	/**
	 * Helper method run before any of the tests is executed. It prepares the class for all of the tests.
	 * 
	 * @throws Exception when initialization of the tested object failed
	 */
	@BeforeTest
	@Parameters({ "rendererType", "recordsCount", "scaleType" })
	public void setUpTests(@Optional String rendererType, @Optional Integer recordsCount,
			@Optional String scaleType) throws Exception {
		// default values if parameters are not set
		if (rendererType == null || rendererType.isEmpty()) {
			rendererType = "spline";
		}
		if (recordsCount == null || recordsCount.intValue() == 0) {
			recordsCount = 30;
		}
		if (scaleType == null || scaleType.isEmpty()) {
			scaleType = "separated";
		}
		
		// set the database to use test connection
		DatabaseServiceProvider.setPersistenceUnitName(DatabaseManager.PersistenceType.TEST_CONN);
		
		// set up some properties for line charts generation (even if the settings are the same,
		// to make sure that exactly this configuration will be active)
		ConfigurationProvider.setPropertyValue("selected_records_type", "entries");
		ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "policies");
		ConfigurationProvider.setPropertyValue("chart_generate_policies", "true");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_without_policies", "true");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk", "true");
		
		// set scale type after checking if its value is valid
		if (scaleType.equals("separated") || scaleType.equals("common")) {
			ConfigurationProvider.setPropertyValue("chart_combined_charts_scale_type", scaleType);
		} else {
			throw new IllegalArgumentException("The value <" + scaleType + "> of scaleType is invalid!");
		}
		this.scaleType = scaleType;
		
		// set records count after checking if its value is valid
		if (recordsCount == 30 || recordsCount == 120) {
			ConfigurationProvider.setPropertyValue("selected_records_number", Integer.toString(recordsCount));
		} else {
			throw new IllegalArgumentException("The value <" + recordsCount + "> of recordsCount is invalid!");
		}
		this.recordsCount = recordsCount;
		
		// set renderer type after checking if its value is valid
		if (rendererType.equals("spline")) {
			ConfigurationProvider.setPropertyValue("chart_use_plot_smoothed_line", "true");
		} else if (rendererType.equals("xy")) {
			ConfigurationProvider.setPropertyValue("chart_use_plot_smoothed_line", "false");
		} else {
			throw new IllegalArgumentException("The value <" + rendererType + "> of rendererType is invalid!");
		}
		this.rendererType = rendererType;
		
		// just in case set the language to polish (model line charts images are created in polish)
		ConfigurationProvider.setPropertyValue("user_language", "pl");
		
		imagesResourcesPath = new File("src/test/resources/charts_test_images").getAbsolutePath();
		this.chartsGenerator = new LineChartsGenerator();
	}
	
	/**
	 * Helper method run after execution of each test method. Deletes created charts images.
	 */
	@AfterMethod()
	public void cleanAfterTests() {
		this.chartsGenerator.deleteChartsImagesFiles();
	}
	
	/**
	 * Test for correctness of generating line chart for whole Aegon account. In the test 2 cases are taken
	 * into account: whole account with and without policies (on one chart), corresponding to possible values
	 * of <i>chart_generate_whole_account</i> parameter.
	 * 
	 * @param testImagePaths map containing relative file paths to images of whole account line chart(s)
	 */
	@Test(dataProvider = "AegonTestDataProvider-StringInputIndex", dataProviderClass = utils.TestDataProvider.class)
	public void testValidGenerateChartForWholeAccount(HashMap<String, HashMap<String, String>> testImagePaths) {
		// index such as "120_spline_separated" (see the Excel file for more details)
		String testDataIndex = Integer.toString(this.recordsCount) + "_" + this.rendererType + "_"
				+ this.scaleType;
		String relativeTestFilePath;
		
		// prior to test, disable other charts
		ConfigurationProvider.setPropertyValue("chart_generate_policies", "false");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_without_policies", "false");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk", "false");
		
		// #1 test - for whole account only (without policies)
		ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "yes");
		// get the path to the test image to be compared with the actual one
		relativeTestFilePath = testImagePaths.get(testDataIndex).get("accountWithoutPolicies");
		this.verifyLineChartForWholeAccount(relativeTestFilePath);
		
		// #2 test - for whole account with policies on one chart
		// repeat the steps above
		ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "policies");
		relativeTestFilePath = testImagePaths.get(testDataIndex).get("accountWithPolicies");
		this.verifyLineChartForWholeAccount(relativeTestFilePath);
	}
	
	/**
	 * Test for invalid behavior of method <i>generateChartForWholeAccount</i> when whole account chart is
	 * actually not generated. Expected result is that no chart image file is available in that case.
	 */
	@Test
	public void testInvalidGenerateChartForWholeAccount() {
		// set the configuration to skip generating whole account chart
		ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "no");
		
		String actualFilePath = null;
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.generateChartForWholeAccount();
			actualFilePath = this.chartsGenerator.getImageFilePathToWholeAccountChart();
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
			e.printStackTrace();
		}
		
		Assert.assertNull(actualFilePath, "Actual file path for whole account should be null!");
	}
	
	/**
	 * Test for correctness of generating line charts for all policies in the account. In the test 3 cases are
	 * taken into account, corresponding to possible values of <i>chart_generate_policies_type</i> parameter:
	 * joined policies (one on chart), separated policies (each one on a separate chart) and policies with sub
	 * accounts (also each one on a separate chart).
	 * 
	 * @param testImagePaths map containing relative file paths to images of policies line charts
	 */
	@Test(dataProvider = "AegonTestDataProvider-StringInputIndex", dataProviderClass = utils.TestDataProvider.class)
	public void testValidGenerateChartsForAllPolicies(HashMap<String, HashMap<String, String>> testImagePaths) {
		Gson gson = new Gson();
		String testDataIndex = Integer.toString(this.recordsCount) + "_" + this.rendererType + "_"
				+ this.scaleType;
		String serializedTestFilePaths;
		HashMap<Integer, String> testFilePaths;
		
		// prior to test, disable other charts
		ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "no");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_without_policies", "false");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk", "false");
		
		// #1 test - for joined policies (one chart image generated)
		ConfigurationProvider.setPropertyValue("chart_generate_policies", "true");
		ConfigurationProvider.setPropertyValue("chart_generate_policies_type", "joined");
		// get the paths to the test images to be compared with the actual ones
		serializedTestFilePaths = testImagePaths.get(testDataIndex).get("policiesJoined");
		// deserialize the string with the file paths as the HashMap
		testFilePaths = gson.fromJson(serializedTestFilePaths, new TypeToken<HashMap<Integer, String>>() {
		}.getType());
		this.verifyLineChartsForPolicies(testFilePaths);
		
		// #2 test - for separated policies (multiple chart images generated)
		// repeat the steps above
		ConfigurationProvider.setPropertyValue("chart_generate_policies_type", "separated");
		serializedTestFilePaths = testImagePaths.get(testDataIndex).get("policiesSeparated");
		testFilePaths = gson.fromJson(serializedTestFilePaths, new TypeToken<HashMap<Integer, String>>() {
		}.getType());
		this.verifyLineChartsForPolicies(testFilePaths);
		
		// #3 test - for separated policies with sub accounts (multiple chart images generated)
		// repeat the steps above
		ConfigurationProvider.setPropertyValue("chart_generate_policies_type", "subaccounts");
		serializedTestFilePaths = testImagePaths.get(testDataIndex).get("policiesSubaccounts");
		testFilePaths = gson.fromJson(serializedTestFilePaths, new TypeToken<HashMap<Integer, String>>() {
		}.getType());
		this.verifyLineChartsForPolicies(testFilePaths);
	}
	
	/**
	 * Test for invalid behavior of method <i>generateChartsForAllPolicies</i> when policies charts are
	 * actually not generated. Expected result is that no chart image file is available in that case.
	 */
	@Test
	public void testInvalidGenerateChartsForAllPolicies() {
		// set the configuration to skip generating policies charts
		ConfigurationProvider.setPropertyValue("chart_generate_policies", "false");
		
		HashMap<Integer, String> actualFilePaths = null;
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.generateChartsForAllPolicies();
			actualFilePaths = this.chartsGenerator.getImageFilePathsToPoliciesCharts();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("There was unexpected exception!");
		}
		
		Assert.assertNull(actualFilePaths, "Actual file paths map for policies should be null!");
	}
	
	/**
	 * Test for correctness of generating line charts for all sub accounts in every policy. In the test there
	 * is only one scenario, which is taken into account - (as the name of the test suggests), generating
	 * charts for sub accounts without policies (to which they belong).
	 * 
	 * @param testImagePaths map containing relative file paths to images of sub accounts line charts
	 */
	@Test(dataProvider = "AegonTestDataProvider-StringInputIndex", dataProviderClass = utils.TestDataProvider.class)
	public void testValidGenerateChartsForSubAccountsWithoutPolicies(
			HashMap<String, HashMap<String, String>> testImagePaths) {
		Gson gson = new Gson();
		String testDataIndex = Integer.toString(this.recordsCount) + "_" + this.rendererType + "_"
				+ this.scaleType;
		String serializedTestFilePaths;
		HashMap<Integer, String> testFilePaths;
		
		// prior to test, disable other charts
		ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "no");
		ConfigurationProvider.setPropertyValue("chart_generate_policies", "false");
		ConfigurationProvider.setPropertyValue("chart_generate_policies_type", "joined");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_without_policies", "true");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk", "false");
		
		// only one test here - for sub accounts without their policies (multiple chart images generated)
		// get the paths to the test images to be compared with the actual ones
		serializedTestFilePaths = testImagePaths.get(testDataIndex).get("subAccountsWithoutPolicies");
		// deserialize the string with the file paths as the HashMap
		testFilePaths = gson.fromJson(serializedTestFilePaths, new TypeToken<HashMap<Integer, String>>() {
		}.getType());
		this.verifyLineChartsForSubAccounts(testFilePaths);
	}
	
	/**
	 * Test for invalid behavior of method <i>generateChartsForSubAccountsWithoutPolicies</i> when sub
	 * accounts charts are actually not generated. Expected result is that no chart image file is available in
	 * that case.
	 */
	@Test
	public void testInvalidGenerateChartsForSubAccountsWithoutPolicies() {
		// set the configuration to skip generating sub accounts charts
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_without_policies", "false");
		
		HashMap<Integer, String> actualFilePaths = null;
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.generateChartsForSubAccountsWithoutPolicies();
			actualFilePaths = this.chartsGenerator.getImageFilePathsToSubAccountsCharts();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("There was unexpected exception!");
		}
		
		Assert.assertNull(actualFilePaths, "Actual file paths map for sub accounts should be null!");
	}
	
	/**
	 * Test for correctness of generating line charts for sub accounts UFKs. In the test 2 cases are into
	 * account: UFKs with and without their sub accounts (to which they belong), corresponding to possible
	 * values of <i>chart_generate_subaccounts_ufk_with_subaccounts</i> parameter.
	 * 
	 * @param testImagePaths map containing relative file paths to images of sub accounts UFKs line charts
	 */
	@Test(dataProvider = "AegonTestDataProvider-StringInputIndex", dataProviderClass = utils.TestDataProvider.class)
	public void testValidGenerateChartsForSubAccountsUFK(
			HashMap<String, HashMap<String, String>> testImagePaths) {
		Gson gson = new Gson();
		String testDataIndex = Integer.toString(this.recordsCount) + "_" + this.rendererType + "_"
				+ this.scaleType;
		String serializedTestFilePaths;
		HashMap<Integer, String> testFilePaths;
		
		// prior to test, disable other charts
		ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "no");
		ConfigurationProvider.setPropertyValue("chart_generate_policies", "false");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_without_policies", "false");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk", "true");
		
		// #1 test - for UFKs without their sub accounts (multiple chart images generated)
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk_with_subaccounts", "false");
		// get the paths to the test images to be compared with the actual ones
		serializedTestFilePaths = testImagePaths.get(testDataIndex).get("UFKWithoutSubaccounts");
		// deserialize the string with the file paths as the HashMap
		testFilePaths = gson.fromJson(serializedTestFilePaths, new TypeToken<HashMap<Integer, String>>() {
		}.getType());
		this.verifyLineChartsForSubAccountsUFK(testFilePaths);
		
		// #2 test - for UFKs with their sub accounts (multiple chart images generated)
		// repeat the steps above
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk_with_subaccounts", "true");
		serializedTestFilePaths = testImagePaths.get(testDataIndex).get("UFKWithSubaccounts");
		testFilePaths = gson.fromJson(serializedTestFilePaths, new TypeToken<HashMap<Integer, String>>() {
		}.getType());
		this.verifyLineChartsForSubAccountsUFK(testFilePaths);
	}
	
	/**
	 * Test for invalid behavior of method <i>generateChartsForSubAccountsUFK</i> when sub accounts UFK charts
	 * are actually not generated. Expected result is that no chart image file is available in that case.
	 */
	@Test
	public void testInvalidGenerateChartsForSubAccountsUFK() {
		// set the configuration to skip generating sub accounts UFK charts
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk", "false");
		
		HashMap<Integer, String> actualFilePaths = null;
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.generateChartsForSubAccountsUFK();
			actualFilePaths = this.chartsGenerator.getImageFilePathsToSubAccountsUFKCharts();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("There was unexpected exception!");
		}
		
		Assert.assertNull(actualFilePaths, "Actual file paths map for sub accounts UFK should be null!");
	}
	
	/**
	 * Test for correctness of generating all line charts images.
	 */
	@Test
	public void testGenerateAllCharts() {
		// prior to test, enable all charts
		ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "yes");
		ConfigurationProvider.setPropertyValue("chart_generate_policies", "true");
		ConfigurationProvider.setPropertyValue("chart_generate_policies_type", "separated");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_without_policies", "true");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk", "true");
		try {
			// since tested method is just the wrapper for more specific methods (tested in this class),
			// we only verify if all the image file paths are available
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.generateAllCharts();
			
			Assert.assertNotNull(this.chartsGenerator.getImageFilePathToWholeAccountChart(),
					"Image file path does not exist for whole account chart!");
			Assert.assertNotNull(this.chartsGenerator.getImageFilePathsToPoliciesCharts(),
					"Image file paths map does not exist for policies line charts!");
			Assert.assertNotNull(this.chartsGenerator.getImageFilePathsToSubAccountsCharts(),
					"Image file paths map does not exist for sub accounts line charts!");
			Assert.assertNotNull(this.chartsGenerator.getImageFilePathsToSubAccountsUFKCharts(),
					"Image file paths map does not exist for UFK line charts!");
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
	}
	
	/**
	 * Test for correctness of the data returned by method <i>getTimeSeriesForWholeAccount</i> - that is the
	 * TimeSeries object for whole Aegon account.
	 */
	@Test
	public void testGetTimeSeriesForWholeAccount() {
		ConfigurationProvider.setPropertyValue("chart_generate_whole_account", "yes");
		int testRecordsCount = 100;
		TimeSeries chartTimeSeries = null;
		
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.setSelectedRecordsCount(testRecordsCount);
			this.chartsGenerator.generateChartForWholeAccount();
			chartTimeSeries = this.chartsGenerator.getTimeSeriesForWholeAccount();
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
		
		// check that the number of elements in TimeSeries is as expected
		Assert.assertNotNull(chartTimeSeries, "Actual TimeSeries for whole account does not exist!");
		int tsItemsCount = chartTimeSeries.getItemCount();
		Assert.assertEquals(tsItemsCount, testRecordsCount, "Expected " + testRecordsCount
				+ " elements in whole account TimeSeries!");
		
		// verify the content of retrieved TimeSeries
		DBSimpleService db = new DBSimpleService();
		String sqlQuery = "SELECT current_balance FROM aegon_entry_log ORDER BY reading_date DESC LIMIT ?";
		List<Object> paramsList = (List<Object>) Arrays.asList((Object) new Integer(testRecordsCount));
		List<HashMap<String, Object>> dbResultList = db.getPreparedSqlQueryResult(sqlQuery, paramsList);
		db.closeConnection();
		
		Collections.reverse(dbResultList);
		// compare all values - we expect them to be identical
		boolean resultsEqual = compareTimeSeriesBalancesWithDatabaseResults(chartTimeSeries, dbResultList,
				"current_balance");
		Assert.assertEquals(resultsEqual, true, "Whole account TimeSeries and database values differ!");
	}
	
	/**
	 * Test for correctness of the data returned by method <i>getTimeSeriesListForPolicies</i> - that is the
	 * map of TimeSeries objects for policies list.
	 */
	@Test
	public void testGetTimeSeriesListForPolicies() {
		ConfigurationProvider.setPropertyValue("chart_generate_policies", "true");
		int testRecordsCount = 100;
		HashMap<Integer, TimeSeries> chartTimeSeriesMap = null;
		
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.setSelectedRecordsCount(testRecordsCount);
			this.chartsGenerator.generateChartsForAllPolicies();
			chartTimeSeriesMap = this.chartsGenerator.getTimeSeriesListForPolicies();
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
		
		// check that the number of elements for each TimeSeries is as expected
		Assert.assertNotNull(chartTimeSeriesMap, "Actual TimeSeries map for policies does not exist!");
		for (Integer i : chartTimeSeriesMap.keySet()) {
			int tsItemsCount = chartTimeSeriesMap.get(i).getItemCount();
			Assert.assertEquals(tsItemsCount, testRecordsCount, "Expected " + testRecordsCount
					+ " elements in each policies TimeSeries!");
		}
		
		// get ids of policies
		DBSimpleService db = new DBSimpleService();
		String sqlQuery = "SELECT id_policy FROM aegon_policy ORDER BY id_policy ASC";
		List<HashMap<String, Object>> dbResultList = db.getSqlQueryResult(sqlQuery);
		
		// build list of these ids from HashMap
		List<Integer> policyIds = new ArrayList<Integer>();
		for (HashMap<String, Object> resultItem : dbResultList) {
			policyIds.add((Integer) resultItem.get("id_policy"));
		}
		
		// verify the content of retrieved all TimeSeries
		for (Integer i : policyIds) {
			sqlQuery = "SELECT ph.balance FROM aegon_policy_history ph JOIN aegon_entry_log el "
					+ "ON (ph.id_entry_log = el.id_entry_log) WHERE id_policy = ? ORDER BY el.reading_date "
					+ "DESC LIMIT ?";
			List<Object> paramsList = new ArrayList<Object>();
			paramsList.add(i);
			paramsList.add(testRecordsCount);
			
			dbResultList = db.getPreparedSqlQueryResult(sqlQuery, paramsList);
			Collections.reverse(dbResultList);
			
			// compare all values - we expect them to be identical
			boolean resultsEqual = compareTimeSeriesBalancesWithDatabaseResults(chartTimeSeriesMap.get(i),
					dbResultList, "balance");
			Assert.assertEquals(resultsEqual, true, "Policies TimeSeries and database values differ!");
		}
		db.closeConnection();
	}
	
	/**
	 * Test for correctness of the data returned by method <i>getTimeSeriesListForSubAccounts</i> - that is
	 * the map of TimeSeries objects for sub accounts list.
	 */
	@Test
	public void testGetTimeSeriesListForSubAccounts() {
		ConfigurationProvider.setPropertyValue("chart_generate_policies_type", "joined");
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_without_policies", "true");
		int testRecordsCount = 100;
		HashMap<Integer, TimeSeries> chartTimeSeriesMap = null;
		
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.setSelectedRecordsCount(testRecordsCount);
			this.chartsGenerator.generateChartsForSubAccountsWithoutPolicies();
			chartTimeSeriesMap = this.chartsGenerator.getTimeSeriesListForSubAccounts();
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
		
		// check that the number of elements for each TimeSeries is as expected
		Assert.assertNotNull(chartTimeSeriesMap, "Actual TimeSeries map for sub accounts does not exist!");
		for (Integer i : chartTimeSeriesMap.keySet()) {
			int tsItemsCount = chartTimeSeriesMap.get(i).getItemCount();
			Assert.assertEquals(tsItemsCount, testRecordsCount, "Expected " + testRecordsCount
					+ " elements in each sub accounts TimeSeries!");
		}
		
		// get ids of sub accounts
		DBSimpleService db = new DBSimpleService();
		String sqlQuery = "SELECT id_sub_account FROM aegon_sub_account ORDER BY id_sub_account ASC";
		List<HashMap<String, Object>> dbResultList = db.getSqlQueryResult(sqlQuery);
		
		// build list of these ids from HashMap
		List<Integer> subAccountIds = new ArrayList<Integer>();
		for (HashMap<String, Object> resultItem : dbResultList) {
			subAccountIds.add((Integer) resultItem.get("id_sub_account"));
		}
		
		// verify the content of retrieved all TimeSeries
		for (Integer i : subAccountIds) {
			sqlQuery = "SELECT sah.balance FROM aegon_sub_account_history sah JOIN aegon_entry_log el "
					+ "ON (sah.id_entry_log = el.id_entry_log) WHERE id_sub_account = ? ORDER BY el.reading_date "
					+ "DESC LIMIT ?";
			List<Object> paramsList = new ArrayList<Object>();
			paramsList.add(i);
			paramsList.add(testRecordsCount);
			
			dbResultList = db.getPreparedSqlQueryResult(sqlQuery, paramsList);
			Collections.reverse(dbResultList);
			
			// compare all values - we expect them to be identical
			boolean resultsEqual = compareTimeSeriesBalancesWithDatabaseResults(chartTimeSeriesMap.get(i),
					dbResultList, "balance");
			Assert.assertEquals(resultsEqual, true, "Sub accounts TimeSeries and database values differ!");
		}
		db.closeConnection();
	}
	
	/**
	 * Test for correctness of the data returned by method <i>getTimeSeriesListForSubAccountsUFK</i> - that is
	 * the map of TimeSeries objects for sub accounts UFK list.
	 */
	@Test
	public void testGetTimeSeriesListForSubAccountsUFK() {
		ConfigurationProvider.setPropertyValue("chart_generate_subaccounts_ufk", "true");
		int testRecordsCount = 100;
		HashMap<String, TimeSeries> chartTimeSeriesMap = null;
		
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.setSelectedRecordsCount(testRecordsCount);
			this.chartsGenerator.generateChartsForSubAccountsUFK();
			chartTimeSeriesMap = this.chartsGenerator.getTimeSeriesListForSubAccountsUFK();
		} catch (Exception e) {
			Assert.fail("There was unexpected exception!");
		}
		
		// check that the number of elements for each TimeSeries is as expected
		Assert.assertNotNull(chartTimeSeriesMap, "Actual TimeSeries map for sub accounts UFK does not exist!");
		for (String index : chartTimeSeriesMap.keySet()) {
			int tsItemsCount = chartTimeSeriesMap.get(index).getItemCount();
			Assert.assertEquals(tsItemsCount, testRecordsCount, "Expected " + testRecordsCount
					+ " elements in each sub accounts UFK TimeSeries!");
		}
		
		// get pseudo-ids of sub accounts (as Strings)
		DBSimpleService db = new DBSimpleService();
		String sqlQuery = "SELECT DISTINCT concat(id_sub_account, '_', ufk_name) AS id_ufk FROM "
				+ "aegon_sub_account_balance_history ORDER BY id_sub_account ASC;";
		List<HashMap<String, Object>> dbResultList = db.getSqlQueryResult(sqlQuery);
		
		// build list of these ids from HashMap
		List<String> subAccountUFKIds = new ArrayList<String>();
		for (HashMap<String, Object> resultItem : dbResultList) {
			subAccountUFKIds.add((String) resultItem.get("id_ufk"));
		}
		
		// verify the content of retrieved all TimeSeries
		for (String ufkId : subAccountUFKIds) {
			sqlQuery = "SELECT sabh.ufk_invest_unit_collect_value FROM aegon_sub_account_balance_history sabh "
					+ "JOIN aegon_entry_log el ON (sabh.id_entry_log = el.id_entry_log) WHERE "
					+ "concat(id_sub_account, '_', ufk_name) = ? ORDER BY el.reading_date DESC LIMIT ?";
			List<Object> paramsList = new ArrayList<Object>();
			paramsList.add(ufkId);
			paramsList.add(testRecordsCount);
			
			dbResultList = db.getPreparedSqlQueryResult(sqlQuery, paramsList);
			Collections.reverse(dbResultList);
			
			// compare all values - we expect them to be identical
			boolean resultsEqual = compareTimeSeriesBalancesWithDatabaseResults(
					chartTimeSeriesMap.get(ufkId), dbResultList, "ufk_invest_unit_collect_value");
			Assert.assertEquals(resultsEqual, true, "Sub accounts UFK TimeSeries and database values differ!");
		}
		db.closeConnection();
	}
	
	/**
	 * Helper method running actual test (assertions) of line chart generation for whole Aegon account. The
	 * line chart is generated and then image created for it is compared with model image file.
	 * 
	 * @param expectedFilePath path to the expected image file of whole account chart
	 */
	private void verifyLineChartForWholeAccount(String expectedFilePath) {
		Exception ex = null;
		// build full path to the test image to be compared with the actual one
		expectedFilePath = imagesResourcesPath + expectedFilePath;
		String actualFilePath = null;
		
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.generateChartForWholeAccount();
			actualFilePath = this.chartsGenerator.getImageFilePathToWholeAccountChart();
		} catch (Exception e) {
			ex = e;
		}
		
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		Assert.assertNotNull(actualFilePath, "Actual file path does not exist!");
		
		// last but not least, compare the files
		try {
			boolean result = this.compareFilesBinary(actualFilePath, expectedFilePath);
			Assert.assertTrue(result, "Actual whole account line chart image differs from expected!");
		} catch (IOException e) {
			Assert.fail("There was unexpected exception!");
		}
		
		cleanAfterTests();
	}
	
	/**
	 * Helper method running actual test (assertions) of line charts generation for all policies. The line
	 * charts are generated and then images created for them are compared with model image files.
	 * 
	 * @param testImagePaths map containing relative file paths to the expected images of policies charts
	 */
	private void verifyLineChartsForPolicies(HashMap<Integer, String> testImagePaths) {
		Exception ex = null;
		HashMap<Integer, String> actualFilePaths = null;
		
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.generateChartsForAllPolicies();
			actualFilePaths = this.chartsGenerator.getImageFilePathsToPoliciesCharts();
		} catch (Exception e) {
			ex = e;
		}
		
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		Assert.assertNotNull(actualFilePaths, "Actual file paths map does not exist!");
		
		// finally compare all the image files
		try {
			for (int i : actualFilePaths.keySet()) {
				String actualFilePath = actualFilePaths.get(i);
				// get the path to the test image to be compared with the actual one
				String expectedFilePath = testImagePaths.get(i);
				expectedFilePath = imagesResourcesPath + expectedFilePath;
				
				boolean result = this.compareFilesBinary(actualFilePath, expectedFilePath);
				Assert.assertTrue(result, "Actual policies line chart images differ from expected!");
			}
		} catch (IOException e) {
			Assert.fail("There was unexpected exception!");
		}
		
		cleanAfterTests();
	}
	
	/**
	 * Helper method running actual test (assertions) of line charts generation for sub accounts. The line
	 * charts are generated and then images created for them are compared with model image files.
	 * 
	 * @param testImagePaths map containing relative file paths to the expected images of sub accounts charts
	 */
	private void verifyLineChartsForSubAccounts(HashMap<Integer, String> testImagePaths) {
		Exception ex = null;
		HashMap<Integer, String> actualFilePaths = null;
		
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.generateChartsForSubAccountsWithoutPolicies();
			actualFilePaths = this.chartsGenerator.getImageFilePathsToSubAccountsCharts();
		} catch (Exception e) {
			ex = e;
		}
		
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		Assert.assertNotNull(actualFilePaths, "Actual file paths map does not exist!");
		
		// finally compare all the image files
		try {
			for (int i : actualFilePaths.keySet()) {
				String actualFilePath = actualFilePaths.get(i);
				// get the path to the test image to be compared with the actual one
				String expectedFilePath = testImagePaths.get(i);
				expectedFilePath = imagesResourcesPath + expectedFilePath;
				
				boolean result = this.compareFilesBinary(actualFilePath, expectedFilePath);
				Assert.assertTrue(result, "Actual sub accounts line chart images differ from expected!");
			}
		} catch (IOException e) {
			Assert.fail("There was unexpected exception!");
		}
		
		cleanAfterTests();
	}
	
	/**
	 * Helper method running actual test (assertions) of line charts generation for UFKs. The line charts are
	 * generated and then images created for them are compared with model image files.
	 * 
	 * @param testImagePaths map containing relative file paths to the expected images of UFK charts
	 */
	private void verifyLineChartsForSubAccountsUFK(HashMap<Integer, String> testImagePaths) {
		Exception ex = null;
		HashMap<Integer, String> actualFilePaths = null;
		
		try {
			this.chartsGenerator = new LineChartsGenerator();
			this.chartsGenerator.generateChartsForSubAccountsUFK();
			actualFilePaths = this.chartsGenerator.getImageFilePathsToSubAccountsUFKCharts();
		} catch (Exception e) {
			ex = e;
		}
		
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		Assert.assertNotNull(actualFilePaths, "Actual file paths map does not exist!");
		
		// finally compare all the image files
		try {
			for (int i : actualFilePaths.keySet()) {
				String actualFilePath = actualFilePaths.get(i);
				// get the path to the test image to be compared with the actual one
				String expectedFilePath = testImagePaths.get(i);
				expectedFilePath = imagesResourcesPath + expectedFilePath;
				
				boolean result = this.compareFilesBinary(actualFilePath, expectedFilePath);
				Assert.assertTrue(result, "Actual UFK line chart images differ from expected!");
			}
		} catch (IOException e) {
			Assert.fail("There was unexpected exception!");
		}
		
		cleanAfterTests();
	}
	
	/**
	 * Helper method used to check if two given files denoted by their paths are identical.
	 * 
	 * @param srcFilePath the path to the first, source file to compare (from resources)
	 * @param destFilePath the path to the second, destination file to compare (from test execution)
	 * @return true if the files are binary identical, false otherwise
	 * @throws IOException when comparing files failed
	 */
	private boolean compareFilesBinary(String srcFilePath, String destFilePath) throws IOException {
		Exception ex = null;
		boolean result = false;
		File srcFile = null, destFile = null;
		
		try {
			srcFile = new File(srcFilePath);
			if (!srcFile.exists() || !srcFile.isFile()) {
				throw new IOException("First file pointed by path <" + srcFilePath
						+ "> apparently is not a file!");
			}
		} catch (Exception e) {
			ex = e;
		}
		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		
		try {
			destFile = new File(destFilePath);
			if (!destFile.exists() || !destFile.isFile()) {
				throw new IOException("First file pointed by path <" + destFilePath
						+ "> apparently is not a file!");
			}
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertNull(ex, "There was unexpected exception!");
		
		try {
			result = Files.equal(srcFile, destFile);
		} catch (IOException e) {
			ex = e;
			throw e;
		}
		Assert.assertNull(ex, "There was unexpected exception!");
		
		return result;
	}
	
	/**
	 * Helper method used to compare the content of input TimeSeries (that is the list of values being
	 * balances for tested entities) with the list of the same results retrieved directly from database. Both
	 * lists are conceived to be equal if every balance value from TimeSeries and value from database list are
	 * identical.
	 * 
	 * @param chartTimeSeries TimeSeries containing list of values to be tested
	 * @param dbResultList list of values retrieved from the database
	 * @param balanceFieldName the name of the column in database table holding balance values
	 * @return true if all elements are equal, false otherwise
	 */
	private boolean compareTimeSeriesBalancesWithDatabaseResults(TimeSeries chartTimeSeries,
			List<HashMap<String, Object>> dbResultList, String balanceFieldName) {
		boolean result = true;
		
		for (int i = 0; i < chartTimeSeries.getItemCount(); i++) {
			// get the value of current balance as double with 2 digits precision
			double tsValue = chartTimeSeries.getValue(i).doubleValue();
			BigDecimal bd = new BigDecimal(tsValue).setScale(2, RoundingMode.HALF_UP);
			double actualBalanceValue = bd.doubleValue();
			
			// we expect to have BigDecimal here (corresponding to column's SQL type)
			BigDecimal dbValue = (BigDecimal) dbResultList.get(i).get(balanceFieldName);
			double expectedBalanceValue = dbValue.doubleValue();
			
			// compare all values - we expect them to be identical
			if (Double.compare(actualBalanceValue, expectedBalanceValue) != 0) {
				result = false;
				break;
			}
		}
		
		return result;
	}
}
