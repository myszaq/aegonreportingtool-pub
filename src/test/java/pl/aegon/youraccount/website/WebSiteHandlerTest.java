/**
 * This is WebSiteHandlerTest class with the test methods covering all public methods
 * in the tested class - WebSiteHandler.
 * 
 * @author Michał Myszkowski <myszaq85@gmail.com>
 */
package pl.aegon.youraccount.website;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.testng.Assert;
import org.testng.annotations.*;
import pl.aegon.youraccount.config.ConfigurationProvider;
import pl.aegon.youraccount.entity.*;
import pl.aegon.youraccount.website.WebSiteHandler;
import utils.TestWebServerHelper;

public class WebSiteHandlerTest {
	// the reference to tested class object
	private WebSiteHandler handler;
	private String browser;
	private String customUrl = "http://localhost:8080/AEGON Online Logowanie.html";
	private static String originalLogin;

	/**
	 * Helper method starting Moongoose webserver at the beginning of the test suite.
	 */
	@BeforeSuite
	public void setUpWebServer() {
		Exception ex = null;
		try {
			TestWebServerHelper.startLocalWebServer();
		} catch (Exception e) {
			ex = e;
		}

		// assert that there was no exception
		Assert.assertNull(ex);
	}
	
	/**
	 * Helper method stopping Moongoose webserver at the end of the test suite.
	 */
	@AfterSuite
	public void tearDownWebServer() {
		TestWebServerHelper.stopLocalWebServer();
	}
	
	/**
	 * Helper method run before any of the tests is executed. Sets the correct login to work
	 * with test version of Aegon service.
	 */
	@BeforeClass
	public void setUpTests() {
		originalLogin = ConfigurationProvider.getPropertyValue("user_login");
		// overwrite login with test value (not real one) which still guarantees correct login
		ConfigurationProvider.setPropertyValue("user_login", "ABC12345678");		
	}

	/**
	 * Helper method run after all of the tests have been executed. Restores the original
	 * login.
	 */
	@AfterClass
	public void tearDownTests() {
		// restore original login and password
		ConfigurationProvider.setPropertyValue("user_login", originalLogin);
	}
	
	/**
	 * This method sets up the web browser and is run before every test method.
	 * 
	 * @param browser name of the browser to use for tests
	 * @throws Exception when the web browser was not initialized or if another error occured
	 */
	@BeforeMethod
	@Parameters("browser")
	public void setUpHandler(@Optional String browser) throws Exception {
		// default browser is firefox
		if (browser == null || browser.isEmpty()) {
			browser = "firefox";
		}

		this.browser = browser;
		this.handler = new WebSiteHandler();

		// String origBrowser = ConfigurationProvider.getPropertyValue("browser_selected");
		ConfigurationProvider.setPropertyValue("browser_selected", browser);
		// although this is the call for the test method, it can be also used for setting custom url
		testSetValidCustomUrl();
		this.handler.setUpBrowser();
	}

	/**
	 * This method closes the web browser and is run after every test method.
	 * 
	 * @throws Exception when the browser was not initialized before at all
	 */
	@AfterMethod
	public void tearDownHandler() throws Exception {
		this.handler.tearDownBrowser();
	}

	/**
	 * Test for setting valid custom url for handler object.
	 */
	@Test
	public void testSetValidCustomUrl() {
		Exception ex = null;

		try {
			this.handler.setCustomUrl(customUrl);
		} catch (Exception e) {
			// e.printStackTrace();
			ex = e;
		}

		// assert that there was no exception
		Assert.assertNull(ex);
	}

	/**
	 * Test for verification of the WebSiteHandler when invalid custom url was passed.
	 */
	@Test
	public void testSetInvalidCustomUrl() {
		Exception ex = null;

		// test for null
		try {
			this.handler.setCustomUrl(null);
		} catch (Exception e) {
			// e.printStackTrace();
			ex = e;
		}

		// assert that there was the exception
		Assert.assertTrue(ex instanceof IllegalArgumentException);
		Assert.assertEquals(ex.getMessage(), "Invalid custom url passed as an argument!");

		// repeat test with different value
		try {
			this.handler.setCustomUrl("this_ain't any.url!");
		} catch (Exception e) {
			// e.printStackTrace();
			ex = e;
		}

		Assert.assertTrue(ex instanceof MalformedURLException);
	}

	/**
	 * Test for logging to Aegon application using valid credentials.
	 */
	@Test
	public void testValidLoginToApplication() {
		Exception ex = null;

		try {
			this.handler.loginToApplicationAndCheckMainPage();
		} catch (Exception e) {
			// e.printStackTrace();
			ex = e;
		}

		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
	}

	/**
	 * Test for logging to Aegon application using invalid credentials.
	 */
	@Test
	public void testInvalidLoginToApplication() {
		// save original, correct login and password
		String origLogin = ConfigurationProvider.getPropertyValue("user_login");
		String origPassword = ConfigurationProvider.getPropertyValue("user_password");
		// overwrite login and password with invalid values that will guarantee login failure
		ConfigurationProvider.setPropertyValue("user_login", "definitely_bad_login");
		ConfigurationProvider.setPropertyValue("user_password", "definitely_bad_password");

		Exception ex = null;
		try {
			this.handler.loginToApplicationAndCheckMainPage();
		} catch (Exception e) {
			ex = e;
		}

		Assert.assertNotNull(ex);
		Assert.assertTrue(ex.getMessage().contains("Logging operation failed! Website returned message"));
		// restore original login and password
		ConfigurationProvider.setPropertyValue("user_login", origLogin);
		ConfigurationProvider.setPropertyValue("user_password", origPassword);

		// since after failed login the browser should be closed, we must start it again
		try {
			setUpHandler(this.browser);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test the correctness of logout operation.
	 */
	@Test
	public void testValidLogoutFromApplication() {
		// first login to the application
		testValidLoginToApplication();

		Exception ex = null;
		// check logout
		try {
			this.handler.logoutFromApplication();
		} catch (Exception e) {
			ex = e;
		}

		// assert that there was no exception
		Assert.assertNull(ex);
	}

	/**
	 * Test the logout operation error when the dependant data is malformed.
	 */
	@Test
	public void testInvalidLogoutFromApplication() {
		// save original, correct expected logout title
		String origLogoutTitle = ConfigurationProvider.getPropertyValue("expect_logout_title");
		// overwrite this property with invalid value in order to guarantee logout failure
		ConfigurationProvider.setPropertyValue("expect_logout_title", "This_ain't_logout_page");

		// first login to the application
		testValidLoginToApplication();

		Exception ex = null;
		try {
			this.handler.logoutFromApplication();
		} catch (Exception e) {
			ex = e;
		}

		// assert the correctness of the caught exception
		Assert.assertNotNull(ex);
		Assert.assertTrue(ex.getMessage().contains("Logging out may have failed"));

		// restore original expected logout title
		ConfigurationProvider.setPropertyValue("expect_logout_title", origLogoutTitle);
	}

	/**
	 * Test collecting and storing general data on the main page and the correctness of retrieved EntryLog
	 * entity.
	 * 
	 * @param generalData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testCollectAndStoreGeneralData(HashMap<Integer, HashMap<String, Object>> generalData) {
		Exception ex = null;

		// initial steps (test preparation)
		try {
			this.handler.loginToApplicationAndCheckMainPage();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// start actual test
		try {
			this.handler.collectAndStoreGeneralData();
		} catch (Exception e) {
			ex = e;
		}

		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		EntryLog entryLog = this.handler.getEntryLogEntity();

		// verify EntryLog entity
		this.assertEntryLog(entryLog, generalData);
	}

	/**
	 * Test collecting and storing policy data on the main page and the correctness of retrieved Policy and
	 * PolicyHistory entities.
	 * 
	 * @param policyData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testCollectAndStorePolicyData(HashMap<Integer, HashMap<String, Object>> policyData) {
		Exception ex = null;

		// initial steps (test preparation)
		try {
			this.handler.loginToApplicationAndCheckMainPage();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// start actual test
		try {
			this.handler.collectAndStorePolicyData();
		} catch (Exception e) {
			ex = e;
		}

		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		List<Policy> policyList = this.handler.getPolicyEntityList();
		List<PolicyHistory> policyHistoryList = this.handler.getPolicyHistoryEntityList();

		// verify Policy and PolicyHistory entity list
		this.assertPolicyList(policyList, policyData);
		this.assertPolicyHistoryList(policyHistoryList, policyList, policyData);
	}

	/**
	 * Test collecting and storing sub account data on the policy page and the correctness of retrieved
	 * SubAccount and SubAccountHistory entities.
	 * 
	 * @param policyIndex index of the Policy (inside Policy list) needed to access corresponding SubAccount
	 * entities
	 * @param subAccountData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testCollectAndStoreSubAccountData(int policyIndex,
			HashMap<String, List<String>> subAccountData) {
		Exception ex = null;

		// initial steps (test preparation)
		try {
			this.handler.loginToApplicationAndCheckMainPage();
			// collect policy data in order to get Policy entities (for verifying SubAccount entities)
			this.handler.collectAndStorePolicyData();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// start actual test
		try {
			this.handler.collectAndStoreSubAccountData(policyIndex);
		} catch (Exception e) {
			ex = e;
		}

		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		List<Policy> policyList = this.handler.getPolicyEntityList();
		List<SubAccount> subAccountList = this.handler.getSubAccountEntityList();
		List<SubAccountHistory> subAccountHistoryList = this.handler.getSubAccountHistoryEntityList();

		// verify SubAccount and SubAccountHistory entity list
		assertSubAccountList(subAccountList, subAccountData, policyList.get(policyIndex));
		assertSubAccountHistoryList(subAccountHistoryList, subAccountList, subAccountData);
	}

	/**
	 * Test collecting and storing sub account balance history data (UFK data) on the sub account details page
	 * and the correctness of retrieved SubAccountBalanceHistory entities.
	 * 
	 * @param policyIndex index of the Policy (inside Policy list) needed to access corresponding SubAccount
	 * entities
	 * @param subAccountUfkData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testCollectAndStoreSubAccountDetailsData(int policyIndex,
			HashMap<String, List<String>> subAccountUfkData) {
		Exception ex = null;

		// initial steps (test preparation)
		try {
			this.handler.loginToApplicationAndCheckMainPage();
			this.handler.collectAndStorePolicyData();
			// collect sub account data in order to verify their assignment to SubAccountBalanceHistory
			// entities
			this.handler.collectAndStoreSubAccountData(policyIndex);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// start actual test
		// get the mapper of SubAccountBalanceHistory index to SubAccount index
		Map<Integer, Integer> sabhToSubAccountMapper = new HashMap<Integer, Integer>();
		try {
			sabhToSubAccountMapper = this.buildUfkToSubAccountEntitiesMapping();
		} catch (Exception e) {
			ex = e;
		}

		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");
		List<SubAccount> subAccountList = this.handler.getSubAccountEntityList();
		List<SubAccountBalanceHistory> subAccountBalanceHistoryList = this.handler
				.getSubAccountBalanceHistoryEntityList();

		// verify SubAccountBalanceHistory entity list
		assertSubAccountBalanceHistoryList(subAccountBalanceHistoryList, subAccountList,
				sabhToSubAccountMapper, subAccountUfkData);
	}

	/**
	 * Test collecting and storing all data (data of Policies, SubAccounts and UFKs) from the Aegon website
	 * (traversing through all the web pages) and the correctness of all retrieved entities. This method tests
	 * therefore the main and the most important method from the tested class.
	 * 
	 * @param testData model data used for comparison of retrieved data, provided by Excel file
	 */
	@Test(dataProvider = "AegonTestDataProvider", dataProviderClass = utils.TestDataProvider.class)
	public void testCollectAllRelevantDataFromApplication(
			HashMap<String, HashMap<Integer, HashMap<String, Object>>> testData) {
		Exception ex = null;

		// close the browser (setUpHandler opens it, but for some reason it is not closed)
		try {
			tearDownHandler();
		} catch (Exception e) {
			// rather don't care about the possible exception
		}

		try {
			this.handler.collectAllRelevantDataFromApplication();
		} catch (Exception e) {
			System.out.println("Caught exception: " + e.getMessage());
			e.printStackTrace();
			ex = e;
		}

		EntryLog entryLog = this.handler.getEntryLogEntity();
		List<Policy> policyList = this.handler.getPolicyEntityList();
		List<PolicyHistory> policyHistoryList = this.handler.getPolicyHistoryEntityList();
		List<SubAccount> subAccountList = this.handler.getSubAccountEntityList();
		List<SubAccountHistory> subAccountHistoryList = this.handler.getSubAccountHistoryEntityList();
		List<SubAccountBalanceHistory> subAccountBalanceHistoryList = this.handler
				.getSubAccountBalanceHistoryEntityList();

		// assert that there was no exception
		Assert.assertNull(ex, "There was unexpected exception!");

		// verify the coherence of entity history lists in terms of size
		Assert.assertEquals(policyList.size(), policyHistoryList.size());
		Assert.assertEquals(subAccountList.size(), subAccountHistoryList.size());

		// verify EntryLog entity
		assertEntryLog(entryLog, testData.get("testCollectAndStoreGeneralData"));
		// verify Policy and PolicyHistory entity list
		assertPolicyList(policyList, testData.get("testCollectAndStorePolicyData"));
		assertPolicyHistoryList(policyHistoryList, policyList, testData.get("testCollectAndStorePolicyData"));
		// verify SubAccount and SubAccountHistory entity list

		// prepare the lists of SubAccount and SubAccountHistory entities to be able to perform assertions on
		// them
		List<SubAccount> subAccountPerPolicyList = new ArrayList<SubAccount>();
		List<SubAccountHistory> subAccountHistoryPerPolicyList = new ArrayList<SubAccountHistory>();
		int i = 0;
		for (Policy policy : policyList) {
			int j = 0; // internal index of subAccount and subAccountHistory lists for the loop below
			// create the list of SubAccount and SubAccountHistory entities per current Policy...
			for (SubAccount subAccount : subAccountList) {
				// ... using policy number for comparison
				if (subAccount.getPolicy().getPolicyNumber() == policy.getPolicyNumber()) {
					subAccountPerPolicyList.add(subAccount);
					subAccountHistoryPerPolicyList.add(subAccountHistoryList.get(j));
				}
				j++;
			}

			// convert SubAccount entities data from HashMap<String, Object> into HashMap<String, List<String>
			// so that it can be used in the assertions methods
			HashMap<String, List<String>> convertedTestData = this.convertSubAccountTestData(
					testData.get("testCollectAndStoreSubAccountData"), i);

			// verify SubAccount and SubAccountHistory entities list for current policy
			assertSubAccountList(subAccountPerPolicyList, convertedTestData, policy);
			assertSubAccountHistoryList(subAccountHistoryPerPolicyList, subAccountPerPolicyList,
					convertedTestData);

			subAccountPerPolicyList.clear();
			subAccountHistoryPerPolicyList.clear();
			i++;
		}

		// merge the test data purposed for testing subAccountBalanceHistory entities list
		// in such a way, that there will be one list of UFK data (and not separated into subAccount index -
		// see the original Excel file); also convert the UFK data from HashMap<String, Object> into
		// HashMap<String, List<String>>
		HashMap<String, List<String>> convertedTestData = this.convertAndMergeUfkTestData(testData
				.get("testCollectAndStoreSubAccountDetailsData"));
		// create mapping of SubAccountBalanceHistory index to SubAccount index
		Map<Integer, Integer> sabhToSubAccountMapper = this.buildUfkToSubAccountEntitiesMapping(
				subAccountList, subAccountBalanceHistoryList);

		// finally verify SubAccountBalanceHistory entity list
		assertSubAccountBalanceHistoryList(subAccountBalanceHistoryList, subAccountList,
				sabhToSubAccountMapper, convertedTestData);
	}

	/**
	 * Helper method used for performing internal assertions of provided EntryLog entity.
	 * 
	 * @param entryLog EntryLog entity to be tested (compared with model data)
	 * @param testData set of data containing test EntryLog entity (model data)
	 */
	private void assertEntryLog(EntryLog entryLog, HashMap<Integer, HashMap<String, Object>> testData) {
		// verify the correctness of retrieved EntryLog entity
		String expectedValue = (String) testData.get(0).get("expectedBalance");
		Assert.assertEquals(entryLog.getCurrentBalance(), Float.parseFloat(expectedValue));

		expectedValue = (String) testData.get(0).get("expectedDataFromDayDate");
		Assert.assertEquals(entryLog.getDataFromDayDate(), expectedValue);

		expectedValue = (String) testData.get(0).get("expectedLastLoginDate");
		Assert.assertEquals(entryLog.getLastLoginDate(), expectedValue);

		expectedValue = (String) testData.get(0).get("expectedLastModifiedDate");
		Assert.assertEquals(entryLog.getLastModifiedDate(), expectedValue);
	}

	/**
	 * Helper method used for performing internal assertions of provided Policy entities.
	 * 
	 * @param policyList the list of Policy entities to be tested (compared with model data)
	 * @param testData set of data containing test Policy entities (model data)
	 */
	private void assertPolicyList(List<Policy> policyList, HashMap<Integer, HashMap<String, Object>> testData) {
		int i = 0;
		String expectedValue = null;

		// verify the correctness of retrieved Policy entities
		Assert.assertEquals(policyList.size(), testData.size());
		for (Policy policy : policyList) {
			expectedValue = (String) testData.get(i).get("expectedBalance");
			Assert.assertEquals(policy.getCurrentBalance(), Float.parseFloat(expectedValue));

			expectedValue = (String) testData.get(i).get("expectedPolicyName");
			Assert.assertEquals(policy.getPolicyName(), expectedValue);

			expectedValue = (String) testData.get(i).get("expectedPolicyNumber");
			Assert.assertEquals(policy.getPolicyNumber(), expectedValue);

			expectedValue = (String) testData.get(i).get("expectedProductName");
			Assert.assertEquals(policy.getProductName(), expectedValue);
			i++;
		}
	}

	/**
	 * Helper method used for performing internal assertions of provided PolicyHistory entities.
	 * 
	 * @param policyHistoryList the list of PolicyHistory entities to be tested (compared with model data)
	 * @param policyList the list of Policy entities used in additional comparison with PolicyHistory
	 * reference
	 * @param testData set of data containing test Policy entities (model data)
	 */
	private void assertPolicyHistoryList(List<PolicyHistory> policyHistoryList, List<Policy> policyList,
			HashMap<Integer, HashMap<String, Object>> testData) {
		int i = 0;
		// verify the correctness of retrieved PolicyHistory entities
		Assert.assertEquals(policyHistoryList.size(), testData.size());
		for (PolicyHistory policyHistory : policyHistoryList) {
			String expectedValue = (String) testData.get(i).get("expectedBalance");

			Assert.assertEquals(policyHistory.getBalance(), Float.parseFloat(expectedValue));
			Assert.assertEquals(policyHistory.getPolicy(), policyList.get(i));
			i++;
		}
	}

	/**
	 * Helper method used for performing internal assertions of provided SubAccount entities.
	 * 
	 * @param subAccountList the list of SubAccount entities to be tested (compared with model data)
	 * @param testData set of data containing test SubAccount entities (model data)
	 * @param currentTestPolicy Policy entity used in additional comparison with SubAccount reference
	 */
	private void assertSubAccountList(List<SubAccount> subAccountList,
			HashMap<String, List<String>> testData, Policy currentTestPolicy) {
		int i = 0;
		String expectedValue = null;

		// verify the correctness of retrieved SubAccount entities
		Assert.assertEquals(subAccountList.size(), testData.get("expectedSubAccountNames").size());
		for (SubAccount subAccount : subAccountList) {
			expectedValue = testData.get("expectedBalances").get(i);
			Assert.assertEquals(subAccount.getCurrentBalance(), Float.parseFloat(expectedValue));

			expectedValue = testData.get("expectedSubAccountNames").get(i);
			Assert.assertEquals(subAccount.getSubAccountName(), expectedValue);
			Assert.assertEquals(subAccount.getPolicy(), currentTestPolicy);
			i++;
		}
	}

	/**
	 * Helper method used for performing internal assertions of provided SubAccountHistory entities.
	 * 
	 * @param subAccountHistoryList the list of SubAccountHistory entities to be tested (compared with model
	 * data)
	 * @param subAccountList the list of SubAccount entities used in additional comparison with
	 * SubAccountHistory reference
	 * @param testData set of data containing test SubAccount entities (model data)
	 */
	private void assertSubAccountHistoryList(List<SubAccountHistory> subAccountHistoryList,
			List<SubAccount> subAccountList, HashMap<String, List<String>> testData) {
		int i = 0;
		// verify the correctness of retrieved SubAccountHistory entities
		Assert.assertEquals(subAccountHistoryList.size(), testData.get("expectedSubAccountNames").size());
		for (SubAccountHistory subAccountHistory : subAccountHistoryList) {
			String expectedValue = testData.get("expectedBalances").get(i);
			Assert.assertEquals(subAccountHistory.getBalance(), Float.parseFloat(expectedValue));
			Assert.assertEquals(subAccountHistory.getSubAccount(), subAccountList.get(i));
			i++;
		}
	}

	/**
	 * Helper method used for performing internal assertions of provided SubAccountBalanceHistory entities.
	 * 
	 * @param subAccountBalanceHistoryList the list of SubAccountBalanceHistory entities to be tested
	 * (compared with model data)
	 * @param subAccountList the list of SubAccount entities used in additional comparison with
	 * SubAccountBalanceHistory reference
	 * @param entitiesMapper the map containing associations in the form (SubAccountBalanceHistory index ->
	 * SubAccount index), allowing to do the mapping of provided SubAccountBalanceHistory entity with its
	 * corresponding SubAccount entity)
	 * @param testData set of data containing test SubAccountBalanceHistory entities (model data)
	 */
	private void assertSubAccountBalanceHistoryList(
			List<SubAccountBalanceHistory> subAccountBalanceHistoryList, List<SubAccount> subAccountList,
			Map<Integer, Integer> entitiesMapper, HashMap<String, List<String>> testData) {
		int i = 0;
		String expectedValue = null;

		// verify the correctness of retrieved SubAccountBalanceHistory entities
		Assert.assertEquals(subAccountBalanceHistoryList.size(), testData.get("expectedUfkNames").size());
		for (SubAccountBalanceHistory subAccountBalanceHistory : subAccountBalanceHistoryList) {
			expectedValue = testData.get("expectedUfkInvestUnitCollectedValues").get(i);
			Assert.assertEquals(subAccountBalanceHistory.getUfkInvestUnitCollectValue(),
					Float.parseFloat(expectedValue));

			expectedValue = testData.get("expectedUfkInvestUnitCounts").get(i);
			Assert.assertEquals(subAccountBalanceHistory.getUfkInvestUnitCount(),
					Double.parseDouble(expectedValue));

			expectedValue = testData.get("expectedUfkInvestUnitValues").get(i);
			Assert.assertEquals(subAccountBalanceHistory.getUfkInvestUnitValue(),
					Double.parseDouble(expectedValue));

			expectedValue = testData.get("expectedUfkNames").get(i);
			Assert.assertEquals(subAccountBalanceHistory.getUfkName(), expectedValue);
			// get the corresponding SubAccount to which current SubAccountBalanceHistory is linked
			Assert.assertEquals(subAccountBalanceHistory.getSubAccount(),
					subAccountList.get(entitiesMapper.get(i)));
			i++;
		}
	}

	/**
	 * Helper method used to build the mapping between each SubAccountBalanceHistory entity and SubAccount
	 * entity inside the lists of both entity types. This method is used when the tested handler object
	 * methods are called (see the reference to this method for more details).
	 * 
	 * @return mapping of SubAccountBalanceHistory index to SubAccount index
	 * @throws Exception when called handler method collectAndStoreSubAccountDetailsData failed
	 */
	private Map<Integer, Integer> buildUfkToSubAccountEntitiesMapping() throws Exception {
		// create mapping of SubAccountBalanceHistory index to SubAccount index
		Map<Integer, Integer> sabhToSubAccountMapper = new HashMap<Integer, Integer>();
		try {
			int subAccountNumber = this.handler.getSubAccountEntityList().size();
			for (int i = 0; i < subAccountNumber; i++) {
				this.handler.collectAndStoreSubAccountDetailsData(i, i);
				// iterate through collected SubAccountBalanceHistory entities to assign the index of
				// SubAccount they belong to
				for (int j = 0; j < this.handler.getSubAccountBalanceHistoryEntityList().size(); j++) {
					// add only new keys to have unique mapping (the key is the index in
					// SubAccountBalanceHistory list and the value is the index
					// in corresponding SubAccount list)
					if (!sabhToSubAccountMapper.containsKey(j)) {
						sabhToSubAccountMapper.put(j, i);
					}
				}
			}
		} catch (Exception e) {
			throw e;
		}

		return sabhToSubAccountMapper;
	}

	/**
	 * Helper method used to build the mapping between each SubAccountBalanceHistory entity and SubAccount
	 * entity inside the lists of both entity types. This method is used for handling and manipulating
	 * external test data (see the reference to this method for more details).
	 * 
	 * @param subAccountList the list of SubAccount entities needed to build the mapping (right side of the
	 * map)
	 * @param subAccountBalanceHistoryList the list of SubAccountBalanceHistory entities needed to build the
	 * mapping (left side of the map)
	 * @return mapping of SubAccountBalanceHistory index to SubAccount index
	 */
	private Map<Integer, Integer> buildUfkToSubAccountEntitiesMapping(List<SubAccount> subAccountList,
			List<SubAccountBalanceHistory> subAccountBalanceHistoryList) {
		// create mapping of SubAccountBalanceHistory index to SubAccount index
		Map<Integer, Integer> sabhToSubAccountMapper = new HashMap<Integer, Integer>();
		for (int k = 0; k < subAccountBalanceHistoryList.size(); k++) {
			SubAccount subAccount = subAccountBalanceHistoryList.get(k).getSubAccount();
			for (int m = 0; m < subAccountList.size(); m++) {
				if (subAccount.equals(subAccountList.get(m))) {
					sabhToSubAccountMapper.put(k, m);
				}
			}
		}

		return sabhToSubAccountMapper;
	}

	/**
	 * Helper method used to convert (change values of provided HashMap from Object to List<String>) and merge
	 * ( join all values of provided HashMap into one set of data) model data of SubAccountBalanceHistory
	 * entities taken from Excel file. This allows to use the output data directly in assertions method. See
	 * the reference to this method for more details.
	 * 
	 * @param testData the map containing test data (model data) for SubAccountBalanceHistory entities (or
	 * UFK) taken from Excel file
	 * @return the map of joined data for all SubAccountBalanceHistory entities
	 */
	private HashMap<String, List<String>> convertAndMergeUfkTestData(
			HashMap<Integer, HashMap<String, Object>> testData) {
		HashMap<String, List<String>> convertedTestData = new HashMap<String, List<String>>();
		for (Integer value : testData.keySet()) {
			for (Map.Entry<String, Object> entry : testData.get(value).entrySet()) {
				// firstly, convert the value of the map (Object) to different form (List<String>)
				String key = entry.getKey();
				@SuppressWarnings("unchecked")
				List<String> valuesList = (List<String>) entry.getValue();

				// secondly, merge the values of the map (that is join the internal List<String> lists
				if (!convertedTestData.containsKey(key)) {
					convertedTestData.put(key, valuesList);
				} else {
					// without using the new ArrayList (and using putAll method instead) we would get
					// UnsupportedOperationException...
					List<String> existingValues = new ArrayList<String>();
					existingValues.addAll(convertedTestData.get(key));
					existingValues.addAll(valuesList);
					convertedTestData.put(key, existingValues);
				}
			}
		}

		return convertedTestData;
	}

	/**
	 * Helper method used to convert (change values of provided HashMap from Object to List<String>) model
	 * data of SubAccount entities taken from Excel file. This allows to use the output data directly in
	 * assertions methods. See the reference to this method for more details.
	 * 
	 * @param testData the map containing test data (model data) for SubAccount entities taken from Excel file
	 * @param testIndex the index of currently processed data (corresponding to the policy to which given sub
	 * accounts belong)
	 * @return the map of converted SubAccount entities data under given index
	 */
	private HashMap<String, List<String>> convertSubAccountTestData(
			HashMap<Integer, HashMap<String, Object>> testData, int testIndex) {
		HashMap<String, List<String>> convertedTestData = new HashMap<String, List<String>>();

		// convert test data map from HashMap<String, Object> into HashMap<String, List<String>>
		HashMap<String, Object> subAccountTestData = testData.get(testIndex);
		for (Map.Entry<String, Object> entry : subAccountTestData.entrySet()) {
			String key = entry.getKey();
			@SuppressWarnings("unchecked")
			List<String> value = (List<String>) entry.getValue();

			convertedTestData.put(key, value);
		}

		return convertedTestData;
	}
}