This is the application for building custom reports for the website application of Aegon for investment policies (available at https://twojrachunek.pl/TwojRachunek/index.html).
The app is written in Java and its main purpose is to gather investment data (such as current balance) from the website on defined time interval, save them in the database
and later build a monthly (or other) report with a chart, so that the user is more aware of his financial situation.
The application is for private use, however in the future it could be distributed to wider, potential audience.